<?php

function gerarList($table, $class, $key, $label, $ext, $sobrescrever, $arrCampos, $arrAcoes, $arrFiltros){

    $database = new Database();

    $objBanco = new Database();

    $idProjeto = $_POST["projeto"];

    $objBanco->query("SELECT p.nomeBanco, c.hostnameBanco, c.usuarioBanco, c.senhaBanco, p.diretorioLists FROM projetos p, conexoes c WHERE c.id = p.conexaoBanco_INT AND p.id=$idProjeto");

    $database = new Database(Database::mysqli_result($objBanco->result, 0, 0));
    
    $database->database = Database::mysqli_result($objBanco->result, 0, 0);
    $database->host = Database::mysqli_result($objBanco->result, 0, 1);
    $database->user = Database::mysqli_result($objBanco->result, 0, 2);
    $database->password = Database::mysqli_result($objBanco->result, 0, 3);
    $diretorio = Database::mysqli_result($objBanco->result, 0, 4);
    
    $database->OpenLink();

    $dir = dirname(__FILE__);
    $filedate = date("d.m.Y");

    $filename = $dir . "/../../" . $diretorio ."/" . $table . ".php";

    $sql = "SHOW TABLES LIKE '$table';";
    $database->query($sql);

    if($database->rows < 1){

        return;

    }

    $permissaoSobreescrita = Helper::conferirPermissaoSobreEscrita($filename);

    // if file exists, then delete it
    if($permissaoSobreescrita && $sobrescrever && file_exists($filename))
    {
        unlink($filename);
    }

    if(!file_exists($filename)){

        $extFile = fopen($filename, "w+");

        $tituloFieldset = "Lista de " . ucwords($_POST["entidade_plural"]);
        
        if($_POST["genero_entidade"] == "F"){
            
            $mensagemNenhumRegistro = "Nenhuma {$_POST["entidade_singular"]} foi cadastrada at� o momento.";
            
        }
        elseif($_POST["genero_entidade"] == "M"){
            
            $mensagemNenhumRegistro = "Nenhum {$_POST["entidade_singular"]} foi cadastrado at� o momento.";
            
        }

        $conteudo = "<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA LIST:       $table
    * NOME DA CLASSE DAO: $class
    * DATA DE GERA��O:    $filedate
    * ARQUIVO:            $ext.php
    * TABELA MYSQL:       $table
    * BANCO DE DADOS:     $database->database
    * -------------------------------------------------------
    *
    */


    //Mensagens e Textos dos Tooltips
    \$acoes[\"mensagem_exclusao\"] = \"Tem certeza que deseja excluir este registro?\";
    \$acoes[\"tooltip_exclusao\"] = \"Clique aqui para excluir este registro\";
    \$acoes[\"tooltip_edicao\"] = \"Clique aqui para editar este registro\";
    \$acoes[\"tooltip_visualizacao\"] = \"Clique aqui para visualizar este registro\";

    include(\"filters/$table.php\");

    \$registrosPorPagina = REGISTROS_POR_PAGINA;

    \$registrosPesquisa = 1;

    \$obj = new $ext();
    \$obj->setByGet(\$registrosPesquisa);
    \$obj->formatarParaSQL();

    \$strCondicao = array();
    \$strGET = array();

    ";

    //$sql = "SHOW COLUMNS FROM $table;";
    //$database->query($sql);
    //$result = $database->result;

    //$database->query($sql);
    //$result = $database->result;

    for($j=0; $j < count($arrFiltros); $j++){

        //$col = $dados[0];
        //$colUpper = ucfirst($col);

        $col = $arrFiltros[$j];
        $colUpper = ucfirst($col);

        if(Helper::getTipoCampo($col) == "TEXTO"){

            $strWhere = "\$strCondicao[] = \"$col LIKE '%{\$obj->get$colUpper()}%'\";";

        }
        else{

            $strWhere = "\$strCondicao[] = \"{$col}1={\$obj->get$colUpper()}\";";

        }

        $conteudo .=  "

         if(!Helper::isNull(\$obj->get$colUpper())){

            $strWhere
            \$strGET[] = \"{$col}1={\$obj->get$colUpper()}\";

        }";


    }

    $conteudo .= "

    \$consulta = \"\";

    for(\$i=0; \$i < count(\$strCondicao); \$i++){

        \$consulta .= \" AND \" . \$strCondicao[\$i];

    }
    
    for(\$i=0; \$i < count(\$strGET); \$i++){

        \$varGET .= \"&\" . \$strGET[\$i];

    }

    \$consultaNumero = \"SELECT COUNT($key) FROM $table WHERE excluido_BOOLEAN=0 {\$consulta}\";

    \$objBanco = new Database();

    \$objBanco->query(\$consultaNumero);
    \$numeroRegistros = \$objBanco->getPrimeiraTuplaDoResultSet(0);

    \$limites = Helper::getLimitesRegsPaginacao(\$registrosPorPagina, \$numeroRegistros);

    \$consultaRegistros = \"SELECT $key FROM $table WHERE excluido_BOOLEAN=0 {\$consulta} ORDER BY $label LIMIT {\$limites[0]},{\$limites[1]}\";

    \$objBanco->query(\$consultaRegistros);

    ?>

    ";

    $conteudo .= "

   <fieldset class=\"fieldset_list\">
            <legend class=\"legend_list\">$tituloFieldset</legend>

   <table class=\"tabela_list\">
   		<colgroup>\n";

   $cAcoes = 0;

   if(count($arrAcoes) > 0)
       $cAcoes++;
   
   $numeroColunas = count($arrCampos) + $cAcoes;

   $porcentagemMedia = round(100/(count($arrCampos) + $cAcoes));

   for($i=0; $i < count($arrCampos) + $cAcoes; $i++){

       $conteudo .= "\t\t\t<col width=\"{$porcentagemMedia}%\" />\n";

   }

   $conteudo .= "\t\t</colgroup>
        <thead>
		<tr class=\"tr_list_titulos\">\n\n";

    for($i=0; $i < count($arrCampos); $i++){

        $col = $arrCampos[$i];
        $colUpper = ucfirst($col);

        $conteudo .= "\t\t\t<td class=\"td_list_titulos\"><?=\$obj->label_$col ?></td>\n";


    }

    if(count ($arrAcoes) > 0){

        $conteudo .= "\t\t\t<td class=\"td_list_titulos\">A��es</td>\n";

    }

    $conteudo .= "
		</tr>
		</thead>
    	<tbody>

    <? 
    
    if(\$objBanco->rows == 0){
    
    ?>
    
    <tr class=\"tr_list_conteudo_impar\">
        <td  colspan=\"{$numeroColunas}\">
            <?=Helper::imprimirMensagem(\"$mensagemNenhumRegistro\") ?>
        </td>
    </tr>

    <?

    }

    for(\$i=1; \$regs = \$objBanco->fetchArray(); \$i++){

    	\$obj->select(\$regs[0]);
    	\$obj->formatarParaExibicao();

    	\$classTr = (\$i%2)?\"tr_list_conteudo_impar\":\"tr_list_conteudo_par\"


    ?>

    ";

    $conteudo .= "\t<tr class=\"<?=\$classTr ?>\">\n";

    $strAcoes .= "\n";

    $keyUpper = ucfirst($key);

    for($i=0; $i < count ($arrAcoes); $i++){

        if($i==0)
            $strAcoes .= "\t\t\t<td class=\"td_list_conteudo\" style=\"text-align: center;\">\n";

        if($arrAcoes[$i] == "editar")
            $strAcoes .= "\t\t\t\t<img class=\"icones_list\" src=\"imgs/icone_editar.png\" onclick=\"javascript:location.href='index.php?tipo=forms&page=$table&id1=<?=\$obj->get$keyUpper(); ?>'\" onmouseover=\"javascript:tip('<?=\$acoes['tooltip_edicao'] ?>')\" onmouseout=\"javascript:notip()\">&nbsp;\n";

        elseif($arrAcoes[$i] == "visualizar")
            $strAcoes .= "\t\t\t\t<img class=\"icones_list\" src=\"imgs/icone_detalhes.png\" onclick=\"javascript:location.href='index.php?tipo=forms&page=$table&id1=<?=\$obj->get$keyUpper(); ?>'\" onmouseover=\"javascript:tip('<?=\$acoes['tooltip_visualizacao'] ?>')\" onmouseout=\"javascript:notip()\">&nbsp;\n";

        elseif($arrAcoes[$i] == "excluir")
            $strAcoes .= "\t\t\t\t<img class=\"icones_list\" src=\"imgs/icone_excluir.png\" onclick=\"javascript:confirmarExclusao('actions.php?class=$ext&action=remove&id=<?=\$obj->get$keyUpper(); ?>','<?=\$acoes['mensagem_exclusao'] ?>')\" onmouseover=\"javascript:tip('<?=\$acoes['tooltip_exclusao'] ?>')\" onmouseout=\"javascript:notip()\">&nbsp;\n";

        if($i == count($arrAcoes)-1)
            $strAcoes .= "\t\t\t</td>\n";

    }

    for($i=0; $i < count($arrCampos); $i++){

        $col = $arrCampos[$i];
        $colUpper = ucfirst($col);

        if(substr_count($col, "_id_") > 0){

            if(substr_count($col, "_id_value") > 0){

                $tabela = substr($col, 0, strpos($col, "_id_"));

                $conteudo .= "

    		<td class=\"td_list_conteudo\" style=\"text-align: left; padding-left: 5px;\">
    			<?=\$obj->get$colUpper() ?>
    		</td>\n";

            }
            else{

                $tabela = substr($col, 0, strpos($col, "_id_"));
                $objTabela = "obj" . ucfirst($tabela);
                $nomeMetodo = "All" . ucfirst($tabela);

                $conteudo .= "
                <td class=\"td_list_conteudo\" style=\"text-align: left; padding-left: 5px;\">
                    
                    <? if(strlen(\$obj->get$colUpper())){
                
                        \$obj->{$objTabela}->select(\$obj->get$colUpper());
                        \$obj->{$objTabela}->formatarParaExibicao();
                        
                    ?>
                        
                        <?=\$obj->{$objTabela}->valorCampoLabel() ?>

                    <? } ?>
                    
                </td>\n";

            }

        }
        elseif(substr_count($col, "_BOOLEAN") > 0){

            $labelTrue = "Sim";
            $labelFalse = "N�o";

            if(substr_count($col, "status") > 0){

                $labelTrue = "Ativo";
                $labelFalse = "Inativo";

            }

            $conteudo .= "
    		<td class=\"td_list_conteudo\" style=\"text-align: left; padding-left: 5px;\">
    			<?=\$obj->get$colUpper()?\"$labelTrue\":\"$labelFalse\" ?>
    		</td>\n";



        }
        else{

            $conteudo .= "
    		<td class=\"td_list_conteudo\" style=\"text-align: left; padding-left: 5px;\">
    			<?=\$obj->get$colUpper() ?>
    		</td>\n";

        }

    }

	$conteudo .= $strAcoes;

    $conteudo .= "

    \n\t\t</tr>

    <? } ?>

    </tbody>
    </table>

    </fieldset>

    <br/>
    <br/>

    <?

    //Pagina��o

    \$paginaAtual = Helper::GET(\"pagina\")?Helper::GET(\"pagina\"):\"1\";
    \$numeroPaginas = Helper::getNumeroPaginas(\$registrosPorPagina, \$numeroRegistros);

    if(\$numeroPaginas > 1){

    ?>

    <fieldset class=\"fieldset_paginacao\">
            <legend class=\"legend_paginacao\">Pagina��o</legend>

	<table class=\"table_paginacao\">
		<tr class=\"tr_paginacao\">

	<?

	for(\$i=1; \$i <= \$numeroPaginas; \$i++){

		\$class = (\$i==\$paginaAtual)?\"td_paginacao_pag_atual\":\"td_paginacao\"

	?>

		<td class=\"<?=\$class ?>\" onclick=\"javascript:location.href='index.php?tipo=lists&page=$table&pagina=<?=\$i ?><?=\$varGET ?>'\"><?=\$i ?></td>

	<? } ?>

	    </tr>
	</table>

	</fieldset>

	<? } ?>

	";

    $conteudo.= "";

        fwrite($extFile, $conteudo);
        fclose($extFile);
		
		$strGerouExt = "<p class=\"mensagem_retorno\">
            &bull;&nbsp;&nbsp;List <b>$table</b> gerado com sucesso no arquivo <b>$table.php</b>.
            </p>";

    print $strGerouExt;

    }

}

?>