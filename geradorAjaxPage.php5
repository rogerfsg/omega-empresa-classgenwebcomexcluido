<?php

function gerarAjaxPage($table, $class, $key, $label, $ext, $sobrescrever, $numeroRegs){

    $database = new Database();

    $objBanco = new Database();

    $idProjeto = $_POST["projeto"];

    $objBanco->query("SELECT p.nomeBanco, c.hostnameBanco, c.usuarioBanco, c.senhaBanco, p.diretorioAjaxPages, p.colunasForms_INT FROM projetos p, conexoes c WHERE c.id = p.conexaoBanco_INT AND p.id=$idProjeto");

    $database = new Database(Database::mysqli_result($objBanco->result, 0, 0));
    
    $database->database = Database::mysqli_result($objBanco->result, 0, 0);
    $database->host = Database::mysqli_result($objBanco->result, 0, 1);
    $database->user = Database::mysqli_result($objBanco->result, 0, 2);
    $database->password = Database::mysqli_result($objBanco->result, 0, 3);
    $diretorio = Database::mysqli_result($objBanco->result, 0, 4);
    $colunas = Database::mysqli_result($objBanco->result, 0, 5);
    
    $database->OpenLink();

    $dir = dirname(__FILE__);
    $filedate = date("d.m.Y");

    $filename = $dir . "/../" . $diretorio ."/" . $table . ".php";

    $sql = "SHOW TABLES LIKE '$table';";
    $database->query($sql);

    if($database->rows < 1){

        return;

    }

    $permissaoSobreescrita = Helper::conferirPermissaoSobreEscrita($filename);

    // if file exists, then delete it
    if($permissaoSobreescrita && $sobrescrever && file_exists($filename))
    {
        unlink($filename);
    }

    if(!file_exists($filename)){

        $file = fopen($filename, "w+");

        $genero   = $_POST["genero_entidade"]; //F ou M
        $nomeSing = $_POST["entidade_singular"];
        $nomePlu  = $_POST["entidade_plural"];
        $numeroCadastros = $_POST["numeroCadastros"];

        $index = 0;

        $novo[0] = "novo";
        $novo[1] = "nova";
        $novo[2] = "novos";
        $novo[3] = "novas";

        if($genero == "F")
            $index += 1;

        $nomeMsg = $nomeSing;

        $mensagemExclusaoSucesso = $artigo[$index] . " " . $foi[$index] . " " . $excluido[$index] . " com sucesso.";

        if($numeroCadastros > 1){

            $index += 2;
            $nomeMsg = $nomePlu;

        }

        $mensagemAdicionar = "Adicionar " . $novo[$index] . " " . $nomeMsg;
        $mensagemListar = "Listar " . $nomePlu;

        $conteudo = "<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DO AJAX PAGE:  $table
    * DATA DE GERA��O:    $filedate
    * ARQUIVO:            $table.php
    * TABELA MYSQL:       $table
    * BANCO DE DADOS:     $database->database
    * -------------------------------------------------------
    *
    */

    \$obj = new $ext();

    \$objArg = new Generic_Argument();

    \$numeroRegistros = 1;
    \$cont = 1;
    \$class = \$obj->nomeClasse;
    \$action = (Helper::GET(\"id1\")?\"edit_ajax\": \"add_ajax\");
    \$postar = \"actions.php\";

    \$nextActions = array(\"add_ajax_$table\"=>\"$mensagemAdicionar\",
    					 \"list_$table\"=>\"$mensagemListar\");

    ?>
    ";

    $numeroColunasDb = count($_POST["camposajaxmestres"]);

    for($i=0; $i<$numeroColunasDb+1; $i++){

        if($i == $numeroColunasDb){

            $strFields .= "\t\tstrRetorno += \"numero_registros_ajax=\" + document.getElementById('numero_registros_ajax').value;";
        	$strIf .= "document.getElementById('numero_registros_ajax').value != \"\" && ";

        }
        else{

            $strFields .= "\t\tstrRetorno += \"{$_POST["camposajaxmestres"][$i]}=\" + document.getElementById('{$_POST["camposajaxmestres"][$i]}1').value + \"&\";\n";
        	$strIf .= "document.getElementById('{$_POST["camposajaxmestres"][$i]}1').value != \"\" && ";

        }

    }

    $strIf = substr($strIf, 0, strlen($strIf) - 4);

    $conteudo .= "

    <script language=\"javascript\">

        function carregarAjaxAtual(load){

            if({$strIf}){

                carregarListAjax('{$table}', \"ajax_list\", 'montarStrFields()', 'posCarregarListAjax()', 1);

            }
            else if(!load){

                alerta('Todos os campos devem ser preenchidos antes de continuar', 1);

            }

        }

        function montarStrFields(){

            var strRetorno = \"\";

$strFields

            return strRetorno;

        }

    </script>

    <?=\$obj->getCabecalhoFormulario(\$postar); ?>";

        if($_POST["numero_cadastros_ajax"] != "0"){

            $conteudo .= "
        <input type=\"hidden\" name=\"numero_registros_ajax\" id=\"numero_registros_ajax\" value=\"{$_POST["numero_cadastros_ajax"]}\">";

        }

    	$conteudo .= "

    	<input type=\"hidden\" name=\"class\" id=\"class\" value=\"<?=\$class; ?>\">
        <input type=\"hidden\" name=\"action\" id=\"action\" value=\"<?=\$action; ?>\">
    	<input type=\"hidden\" name=\"origin_action\" id=\"origin_action\" value=\"<?=\$action; ?>_{$table}\">

    	<?

            if(Helper::SESSION(\"erro\")){

                unset(\$_SESSION[\"erro\"]);

               \$obj->setBySession();

            }

            ";



      for($i=0; $i<$numeroColunasDb; $i++){

            $colUpper = ucfirst($_POST["camposajaxmestres"][$i]);

            if(in_array($_POST["camposajaxmestres"][$i], $_POST["camposajaxget"])){

                $conteudo .= "

            if(Helper::GET(\"{$_POST["camposajaxmestres"][$i]}\")){

                \$valor = Helper::GET(\"{$_POST["camposajaxmestres"][$i]}\");

                \$obj->set{$colUpper}(\$valor);

            }

                ";

            }

      }

            $conteudo .= "

            \$obj->formatarParaExibicao();

    	?>

    	<input type=\"hidden\" name=\"$key<?=\$cont ?>\" id=\"$key<?=\$cont ?>\" value=\"<?=\$obj->get" . ucfirst($key) . "(); ?>\">

        <table class=\"tabela_form\">

        ";

    	//adiciona slot para mais um campo no formulario
    	$fieldNumero=$_POST["numero_cadastros_ajax"]=="0"?1:0;

    	if($fieldNumero){

    	    $strCampo = "

    	    <td class=\"td_form_label\">N� de {$_POST["entidade_plural"]} � cadastrar:</td>
			<td class=\"td_form_campo\">

			<select class=\"input_text\"
			        name=\"numero_registros_ajax\"
			        id=\"numero_registros_ajax\">

			";

			$numeroMaximo=$_POST["numero_maximo_ajax"]=="0"?20:$_POST["numero_maximo_ajax"];

			$strCampo .= "<?=Helper::gerarOptionsIntervalo(1, $numeroMaximo, Helper::GET(\"numero_registros_ajax\")) ?>";

			$strCampo .= "

			</select>

			</td>\n

    	    ";


    	}

        if(($numeroColunasDb + $fieldNumero) % $colunas == 0)
            $contador = $numeroColunasDb + $fieldNumero;
        else
            $contador = $numeroColunasDb + $fieldNumero + $colunas - (($numeroColunasDb + $fieldNumero) % $colunas) ;

        $colspan = $colunas * 2;

        for ($i=0; $i < $contador; $i++)
        {

            $col = $_POST["camposajaxmestres"][$i];
            $colUpper = ucfirst($col);

            if($i > $numeroColunasDb -1){


                if($fieldNumero && $i == $numeroColunasDb){

                    $conteudo .= $strCampo;

                }else{

                    $conteudo .="

                	<td class=\"td_form_label\"></td>
        			<td class=\"td_form_campo\"></td>\n";

                }

                    if($i % $colunas == $colunas -1){

                        $conteudo .= "\t\t\t</tr>\n";

                    }

                continue;

            }

            if($i % $colunas == 0){

                $conteudo .= "\t\t\t<tr class=\"tr_form\">\n";

            }

            $obrigatorio = is_array($_POST["camposobrigatorios"]) && in_array($col, $_POST["camposobrigatorios"])?"true":"false";

            if(substr_count($col, "_id_") > 0){

                $tabela = substr($col, 0, strpos($col, "_id_"));
                $nomeMetodo = "All" . ucfirst($tabela);

                $conteudo .= "

    			<?

    			\$objArg->numeroDoRegistro = \$cont;
    			\$objArg->label = \$obj->label_{$col};
    			\$objArg->valor = \$obj->get$colUpper();
    			\$objArg->classeCss = \"input_text\";
    			\$objArg->classeCss = \"focus_text\";
    			\$objArg->obrigatorio = {$obrigatorio};
    			\$objArg->largura = 200;

    			\$obj->addInfoCampos(\"$col\", \$objArg->label, \"TEXTO\", \$objArg->obrigatorio);

    			?>

    			<td class=\"td_form_label\"><?=\$objArg->getLabel() ?></td>
    			<td class=\"td_form_campo\">
    			    <?=\$obj->getComboBox{$nomeMetodo}(\$objArg); ?>
    			</td>\n

    			";


            }
            elseif(substr_count($col, "_BOOLEAN") > 0){

                $labelTrue = "Sim";
                $labelFalse = "N�o";

                if(substr_count($col, "status") > 0){

                   $labelTrue = "Ativo";
                   $labelFalse = "Inativo";

                }

                $conteudo .= "

    			<?

    			\$objArg->numeroDoRegistro = \$cont;
    			\$objArg->label = \$obj->label_{$col};
    			\$objArg->labelTrue = \"{$labelTrue}\";
    			\$objArg->labelFalse = \"{$labelFalse}\";
    			\$objArg->valor = \$obj->get$colUpper();
    			\$objArg->classeCss = \"input_text\";
    			\$objArg->classeCss = \"focus_text\";
    			\$objArg->obrigatorio = {$obrigatorio};
    			\$objArg->largura = 20;

    			?>

    			<td class=\"td_form_label\"><?=\$objArg->getLabel() ?></td>
    			<td class=\"td_form_campo\"><?=\$obj->imprimirCampo$colUpper(\$objArg); ?></td>\n";

            }
            else{

                $conteudo .= "

    			<?

    			\$objArg->numeroDoRegistro = \$cont;
    			\$objArg->label = \$obj->label_{$col};
    			\$objArg->valor = \$obj->get$colUpper();
    			\$objArg->classeCss = \"input_text\";
    			\$objArg->classeCss = \"focus_text\";
    			\$objArg->obrigatorio = {$obrigatorio};
    			\$objArg->largura = 200;

    			?>

    			<td class=\"td_form_label\"><?=\$objArg->getLabel() ?></td>
    			<td class=\"td_form_campo\"><?=\$obj->imprimirCampo$colUpper(\$objArg); ?></td>\n";


            }

            if($i % $colunas == $colunas -1){

                $conteudo .= "\t\t\t</tr>\n";

            }

        }


     $conteudo .= "

         <tr class=\"tr_form_rodape2\">
        	<td colspan=\"$colspan\" >
        		<div class=\"div_ajax_botoes_inicial\" id=\"ajax_botoes_inicial\">
        		    <?=Helper::imprimirBotoesAjax(true, true, \$action==\"edit\"?true:false); ?>
                </div>
        	</td>
        </tr>

         <tr class=\"tr_div_ajax_list\">
        	<td colspan=\"$colspan\" align=\"center\">
        	    <div class=\"div_ajax_list\" id=\"ajax_list\"></div>
        	</td>
        </tr>

         <tr class=\"tr_form_rodape1\">
        	<td colspan=\"$colspan\">
        	    <div class=\"div_ajax_next_action\" id=\"ajax_next_action\">
        	        <?=Helper::getBarraDaNextAction(\$nextActions); ?>
                </div>
        	</td>
        </tr>
        <tr class=\"tr_form_rodape2\">
        	<td colspan=\"$colspan\" >
        		<div class=\"div_ajax_botoes\" id=\"ajax_botoes\">
        		    <?=Helper::getBarraDeBotoesDoFormulario(true, true, \$action==\"edit\"?true:false); ?>
                </div>
        	</td>
        </tr>\n";

     $conteudo .="\t</table>

	<?=\$obj->getInformacoesDeValidacaoDosCampos(); ?>

	<?=\$obj->getRodapeFormulario(); ?>

	<script language=\"javascript\">

        carregarAjaxAtual(true);

	</script>

";

     fwrite($file, $conteudo);
     fclose($file);

        $strGerouExt = "<font face=\"Verdana\" size=\"3\"><b>
            </b>
            <p>
            <font face=\"Verdana\" size=\"3\" color=\"#008000\">
            &bull;&nbsp;&nbsp;Ajax Page <b>$table</b> gerado com sucesso no arquivo <b>$table.php</b>.
            <p>
            </b></font>";

    print $strGerouExt;

    }

}

?>