<?

class Padronizador_Database {// Classe : in�cio

    public $__idProjeto;
    public $__listTableNameOrdered;

    public function __construct($idProjeto) {
        $this->__idProjeto = $idProjeto;
        $objBanco = $this->getNewDatabaseDoProjetoAtual();
        $this->__listTableNameOrdered = $this->getListTableNameOrdered();
    }

    public function getNewDatabaseDoProjetoAtual() {
        $idProjeto = $this->__idProjeto;
        $database = new Database();

        $objBanco = new Database("projetos");

        $objBanco->query("SELECT p.nomeBanco, c.hostnameBanco, c.usuarioBanco, c.senhaBanco, p.diretorioClassesEXTDAO FROM projetos p, conexoes c WHERE c.id = p.conexaoBanco_INT AND p.id=$idProjeto");

        $database = new Database(Database::mysqli_result($objBanco->result, 0, 0));

        $database->database = Database::mysqli_result($objBanco->result, 0, 0);
        $database->host = Database::mysqli_result($objBanco->result, 0, 1);
        $database->user = Database::mysqli_result($objBanco->result, 0, 2);
        $database->password = Database::mysqli_result($objBanco->result, 0, 3);
        $diretorio = Database::mysqli_result($objBanco->result, 0, 4);

        $database->OpenLink();
        return $database;
    }

    public function setAutoIncrementForAllTable($p_listTableName) {
        $objBanco = $this->getNewDatabaseDoProjetoAtual();
        if (count($p_listTableName) == 0) {
            $p_listTableName = $this->__listTableNameOrdered;
        }
        foreach ($p_listTableName as $v_tableName) {
            
            $objBanco->Query("ALTER TABLE " . $v_tableName . " AUTO_INCREMENT = 1;");
                    
        }
    }

    public function padronizeDatabaseSQL() {
        //$this->setAutoIncrementForAllTable();
        $this->createAllForeignKeyOfAllTablesInDatabase();
    }

    public function createAllForeignKeyOfAllTablesInDatabase() {
        $objBanco = $this->getNewDatabaseDoProjetoAtual();
        $v_listaTabela = $this->__listTableNameOrdered;
        
        foreach ($v_listaTabela as $v_strTableName) {

            $this->createForeignKeyOfTableOfFieldWithDefaultName($v_strTableName);
        }
    }

    public function getMorePossibleTableOfForeignKeyByNameOfField($p_nameToken) {
        if (strlen($p_nameToken) > 0) {
            $v_nameToken = trim(strtolower($p_nameToken));

            for ($i = count($this->__listTableNameOrdered) - 1; $i >= 0; $i--) {
                $v_nameTable = $this->__listTableNameOrdered[$i];

                If (substr_count($v_nameToken, $v_nameTable) > 0) {
                    return $v_nameTable;
                }
            }
        }
        return null;
    }

    public function getNomeDaClasseDoNomeDoCampoChaveExtrangeira($p_strNomeCampo) {
        if (strlen($p_strNomeCampo) > 0) {
            $objBanco = $this->getNewDatabaseDoProjetoAtual();
            $v_listTableNameOrdered = $this->__listTableNameOrdered;

            $v_strTabela = $this->getStrNomeTabelaDaChaveExtrangeira($v_listTableNameOrdered, $p_strNomeCampo);

            if (is_null($v_strTabela)) {
                return ucfirst($p_strNomeCampo);
            } else {
                $v_strTabela = trim(strtolower($v_strTabela));
                $v_strTabela = ucfirst($v_strTabela);
                return $v_strTabela;
            }
        }
    }

    public function createForeignKeyOfTableOfFieldWithDefaultName($p_strTableName) {
        if (strlen($p_strTableName) > 0) {
            $objBanco = $this->getNewDatabaseDoProjetoAtual();
            $v_listTableNameOrdered = $this->__listTableNameOrdered;
            $v_listaField = $objBanco->getListaAtributoDaTabela($p_strTableName);
            $objBanco->query("SET FOREIGN_KEY_CHECKS = 0");
            foreach ($v_listaField as $v_strField) {

                $v_strTabelaReferente = "";
                $v_strNomeChaveEstrangeira = "";

                $v_strNomeAtributo = $v_strField;
                If (substr_count($v_strNomeAtributo, "_id_INT") > 0) {
			
                    if (!$this->isExistenteChaveExtrangeiraDoCampoNaTabela($v_strNomeAtributo, $p_strTableName)) {
			
                        $v_vetorToken = explode("_", $v_strNomeAtributo);
                                                
                        if (!count($v_vetorToken) >= 3) {
                            continue;
                        } elseIf (!( $v_vetorToken[count($v_vetorToken) - 1] = "INT" && $v_vetorToken[count($v_vetorToken) - 2] = "id")) {
                            continue;
                        } else {
                            
                            for ($i = 0; $i <= count($v_vetorToken) - 3; $i++) {
                                If (strlen($v_strTabelaReferente) > 0) {
                                    $v_strTabelaReferente .= "_" . $v_vetorToken[$i];
                                } else {
                                    $v_strTabelaReferente .= $v_vetorToken[$i];
                                }
                            }
                            $v_strTabela = $this->getStrNomeTabelaDaChaveExtrangeira($v_strTabelaReferente);
                            if (is_null($v_strTabela) || strlen($v_strTabela) == 0) {
                                continue;
                            }
                            echo "TABELA REFERENCIA = ".$v_strTabela."</br>";
                            $v_strNomeChaveEstrangeira = $this->getForeignKeyDefaultName($p_strTableName, $v_strTabela);

                            $v_strQuery = "ALTER TABLE " . $p_strTableName . " ADD CONSTRAINT " . $v_strNomeChaveEstrangeira . " FOREIGN KEY (" . $v_strNomeAtributo . ") REFERENCES " . $v_strTabelaReferente . "(id) ON UPDATE CASCADE ON DELETE CASCADE;";
                            
                            $objBanco->query($v_strQuery);
                        }
                    }
                }
            }
        }
    }

    public function getStrNomeTabelaDaChaveExtrangeira($p_strTabelaReferente) {

        if (strlen($p_strTabelaReferente) > 0) {

            if (array_search($p_strTabelaReferente, $this->__listTableNameOrdered)) {
                return $p_strTabelaReferente;
            } else {
                $p_strTabelaReferente = $this->getMorePossibleTableOfForeignKeyByNameOfField($p_strTabelaReferente);
                if (is_null($p_strTabelaReferente) || strlen($p_strTabelaReferente) == 0) {
                    return null;
                } else {
                    return $p_strTabelaReferente;
                }
            }
        }
    }

    public function getListTableNameOrdered() {
        $objDatabase = $this->getNewDatabaseDoProjetoAtual();
        $v_listaTabela = $objDatabase->getListaDosNomesDasTabelas();

        sort($v_listaTabela);

        $v_listaOrdered = array();

        $v_tokenWithLessNumberOfLetter = $v_listaTabela[0];
        $v_index = 0;
        $v_counter = 0;

        for ($i = 0; $i < count($v_listaTabela); $i++) {
            $j = -1;
            foreach ($v_listaTabela as $v_tableName) {
                $j += 1;
                if (is_null($v_tableName) || strlen($v_tableName) == 0) {
                    continue;
                } elseif (is_null($v_tokenWithLessNumberOfLetter)) {
                    $v_tokenWithLessNumberOfLetter = $v_tableName;
                    $v_index = $j;
                }
                If (strlen($v_tableName) < strlen($v_tokenWithLessNumberOfLetter)) {
                    $v_tokenWithLessNumberOfLetter = $v_tableName;
                    $v_index = $j;
                }
            }

            $v_listaOrdered[$v_counter] = $v_tokenWithLessNumberOfLetter;
            $v_tokenWithLessNumberOfLetter = null;
            $v_counter += 1;
            $v_listaTabela[$v_index] = null;
        }

        return $v_listaOrdered;
    }

    public function isExistenteChaveExtrangeiraDoCampoNaTabela($p_strNomeCampo, $p_strNomeTabela) {
        if (strlen($p_strNomeCampo) > 0 && strlen($p_strNomeTabela) > 0) {
            $objBanco = $this->getNewDatabaseDoProjetoAtual();
// Pega todo pedido no status requisitado, seje relativo a um produto individual, ou a um combo, ou promocao.
            $objBanco->Query("SHOW KEYS " .
                    " FROM " . $p_strNomeTabela .
                    " WHERE Column_name='" . $p_strNomeCampo . "' AND
		    Non_unique='1';");
            $v_strDefault = $objBanco->getPrimeiraTuplaDoResultSet(0);
	    
            if (strlen($v_strDefault) > 0) {
                return true;
            }
            else
                return false;
        }
    }

    public function getForeignKeyDefaultName($p_nomeTabela, $p_strNomeTabelaChaveEstrangeira) {


        $v_rand = rand(0, 1000000000);

        $v_strNomeChave = $p_nomeTabela . "_FK_" . $v_rand;
        return $v_strNomeChave;
    }

}

// Class : fim
?>
