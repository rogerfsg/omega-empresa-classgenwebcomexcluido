<?php
include("lib/Database.php5");
include("lib/Helper.php5");
include("lib/Padronizador_Database.php5");
include("geradorDAO.php5");
include("geradorEXT.php5");
include("geradorForm.php5");
include("geradorFiltro.php5");
include("geradorList.php5");
include("geradorAjaxPage.php5");
include("geradorAjaxForm.php5");
include("geradorFormMultiplo.php5");
include("geradorFormComplemento.php5");

if ($_REQUEST["f"] == "") {
    ?>

    <html>
        <head>
            <link type="text/css" rel="stylesheet" href="padrao.css" />
            <title>GERADOR DE PROJETOS OMEGA SOFTWARE</title>
            <script language="javascript">

                function automatico(valor, condicaoChave, condicaoUnderLine){

                    var posicao = 0

                    var valorAnt = "";

                    var underline = "";

                    if(condicaoUnderLine == 1)
                        underline = "_";

                    if(condicaoChave == 1)
                        valorAnt = valor + "_";

                    while(posicao != -1){

                        posicao = valor.indexOf("_", posicao+1);

                        if(posicao != -1)
                            valor = valor.substring(0, posicao) + underline + valor.substring(posicao+1,posicao+2) + valor.substring(posicao+2,valor.length);
                            //valor = valor.substring(0, posicao) + underline + valor.substring(posicao+1,posicao+2).toUpperCase() + valor.substring(posicao+2,valor.length);

                    }

                    var nomeClasse = "DAO_" + valor.substring(0, 1).toUpperCase() + valor.substr(1);

                    var nomeExt = "EXTDAO_" + valor.substring(0, 1).toUpperCase() + valor.substr(1);

                    var nomeCampo = valorAnt + "id";

                    var nomeLabel = valorAnt + "nome";

                    document.getElementById("classname").value = nomeClasse;

                    //document.getElementById("keyname").value = nomeCampo;

                    //document.getElementById("labelname").value = nomeLabel;

                    document.getElementById("extname").value = nomeExt;

                }

                function carregarTabelas(){

                    var projeto = document.getElementById("projeto").value;

                    if(projeto != ""){

                        document.location = "index.php5?projeto=" + projeto;

                    }

                }

            </script>

        </head>

        <body>

            <h3>
                GERADOR DE PROJETOS OMEGA SOFTWARE
            </h3>

            <form action="index.php5" method="POST" name="FORMGEN"> 

                <p>1) Escolha o Projeto:</p> 

                <select class="input_text" name="projeto" id="projeto" onchange="javascript:carregarTabelas();">

                    <option value=""></option>

                    <?
                    $idProjeto = $_GET["projeto"];

                    $objBanco = new Database();
                    $objBanco->host = "localhost";
                    $objBanco->database = "projetos";
                    $objBanco->user = "root";
                    $objBanco->password = "27sete86";

                    $objBanco->query("SELECT id, nome FROM projetos WHERE status_INT=1 ORDER BY nome");

                    for ($i = 0; $i < $objBanco->rows; $i++) {

                        $id = $objBanco->resultSet($i, 0);
                        $nome = $objBanco->resultSet($i, 1);

                        $selecionado = ($idProjeto == $id) ? "selected=\"selected\"" : "";
                        echo "\t<option value=\"$id\" $selecionado>$nome</option>\n";
                    }
                    ?>

                </select>


                <?
                if (isset($_GET["projeto"])) {

                    $objBanco = new Database();
                    $objBanco->query("SELECT p.nomeBanco, c.hostnameBanco, c.usuarioBanco, c.senhaBanco, p.diretorioClassesDAO, p.chaveComNomeTabela, p.underlineNomeClasse FROM projetos p, conexoes c WHERE c.id = p.conexaoBanco_INT AND p.id=$idProjeto");

                    $database = new Database($objBanco->resultSet(0, 0));
                    $database2 = new Database($objBanco->resultSet(0, 0));

                    $database->database = $objBanco->resultSet(0, 0);
                    $database->host = $objBanco->resultSet(0, 1);
                    $database->user = $objBanco->resultSet(0, 2);
                    $database->password = $objBanco->resultSet(0, 3);

                    $database->OpenLink();

                    $database2->database = $objBanco->resultSet(0, 0);
                    $database2->host = $objBanco->resultSet(0, 1);
                    $database2->user = $objBanco->resultSet(0, 2);
                    $database2->password = $objBanco->resultSet(0, 3);

                    $database2->OpenLink();

                    $condicaoChave = $objBanco->resultSet(0, 5);
                    $condicaoUnderline = $objBanco->resultSet(0, 6);
                }
                if (isset($_GET["projeto"]) || isset($_GET["tabela"])) {

                    $tabelaSel = $_GET["tabela"];
                    ?>

                    <p>2.1) Escolha a Tabela:</p>

                    <select class="input_text" name="tablename" id="tablename" onchange="javascript:location.href='index.php5?projeto=<?= $_GET["projeto"] ?>&tabela=' + this.value + '&condicaoUnderline=<?= $condicaoUnderline ?>&condicaoChave=<?= $condicaoChave ?>'">

                        <option value=""></option>

                        <?
                        $database->query("SHOW TABLES FROM {$database->database}", $database->link);

                        while ($row = $database->fetchArray()) {

                            print "<option " . ($tabelaSel == $row[0] ? "selected=\"selected\"" : "") . " value=\"$row[0]\">$row[0]</option>\n";
                        }
                        ?>

                    </select>
                    <br>
                    <br>

                    <? if (!$_GET["tabela"]) { ?>
                    
                        <? 
                        
                        if(isset($_GET["acao"]) && $_GET["acao"] == "corrigir_estrutura_tabelas"){

                            $database->query("SHOW TABLES FROM {$database->database}", $database->link);

                            while ($row = $database->fetchArray()) {
                                
                                $tabelaCorrigida = false;
                                
                                $nomeTabela = $row[0];
                                
                                $sqlCamposMarcacaoDelecao = "SHOW COLUMNS FROM $nomeTabela WHERE `Field` = 'excluido_BOOLEAN'";
                                $database2->query($sqlCamposMarcacaoDelecao);

                                if($database2->rows == 0){

                                    $database2->query("ALTER TABLE {$nomeTabela} ADD COLUMN excluido_BOOLEAN INT(1) DEFAULT 0 NOT NULL");
                                    
                                    $tabelaCorrigida = true;
                                    
                                }

                                $sqlCamposMarcacaoDelecao = "SHOW COLUMNS FROM $nomeTabela WHERE `Field` = 'excluido_DATETIME'";
                                $database2->query($sqlCamposMarcacaoDelecao);

                                if($database2->rows == 0){

                                    $database2->query("ALTER TABLE {$nomeTabela} ADD COLUMN excluido_DATETIME DATETIME");

                                    $tabelaCorrigida = true;
                                    
                                }
                                
                                if($tabelaCorrigida){
                                    
                                    print "<p class=\"mensagem_retorno\">
                                    &bull;&nbsp;&nbsp;Tabela <b>$nomeTabela</b> corrigida com sucesso</b>.
                                    </p>";
                                    
                                }
                                                                
                            }
                    

                        } 
                        elseif(isset($_GET["acao"]) && $_GET["acao"] == "criar_chaves_estrangeiras"){
                        
                            $idProjeto = $_GET["projeto"];
                            
                            $objPadronizador = new Padronizador_Database($idProjeto);
                            $objPadronizador->padronizeDatabaseSQL();
                            
                            print "<p class=\"mensagem_retorno\">
                                    &bull;&nbsp;&nbsp;Chaves estrangeiras criadas com sucesso.
                                    </p>";
                            
                        }
                        ?>
                    
                        <p>2.2) Gerar classe para todas as tabelas deste projeto que tenham configura��es salvas</p>

                        <input type="hidden" value="ok" name="todas">

                        <input type="checkbox" name="gerarDAO" value="1" checked> Gerar DAO &nbsp;&nbsp;
                        <br />
                        <input type="checkbox" name="gerarEXT" value="1"> Gerar EXTDAO &nbsp;&nbsp;
                        <br />
                        <input type="checkbox" name="gerarForm" value="1" checked> Gerar Formul�rio &nbsp;&nbsp;
                        <br />
                        <input type="checkbox" name="gerarList" value="1" checked> Gerar List &nbsp;&nbsp;
                        <br />
                        <input type="checkbox" name="gerarFiltro" value="1" checked> Gerar Filtro &nbsp;&nbsp;
                        <br />
                        <input type="checkbox" name="gerarAjaxList" value="1" checked> Gerar Ajax List &nbsp;&nbsp;
                        <br /><br />

                        <input class="botoes_form" type="submit" value="Gerar">
                        <input type="hidden" name="f" value="formshowed">

                    </form>
            
                    <a class="link_padrao" href="index.php5?projeto=<?=$_GET["projeto"] ?>&acao=corrigir_estrutura_tabelas">Corrigir Estrutura das Tabelas</a>
                    <br />
                    <a class="link_padrao" href="index.php5?projeto=<?=$_GET["projeto"] ?>&acao=criar_chaves_estrangeiras">Criar Chaves Estrangeiras</a>

                <? } ?>

            <? } ?>

            <? if (isset($_GET["tabela"])) { ?>

                <?
                
                $projeto = $_GET["projeto"];
                $tabela = $_GET["tabela"];

                $databaseAux = new Database();
				$qConfigSalvas = "SELECT valores_campos_texto FROM configs_salvas WHERE projetos_id_INT={$projeto} AND tabela_nome='{$tabela}'";
                $databaseAux->query($qConfigSalvas);
			
                $arrCampos = array("camposfiltro" => array(),
                                    "camposlista" => array(),
                                    "acoeslista" => array(),
                                    "camposajax" => array(),
                                    "camposajaxmestres" => array(),
                                    "camposajaxget" => array(),
                                    "obrigatorioxcampo" => array());

                $loadBanco = false;

                if ($databaseAux->rows > 0) {

                    $arrCampos = unserialize(html_entity_decode(Database::mysqli_result($databaseAux->result, 0, 0)));
                    $loadBanco = true;
                }

                if (!array_key_exists("camposobrigatorios", $arrCampos))
                    $arrCampos["camposobrigatorios"] = array();

                if (!array_key_exists("camposfiltro", $arrCampos))
                    $arrCampos["camposfiltro"] = array();

                if (!array_key_exists("camposlista", $arrCampos))
                    $arrCampos["camposlista"] = array();

                if (!array_key_exists("camposajax", $arrCampos))
                    $arrCampos["camposajax"] = array();

                if (!array_key_exists("camposajaxmestres", $arrCampos))
                    $arrCampos["camposajaxmestres"] = array();

                if (!array_key_exists("camposajaxget", $arrCampos))
                    $arrCampos["camposajaxget"] = array();

                if (!array_key_exists("acoeslista", $arrCampos))
                    $arrCampos["acoeslista"] = array();
                ?>

                <p>2.2) Op��o de apagar todos os arquivos gerados nesta tabela:</p>

                <input class="botoes_form" type="button" id="apagar" name="apagar" size="50" value="Apagar Arquivos" onclick="javascript:location.href='index.php5?projeto=' + document.getElementById('projeto').value + '&opcao=apagarArquivos&tabela=' + document.getElementById('tablename').value + '&condicaoUnderline=<?= $condicaoUnderline ?>&condicaoChave=<?= $condicaoChave ?>'">
                <br>

                <p>3.1) Escreva o nome da classe ("ex: DAO_MinhaClasse"):</p>

                <input class="input_text" type="text" id="classname" name="classname" size="50" value="<?= $arrCampos["classname"] ?>">
                <br>

                <p>3.2) Escreva o nome da classe herdeira da classe DAO:</p>

                <input class="input_text" type="text" id="extname" name="extname" value="<?= $arrCampos["extname"] ?>" size="50">

                <p>4.1) Escreva o nome da entidade representada pela tabela no singular:</p>

                <font size=1 >
                <i>Todas os caracteres em min�sculo</i>
                </font>
                <br>
                <input class="input_text" type="text" id="entidade_singular" name="entidade_singular" value="<?= $arrCampos["entidade_singular"] ?>" size="50" value="">
                <br>
                <p>4.2) Escreva o nome da entidade representada pela tabela no plural:</p>

                <font size=1 >
                <i>Todas os caracteres em min�sculo</i>
                </font>
                <br>
                <input class="input_text" type="text" id="entidade_plural" name="entidade_plural" value="<?= $arrCampos["entidade_plural"] ?>" size="50" value="">
                <br>

                <p>4.3) G�nero da entidade:</p>

                <?
                $genero = $arrCampos["genero_entidade"];
                ?>

                <select class="input_text" name="genero_entidade" id="genero_entidade">
                    <option value="F" <?= $genero == "F" ? "selected=\"selected\"" : "" ?>>Feminino</option>
                    <option value="M" <?= $genero == "M" ? "selected=\"selected\"" : "" ?>>Masculino</option>
                </select>
                <br>

                <p>4.4) Defina o nome e obrigatoriedade de preenchimento dos campos contidos na tabela:</p>

                <table class="tabela_list">
                    <tr class="tr_list_titulos">
                        <td class="td_list_titulos">Campo</td>
                        <td class="td_list_titulos">Nome</td>
                        <td class="td_list_titulos">Obrigat�rio ?</td>
                    </tr>

                    <?
                    $tabela = $_GET["tabela"];

                    //PROCURANDO COLUNAS NOT NULL
                    if (!$loadBanco) {

                        $database->query("SHOW COLUMNS FROM $tabela WHERE `Null` = 'NO';");

                        $result = $database->result;

                        $achouColuna = false;

                        while ($colunas = $database->fetchArray()) {

                            $arrCampos["camposobrigatorios"][] = $colunas[0];
                        }
                    }

                    $database->query("SHOW COLUMNS FROM $tabela");

                    $result = $database->result;

                    $achouColuna = false;

                    for ($y = 0; $colunas = $database->fetchArray(); $y++) {

                        $cssClassTr = ($y % 2 == 1) ? "tr_list_conteudo_par" : "tr_list_conteudo_impar";

                        $nomeColuna = $colunas[0];

                        if ($nomeColuna == "dataCadastro")
                            $nomePadrao = "data de cadastro";
                        elseif ($nomeColuna == "dataEdicao")
                            $nomePadrao = "data de edi��o";
                        else
                            $nomePadrao = "";
                        ?>
                        <tr class="<?= $cssClassTr ?>">
                            <td><?= $nomeColuna ?></td>
                            <td>
                                <input class="input_text" type="text" style="width: 300px;" name="nomexcampo_<?= $nomeColuna ?>" value="<?= $loadBanco ? $arrCampos["nomexcampo_" . $nomeColuna] : $nomePadrao ?>" />
                            </td>
                            <td align="center">
                                <input class="input_text" type="checkbox" name="camposobrigatorios[]" value="<?= $nomeColuna ?>" <?= in_array($nomeColuna, $arrCampos["camposobrigatorios"]) ? "checked=\"checked\"" : "" ?> />
                            </td>
                        </tr>


                    <? } ?>

                </table>

                <br>

                <p>5) Escreva o nome do campo que cont�m a chave prim�ria da tabela:</p>

                <select class="input_text" name="keyname" id="keyname">

                    <?
                    $tabela = $_GET["tabela"];

                    $database->query("SHOW COLUMNS FROM $tabela WHERE `Key` = 'PRI';");

                    $result = $database->result;

                    $achouColuna = false;

                    while ($colunas = $database->fetchArray()) {


                        $nomeColuna = $colunas[0];

                        if ($arrCampos["keyname"] == $nomeColuna) {

                            $selected = "selected=\"selected\"";
                        } else {

                            $selected = "";
                        }
                        ?>

                        <option value="<?= $nomeColuna ?>" <?= $selected ?>><?= $nomeColuna ?></option>

                    <? } ?>

                </select>

                <br>
                <font size=1 >
                <i>Nome do �ndice da tabela, campo num�rico com auto-incremento</i>
                </font>
                <br>

                <p>6) Escreva o nome do campo que cont�m o campo principal da tabela:</p>

                <select class="input_text" name="labelname" id="labelname">

                    <?
                    $tabela = $_GET["tabela"];

                    $database->query("SHOW COLUMNS FROM $tabela");

                    $result = $database->result;

                    while ($colunas = $database->fetchArray()) {

                        $nomeColuna = $colunas[0];

                        if (isset($arrCampos["labelname"])) {

                            if ($arrCampos["labelname"] == $nomeColuna) {

                                $selected = "selected=\"selected\"";
                            } else {

                                $selected = "";
                            }
                        } else {

                            if (!$achouColuna && substr_count($nomeColuna, "nome") > 0) {

                                $selected = "selected=\"selected\"";
                                $achouColuna = true;
                            } else {

                                $selected = "";
                            }
                        }
                        ?>

                        <option value="<?= $nomeColuna ?>" <?= $selected ?>><?= $nomeColuna ?></option>

                    <? } ?>

                </select>
                <br>
                <br>

                <p>6.1) Defina os campos que ser�o escondidos (input type='hidden') no formul�rio:</p>

                <?
                $tabela = $_GET["tabela"];

                $database->query("SHOW COLUMNS FROM $tabela  WHERE `Key` <> 'PRI';");

                $result = $database->result;

                while ($colunas = $database->fetchArray()) {

                    $nomeColuna = $colunas[0];
                    ?>

                    <input class="input_text" <?= in_array($nomeColuna, $arrCampos["camposformulariohidden"]) ? "checked=\"checked\"" : "" ?> type="checkbox" id="camposformulariohidden" name="camposformulariohidden[]" value="<?= $nomeColuna ?>"> <?= $nomeColuna ?><br/>

                <? } ?>

                <br>
                
                <p>6.2) Defina os campos com preenchimento do tipo Autocompletar:</p>

                <?
                $tabela = $_GET["tabela"];

                $database->query("SHOW COLUMNS FROM $tabela  WHERE `Key` <> 'PRI';");

                $result = $database->result;

                while ($colunas = $database->fetchArray()) {

                    $nomeColuna = $colunas[0];
                    
                    if(!substr_count($nomeColuna, "_HTML") && 
                            !substr_count($nomeColuna, "_TEXTO") && 
                            !substr_count($nomeColuna, "_BOOLEAN") && 
                            !substr_count($nomeColuna, "_id_INT") &&
                            $nomeColuna != "dataCadastro" &&
                            $nomeColuna != "dataEdicao" ){
                    
                    ?>

                    <input class="input_text" <?= in_array($nomeColuna, $arrCampos["camposformularioautocompletar"]) ? "checked=\"checked\"" : "" ?> type="checkbox" id="camposformularioautocompletar" name="camposformularioautocompletar[]" value="<?= $nomeColuna ?>"> <?= $nomeColuna ?><br/>

                <? 
                
                    }
                
                } 
                
                ?>

                <br>
                
                <p>6.3) Defina os campos de relacionamento com preenchimento do tipo Autocompletar:</p>

                <?
                $tabela = $_GET["tabela"];

                $database->query("SHOW COLUMNS FROM $tabela  WHERE `Key` <> 'PRI';");

                $result = $database->result;

                while ($colunas = $database->fetchArray()) {

                    $nomeColuna = $colunas[0];
                    
                    if(substr_count($nomeColuna, "_id_INT")){
                    
                    ?>

                    <input class="input_text" <?= in_array($nomeColuna, $arrCampos["camposformularioautocompletarrel"]) ? "checked=\"checked\"" : "" ?> type="checkbox" id="camposformularioautocompletarrel" name="camposformularioautocompletarrel[]" value="<?= $nomeColuna ?>"> <?= $nomeColuna ?><br/>

                <? 
                
                    }
                
                } 
                
                ?>

                <br>
                
                <p>6.4) Defina as entidades que ser�o cadastradas junto a entidade corrente:</p>

                <?
                $database->query("SHOW TABLES");

                while ($tabelas = $database->fetchArray()) {

                    $tabela = $tabelas[0];
                    
                    if($tabela != $tabelaSel){

                        $database2->query("SHOW COLUMNS FROM $tabela");

                        while ($colunas = $database2->fetchArray()) {

                            $nomeColuna = $colunas[0];

                            if($nomeColuna == "{$tabelaSel}_id_INT"){

                            ?>

                            <input class="input_text" <?=in_array($tabela, $arrCampos["tabelasrelacionamento"]) ? "checked=\"checked\"" : "" ?> type="checkbox" id="tabelasrelacionamento" name="tabelasrelacionamento[]" value="<?=$tabela ?>"><?=$tabela ?><br/>

                         <? } ?>

                    <? } ?>
                            
                <? } ?>        

            <? } ?>



        <br>

        <p>7) Defina os campos que ir�o compor o filtro da pesquisa:</p>

        <?
        $tabela = $_GET["tabela"];

        $database->query("SHOW COLUMNS FROM $tabela");

        $result = $database->result;

        while ($colunas = $database->fetchArray()) {

            $nomeColuna = $colunas[0];
            ?>

                    <input class="input_text" <?= in_array($nomeColuna, $arrCampos["camposfiltro"]) ? "checked=\"checked\"" : "" ?> type="checkbox" id="camposfiltro" name="camposfiltro[]" value="<?= $nomeColuna ?>"> <?= $nomeColuna ?><br/>

        <? } ?>

                <br>
                <br>
                <p>8) Defina os campos que ir�o compor a lista de registros:</p>

        <?
        $tabela = $_GET["tabela"];

        $database->query("SHOW COLUMNS FROM $tabela");

        $result = $database->result;

        while ($colunas = $database->fetchArray()) {

            $nomeColuna = $colunas[0];
            ?>

                    <input class="input_text" <?= in_array($nomeColuna, $arrCampos["camposlista"]) ? "checked=\"checked\"" : "" ?> type="checkbox" id="camposlista" name="camposlista[]" value="<?= $nomeColuna ?>"> <?= $nomeColuna ?><br/>

        <? } ?>

                <br>
                <br>
                <p>9.1) Defina os campos que ir�o compor a lista de cadastro/atualiza��o multipla (Ajax)</p>

                <table class="tabela_list">
                    <tr class="tr_list_titulos">
                        <td class="td_list_titulos">Campo</td>
                        <td class="td_list_titulos" width="150px" align="center">Listagem</td>
                        <td class="td_list_titulos" width="150px" align="center">Mestre</td>
                        <td class="td_list_titulos" width="150px" align="center">Mestre $_GET</td>
                    </tr>

        <?
        $tabela = $_GET["tabela"];

        $database->query("SHOW COLUMNS FROM $tabela");

        $result = $database->result;

        $achouColuna = false;

        for ($y = 0; $colunas = $database->fetchArray(); $y++) {

            $cssClassTr = ($y % 2 == 1) ? "tr_list_conteudo_par" : "tr_list_conteudo_impar";

            $nomeColuna = $colunas[0];
            ?>
                        <tr class="<?= $cssClassTr ?>">
                            <td><?= $nomeColuna ?></td>
                            <td align="center"><input class="input_text" type="checkbox" name="camposajax[]" value="<?= $nomeColuna ?>" <?= in_array($nomeColuna, $arrCampos["camposajax"]) ? "checked=\"checked\"" : "" ?> /></td>
                            <td align="center"><input class="input_text" type="checkbox" name="camposajaxmestres[]" value="<?= $nomeColuna ?>" <?= in_array($nomeColuna, $arrCampos["camposajaxmestres"]) ? "checked=\"checked\"" : "" ?> /></td>
                            <td align="center"><input class="input_text" type="checkbox" name="camposajaxget[]" value="<?= $nomeColuna ?>" <?= in_array($nomeColuna, $arrCampos["camposajaxget"]) ? "checked=\"checked\"" : "" ?> /></td>
                        </tr>

        <? } ?>

                </table>

                <br>
                <br>

                <p>9.2) Defina o n�mero de cadastros simult�neos do formul�rio ajax [0 (zero) gera ComboBox p/ o usu�rio selecionar]:</p>

                <input class="input_text" type="text" id="numero_cadastros_ajax" name="numero_cadastros_ajax" value="<?= $arrCampos["numero_cadastros_ajax"] ? $arrCampos["numero_cadastros_ajax"] : "0" ?>" size="2">
                <br>
                <br>

                <p>9.3) Defina o n�mero limite no ComboBox de quantidade de registros:</p>

                <input class="input_text" type="text" id="numero_maximo_ajax" name="numero_maximo_ajax" value="<?= $arrCampos["numero_maximo_ajax"] ? $arrCampos["numero_maximo_ajax"] : "0" ?>" size="2">
                <br>
                <br>

                <p>10) Defina as a��es dispon�veis na lista:</p>

                <input <?= $loadBanco ? in_array("editar", $arrCampos["acoeslista"]) ? "checked=\"checked\"" : ""  : "checked=\"checked\"" ?> type="checkbox" id="acoeslista" name="acoeslista[]" value="editar">Editar&nbsp;&nbsp;
                <input <?= $loadBanco ? in_array("visualizar", $arrCampos["acoeslista"]) ? "checked=\"checked\"" : ""  : "checked=\"checked\"" ?> type="checkbox" id="acoeslista" name="acoeslista[]" value="visualizar">Visualizar&nbsp;&nbsp;
                <input <?= $loadBanco ? in_array("excluir", $arrCampos["acoeslista"]) ? "checked=\"checked\"" : ""  : "checked=\"checked\"" ?> type="checkbox" id="acoeslista" name="acoeslista[]" value="excluir">Excluir&nbsp;&nbsp;

                <br>
                <br>

                <p>11) Defina o n�mero de cadastros simult�neos do formul�rio:</p>

                <input class="input_text" type="text" id="numeroCadastros" name="numeroCadastros" value="<?= $arrCampos["numeroCadastros"] ? $arrCampos["numeroCadastros"] : "1" ?>" size="2">

                <br>
                <br>

                <p>12) Defina o que ser� gerado:</p>

                <input <?= $loadBanco ? isset($arrCampos["gerarDAO"]) ? "checked=\"checked\"" : ""  : "checked=\"checked\"" ?> type="checkbox" name="gerarDAO" value="1"> Gerar DAO <br />
                <input <?= $loadBanco ? isset($arrCampos["gerarEXT"]) ? "checked=\"checked\"" : ""  : "checked=\"checked\"" ?> type="checkbox" name="gerarEXT" value="1"> Gerar EXTDAO <br />
                <input <?= $loadBanco ? isset($arrCampos["gerarForm"]) ? "checked=\"checked\"" : ""  : "checked=\"checked\"" ?> type="checkbox" name="gerarForm" value="1"> Gerar Formul�rio <br />
                <input <?= $loadBanco ? isset($arrCampos["gerarList"]) ? "checked=\"checked\"" : ""  : "checked=\"checked\"" ?> type="checkbox" name="gerarList" value="1"> Gerar List <br />
                <input <?= $loadBanco ? isset($arrCampos["gerarFiltro"]) ? "checked=\"checked\"" : ""  : "checked=\"checked\"" ?> type="checkbox" name="gerarFiltro" value="1"> Gerar Filtro <br />
                <input <?= $loadBanco ? isset($arrCampos["gerarAjaxList"]) ? "checked=\"checked\"" : ""  : "" ?> type="checkbox" name="gerarAjaxList" value="1"> Gerar Ajax List <br />
                <br>
                <br>
                <input type="checkbox" checked="checked" name="overwriteDAO" value="1"> Sobrescrever DAO <br />
                <input type="checkbox" checked="checked" name="overwriteEXT" value="1"> Sobrescrever EXTDAO <br />
                <input type="checkbox" checked="checked" name="overwriteForm" value="1"> Sobrescrever Formul�rio <br />
                <input type="checkbox" checked="checked" name="overwriteList" value="1"> Sobrescrever List <br />
                <input type="checkbox" checked="checked" name="overwriteFiltro" value="1"> Sobrescrever Filtro <br />
                <input type="checkbox" checked="checked" name="overwriteAjaxList" value="1"> Sobrescrever Ajax List <br />
                <br>
                <br>

                <input class="botoes_form" type="submit" name="s1" value="Gerar Arquivos & Salvar Dados">
                <input type="hidden" name="f" value="formshowed">

            </form>

        </body>

        <script language="javascript">

            automatico("<?= $_GET["tabela"] ?>", <?= $_GET["condicaoChave"] ?>, <?= $_GET["condicaoUnderline"] ?>);

        </script>

        </html>

    <? } ?>

    <?
} elseif ($_REQUEST["todas"] != "") {

    echo "<html>
             <head>
                  <link type=\"text/css\" rel=\"stylesheet\" href=\"padrao.css\" />
              <title>GERADOR DE PROJETOS OMEGA SOFTWARE - TODAS AS TABELAS</title>
             </head>
    ";

    $projeto = $_POST["projeto"];

    $database = new Database();

	$qValoresCampos = "SELECT valores_campos_texto FROM configs_salvas WHERE projetos_id_INT=$projeto";
	
    $database->query($qValoresCampos);
//echo "entruo: qValoresCampos";

    $arrModificacoes = $_POST;

    while ($row = mysqli_fetch_array($database->result)) {

        $arrCampos = unserialize(html_entity_decode($row[0]));

        $_POST = $arrCampos;
		$_POST["projeto"] = $projeto;
		
        $tabela = $_POST["tablename"];
        $class = $_POST["classname"];
        $key = $_POST["keyname"];
        $label = $_POST["labelname"];
        $ext = $_POST["extname"];
        $numeroRegs = $_POST["numeroCadastros"];

        if ($arrModificacoes["gerarDAO"] == "1")
            gerarDAO($tabela, $class, $key, $label, $ext, "1");

        if ($arrModificacoes["gerarEXT"] == "1")
            gerarEXT($tabela, $class, $key, $label, $ext, "1");

        if ($arrModificacoes["gerarForm"] == "1"){
         
            if(is_array($_POST["tabelasrelacionamento"])){
                
                gerarFormMultiplo($tabela, $class, $key, $label, $ext, "1", $numeroRegs);
                
            }
            else{
            
                gerarForm($tabela, $class, $key, $label, $ext, "1", $numeroRegs);

            }            
            
        }
            
        if ($arrModificacoes["gerarFiltro"] == "1")
            gerarFiltro($tabela, $class, $key, $label, $ext, "1", $_POST["camposfiltro"]);

        if ($arrModificacoes["gerarList"] == "1")
            gerarList($tabela, $class, $key, $label, $ext, "1", $_POST["camposlista"], $_POST["acoeslista"], $_POST["camposfiltro"]);


        if ($arrModificacoes["gerarAjaxList"] == "1") {

            gerarAjaxForm($tabela, $class, $key, $label, $ext, "1", $numeroRegs);

            gerarAjaxPage($tabela, $class, $key, $label, $ext, "1", $numeroRegs);
        }

        echo "<br /><br />";
    }
} else {

    $projeto = $_REQUEST["projeto"];
    $tabela = $_REQUEST["tablename"];
    $class = $_REQUEST["classname"];
    $key = $_REQUEST["keyname"];
    $label = $_REQUEST["labelname"];
    $ext = $_REQUEST["extname"];
    $numeroRegs = $_REQUEST["numeroCadastros"];

    $todosCampos = htmlentities(serialize($_POST));

    $database = new Database();
    $database->query("SELECT id FROM configs_salvas WHERE projetos_id_INT=$projeto AND tabela_nome='$tabela'");

    if ($database->rows > 0) {

        $idConfig = Database::mysqli_result($database->result, 0, 0);

        $database->query("UPDATE configs_salvas SET valores_campos_texto='$todosCampos' WHERE id=$idConfig");
    } else {

        $database->query("INSERT INTO configs_salvas VALUES (null, $projeto, '$tabela', '$todosCampos')");
    }

    echo "<html>
             <head>
                      <link type=\"text/css\" rel=\"stylesheet\" href=\"padrao.css\" />
                      <title>GERADOR DE PROJETOS OMEGA SOFTWARE</title>
             </head>
	";

    $sobrescreverDAO = $_POST["overwriteDAO"];
    $sobrescreverEXT = $_POST["overwriteEXT"];
    $sobrescreverForm = $_POST["overwriteForm"];
    $sobrescreverFiltro = $_POST["overwriteFiltro"];
    $sobrescreverAjaxList = $_POST["overwriteAjaxList"];

    if ($_POST["gerarDAO"] == "1")
        gerarDAO($tabela, $class, $key, $label, $ext, $sobrescreverDAO);

    if ($_POST["gerarEXT"] == "1")
        gerarEXT($tabela, $class, $key, $label, $ext, $sobrescreverEXT);

    if ($_POST["gerarForm"] == "1"){
     
        if(is_array($_POST["tabelasrelacionamento"])){
                
            gerarFormMultiplo($tabela, $class, $key, $label, $ext, $sobrescreverForm, $numeroRegs);

        }
        else{

            gerarForm($tabela, $class, $key, $label, $ext, $sobrescreverForm, $numeroRegs);

        } 
      
    }
        
    if ($_POST["gerarAjaxList"] == "1") {

        gerarAjaxPage($tabela, $class, $key, $label, $ext, $sobrescreverAjaxList, $numeroRegs);
        gerarAjaxForm($tabela, $class, $key, $label, $ext, $sobrescreverAjaxList, $numeroRegs);
    }

    if ($_POST["gerarFiltro"] == "1")
        gerarFiltro($tabela, $class, $key, $label, $ext, $sobrescreverFiltro, $_POST["camposfiltro"]);

    if ($_POST["gerarList"] == "1")
        gerarList($tabela, $class, $key, $label, $ext, $sobrescreverFiltro, $_POST["camposlista"], $_POST["acoeslista"], $_POST["camposfiltro"]);
    ?>

    <center><input class="botoes_form" type="button" onclick="javascript:history.back()" value="Voltar"></center>

<? } ?>

