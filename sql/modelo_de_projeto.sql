/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50524
Source Host           : localhost:3306
Source Database       : modelo_de_projeto

Target Server Type    : MYSQL
Target Server Version : 50524
File Encoding         : 65001

Date: 2017-05-11 19:06:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `acesso`
-- ----------------------------
DROP TABLE IF EXISTS `acesso`;
CREATE TABLE `acesso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) NOT NULL,
  `data_login_DATETIME` datetime NOT NULL,
  `data_logout_DATETIME` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1169 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of acesso
-- ----------------------------
INSERT INTO `acesso` VALUES ('1', '1', '2010-05-08 16:55:41', null);
INSERT INTO `acesso` VALUES ('2', '1', '2010-05-08 17:22:13', null);
INSERT INTO `acesso` VALUES ('3', '1', '2010-05-09 10:44:53', null);
INSERT INTO `acesso` VALUES ('4', '1', '2010-05-09 12:09:21', null);
INSERT INTO `acesso` VALUES ('5', '2', '2010-05-09 12:42:04', null);
INSERT INTO `acesso` VALUES ('6', '3', '2010-05-09 18:02:50', null);
INSERT INTO `acesso` VALUES ('7', '1', '2010-05-09 18:05:16', null);
INSERT INTO `acesso` VALUES ('8', '1', '2010-05-09 18:08:20', null);
INSERT INTO `acesso` VALUES ('9', '3', '2010-05-09 18:12:07', null);
INSERT INTO `acesso` VALUES ('10', '3', '2010-05-09 22:40:00', null);
INSERT INTO `acesso` VALUES ('11', '2', '2010-05-10 04:59:23', null);
INSERT INTO `acesso` VALUES ('12', '3', '2010-05-10 07:22:43', null);
INSERT INTO `acesso` VALUES ('13', '3', '2010-05-10 09:48:34', null);
INSERT INTO `acesso` VALUES ('14', '3', '2010-05-11 15:04:47', null);
INSERT INTO `acesso` VALUES ('15', '3', '2010-05-12 13:45:56', null);
INSERT INTO `acesso` VALUES ('16', '2', '2010-05-12 14:21:59', null);
INSERT INTO `acesso` VALUES ('17', '2', '2010-05-12 16:10:28', null);
INSERT INTO `acesso` VALUES ('18', '2', '2010-05-13 11:51:39', null);
INSERT INTO `acesso` VALUES ('19', '1', '2010-05-17 23:57:11', null);
INSERT INTO `acesso` VALUES ('20', '1', '2010-05-18 00:14:02', null);
INSERT INTO `acesso` VALUES ('21', '1', '2010-05-18 06:18:19', null);
INSERT INTO `acesso` VALUES ('22', '2', '2010-05-18 09:55:56', null);
INSERT INTO `acesso` VALUES ('23', '2', '2010-05-18 12:04:57', null);
INSERT INTO `acesso` VALUES ('24', '2', '2010-05-21 10:41:21', null);
INSERT INTO `acesso` VALUES ('25', '2', '2010-05-21 14:08:14', null);
INSERT INTO `acesso` VALUES ('26', '2', '2010-05-21 17:54:42', null);
INSERT INTO `acesso` VALUES ('27', '2', '2010-05-24 07:54:40', null);
INSERT INTO `acesso` VALUES ('28', '2', '2010-05-24 10:02:10', null);
INSERT INTO `acesso` VALUES ('29', '3', '2010-05-24 15:45:18', null);
INSERT INTO `acesso` VALUES ('30', '2', '2010-05-25 04:24:33', null);
INSERT INTO `acesso` VALUES ('31', '2', '2010-05-25 11:17:06', null);
INSERT INTO `acesso` VALUES ('32', '2', '2010-05-25 15:57:08', null);
INSERT INTO `acesso` VALUES ('33', '2', '2010-05-25 19:05:28', null);
INSERT INTO `acesso` VALUES ('34', '2', '2010-05-25 19:59:55', null);
INSERT INTO `acesso` VALUES ('35', '2', '2010-05-26 10:08:22', null);
INSERT INTO `acesso` VALUES ('36', '1', '2010-05-26 12:19:47', null);
INSERT INTO `acesso` VALUES ('37', '2', '2010-05-26 14:57:53', null);
INSERT INTO `acesso` VALUES ('38', '1', '2010-05-26 16:42:06', null);
INSERT INTO `acesso` VALUES ('39', '2', '2010-05-26 17:12:32', null);
INSERT INTO `acesso` VALUES ('40', '1', '2010-05-26 21:28:35', null);
INSERT INTO `acesso` VALUES ('41', '2', '2010-05-26 23:26:00', null);
INSERT INTO `acesso` VALUES ('42', '2', '2010-05-27 10:07:30', null);
INSERT INTO `acesso` VALUES ('43', '2', '2010-05-27 10:48:44', null);
INSERT INTO `acesso` VALUES ('44', '2', '2010-05-27 12:22:05', null);
INSERT INTO `acesso` VALUES ('45', '2', '2010-05-31 06:32:28', null);
INSERT INTO `acesso` VALUES ('46', '2', '2010-05-31 08:57:02', null);
INSERT INTO `acesso` VALUES ('47', '2', '2010-05-31 15:11:17', null);
INSERT INTO `acesso` VALUES ('48', '5', '2010-05-31 16:11:27', null);
INSERT INTO `acesso` VALUES ('49', '2', '2010-06-01 11:42:35', null);
INSERT INTO `acesso` VALUES ('50', '2', '2010-06-01 14:10:25', null);
INSERT INTO `acesso` VALUES ('51', '2', '2010-06-01 14:52:41', null);
INSERT INTO `acesso` VALUES ('52', '2', '2010-06-01 16:33:03', null);
INSERT INTO `acesso` VALUES ('53', '2', '2010-06-01 17:53:53', null);
INSERT INTO `acesso` VALUES ('54', '2', '2010-06-01 19:51:24', null);
INSERT INTO `acesso` VALUES ('55', '2', '2010-06-02 06:26:23', null);
INSERT INTO `acesso` VALUES ('56', '1', '2010-06-09 12:20:19', null);
INSERT INTO `acesso` VALUES ('57', '1', '2010-06-11 08:15:20', null);
INSERT INTO `acesso` VALUES ('58', '1', '2010-06-12 09:12:25', null);
INSERT INTO `acesso` VALUES ('59', '1', '2010-06-14 10:03:25', null);
INSERT INTO `acesso` VALUES ('60', '1', '2010-06-16 10:46:56', null);
INSERT INTO `acesso` VALUES ('61', '1', '2010-06-16 12:36:43', null);
INSERT INTO `acesso` VALUES ('62', '2', '2010-06-16 13:52:16', null);
INSERT INTO `acesso` VALUES ('63', '1', '2010-06-18 06:52:21', null);
INSERT INTO `acesso` VALUES ('64', '1', '2010-06-19 08:47:08', null);
INSERT INTO `acesso` VALUES ('65', '1', '2010-06-19 09:57:52', null);
INSERT INTO `acesso` VALUES ('66', '1', '2010-06-21 09:32:19', null);
INSERT INTO `acesso` VALUES ('67', '2', '2010-06-21 11:25:11', null);
INSERT INTO `acesso` VALUES ('68', '1', '2010-06-21 14:50:42', null);
INSERT INTO `acesso` VALUES ('69', '2', '2010-06-21 16:56:10', null);
INSERT INTO `acesso` VALUES ('70', '1', '2010-06-22 09:11:25', null);
INSERT INTO `acesso` VALUES ('71', '2', '2010-06-22 10:20:12', null);
INSERT INTO `acesso` VALUES ('72', '2', '2010-06-22 12:10:55', null);
INSERT INTO `acesso` VALUES ('73', '1', '2010-06-22 22:04:22', null);
INSERT INTO `acesso` VALUES ('74', '1', '2010-06-23 07:31:26', null);
INSERT INTO `acesso` VALUES ('75', '1', '2010-06-24 11:20:48', null);
INSERT INTO `acesso` VALUES ('76', '1', '2010-06-24 15:28:20', null);
INSERT INTO `acesso` VALUES ('77', '1', '2010-06-24 17:53:13', null);
INSERT INTO `acesso` VALUES ('78', '1', '2010-06-26 08:12:05', null);
INSERT INTO `acesso` VALUES ('79', '1', '2010-06-26 10:41:40', null);
INSERT INTO `acesso` VALUES ('80', '1', '2010-06-26 12:19:55', null);
INSERT INTO `acesso` VALUES ('81', '1', '2010-06-26 13:37:34', null);
INSERT INTO `acesso` VALUES ('82', '1', '2010-06-28 10:05:09', null);
INSERT INTO `acesso` VALUES ('83', '1', '2010-07-05 09:17:31', null);
INSERT INTO `acesso` VALUES ('84', '1', '2010-07-07 09:03:28', null);
INSERT INTO `acesso` VALUES ('85', '1', '2010-07-07 14:07:37', null);
INSERT INTO `acesso` VALUES ('86', '1', '2010-07-08 05:34:45', null);
INSERT INTO `acesso` VALUES ('87', '1', '2010-07-08 08:01:25', null);
INSERT INTO `acesso` VALUES ('88', '3', '2010-07-08 12:04:03', null);
INSERT INTO `acesso` VALUES ('89', '1', '2010-07-08 16:39:38', null);
INSERT INTO `acesso` VALUES ('90', '1', '2010-07-08 16:42:00', null);
INSERT INTO `acesso` VALUES ('91', '1', '2010-07-08 16:42:22', null);
INSERT INTO `acesso` VALUES ('92', '1', '2010-07-08 16:45:33', null);
INSERT INTO `acesso` VALUES ('93', '1', '2010-07-08 16:49:11', null);
INSERT INTO `acesso` VALUES ('94', '1', '2010-07-09 08:38:09', null);
INSERT INTO `acesso` VALUES ('95', '1', '2010-07-09 09:00:43', null);
INSERT INTO `acesso` VALUES ('96', '1', '2010-07-09 09:10:01', null);
INSERT INTO `acesso` VALUES ('97', '1', '2010-07-09 09:13:54', null);
INSERT INTO `acesso` VALUES ('98', '1', '2010-07-09 09:21:22', null);
INSERT INTO `acesso` VALUES ('99', '1', '2010-07-09 09:28:51', null);
INSERT INTO `acesso` VALUES ('100', '1', '2010-07-09 09:29:36', null);
INSERT INTO `acesso` VALUES ('101', '1', '2010-07-09 09:54:27', null);
INSERT INTO `acesso` VALUES ('102', '1', '2010-07-09 12:25:42', null);
INSERT INTO `acesso` VALUES ('103', '1', '2010-07-09 12:33:06', null);
INSERT INTO `acesso` VALUES ('104', '1', '2010-07-09 12:57:24', null);
INSERT INTO `acesso` VALUES ('105', '1', '2010-07-09 13:54:02', null);
INSERT INTO `acesso` VALUES ('106', '1', '2010-07-09 13:54:07', null);
INSERT INTO `acesso` VALUES ('107', '1', '2010-07-09 13:54:23', null);
INSERT INTO `acesso` VALUES ('108', '1', '2010-07-09 13:56:48', null);
INSERT INTO `acesso` VALUES ('109', '2', '2010-07-09 14:26:23', null);
INSERT INTO `acesso` VALUES ('110', '2', '2010-07-09 14:31:24', null);
INSERT INTO `acesso` VALUES ('111', '1', '2010-07-09 14:32:15', null);
INSERT INTO `acesso` VALUES ('112', '1', '2010-07-10 16:23:35', null);
INSERT INTO `acesso` VALUES ('113', '1', '2010-07-10 18:39:22', null);
INSERT INTO `acesso` VALUES ('114', '1', '2010-07-11 15:38:51', null);
INSERT INTO `acesso` VALUES ('115', '1', '2010-07-12 06:46:07', null);
INSERT INTO `acesso` VALUES ('116', '1', '2010-07-12 09:33:24', null);
INSERT INTO `acesso` VALUES ('117', '2', '2010-07-12 09:49:12', null);
INSERT INTO `acesso` VALUES ('118', '2', '2010-07-12 10:17:09', null);
INSERT INTO `acesso` VALUES ('119', '1', '2010-07-12 13:42:26', null);
INSERT INTO `acesso` VALUES ('120', '1', '2010-07-13 07:59:08', null);
INSERT INTO `acesso` VALUES ('121', '1', '2010-07-13 11:45:23', null);
INSERT INTO `acesso` VALUES ('122', '1', '2010-07-13 11:46:05', null);
INSERT INTO `acesso` VALUES ('123', '2', '2010-07-13 11:46:56', null);
INSERT INTO `acesso` VALUES ('124', '1', '2010-07-13 13:38:41', null);
INSERT INTO `acesso` VALUES ('125', '2', '2010-07-13 17:04:41', null);
INSERT INTO `acesso` VALUES ('126', '2', '2010-07-13 17:25:36', null);
INSERT INTO `acesso` VALUES ('127', '1', '2010-07-14 08:19:21', null);
INSERT INTO `acesso` VALUES ('128', '1', '2010-07-14 09:43:09', null);
INSERT INTO `acesso` VALUES ('129', '2', '2010-07-14 09:45:57', null);
INSERT INTO `acesso` VALUES ('130', '1', '2010-07-14 10:18:42', null);
INSERT INTO `acesso` VALUES ('131', '2', '2010-07-14 17:25:43', null);
INSERT INTO `acesso` VALUES ('132', '1', '2010-07-15 08:01:29', null);
INSERT INTO `acesso` VALUES ('133', '1', '2010-07-15 09:24:05', null);
INSERT INTO `acesso` VALUES ('134', '2', '2010-07-15 09:40:42', null);
INSERT INTO `acesso` VALUES ('135', '1', '2010-07-15 15:58:28', null);
INSERT INTO `acesso` VALUES ('136', '1', '2010-07-17 10:36:53', null);
INSERT INTO `acesso` VALUES ('137', '1', '2010-07-17 11:25:35', null);
INSERT INTO `acesso` VALUES ('138', '1', '2010-07-17 14:11:04', null);
INSERT INTO `acesso` VALUES ('139', '1', '2010-07-17 14:57:28', null);
INSERT INTO `acesso` VALUES ('140', '1', '2010-07-18 10:39:47', null);
INSERT INTO `acesso` VALUES ('141', '1', '2010-07-18 22:25:01', null);
INSERT INTO `acesso` VALUES ('142', '1', '2010-07-20 21:15:21', null);
INSERT INTO `acesso` VALUES ('143', '2', '2010-07-21 17:57:49', null);
INSERT INTO `acesso` VALUES ('144', '2', '2010-07-26 19:48:29', null);
INSERT INTO `acesso` VALUES ('145', '2', '2010-08-02 16:42:12', null);
INSERT INTO `acesso` VALUES ('146', '2', '2010-08-02 17:09:50', null);
INSERT INTO `acesso` VALUES ('147', '2', '2010-08-02 18:23:51', null);
INSERT INTO `acesso` VALUES ('148', '2', '2010-08-02 19:40:41', null);
INSERT INTO `acesso` VALUES ('149', '2', '2010-08-02 20:58:21', null);
INSERT INTO `acesso` VALUES ('150', '1', '2010-08-08 18:32:45', null);
INSERT INTO `acesso` VALUES ('151', '5', '2010-08-09 14:52:18', null);
INSERT INTO `acesso` VALUES ('152', '5', '2010-08-09 15:03:21', null);
INSERT INTO `acesso` VALUES ('153', '1', '2010-08-10 09:41:34', null);
INSERT INTO `acesso` VALUES ('154', '1', '2010-08-10 11:10:07', null);
INSERT INTO `acesso` VALUES ('155', '5', '2010-08-10 11:10:53', null);
INSERT INTO `acesso` VALUES ('156', '1', '2010-08-10 11:13:32', null);
INSERT INTO `acesso` VALUES ('157', '5', '2010-08-10 11:25:25', null);
INSERT INTO `acesso` VALUES ('158', '1', '2010-08-10 11:29:49', null);
INSERT INTO `acesso` VALUES ('159', '1', '2010-08-10 13:31:52', null);
INSERT INTO `acesso` VALUES ('160', '1', '2010-08-10 13:34:50', null);
INSERT INTO `acesso` VALUES ('161', '1', '2010-08-10 13:36:08', null);
INSERT INTO `acesso` VALUES ('162', '1', '2010-08-10 13:37:07', null);
INSERT INTO `acesso` VALUES ('163', '1', '2010-08-10 13:37:17', null);
INSERT INTO `acesso` VALUES ('164', '1', '2010-08-10 13:37:24', null);
INSERT INTO `acesso` VALUES ('165', '1', '2010-08-10 13:37:47', null);
INSERT INTO `acesso` VALUES ('166', '1', '2010-08-10 13:41:53', null);
INSERT INTO `acesso` VALUES ('167', '1', '2010-08-10 13:43:46', null);
INSERT INTO `acesso` VALUES ('168', '1', '2010-08-10 18:34:46', null);
INSERT INTO `acesso` VALUES ('169', '1', '2010-08-10 19:28:17', null);
INSERT INTO `acesso` VALUES ('170', '1', '2010-08-11 15:39:14', null);
INSERT INTO `acesso` VALUES ('171', '1', '2010-08-11 15:39:49', null);
INSERT INTO `acesso` VALUES ('172', '1', '2010-08-11 15:41:30', null);
INSERT INTO `acesso` VALUES ('173', '1', '2010-08-11 20:50:09', null);
INSERT INTO `acesso` VALUES ('174', '1', '2010-08-16 15:33:32', null);
INSERT INTO `acesso` VALUES ('175', '1', '2010-08-17 08:11:45', null);
INSERT INTO `acesso` VALUES ('176', '1', '2010-08-18 05:17:05', null);
INSERT INTO `acesso` VALUES ('177', '5', '2010-08-18 05:38:56', null);
INSERT INTO `acesso` VALUES ('178', '1', '2010-08-18 12:01:05', null);
INSERT INTO `acesso` VALUES ('179', '5', '2010-08-18 12:03:13', null);
INSERT INTO `acesso` VALUES ('180', '5', '2010-08-18 12:11:10', null);
INSERT INTO `acesso` VALUES ('181', '1', '2010-08-18 15:51:16', null);
INSERT INTO `acesso` VALUES ('182', '1', '2010-08-18 18:06:26', null);
INSERT INTO `acesso` VALUES ('183', '1', '2010-08-18 20:00:05', null);
INSERT INTO `acesso` VALUES ('184', '2', '2010-08-20 07:18:07', null);
INSERT INTO `acesso` VALUES ('185', '3', '2010-08-20 14:41:32', null);
INSERT INTO `acesso` VALUES ('186', '2', '2010-08-20 15:09:00', null);
INSERT INTO `acesso` VALUES ('187', '2', '2010-08-20 18:36:48', null);
INSERT INTO `acesso` VALUES ('188', '2', '2010-08-22 21:33:56', null);
INSERT INTO `acesso` VALUES ('189', '2', '2010-08-23 03:50:59', null);
INSERT INTO `acesso` VALUES ('190', '2', '2010-08-23 08:15:20', null);
INSERT INTO `acesso` VALUES ('191', '2', '2010-08-23 17:24:05', null);
INSERT INTO `acesso` VALUES ('192', '2', '2010-08-23 21:24:14', null);
INSERT INTO `acesso` VALUES ('193', '2', '2010-08-24 08:24:23', null);
INSERT INTO `acesso` VALUES ('194', '2', '2010-08-24 09:26:55', null);
INSERT INTO `acesso` VALUES ('195', '2', '2010-08-24 12:03:47', null);
INSERT INTO `acesso` VALUES ('196', '2', '2010-08-24 17:02:30', null);
INSERT INTO `acesso` VALUES ('197', '1', '2010-08-26 11:09:42', null);
INSERT INTO `acesso` VALUES ('223', '5', '2010-08-26 11:57:57', null);
INSERT INTO `acesso` VALUES ('224', '5', '2010-08-26 11:58:57', null);
INSERT INTO `acesso` VALUES ('225', '1', '2010-09-01 13:06:42', null);
INSERT INTO `acesso` VALUES ('226', '1', '2010-09-01 13:37:42', null);
INSERT INTO `acesso` VALUES ('227', '3', '2010-09-01 15:02:20', null);
INSERT INTO `acesso` VALUES ('228', '1', '2010-09-01 20:35:10', null);
INSERT INTO `acesso` VALUES ('229', '9', '2010-09-01 20:36:25', null);
INSERT INTO `acesso` VALUES ('230', '1', '2010-09-01 20:37:24', null);
INSERT INTO `acesso` VALUES ('231', '1', '2010-09-01 21:27:58', null);
INSERT INTO `acesso` VALUES ('232', '1', '2010-09-02 08:15:15', null);
INSERT INTO `acesso` VALUES ('233', '1', '2010-09-02 10:06:08', null);
INSERT INTO `acesso` VALUES ('234', '5', '2010-09-02 10:51:01', null);
INSERT INTO `acesso` VALUES ('235', '1', '2010-09-02 10:51:26', null);
INSERT INTO `acesso` VALUES ('236', '5', '2010-09-02 10:52:15', null);
INSERT INTO `acesso` VALUES ('237', '1', '2010-09-02 11:19:46', null);
INSERT INTO `acesso` VALUES ('238', '1', '2010-09-02 18:23:52', null);
INSERT INTO `acesso` VALUES ('239', '1', '2010-09-02 18:49:04', null);
INSERT INTO `acesso` VALUES ('240', '1', '2010-09-02 18:55:27', null);
INSERT INTO `acesso` VALUES ('241', '10', '2010-09-03 15:19:46', null);
INSERT INTO `acesso` VALUES ('242', '1', '2010-09-16 12:57:02', null);
INSERT INTO `acesso` VALUES ('243', '5', '2010-09-16 13:18:39', null);
INSERT INTO `acesso` VALUES ('244', '1', '2010-09-20 20:36:15', null);
INSERT INTO `acesso` VALUES ('245', '10', '2010-09-22 08:24:11', null);
INSERT INTO `acesso` VALUES ('246', '10', '2010-09-22 10:57:08', null);
INSERT INTO `acesso` VALUES ('247', '10', '2010-09-22 14:53:44', null);
INSERT INTO `acesso` VALUES ('248', '3', '2010-09-22 14:55:29', null);
INSERT INTO `acesso` VALUES ('249', '3', '2010-09-22 15:35:57', null);
INSERT INTO `acesso` VALUES ('250', '10', '2010-09-22 15:40:43', null);
INSERT INTO `acesso` VALUES ('251', '9', '2010-09-22 15:41:16', null);
INSERT INTO `acesso` VALUES ('252', '9', '2010-09-22 15:44:21', null);
INSERT INTO `acesso` VALUES ('253', '9', '2010-09-22 15:50:34', null);
INSERT INTO `acesso` VALUES ('254', '1', '2010-09-22 16:05:16', null);
INSERT INTO `acesso` VALUES ('255', '1', '2010-09-22 17:25:00', null);
INSERT INTO `acesso` VALUES ('256', '10', '2010-09-22 17:27:28', null);
INSERT INTO `acesso` VALUES ('257', '1', '2010-09-22 17:27:52', null);
INSERT INTO `acesso` VALUES ('258', '10', '2010-09-22 17:28:51', null);
INSERT INTO `acesso` VALUES ('259', '1', '2010-09-22 17:38:04', null);
INSERT INTO `acesso` VALUES ('260', '10', '2010-09-22 17:39:57', null);
INSERT INTO `acesso` VALUES ('261', '10', '2010-09-22 17:42:36', null);
INSERT INTO `acesso` VALUES ('262', '10', '2010-09-22 22:03:38', null);
INSERT INTO `acesso` VALUES ('263', '10', '2010-09-23 10:15:30', null);
INSERT INTO `acesso` VALUES ('264', '10', '2010-09-23 10:37:54', null);
INSERT INTO `acesso` VALUES ('265', '1', '2010-09-23 13:00:59', null);
INSERT INTO `acesso` VALUES ('266', '9', '2010-09-23 13:23:21', null);
INSERT INTO `acesso` VALUES ('267', '5', '2010-09-27 15:07:23', null);
INSERT INTO `acesso` VALUES ('268', '5', '2010-09-27 15:11:37', null);
INSERT INTO `acesso` VALUES ('269', '1', '2010-09-27 15:47:04', null);
INSERT INTO `acesso` VALUES ('270', '5', '2010-09-27 15:48:23', null);
INSERT INTO `acesso` VALUES ('271', '5', '2010-09-27 16:44:08', null);
INSERT INTO `acesso` VALUES ('272', '1', '2010-09-28 00:02:42', null);
INSERT INTO `acesso` VALUES ('273', '3', '2010-09-28 17:40:51', null);
INSERT INTO `acesso` VALUES ('274', '1', '2010-10-01 07:19:09', null);
INSERT INTO `acesso` VALUES ('275', '1', '2010-10-01 07:24:07', null);
INSERT INTO `acesso` VALUES ('276', '1', '2010-10-01 07:35:43', null);
INSERT INTO `acesso` VALUES ('277', '1', '2010-10-01 10:14:53', null);
INSERT INTO `acesso` VALUES ('278', '10', '2010-10-01 11:07:58', null);
INSERT INTO `acesso` VALUES ('279', '1', '2010-10-01 15:47:03', null);
INSERT INTO `acesso` VALUES ('280', '5', '2010-10-01 15:49:01', null);
INSERT INTO `acesso` VALUES ('281', '5', '2010-10-01 16:21:53', null);
INSERT INTO `acesso` VALUES ('282', '1', '2010-10-01 16:31:08', null);
INSERT INTO `acesso` VALUES ('283', '1', '2010-10-02 11:47:39', null);
INSERT INTO `acesso` VALUES ('284', '1', '2010-10-06 14:15:11', null);
INSERT INTO `acesso` VALUES ('285', '1', '2010-10-06 19:45:44', null);
INSERT INTO `acesso` VALUES ('286', '1', '2010-10-06 22:10:05', null);
INSERT INTO `acesso` VALUES ('287', '1', '2010-10-06 23:26:10', null);
INSERT INTO `acesso` VALUES ('288', '1', '2010-10-07 00:10:24', null);
INSERT INTO `acesso` VALUES ('289', '1', '2010-10-07 01:38:38', null);
INSERT INTO `acesso` VALUES ('290', '5', '2010-10-07 08:35:33', null);
INSERT INTO `acesso` VALUES ('291', '5', '2010-10-07 09:32:20', null);
INSERT INTO `acesso` VALUES ('292', '1', '2010-10-07 09:38:13', null);
INSERT INTO `acesso` VALUES ('293', '11', '2010-10-07 09:41:02', null);
INSERT INTO `acesso` VALUES ('294', '11', '2010-10-07 09:47:23', null);
INSERT INTO `acesso` VALUES ('295', '1', '2010-10-07 14:55:02', null);
INSERT INTO `acesso` VALUES ('296', '1', '2010-10-07 16:12:47', null);
INSERT INTO `acesso` VALUES ('297', '13', '2010-10-07 16:15:09', null);
INSERT INTO `acesso` VALUES ('298', '3', '2010-10-08 06:06:58', null);
INSERT INTO `acesso` VALUES ('299', '1', '2010-10-08 07:20:36', null);
INSERT INTO `acesso` VALUES ('300', '5', '2010-10-08 07:51:46', null);
INSERT INTO `acesso` VALUES ('301', '14', '2010-10-08 08:23:46', null);
INSERT INTO `acesso` VALUES ('302', '11', '2010-10-08 10:23:14', null);
INSERT INTO `acesso` VALUES ('303', '1', '2010-10-08 12:17:30', null);
INSERT INTO `acesso` VALUES ('304', '1', '2010-10-09 10:31:27', null);
INSERT INTO `acesso` VALUES ('305', '1', '2010-10-09 13:04:43', null);
INSERT INTO `acesso` VALUES ('306', '1', '2010-10-11 09:47:55', null);
INSERT INTO `acesso` VALUES ('307', '14', '2010-10-11 10:33:56', null);
INSERT INTO `acesso` VALUES ('308', '13', '2010-10-11 11:11:33', null);
INSERT INTO `acesso` VALUES ('309', '13', '2010-10-11 11:36:49', null);
INSERT INTO `acesso` VALUES ('310', '14', '2010-10-11 14:49:52', null);
INSERT INTO `acesso` VALUES ('311', '13', '2010-10-11 17:28:46', null);
INSERT INTO `acesso` VALUES ('312', '13', '2010-10-11 17:36:52', null);
INSERT INTO `acesso` VALUES ('313', '1', '2010-10-12 13:16:41', null);
INSERT INTO `acesso` VALUES ('314', '1', '2010-10-12 18:36:51', null);
INSERT INTO `acesso` VALUES ('315', '1', '2010-10-12 19:32:02', null);
INSERT INTO `acesso` VALUES ('316', '14', '2010-10-13 12:18:08', null);
INSERT INTO `acesso` VALUES ('317', '1', '2010-10-13 14:56:00', null);
INSERT INTO `acesso` VALUES ('318', '14', '2010-10-13 16:40:53', null);
INSERT INTO `acesso` VALUES ('319', '14', '2010-10-13 17:28:21', null);
INSERT INTO `acesso` VALUES ('320', '1', '2010-10-13 21:02:42', null);
INSERT INTO `acesso` VALUES ('321', '1', '2010-10-13 20:47:41', null);
INSERT INTO `acesso` VALUES ('322', '1', '2010-10-13 22:00:32', null);
INSERT INTO `acesso` VALUES ('323', '5', '2010-10-14 09:54:16', null);
INSERT INTO `acesso` VALUES ('324', '1', '2010-10-14 11:00:35', null);
INSERT INTO `acesso` VALUES ('325', '1', '2010-10-14 11:03:04', null);
INSERT INTO `acesso` VALUES ('326', '14', '2010-10-14 12:25:06', null);
INSERT INTO `acesso` VALUES ('327', '5', '2010-10-14 12:30:44', null);
INSERT INTO `acesso` VALUES ('328', '14', '2010-10-14 12:45:11', null);
INSERT INTO `acesso` VALUES ('329', '14', '2010-10-14 14:56:06', null);
INSERT INTO `acesso` VALUES ('330', '14', '2010-10-14 17:21:33', null);
INSERT INTO `acesso` VALUES ('331', '5', '2010-10-14 19:34:27', null);
INSERT INTO `acesso` VALUES ('332', '5', '2010-10-15 09:05:31', null);
INSERT INTO `acesso` VALUES ('333', '1', '2010-10-15 12:45:50', null);
INSERT INTO `acesso` VALUES ('334', '1', '2010-10-15 13:10:52', null);
INSERT INTO `acesso` VALUES ('335', '5', '2010-10-15 14:33:52', null);
INSERT INTO `acesso` VALUES ('336', '14', '2010-10-15 14:45:34', null);
INSERT INTO `acesso` VALUES ('337', '14', '2010-10-15 14:59:26', null);
INSERT INTO `acesso` VALUES ('338', '1', '2010-10-15 15:50:21', null);
INSERT INTO `acesso` VALUES ('339', '3', '2010-10-15 16:04:56', null);
INSERT INTO `acesso` VALUES ('340', '14', '2010-10-15 16:19:05', null);
INSERT INTO `acesso` VALUES ('341', '3', '2010-10-15 17:15:30', null);
INSERT INTO `acesso` VALUES ('342', '14', '2010-10-15 17:36:06', null);
INSERT INTO `acesso` VALUES ('343', '5', '2010-10-15 17:54:36', null);
INSERT INTO `acesso` VALUES ('344', '1', '2010-10-15 19:54:26', null);
INSERT INTO `acesso` VALUES ('345', '9', '2010-10-15 20:00:37', null);
INSERT INTO `acesso` VALUES ('346', '9', '2010-10-16 00:46:05', null);
INSERT INTO `acesso` VALUES ('347', '9', '2010-10-16 00:51:59', null);
INSERT INTO `acesso` VALUES ('348', '1', '2010-10-16 15:19:26', null);
INSERT INTO `acesso` VALUES ('349', '3', '2010-10-16 18:37:34', null);
INSERT INTO `acesso` VALUES ('350', '1', '2010-10-17 13:50:53', null);
INSERT INTO `acesso` VALUES ('351', '1', '2010-10-17 14:22:18', null);
INSERT INTO `acesso` VALUES ('352', '1', '2010-10-17 15:25:34', null);
INSERT INTO `acesso` VALUES ('353', '3', '2010-10-17 16:47:51', null);
INSERT INTO `acesso` VALUES ('354', '3', '2010-10-17 18:11:29', null);
INSERT INTO `acesso` VALUES ('355', '1', '2010-10-17 20:12:49', null);
INSERT INTO `acesso` VALUES ('356', '16', '2010-10-17 21:57:40', null);
INSERT INTO `acesso` VALUES ('357', '16', '2010-10-17 22:21:31', null);
INSERT INTO `acesso` VALUES ('358', '15', '2010-10-17 22:42:13', null);
INSERT INTO `acesso` VALUES ('359', '16', '2010-10-17 23:49:00', null);
INSERT INTO `acesso` VALUES ('360', '1', '2010-10-17 23:49:12', null);
INSERT INTO `acesso` VALUES ('361', '1', '2010-10-17 23:50:40', null);
INSERT INTO `acesso` VALUES ('362', '11', '2010-10-18 11:31:55', null);
INSERT INTO `acesso` VALUES ('363', '1', '2010-10-18 12:34:03', null);
INSERT INTO `acesso` VALUES ('364', '1', '2010-10-18 12:39:40', null);
INSERT INTO `acesso` VALUES ('365', '1', '2010-10-18 13:19:37', null);
INSERT INTO `acesso` VALUES ('366', '1', '2010-10-18 16:05:00', null);
INSERT INTO `acesso` VALUES ('367', '14', '2010-10-18 16:25:43', null);
INSERT INTO `acesso` VALUES ('368', '1', '2010-10-18 16:50:20', null);
INSERT INTO `acesso` VALUES ('369', '14', '2010-10-18 16:56:07', null);
INSERT INTO `acesso` VALUES ('370', '9', '2010-10-18 18:13:50', null);
INSERT INTO `acesso` VALUES ('371', '1', '2010-10-18 20:39:33', null);
INSERT INTO `acesso` VALUES ('372', '14', '2010-10-18 20:40:40', null);
INSERT INTO `acesso` VALUES ('373', '1', '2010-10-18 20:41:52', null);
INSERT INTO `acesso` VALUES ('374', '5', '2010-10-18 22:09:44', null);
INSERT INTO `acesso` VALUES ('375', '1', '2010-10-19 07:28:28', null);
INSERT INTO `acesso` VALUES ('376', '1', '2010-10-19 08:02:57', null);
INSERT INTO `acesso` VALUES ('377', '14', '2010-10-19 08:09:09', null);
INSERT INTO `acesso` VALUES ('378', '1', '2010-10-19 08:09:44', null);
INSERT INTO `acesso` VALUES ('379', '1', '2010-10-19 08:10:23', null);
INSERT INTO `acesso` VALUES ('380', '14', '2010-10-19 08:11:06', null);
INSERT INTO `acesso` VALUES ('381', '14', '2010-10-19 09:43:29', null);
INSERT INTO `acesso` VALUES ('382', '14', '2010-10-19 10:05:53', null);
INSERT INTO `acesso` VALUES ('383', '14', '2010-10-19 10:42:42', null);
INSERT INTO `acesso` VALUES ('384', '14', '2010-10-19 10:57:51', null);
INSERT INTO `acesso` VALUES ('385', '1', '2010-10-19 11:14:02', null);
INSERT INTO `acesso` VALUES ('386', '14', '2010-10-19 11:38:16', null);
INSERT INTO `acesso` VALUES ('387', '9', '2010-10-19 12:01:41', null);
INSERT INTO `acesso` VALUES ('388', '14', '2010-10-19 12:04:11', null);
INSERT INTO `acesso` VALUES ('389', '5', '2010-10-19 12:23:47', null);
INSERT INTO `acesso` VALUES ('390', '14', '2010-10-19 13:41:23', null);
INSERT INTO `acesso` VALUES ('391', '14', '2010-10-19 17:28:49', null);
INSERT INTO `acesso` VALUES ('392', '9', '2010-10-19 17:59:13', null);
INSERT INTO `acesso` VALUES ('393', '9', '2010-10-19 18:15:13', null);
INSERT INTO `acesso` VALUES ('394', '9', '2010-10-19 19:28:06', null);
INSERT INTO `acesso` VALUES ('395', '9', '2010-10-19 19:46:44', null);
INSERT INTO `acesso` VALUES ('396', '1', '2010-10-19 20:11:09', null);
INSERT INTO `acesso` VALUES ('397', '3', '2010-10-20 06:02:52', null);
INSERT INTO `acesso` VALUES ('398', '3', '2010-10-20 07:23:06', null);
INSERT INTO `acesso` VALUES ('399', '1', '2010-10-20 11:06:22', null);
INSERT INTO `acesso` VALUES ('400', '1', '2010-10-20 13:46:21', null);
INSERT INTO `acesso` VALUES ('401', '5', '2010-10-20 14:19:56', null);
INSERT INTO `acesso` VALUES ('402', '5', '2010-10-20 14:20:52', null);
INSERT INTO `acesso` VALUES ('403', '14', '2010-10-20 17:11:01', null);
INSERT INTO `acesso` VALUES ('404', '1', '2010-10-20 23:47:26', null);
INSERT INTO `acesso` VALUES ('405', '13', '2010-10-21 11:02:41', null);
INSERT INTO `acesso` VALUES ('406', '13', '2010-10-21 11:57:47', null);
INSERT INTO `acesso` VALUES ('407', '14', '2010-10-21 12:13:23', null);
INSERT INTO `acesso` VALUES ('408', '1', '2010-10-21 12:22:49', null);
INSERT INTO `acesso` VALUES ('409', '14', '2010-10-21 12:33:43', null);
INSERT INTO `acesso` VALUES ('410', '14', '2010-10-21 12:33:44', null);
INSERT INTO `acesso` VALUES ('411', '14', '2010-10-21 12:51:52', null);
INSERT INTO `acesso` VALUES ('412', '14', '2010-10-21 12:51:53', null);
INSERT INTO `acesso` VALUES ('413', '13', '2010-10-21 13:15:06', null);
INSERT INTO `acesso` VALUES ('414', '1', '2010-10-21 13:47:47', null);
INSERT INTO `acesso` VALUES ('415', '14', '2010-10-21 14:35:59', null);
INSERT INTO `acesso` VALUES ('416', '14', '2010-10-21 15:41:25', null);
INSERT INTO `acesso` VALUES ('417', '1', '2010-10-22 10:29:17', null);
INSERT INTO `acesso` VALUES ('418', '1', '2010-10-22 10:54:30', null);
INSERT INTO `acesso` VALUES ('419', '14', '2010-10-22 11:11:39', null);
INSERT INTO `acesso` VALUES ('420', '13', '2010-10-22 11:50:51', null);
INSERT INTO `acesso` VALUES ('421', '14', '2010-10-22 11:52:51', null);
INSERT INTO `acesso` VALUES ('422', '1', '2010-10-22 12:01:30', null);
INSERT INTO `acesso` VALUES ('423', '1', '2010-10-22 12:51:44', null);
INSERT INTO `acesso` VALUES ('424', '1', '2010-10-22 13:04:50', null);
INSERT INTO `acesso` VALUES ('425', '13', '2010-10-22 13:28:37', null);
INSERT INTO `acesso` VALUES ('426', '1', '2010-10-22 15:12:42', null);
INSERT INTO `acesso` VALUES ('427', '1', '2010-10-22 21:38:28', null);
INSERT INTO `acesso` VALUES ('428', '1', '2010-10-23 10:44:51', null);
INSERT INTO `acesso` VALUES ('429', '9', '2010-10-23 10:46:22', null);
INSERT INTO `acesso` VALUES ('430', '1', '2010-10-23 10:50:22', null);
INSERT INTO `acesso` VALUES ('431', '18', '2010-10-23 10:51:32', null);
INSERT INTO `acesso` VALUES ('432', '13', '2010-10-25 14:18:07', null);
INSERT INTO `acesso` VALUES ('433', '13', '2010-10-25 17:07:54', null);
INSERT INTO `acesso` VALUES ('434', '13', '2010-10-26 10:07:23', null);
INSERT INTO `acesso` VALUES ('435', '14', '2010-10-26 11:53:12', null);
INSERT INTO `acesso` VALUES ('436', '14', '2010-10-26 12:09:50', null);
INSERT INTO `acesso` VALUES ('437', '5', '2010-10-26 12:16:28', null);
INSERT INTO `acesso` VALUES ('438', '5', '2010-10-26 16:09:05', null);
INSERT INTO `acesso` VALUES ('439', '14', '2010-10-26 17:13:30', null);
INSERT INTO `acesso` VALUES ('440', '1', '2010-10-26 20:21:46', null);
INSERT INTO `acesso` VALUES ('441', '3', '2010-10-27 05:53:58', null);
INSERT INTO `acesso` VALUES ('442', '13', '2010-10-27 12:09:05', null);
INSERT INTO `acesso` VALUES ('443', '1', '2010-10-27 16:29:43', null);
INSERT INTO `acesso` VALUES ('444', '14', '2010-10-28 14:49:11', null);
INSERT INTO `acesso` VALUES ('445', '1', '2010-10-28 15:33:07', null);
INSERT INTO `acesso` VALUES ('446', '14', '2010-10-28 16:22:36', null);
INSERT INTO `acesso` VALUES ('447', '1', '2010-10-28 21:35:51', null);
INSERT INTO `acesso` VALUES ('448', '1', '2010-10-28 22:30:02', null);
INSERT INTO `acesso` VALUES ('449', '1', '2010-10-28 22:31:34', null);
INSERT INTO `acesso` VALUES ('450', '5', '2010-10-29 09:44:42', null);
INSERT INTO `acesso` VALUES ('451', '13', '2010-11-01 11:39:09', null);
INSERT INTO `acesso` VALUES ('452', '13', '2010-11-01 12:02:22', null);
INSERT INTO `acesso` VALUES ('453', '1', '2010-11-02 20:46:14', null);
INSERT INTO `acesso` VALUES ('454', '5', '2010-11-03 15:16:37', null);
INSERT INTO `acesso` VALUES ('455', '5', '2010-11-03 15:29:39', null);
INSERT INTO `acesso` VALUES ('456', '5', '2010-11-03 16:26:17', null);
INSERT INTO `acesso` VALUES ('457', '1', '2010-11-04 06:42:17', null);
INSERT INTO `acesso` VALUES ('458', '1', '2010-11-05 12:54:55', null);
INSERT INTO `acesso` VALUES ('459', '1', '2010-11-05 13:25:44', null);
INSERT INTO `acesso` VALUES ('460', '1', '2010-11-05 13:30:26', null);
INSERT INTO `acesso` VALUES ('461', '1', '2010-11-05 13:31:01', null);
INSERT INTO `acesso` VALUES ('462', '1', '2010-11-05 15:48:22', null);
INSERT INTO `acesso` VALUES ('463', '1', '2010-11-06 09:12:10', null);
INSERT INTO `acesso` VALUES ('464', '1', '2010-11-06 13:51:38', null);
INSERT INTO `acesso` VALUES ('465', '1', '2010-11-06 14:05:14', null);
INSERT INTO `acesso` VALUES ('466', '1', '2010-11-06 17:31:22', null);
INSERT INTO `acesso` VALUES ('467', '1', '2010-11-07 21:22:58', null);
INSERT INTO `acesso` VALUES ('468', '1', '2010-11-08 08:48:45', null);
INSERT INTO `acesso` VALUES ('469', '1', '2010-11-08 09:04:13', null);
INSERT INTO `acesso` VALUES ('470', '19', '2010-11-08 09:12:54', null);
INSERT INTO `acesso` VALUES ('471', '1', '2010-11-08 09:15:22', null);
INSERT INTO `acesso` VALUES ('472', '1', '2010-11-08 09:17:29', null);
INSERT INTO `acesso` VALUES ('473', '13', '2010-11-08 09:45:59', null);
INSERT INTO `acesso` VALUES ('474', '19', '2010-11-08 09:56:48', null);
INSERT INTO `acesso` VALUES ('475', '13', '2010-11-08 10:30:08', null);
INSERT INTO `acesso` VALUES ('476', '1', '2010-11-08 10:51:25', null);
INSERT INTO `acesso` VALUES ('477', '13', '2010-11-08 11:39:50', null);
INSERT INTO `acesso` VALUES ('478', '1', '2010-11-08 12:10:24', null);
INSERT INTO `acesso` VALUES ('479', '1', '2010-11-08 12:23:53', null);
INSERT INTO `acesso` VALUES ('480', '13', '2010-11-08 14:29:00', null);
INSERT INTO `acesso` VALUES ('481', '13', '2010-11-08 15:46:55', null);
INSERT INTO `acesso` VALUES ('482', '13', '2010-11-08 17:19:05', null);
INSERT INTO `acesso` VALUES ('483', '1', '2010-11-08 21:09:10', null);
INSERT INTO `acesso` VALUES ('484', '1', '2010-11-08 22:15:30', null);
INSERT INTO `acesso` VALUES ('485', '1', '2010-11-09 10:54:36', null);
INSERT INTO `acesso` VALUES ('486', '5', '2010-11-09 12:43:55', null);
INSERT INTO `acesso` VALUES ('487', '1', '2010-11-09 12:49:18', null);
INSERT INTO `acesso` VALUES ('488', '14', '2010-11-09 12:50:46', null);
INSERT INTO `acesso` VALUES ('489', '5', '2010-11-09 15:03:44', null);
INSERT INTO `acesso` VALUES ('490', '5', '2010-11-09 15:05:38', null);
INSERT INTO `acesso` VALUES ('491', '5', '2010-11-09 15:15:49', null);
INSERT INTO `acesso` VALUES ('492', '1', '2010-11-09 16:16:17', null);
INSERT INTO `acesso` VALUES ('493', '1', '2010-11-09 19:46:30', null);
INSERT INTO `acesso` VALUES ('494', '1', '2010-11-09 19:16:14', null);
INSERT INTO `acesso` VALUES ('495', '5', '2010-11-10 09:03:15', null);
INSERT INTO `acesso` VALUES ('496', '19', '2010-11-10 09:32:26', null);
INSERT INTO `acesso` VALUES ('497', '1', '2010-11-10 10:40:35', null);
INSERT INTO `acesso` VALUES ('498', '5', '2010-11-10 10:50:44', null);
INSERT INTO `acesso` VALUES ('499', '1', '2010-11-10 11:19:22', null);
INSERT INTO `acesso` VALUES ('500', '5', '2010-11-10 11:38:10', null);
INSERT INTO `acesso` VALUES ('501', '13', '2010-11-10 11:49:40', null);
INSERT INTO `acesso` VALUES ('502', '1', '2010-11-10 12:46:22', null);
INSERT INTO `acesso` VALUES ('503', '5', '2010-11-10 12:50:04', null);
INSERT INTO `acesso` VALUES ('504', '13', '2010-11-10 13:55:25', null);
INSERT INTO `acesso` VALUES ('505', '5', '2010-11-10 14:05:12', null);
INSERT INTO `acesso` VALUES ('506', '5', '2010-11-10 14:07:15', null);
INSERT INTO `acesso` VALUES ('507', '13', '2010-11-10 16:18:40', null);
INSERT INTO `acesso` VALUES ('508', '1', '2010-11-10 17:10:15', null);
INSERT INTO `acesso` VALUES ('509', '5', '2010-11-10 18:06:35', null);
INSERT INTO `acesso` VALUES ('510', '5', '2010-11-11 09:34:58', null);
INSERT INTO `acesso` VALUES ('511', '1', '2010-11-11 11:05:20', null);
INSERT INTO `acesso` VALUES ('512', '1', '2010-11-11 14:36:27', null);
INSERT INTO `acesso` VALUES ('513', '1', '2010-11-11 20:29:17', null);
INSERT INTO `acesso` VALUES ('514', '5', '2010-11-12 15:40:47', null);
INSERT INTO `acesso` VALUES ('515', '1', '2010-11-13 16:59:04', null);
INSERT INTO `acesso` VALUES ('516', '1', '2010-11-13 17:04:07', null);
INSERT INTO `acesso` VALUES ('517', '1', '2010-11-14 18:24:03', null);
INSERT INTO `acesso` VALUES ('518', '1', '2010-11-14 20:48:42', null);
INSERT INTO `acesso` VALUES ('519', '13', '2010-11-15 11:09:58', null);
INSERT INTO `acesso` VALUES ('520', '13', '2010-11-15 17:02:57', null);
INSERT INTO `acesso` VALUES ('521', '13', '2010-11-15 19:03:46', null);
INSERT INTO `acesso` VALUES ('522', '13', '2010-11-15 19:17:32', null);
INSERT INTO `acesso` VALUES ('523', '13', '2010-11-15 20:04:17', null);
INSERT INTO `acesso` VALUES ('524', '1', '2010-11-16 09:07:39', null);
INSERT INTO `acesso` VALUES ('525', '5', '2010-11-16 10:06:35', null);
INSERT INTO `acesso` VALUES ('526', '5', '2010-11-16 11:37:20', null);
INSERT INTO `acesso` VALUES ('527', '13', '2010-11-16 12:19:22', null);
INSERT INTO `acesso` VALUES ('528', '13', '2010-11-16 12:19:23', null);
INSERT INTO `acesso` VALUES ('529', '19', '2010-11-17 10:34:43', null);
INSERT INTO `acesso` VALUES ('530', '13', '2010-11-17 11:18:39', null);
INSERT INTO `acesso` VALUES ('531', '19', '2010-11-17 11:21:36', null);
INSERT INTO `acesso` VALUES ('532', '19', '2010-11-17 11:31:43', null);
INSERT INTO `acesso` VALUES ('533', '19', '2010-11-17 11:41:32', null);
INSERT INTO `acesso` VALUES ('534', '1', '2010-11-17 13:37:09', null);
INSERT INTO `acesso` VALUES ('535', '19', '2010-11-17 14:04:14', null);
INSERT INTO `acesso` VALUES ('536', '13', '2010-11-17 15:02:38', null);
INSERT INTO `acesso` VALUES ('537', '19', '2010-11-17 15:48:51', null);
INSERT INTO `acesso` VALUES ('538', '1', '2010-11-17 15:50:59', null);
INSERT INTO `acesso` VALUES ('539', '1', '2010-11-17 16:01:43', null);
INSERT INTO `acesso` VALUES ('540', '1', '2010-11-17 17:01:42', null);
INSERT INTO `acesso` VALUES ('541', '1', '2010-11-17 17:03:41', null);
INSERT INTO `acesso` VALUES ('542', '1', '2010-11-17 17:07:08', null);
INSERT INTO `acesso` VALUES ('543', '13', '2010-11-17 17:32:43', null);
INSERT INTO `acesso` VALUES ('544', '1', '2010-11-17 19:30:50', null);
INSERT INTO `acesso` VALUES ('545', '13', '2010-11-17 22:15:16', null);
INSERT INTO `acesso` VALUES ('546', '13', '2010-11-17 23:20:33', null);
INSERT INTO `acesso` VALUES ('547', '13', '2010-11-18 09:35:00', null);
INSERT INTO `acesso` VALUES ('548', '13', '2010-11-18 10:13:38', null);
INSERT INTO `acesso` VALUES ('549', '13', '2010-11-18 10:13:39', null);
INSERT INTO `acesso` VALUES ('550', '15', '2010-11-18 15:13:39', null);
INSERT INTO `acesso` VALUES ('551', '13', '2010-11-18 15:33:35', null);
INSERT INTO `acesso` VALUES ('552', '13', '2010-11-18 16:24:07', null);
INSERT INTO `acesso` VALUES ('553', '1', '2010-11-18 17:19:36', null);
INSERT INTO `acesso` VALUES ('554', '13', '2010-11-18 21:31:30', null);
INSERT INTO `acesso` VALUES ('555', '13', '2010-11-18 21:56:42', null);
INSERT INTO `acesso` VALUES ('556', '19', '2010-11-19 10:16:32', null);
INSERT INTO `acesso` VALUES ('557', '13', '2010-11-19 10:23:54', null);
INSERT INTO `acesso` VALUES ('558', '13', '2010-11-19 10:31:19', null);
INSERT INTO `acesso` VALUES ('559', '19', '2010-11-19 10:47:41', null);
INSERT INTO `acesso` VALUES ('560', '19', '2010-11-19 11:08:55', null);
INSERT INTO `acesso` VALUES ('561', '13', '2010-11-19 11:58:05', null);
INSERT INTO `acesso` VALUES ('562', '13', '2010-11-19 15:08:08', null);
INSERT INTO `acesso` VALUES ('563', '1', '2010-11-19 15:10:37', null);
INSERT INTO `acesso` VALUES ('564', '1', '2010-11-19 16:06:43', null);
INSERT INTO `acesso` VALUES ('565', '19', '2010-11-19 16:17:47', null);
INSERT INTO `acesso` VALUES ('566', '15', '2010-11-19 19:40:15', null);
INSERT INTO `acesso` VALUES ('567', '1', '2010-11-19 19:02:48', null);
INSERT INTO `acesso` VALUES ('568', '1', '2010-11-19 19:07:07', null);
INSERT INTO `acesso` VALUES ('569', '15', '2010-11-19 19:08:28', null);
INSERT INTO `acesso` VALUES ('570', '15', '2010-11-19 19:19:55', null);
INSERT INTO `acesso` VALUES ('571', '15', '2010-11-19 19:22:55', null);
INSERT INTO `acesso` VALUES ('572', '1', '2010-11-19 19:23:18', null);
INSERT INTO `acesso` VALUES ('573', '15', '2010-11-19 19:23:41', null);
INSERT INTO `acesso` VALUES ('574', '1', '2010-11-19 19:24:02', null);
INSERT INTO `acesso` VALUES ('575', '1', '2010-11-19 19:26:36', null);
INSERT INTO `acesso` VALUES ('576', '15', '2010-11-19 19:27:01', null);
INSERT INTO `acesso` VALUES ('577', '1', '2010-11-19 19:29:31', null);
INSERT INTO `acesso` VALUES ('578', '1', '2010-11-19 19:29:48', null);
INSERT INTO `acesso` VALUES ('579', '15', '2010-11-19 19:30:30', null);
INSERT INTO `acesso` VALUES ('580', '1', '2010-11-19 19:57:23', null);
INSERT INTO `acesso` VALUES ('581', '1', '2010-11-19 20:04:33', null);
INSERT INTO `acesso` VALUES ('582', '15', '2010-11-19 20:06:43', null);
INSERT INTO `acesso` VALUES ('583', '1', '2010-11-19 20:14:01', null);
INSERT INTO `acesso` VALUES ('584', '15', '2010-11-19 20:16:19', null);
INSERT INTO `acesso` VALUES ('585', '1', '2010-11-19 20:25:19', null);
INSERT INTO `acesso` VALUES ('586', '1', '2010-11-19 21:05:08', null);
INSERT INTO `acesso` VALUES ('587', '1', '2010-11-19 21:07:26', null);
INSERT INTO `acesso` VALUES ('588', '1', '2010-11-20 08:55:34', null);
INSERT INTO `acesso` VALUES ('589', '1', '2010-11-20 09:01:48', null);
INSERT INTO `acesso` VALUES ('590', '1', '2010-11-20 09:21:23', null);
INSERT INTO `acesso` VALUES ('591', '1', '2010-11-20 09:26:11', null);
INSERT INTO `acesso` VALUES ('592', '1', '2010-11-20 15:58:26', null);
INSERT INTO `acesso` VALUES ('593', '1', '2010-11-20 18:57:09', null);
INSERT INTO `acesso` VALUES ('594', '15', '2010-11-21 23:23:33', null);
INSERT INTO `acesso` VALUES ('595', '5', '2010-11-21 23:49:38', null);
INSERT INTO `acesso` VALUES ('596', '13', '2010-11-22 09:52:56', null);
INSERT INTO `acesso` VALUES ('597', '13', '2010-11-22 10:50:07', null);
INSERT INTO `acesso` VALUES ('598', '19', '2010-11-22 14:15:38', null);
INSERT INTO `acesso` VALUES ('599', '13', '2010-11-22 20:09:54', null);
INSERT INTO `acesso` VALUES ('600', '5', '2010-11-23 11:23:00', null);
INSERT INTO `acesso` VALUES ('601', '18', '2010-11-23 12:10:12', null);
INSERT INTO `acesso` VALUES ('602', '5', '2010-11-23 12:20:29', null);
INSERT INTO `acesso` VALUES ('603', '18', '2010-11-23 12:42:33', null);
INSERT INTO `acesso` VALUES ('604', '18', '2010-11-23 12:59:35', null);
INSERT INTO `acesso` VALUES ('605', '19', '2010-11-23 14:02:13', null);
INSERT INTO `acesso` VALUES ('606', '18', '2010-11-23 14:19:10', null);
INSERT INTO `acesso` VALUES ('607', '1', '2010-11-23 14:34:56', null);
INSERT INTO `acesso` VALUES ('608', '19', '2010-11-23 15:42:56', null);
INSERT INTO `acesso` VALUES ('609', '19', '2010-11-23 15:54:48', null);
INSERT INTO `acesso` VALUES ('610', '1', '2010-11-23 16:43:06', null);
INSERT INTO `acesso` VALUES ('611', '1', '2010-11-23 16:50:26', null);
INSERT INTO `acesso` VALUES ('612', '1', '2010-11-23 17:29:15', null);
INSERT INTO `acesso` VALUES ('613', '19', '2010-11-24 09:17:18', null);
INSERT INTO `acesso` VALUES ('614', '1', '2010-11-24 09:38:47', null);
INSERT INTO `acesso` VALUES ('615', '18', '2010-11-24 09:43:21', null);
INSERT INTO `acesso` VALUES ('616', '18', '2010-11-24 12:13:05', null);
INSERT INTO `acesso` VALUES ('617', '19', '2010-11-24 12:52:33', null);
INSERT INTO `acesso` VALUES ('618', '1', '2010-11-24 16:08:55', null);
INSERT INTO `acesso` VALUES ('619', '13', '2010-11-24 21:21:16', null);
INSERT INTO `acesso` VALUES ('620', '1', '2010-11-25 09:40:50', null);
INSERT INTO `acesso` VALUES ('621', '19', '2010-11-25 13:22:15', null);
INSERT INTO `acesso` VALUES ('622', '19', '2010-11-25 15:56:30', null);
INSERT INTO `acesso` VALUES ('623', '19', '2010-11-26 09:05:05', null);
INSERT INTO `acesso` VALUES ('624', '19', '2010-11-26 11:10:25', null);
INSERT INTO `acesso` VALUES ('625', '19', '2010-11-26 13:40:27', null);
INSERT INTO `acesso` VALUES ('626', '5', '2010-11-26 14:59:48', null);
INSERT INTO `acesso` VALUES ('627', '14', '2010-11-26 16:37:38', null);
INSERT INTO `acesso` VALUES ('628', '1', '2010-11-26 20:58:51', null);
INSERT INTO `acesso` VALUES ('629', '13', '2010-11-27 08:15:30', null);
INSERT INTO `acesso` VALUES ('630', '13', '2010-11-27 13:35:57', null);
INSERT INTO `acesso` VALUES ('631', '1', '2010-11-29 08:52:15', null);
INSERT INTO `acesso` VALUES ('632', '1', '2010-11-29 08:20:30', null);
INSERT INTO `acesso` VALUES ('633', '19', '2010-11-29 10:04:00', null);
INSERT INTO `acesso` VALUES ('634', '19', '2010-11-29 11:53:40', null);
INSERT INTO `acesso` VALUES ('635', '1', '2010-11-29 12:46:23', null);
INSERT INTO `acesso` VALUES ('636', '19', '2010-11-29 13:03:26', null);
INSERT INTO `acesso` VALUES ('637', '19', '2010-11-29 13:44:12', null);
INSERT INTO `acesso` VALUES ('638', '19', '2010-11-29 14:08:26', null);
INSERT INTO `acesso` VALUES ('639', '19', '2010-11-29 15:06:20', null);
INSERT INTO `acesso` VALUES ('640', '5', '2010-11-29 15:06:48', null);
INSERT INTO `acesso` VALUES ('641', '5', '2010-11-29 15:08:14', null);
INSERT INTO `acesso` VALUES ('642', '3', '2010-11-29 15:26:01', null);
INSERT INTO `acesso` VALUES ('643', '5', '2010-11-29 15:54:39', null);
INSERT INTO `acesso` VALUES ('644', '1', '2010-11-29 20:21:39', null);
INSERT INTO `acesso` VALUES ('645', '1', '2010-11-29 22:44:09', null);
INSERT INTO `acesso` VALUES ('646', '1', '2010-11-30 10:04:11', null);
INSERT INTO `acesso` VALUES ('647', '1', '2010-11-30 12:03:31', null);
INSERT INTO `acesso` VALUES ('648', '1', '2010-11-30 13:13:16', null);
INSERT INTO `acesso` VALUES ('649', '1', '2010-11-30 14:10:07', null);
INSERT INTO `acesso` VALUES ('650', '1', '2010-11-30 15:17:21', null);
INSERT INTO `acesso` VALUES ('651', '1', '2010-11-30 16:26:30', null);
INSERT INTO `acesso` VALUES ('652', '1', '2010-12-01 00:46:30', null);
INSERT INTO `acesso` VALUES ('653', '1', '2010-12-01 10:46:43', null);
INSERT INTO `acesso` VALUES ('654', '13', '2010-12-01 11:47:11', null);
INSERT INTO `acesso` VALUES ('655', '1', '2010-12-01 17:55:41', null);
INSERT INTO `acesso` VALUES ('656', '1', '2010-12-02 10:33:48', null);
INSERT INTO `acesso` VALUES ('657', '1', '2010-12-02 10:43:34', null);
INSERT INTO `acesso` VALUES ('658', '13', '2010-12-02 10:57:55', null);
INSERT INTO `acesso` VALUES ('659', '1', '2010-12-02 14:21:29', null);
INSERT INTO `acesso` VALUES ('660', '13', '2010-12-02 14:22:52', null);
INSERT INTO `acesso` VALUES ('661', '1', '2010-12-02 15:24:10', null);
INSERT INTO `acesso` VALUES ('662', '1', '2010-12-02 15:27:41', null);
INSERT INTO `acesso` VALUES ('663', '1', '2010-12-02 16:59:59', null);
INSERT INTO `acesso` VALUES ('664', '1', '2010-12-02 20:26:05', null);
INSERT INTO `acesso` VALUES ('665', '1', '2010-12-02 21:00:15', null);
INSERT INTO `acesso` VALUES ('666', '1', '2010-12-02 21:25:43', null);
INSERT INTO `acesso` VALUES ('667', '1', '2010-12-03 11:41:26', null);
INSERT INTO `acesso` VALUES ('668', '1', '2010-12-03 12:45:05', null);
INSERT INTO `acesso` VALUES ('669', '3', '2010-12-03 14:46:52', null);
INSERT INTO `acesso` VALUES ('670', '1', '2010-12-03 14:53:22', null);
INSERT INTO `acesso` VALUES ('671', '1', '2010-12-03 16:10:00', null);
INSERT INTO `acesso` VALUES ('672', '3', '2010-12-03 16:43:42', null);
INSERT INTO `acesso` VALUES ('673', '1', '2010-12-04 11:23:53', null);
INSERT INTO `acesso` VALUES ('674', '1', '2010-12-04 11:15:50', null);
INSERT INTO `acesso` VALUES ('675', '1', '2010-12-04 12:03:00', null);
INSERT INTO `acesso` VALUES ('676', '1', '2010-12-04 12:17:21', null);
INSERT INTO `acesso` VALUES ('677', '1', '2010-12-04 12:21:49', null);
INSERT INTO `acesso` VALUES ('678', '1', '2010-12-04 12:22:29', null);
INSERT INTO `acesso` VALUES ('679', '1', '2010-12-04 12:23:00', null);
INSERT INTO `acesso` VALUES ('680', '1', '2010-12-04 16:05:46', null);
INSERT INTO `acesso` VALUES ('681', '1', '2010-12-04 16:06:15', null);
INSERT INTO `acesso` VALUES ('682', '1', '2010-12-05 17:12:28', null);
INSERT INTO `acesso` VALUES ('683', '1', '2010-12-05 19:57:16', null);
INSERT INTO `acesso` VALUES ('684', '1', '2010-12-05 21:33:37', null);
INSERT INTO `acesso` VALUES ('685', '1', '2010-12-05 21:38:16', null);
INSERT INTO `acesso` VALUES ('686', '1', '2010-12-05 21:40:16', null);
INSERT INTO `acesso` VALUES ('687', '19', '2010-12-06 09:51:36', null);
INSERT INTO `acesso` VALUES ('688', '19', '2010-12-06 16:06:40', null);
INSERT INTO `acesso` VALUES ('689', '19', '2010-12-06 16:42:39', null);
INSERT INTO `acesso` VALUES ('690', '1', '2010-12-06 16:58:30', null);
INSERT INTO `acesso` VALUES ('691', '19', '2010-12-07 09:39:45', null);
INSERT INTO `acesso` VALUES ('692', '13', '2010-12-07 10:26:26', null);
INSERT INTO `acesso` VALUES ('693', '19', '2010-12-07 14:38:02', null);
INSERT INTO `acesso` VALUES ('694', '1', '2010-12-07 15:01:37', null);
INSERT INTO `acesso` VALUES ('695', '1', '2010-12-07 16:01:39', null);
INSERT INTO `acesso` VALUES ('696', '1', '2010-12-07 16:44:13', null);
INSERT INTO `acesso` VALUES ('697', '1', '2010-12-07 18:37:29', null);
INSERT INTO `acesso` VALUES ('698', '1', '2010-12-07 22:01:25', null);
INSERT INTO `acesso` VALUES ('699', '1', '2010-12-08 10:50:40', null);
INSERT INTO `acesso` VALUES ('700', '1', '2010-12-08 15:09:30', null);
INSERT INTO `acesso` VALUES ('701', '1', '2010-12-08 15:11:57', null);
INSERT INTO `acesso` VALUES ('702', '1', '2010-12-08 15:23:49', null);
INSERT INTO `acesso` VALUES ('703', '1', '2010-12-08 15:28:43', null);
INSERT INTO `acesso` VALUES ('704', '1', '2010-12-08 15:35:15', null);
INSERT INTO `acesso` VALUES ('705', '1', '2010-12-09 09:03:54', null);
INSERT INTO `acesso` VALUES ('706', '19', '2010-12-09 09:49:50', null);
INSERT INTO `acesso` VALUES ('707', '1', '2010-12-09 11:09:25', null);
INSERT INTO `acesso` VALUES ('708', '1', '2010-12-09 12:29:36', null);
INSERT INTO `acesso` VALUES ('709', '19', '2010-12-09 13:17:54', null);
INSERT INTO `acesso` VALUES ('710', '1', '2010-12-09 17:23:58', null);
INSERT INTO `acesso` VALUES ('711', '19', '2010-12-10 09:52:48', null);
INSERT INTO `acesso` VALUES ('712', '19', '2010-12-10 10:08:20', null);
INSERT INTO `acesso` VALUES ('713', '14', '2010-12-10 10:23:25', null);
INSERT INTO `acesso` VALUES ('714', '19', '2010-12-10 10:53:01', null);
INSERT INTO `acesso` VALUES ('715', '19', '2010-12-10 14:00:27', null);
INSERT INTO `acesso` VALUES ('716', '1', '2010-12-10 14:09:23', null);
INSERT INTO `acesso` VALUES ('717', '19', '2010-12-10 14:52:21', null);
INSERT INTO `acesso` VALUES ('718', '19', '2010-12-10 15:05:28', null);
INSERT INTO `acesso` VALUES ('719', '19', '2010-12-10 15:51:10', null);
INSERT INTO `acesso` VALUES ('720', '19', '2010-12-13 10:49:25', null);
INSERT INTO `acesso` VALUES ('721', '19', '2010-12-13 11:11:20', null);
INSERT INTO `acesso` VALUES ('722', '1', '2010-12-14 09:19:45', null);
INSERT INTO `acesso` VALUES ('723', '5', '2010-12-14 10:10:43', null);
INSERT INTO `acesso` VALUES ('724', '19', '2010-12-14 10:13:16', null);
INSERT INTO `acesso` VALUES ('725', '1', '2010-12-14 10:37:26', null);
INSERT INTO `acesso` VALUES ('726', '1', '2010-12-14 11:51:17', null);
INSERT INTO `acesso` VALUES ('727', '13', '2010-12-14 13:50:28', null);
INSERT INTO `acesso` VALUES ('728', '1', '2010-12-14 14:12:44', null);
INSERT INTO `acesso` VALUES ('729', '19', '2010-12-14 14:27:02', null);
INSERT INTO `acesso` VALUES ('730', '1', '2010-12-14 14:48:11', null);
INSERT INTO `acesso` VALUES ('731', '1', '2010-12-15 13:02:26', null);
INSERT INTO `acesso` VALUES ('732', '1', '2010-12-15 16:04:58', null);
INSERT INTO `acesso` VALUES ('733', '13', '2010-12-16 21:06:26', null);
INSERT INTO `acesso` VALUES ('734', '19', '2010-12-17 13:27:34', null);
INSERT INTO `acesso` VALUES ('735', '5', '2010-12-17 14:59:56', null);
INSERT INTO `acesso` VALUES ('736', '19', '2010-12-17 15:11:03', null);
INSERT INTO `acesso` VALUES ('737', '19', '2010-12-17 15:55:34', null);
INSERT INTO `acesso` VALUES ('738', '1', '2010-12-19 23:15:27', null);
INSERT INTO `acesso` VALUES ('739', '19', '2010-12-20 09:50:25', null);
INSERT INTO `acesso` VALUES ('740', '19', '2010-12-20 15:27:12', null);
INSERT INTO `acesso` VALUES ('741', '1', '2010-12-20 15:58:49', null);
INSERT INTO `acesso` VALUES ('742', '19', '2010-12-21 09:33:17', null);
INSERT INTO `acesso` VALUES ('743', '19', '2010-12-21 10:02:28', null);
INSERT INTO `acesso` VALUES ('744', '1', '2010-12-21 10:06:34', null);
INSERT INTO `acesso` VALUES ('745', '19', '2010-12-21 10:30:19', null);
INSERT INTO `acesso` VALUES ('746', '19', '2010-12-21 11:02:39', null);
INSERT INTO `acesso` VALUES ('747', '19', '2010-12-21 13:32:07', null);
INSERT INTO `acesso` VALUES ('748', '1', '2010-12-21 14:10:48', null);
INSERT INTO `acesso` VALUES ('749', '5', '2010-12-21 14:14:09', null);
INSERT INTO `acesso` VALUES ('750', '1', '2010-12-21 14:35:25', null);
INSERT INTO `acesso` VALUES ('751', '13', '2010-12-21 15:04:47', null);
INSERT INTO `acesso` VALUES ('752', '19', '2010-12-22 09:25:03', null);
INSERT INTO `acesso` VALUES ('753', '19', '2010-12-22 13:04:12', null);
INSERT INTO `acesso` VALUES ('754', '19', '2010-12-23 11:45:58', null);
INSERT INTO `acesso` VALUES ('755', '19', '2010-12-23 13:55:31', null);
INSERT INTO `acesso` VALUES ('756', '1', '2010-12-23 19:57:49', null);
INSERT INTO `acesso` VALUES ('757', '1', '2010-12-24 11:08:29', null);
INSERT INTO `acesso` VALUES ('758', '19', '2010-12-29 10:18:47', null);
INSERT INTO `acesso` VALUES ('759', '19', '2010-12-30 10:44:21', null);
INSERT INTO `acesso` VALUES ('760', '1', '2010-12-30 10:53:48', null);
INSERT INTO `acesso` VALUES ('761', '19', '2010-12-30 14:03:43', null);
INSERT INTO `acesso` VALUES ('762', '1', '2010-12-30 14:11:43', null);
INSERT INTO `acesso` VALUES ('763', '1', '2010-12-30 14:14:49', null);
INSERT INTO `acesso` VALUES ('764', '1', '2010-12-30 14:21:53', null);
INSERT INTO `acesso` VALUES ('765', '1', '2010-12-31 12:48:35', null);
INSERT INTO `acesso` VALUES ('766', '5', '2011-01-03 18:28:17', null);
INSERT INTO `acesso` VALUES ('767', '19', '2011-01-04 10:35:18', null);
INSERT INTO `acesso` VALUES ('768', '19', '2011-01-04 13:42:28', null);
INSERT INTO `acesso` VALUES ('769', '19', '2011-01-04 16:11:09', null);
INSERT INTO `acesso` VALUES ('770', '19', '2011-01-05 09:41:59', null);
INSERT INTO `acesso` VALUES ('771', '19', '2011-01-05 10:14:13', null);
INSERT INTO `acesso` VALUES ('772', '19', '2011-01-05 14:28:30', null);
INSERT INTO `acesso` VALUES ('773', '1', '2011-01-05 19:31:42', null);
INSERT INTO `acesso` VALUES ('774', '19', '2011-01-06 09:23:10', null);
INSERT INTO `acesso` VALUES ('775', '13', '2011-01-06 09:41:49', null);
INSERT INTO `acesso` VALUES ('776', '1', '2011-01-06 11:41:02', null);
INSERT INTO `acesso` VALUES ('777', '19', '2011-01-06 11:51:02', null);
INSERT INTO `acesso` VALUES ('778', '19', '2011-01-06 12:59:13', null);
INSERT INTO `acesso` VALUES ('779', '19', '2011-01-06 14:01:58', null);
INSERT INTO `acesso` VALUES ('780', '1', '2011-01-06 15:19:18', null);
INSERT INTO `acesso` VALUES ('781', '1', '2011-01-07 09:16:57', null);
INSERT INTO `acesso` VALUES ('782', '19', '2011-01-07 10:15:38', null);
INSERT INTO `acesso` VALUES ('783', '1', '2011-01-08 11:42:06', null);
INSERT INTO `acesso` VALUES ('784', '19', '2011-01-11 09:41:17', null);
INSERT INTO `acesso` VALUES ('785', '1', '2011-01-11 10:26:31', null);
INSERT INTO `acesso` VALUES ('786', '1', '2011-01-11 16:48:21', null);
INSERT INTO `acesso` VALUES ('787', '1', '2011-01-11 17:00:55', null);
INSERT INTO `acesso` VALUES ('788', '1', '2011-01-12 10:22:28', null);
INSERT INTO `acesso` VALUES ('789', '1', '2011-01-12 12:12:55', null);
INSERT INTO `acesso` VALUES ('790', '1', '2011-01-12 12:15:32', null);
INSERT INTO `acesso` VALUES ('791', '1', '2011-01-12 12:16:13', null);
INSERT INTO `acesso` VALUES ('792', '1', '2011-01-12 12:18:15', null);
INSERT INTO `acesso` VALUES ('793', '1', '2011-01-12 12:19:25', null);
INSERT INTO `acesso` VALUES ('794', '1', '2011-01-12 12:24:20', null);
INSERT INTO `acesso` VALUES ('795', '1', '2011-01-13 10:12:46', null);
INSERT INTO `acesso` VALUES ('796', '1', '2011-01-13 11:35:28', null);
INSERT INTO `acesso` VALUES ('797', '1', '2011-01-14 11:59:20', null);
INSERT INTO `acesso` VALUES ('798', '1', '2011-01-14 12:41:25', null);
INSERT INTO `acesso` VALUES ('799', '1', '2011-01-17 09:09:10', null);
INSERT INTO `acesso` VALUES ('800', '19', '2011-01-19 14:42:29', null);
INSERT INTO `acesso` VALUES ('801', '19', '2011-01-19 15:11:24', null);
INSERT INTO `acesso` VALUES ('802', '14', '2011-01-21 10:47:18', null);
INSERT INTO `acesso` VALUES ('803', '19', '2011-01-21 10:57:10', null);
INSERT INTO `acesso` VALUES ('804', '19', '2011-01-21 14:36:44', null);
INSERT INTO `acesso` VALUES ('805', '19', '2011-01-24 10:20:08', null);
INSERT INTO `acesso` VALUES ('806', '19', '2011-01-24 11:37:11', null);
INSERT INTO `acesso` VALUES ('807', '19', '2011-01-24 14:25:10', null);
INSERT INTO `acesso` VALUES ('808', '19', '2011-01-24 15:20:07', null);
INSERT INTO `acesso` VALUES ('809', '19', '2011-01-25 11:23:08', null);
INSERT INTO `acesso` VALUES ('810', '19', '2011-01-26 09:49:32', null);
INSERT INTO `acesso` VALUES ('811', '19', '2011-01-26 13:24:16', null);
INSERT INTO `acesso` VALUES ('812', '19', '2011-01-26 15:14:26', null);
INSERT INTO `acesso` VALUES ('813', '19', '2011-01-27 09:47:37', null);
INSERT INTO `acesso` VALUES ('814', '19', '2011-01-27 15:27:34', null);
INSERT INTO `acesso` VALUES ('815', '19', '2011-01-28 09:52:06', null);
INSERT INTO `acesso` VALUES ('816', '19', '2011-01-28 10:46:25', null);
INSERT INTO `acesso` VALUES ('817', '19', '2011-01-28 11:17:54', null);
INSERT INTO `acesso` VALUES ('818', '19', '2011-01-28 13:59:56', null);
INSERT INTO `acesso` VALUES ('819', '19', '2011-01-28 14:40:35', null);
INSERT INTO `acesso` VALUES ('820', '19', '2011-01-31 09:44:49', null);
INSERT INTO `acesso` VALUES ('821', '19', '2011-01-31 10:59:04', null);
INSERT INTO `acesso` VALUES ('822', '19', '2011-01-31 14:07:56', null);
INSERT INTO `acesso` VALUES ('823', '19', '2011-01-31 15:14:48', null);
INSERT INTO `acesso` VALUES ('824', '19', '2011-02-01 10:26:54', null);
INSERT INTO `acesso` VALUES ('825', '19', '2011-02-01 14:17:08', null);
INSERT INTO `acesso` VALUES ('826', '19', '2011-02-01 15:32:03', null);
INSERT INTO `acesso` VALUES ('827', '19', '2011-02-02 10:12:08', null);
INSERT INTO `acesso` VALUES ('828', '19', '2011-02-02 13:43:09', null);
INSERT INTO `acesso` VALUES ('829', '19', '2011-02-03 09:52:22', null);
INSERT INTO `acesso` VALUES ('830', '1', '2011-02-03 12:39:53', null);
INSERT INTO `acesso` VALUES ('831', '19', '2011-02-03 14:19:22', null);
INSERT INTO `acesso` VALUES ('832', '1', '2011-02-03 18:24:05', null);
INSERT INTO `acesso` VALUES ('833', '19', '2011-02-04 09:38:08', null);
INSERT INTO `acesso` VALUES ('834', '1', '2011-02-04 09:55:08', null);
INSERT INTO `acesso` VALUES ('835', '1', '2011-02-04 13:39:04', null);
INSERT INTO `acesso` VALUES ('836', '1', '2011-02-05 22:20:47', null);
INSERT INTO `acesso` VALUES ('837', '1', '2011-02-06 12:20:53', null);
INSERT INTO `acesso` VALUES ('838', '1', '2011-02-06 17:12:55', null);
INSERT INTO `acesso` VALUES ('839', '19', '2011-02-07 10:00:46', null);
INSERT INTO `acesso` VALUES ('840', '19', '2011-02-07 14:07:19', null);
INSERT INTO `acesso` VALUES ('841', '19', '2011-02-07 14:38:01', null);
INSERT INTO `acesso` VALUES ('842', '19', '2011-02-07 16:00:07', null);
INSERT INTO `acesso` VALUES ('843', '19', '2011-02-07 16:31:02', null);
INSERT INTO `acesso` VALUES ('844', '19', '2011-02-08 13:46:54', null);
INSERT INTO `acesso` VALUES ('845', '19', '2011-02-08 13:46:55', null);
INSERT INTO `acesso` VALUES ('846', '19', '2011-02-08 14:39:12', null);
INSERT INTO `acesso` VALUES ('847', '19', '2011-02-09 11:44:00', null);
INSERT INTO `acesso` VALUES ('848', '19', '2011-02-09 15:15:31', null);
INSERT INTO `acesso` VALUES ('849', '19', '2011-02-10 13:58:26', null);
INSERT INTO `acesso` VALUES ('850', '19', '2011-02-10 14:50:19', null);
INSERT INTO `acesso` VALUES ('851', '19', '2011-02-11 10:18:01', null);
INSERT INTO `acesso` VALUES ('852', '19', '2011-02-11 14:45:28', null);
INSERT INTO `acesso` VALUES ('853', '19', '2011-02-11 15:34:52', null);
INSERT INTO `acesso` VALUES ('854', '19', '2011-02-14 00:46:05', null);
INSERT INTO `acesso` VALUES ('855', '19', '2011-02-14 09:52:45', null);
INSERT INTO `acesso` VALUES ('856', '19', '2011-02-14 09:56:34', null);
INSERT INTO `acesso` VALUES ('857', '19', '2011-02-14 10:14:14', null);
INSERT INTO `acesso` VALUES ('858', '1', '2011-02-15 08:41:34', null);
INSERT INTO `acesso` VALUES ('859', '19', '2011-02-15 10:18:44', null);
INSERT INTO `acesso` VALUES ('860', '19', '2011-02-15 10:26:52', null);
INSERT INTO `acesso` VALUES ('861', '1', '2011-02-15 10:31:06', null);
INSERT INTO `acesso` VALUES ('862', '19', '2011-02-15 10:32:20', null);
INSERT INTO `acesso` VALUES ('863', '19', '2011-02-15 11:10:26', null);
INSERT INTO `acesso` VALUES ('864', '19', '2011-02-15 11:54:11', null);
INSERT INTO `acesso` VALUES ('865', '1', '2011-02-15 13:48:53', null);
INSERT INTO `acesso` VALUES ('866', '19', '2011-02-15 14:51:16', null);
INSERT INTO `acesso` VALUES ('867', '1', '2011-02-15 15:04:50', null);
INSERT INTO `acesso` VALUES ('868', '1', '2011-02-15 22:57:15', null);
INSERT INTO `acesso` VALUES ('869', '19', '2011-02-16 09:50:30', null);
INSERT INTO `acesso` VALUES ('870', '19', '2011-02-16 11:38:30', null);
INSERT INTO `acesso` VALUES ('871', '19', '2011-02-16 13:51:04', null);
INSERT INTO `acesso` VALUES ('872', '19', '2011-02-16 14:52:37', null);
INSERT INTO `acesso` VALUES ('873', '19', '2011-02-17 10:31:38', null);
INSERT INTO `acesso` VALUES ('874', '19', '2011-02-17 14:49:28', null);
INSERT INTO `acesso` VALUES ('875', '1', '2011-02-17 15:18:37', null);
INSERT INTO `acesso` VALUES ('876', '1', '2011-02-18 10:31:28', null);
INSERT INTO `acesso` VALUES ('877', '3', '2011-02-18 15:16:51', null);
INSERT INTO `acesso` VALUES ('878', '1', '2011-02-18 15:37:04', null);
INSERT INTO `acesso` VALUES ('879', '1', '2011-02-18 17:35:26', null);
INSERT INTO `acesso` VALUES ('880', '1', '2011-02-18 19:52:03', null);
INSERT INTO `acesso` VALUES ('881', '1', '2011-02-18 20:49:50', null);
INSERT INTO `acesso` VALUES ('882', '19', '2011-02-21 08:58:05', null);
INSERT INTO `acesso` VALUES ('883', '19', '2011-02-21 14:08:45', null);
INSERT INTO `acesso` VALUES ('884', '19', '2011-02-21 14:23:50', null);
INSERT INTO `acesso` VALUES ('885', '19', '2011-02-21 14:44:41', null);
INSERT INTO `acesso` VALUES ('886', '19', '2011-02-21 15:41:50', null);
INSERT INTO `acesso` VALUES ('887', '19', '2011-02-21 15:49:29', null);
INSERT INTO `acesso` VALUES ('888', '1', '2011-02-21 16:47:01', null);
INSERT INTO `acesso` VALUES ('889', '1', '2011-02-21 19:49:40', null);
INSERT INTO `acesso` VALUES ('890', '19', '2011-02-22 09:18:08', null);
INSERT INTO `acesso` VALUES ('891', '19', '2011-02-23 11:34:02', null);
INSERT INTO `acesso` VALUES ('892', '1', '2011-02-23 19:11:17', null);
INSERT INTO `acesso` VALUES ('893', '3', '2011-02-23 22:45:16', null);
INSERT INTO `acesso` VALUES ('894', '19', '2011-02-24 14:12:45', null);
INSERT INTO `acesso` VALUES ('895', '1', '2011-02-25 12:45:37', null);
INSERT INTO `acesso` VALUES ('896', '19', '2011-02-25 13:51:30', null);
INSERT INTO `acesso` VALUES ('897', '19', '2011-02-25 14:54:47', null);
INSERT INTO `acesso` VALUES ('898', '19', '2011-02-25 16:04:44', null);
INSERT INTO `acesso` VALUES ('899', '1', '2011-02-25 21:55:10', null);
INSERT INTO `acesso` VALUES ('900', '3', '2011-02-26 09:50:55', null);
INSERT INTO `acesso` VALUES ('901', '1', '2011-02-26 19:28:36', null);
INSERT INTO `acesso` VALUES ('902', '1', '2011-02-28 09:31:21', null);
INSERT INTO `acesso` VALUES ('903', '19', '2011-03-01 09:18:42', null);
INSERT INTO `acesso` VALUES ('904', '1', '2011-03-01 09:19:32', null);
INSERT INTO `acesso` VALUES ('905', '1', '2011-03-01 13:01:01', null);
INSERT INTO `acesso` VALUES ('906', '19', '2011-03-02 09:46:23', null);
INSERT INTO `acesso` VALUES ('907', '19', '2011-03-02 14:36:59', null);
INSERT INTO `acesso` VALUES ('908', '19', '2011-03-02 16:05:33', null);
INSERT INTO `acesso` VALUES ('909', '19', '2011-03-02 16:27:43', null);
INSERT INTO `acesso` VALUES ('910', '19', '2011-03-03 10:11:30', null);
INSERT INTO `acesso` VALUES ('911', '19', '2011-03-03 14:16:35', null);
INSERT INTO `acesso` VALUES ('912', '19', '2011-03-04 09:21:36', null);
INSERT INTO `acesso` VALUES ('913', '19', '2011-03-04 10:44:16', null);
INSERT INTO `acesso` VALUES ('914', '1', '2011-03-04 11:18:17', null);
INSERT INTO `acesso` VALUES ('915', '1', '2011-03-04 21:27:13', null);
INSERT INTO `acesso` VALUES ('916', '19', '2011-03-11 11:23:37', null);
INSERT INTO `acesso` VALUES ('917', '1', '2011-03-13 09:55:28', null);
INSERT INTO `acesso` VALUES ('918', '13', '2011-03-16 13:49:11', null);
INSERT INTO `acesso` VALUES ('919', '13', '2011-03-17 09:47:17', null);
INSERT INTO `acesso` VALUES ('920', '1', '2011-03-17 09:59:17', null);
INSERT INTO `acesso` VALUES ('921', '1', '2011-03-17 10:00:52', null);
INSERT INTO `acesso` VALUES ('922', '1', '2011-03-17 14:02:10', null);
INSERT INTO `acesso` VALUES ('923', '1', '2011-03-17 14:03:48', null);
INSERT INTO `acesso` VALUES ('924', '1', '2011-03-17 14:03:50', null);
INSERT INTO `acesso` VALUES ('925', '1', '2011-03-17 17:13:29', null);
INSERT INTO `acesso` VALUES ('926', '19', '2011-03-21 09:31:17', null);
INSERT INTO `acesso` VALUES ('927', '19', '2011-03-21 09:56:44', null);
INSERT INTO `acesso` VALUES ('928', '19', '2011-03-22 11:32:03', null);
INSERT INTO `acesso` VALUES ('929', '1', '2011-03-22 12:38:24', null);
INSERT INTO `acesso` VALUES ('930', '1', '2011-03-22 12:40:41', null);
INSERT INTO `acesso` VALUES ('931', '19', '2011-03-22 14:29:17', null);
INSERT INTO `acesso` VALUES ('932', '1', '2011-03-22 14:31:45', null);
INSERT INTO `acesso` VALUES ('933', '1', '2011-03-22 14:37:20', null);
INSERT INTO `acesso` VALUES ('934', '5', '2011-03-22 14:47:28', null);
INSERT INTO `acesso` VALUES ('935', '1', '2011-03-22 15:36:59', null);
INSERT INTO `acesso` VALUES ('936', '1', '2011-03-22 15:38:43', null);
INSERT INTO `acesso` VALUES ('937', '1', '2011-03-23 17:56:03', null);
INSERT INTO `acesso` VALUES ('938', '1', '2011-03-23 19:08:17', null);
INSERT INTO `acesso` VALUES ('939', '5', '2011-03-23 21:07:11', null);
INSERT INTO `acesso` VALUES ('940', '13', '2011-03-24 08:18:33', null);
INSERT INTO `acesso` VALUES ('941', '5', '2011-03-24 10:32:37', null);
INSERT INTO `acesso` VALUES ('942', '19', '2011-03-24 10:41:15', null);
INSERT INTO `acesso` VALUES ('943', '5', '2011-03-24 16:29:49', null);
INSERT INTO `acesso` VALUES ('944', '19', '2011-03-25 10:36:38', null);
INSERT INTO `acesso` VALUES ('945', '1', '2011-03-25 14:40:20', null);
INSERT INTO `acesso` VALUES ('946', '1', '2011-03-25 14:40:49', null);
INSERT INTO `acesso` VALUES ('947', '1', '2011-03-25 14:46:07', null);
INSERT INTO `acesso` VALUES ('948', '19', '2011-03-25 14:57:16', null);
INSERT INTO `acesso` VALUES ('949', '1', '2011-03-25 18:14:31', null);
INSERT INTO `acesso` VALUES ('950', '1', '2011-03-29 10:02:21', null);
INSERT INTO `acesso` VALUES ('951', '1', '2011-03-29 15:28:36', null);
INSERT INTO `acesso` VALUES ('952', '1', '2011-03-29 18:19:26', null);
INSERT INTO `acesso` VALUES ('953', '19', '2011-03-30 11:43:10', null);
INSERT INTO `acesso` VALUES ('954', '19', '2011-03-30 15:32:54', null);
INSERT INTO `acesso` VALUES ('955', '19', '2011-03-30 15:37:26', null);
INSERT INTO `acesso` VALUES ('956', '1', '2011-03-30 15:37:57', null);
INSERT INTO `acesso` VALUES ('957', '19', '2011-03-30 15:39:39', null);
INSERT INTO `acesso` VALUES ('958', '19', '2011-03-30 15:44:09', null);
INSERT INTO `acesso` VALUES ('959', '5', '2011-03-30 16:43:04', null);
INSERT INTO `acesso` VALUES ('960', '19', '2011-03-30 16:46:03', null);
INSERT INTO `acesso` VALUES ('961', '1', '2011-03-31 10:26:36', null);
INSERT INTO `acesso` VALUES ('962', '19', '2011-03-31 10:48:50', null);
INSERT INTO `acesso` VALUES ('963', '5', '2011-03-31 10:58:32', null);
INSERT INTO `acesso` VALUES ('964', '5', '2011-03-31 11:00:17', null);
INSERT INTO `acesso` VALUES ('965', '5', '2011-03-31 11:00:35', null);
INSERT INTO `acesso` VALUES ('966', '1', '2011-03-31 11:27:38', null);
INSERT INTO `acesso` VALUES ('967', '5', '2011-03-31 11:58:02', null);
INSERT INTO `acesso` VALUES ('968', '5', '2011-03-31 12:47:27', null);
INSERT INTO `acesso` VALUES ('969', '1', '2011-03-31 13:21:00', null);
INSERT INTO `acesso` VALUES ('970', '5', '2011-03-31 14:26:21', null);
INSERT INTO `acesso` VALUES ('971', '19', '2011-03-31 14:57:38', null);
INSERT INTO `acesso` VALUES ('972', '19', '2011-03-31 16:03:30', null);
INSERT INTO `acesso` VALUES ('973', '19', '2011-04-01 11:07:39', null);
INSERT INTO `acesso` VALUES ('974', '1', '2011-04-03 17:06:37', null);
INSERT INTO `acesso` VALUES ('975', '19', '2011-04-04 08:46:46', null);
INSERT INTO `acesso` VALUES ('976', '19', '2011-04-04 09:20:51', null);
INSERT INTO `acesso` VALUES ('977', '3', '2011-04-04 20:31:19', null);
INSERT INTO `acesso` VALUES ('978', '5', '2011-04-05 10:33:56', null);
INSERT INTO `acesso` VALUES ('979', '5', '2011-04-05 12:27:36', null);
INSERT INTO `acesso` VALUES ('980', '19', '2011-04-05 15:18:00', null);
INSERT INTO `acesso` VALUES ('981', '1', '2011-04-05 15:21:32', null);
INSERT INTO `acesso` VALUES ('982', '19', '2011-04-05 16:10:52', null);
INSERT INTO `acesso` VALUES ('983', '1', '2011-04-05 18:55:56', null);
INSERT INTO `acesso` VALUES ('984', '1', '2011-04-05 20:57:37', null);
INSERT INTO `acesso` VALUES ('985', '19', '2011-04-06 11:39:37', null);
INSERT INTO `acesso` VALUES ('986', '19', '2011-04-06 14:22:13', null);
INSERT INTO `acesso` VALUES ('987', '19', '2011-04-06 14:43:18', null);
INSERT INTO `acesso` VALUES ('988', '19', '2011-04-06 15:13:09', null);
INSERT INTO `acesso` VALUES ('989', '19', '2011-04-07 10:05:51', null);
INSERT INTO `acesso` VALUES ('990', '19', '2011-04-07 11:30:38', null);
INSERT INTO `acesso` VALUES ('991', '19', '2011-04-07 13:44:06', null);
INSERT INTO `acesso` VALUES ('992', '19', '2011-04-07 14:39:45', null);
INSERT INTO `acesso` VALUES ('993', '19', '2011-04-07 15:35:55', null);
INSERT INTO `acesso` VALUES ('994', '19', '2011-04-07 15:56:40', null);
INSERT INTO `acesso` VALUES ('995', '19', '2011-04-08 09:44:09', null);
INSERT INTO `acesso` VALUES ('996', '19', '2011-04-08 15:59:37', null);
INSERT INTO `acesso` VALUES ('997', '1', '2011-04-11 10:01:50', null);
INSERT INTO `acesso` VALUES ('998', '19', '2011-04-11 11:45:26', null);
INSERT INTO `acesso` VALUES ('999', '19', '2011-04-11 13:50:19', null);
INSERT INTO `acesso` VALUES ('1000', '19', '2011-04-11 14:40:31', null);
INSERT INTO `acesso` VALUES ('1001', '1', '2011-04-11 19:50:00', null);
INSERT INTO `acesso` VALUES ('1002', '1', '2011-04-11 19:52:03', null);
INSERT INTO `acesso` VALUES ('1003', '19', '2011-04-12 09:36:49', null);
INSERT INTO `acesso` VALUES ('1004', '19', '2011-04-12 15:12:52', null);
INSERT INTO `acesso` VALUES ('1005', '19', '2011-04-13 09:34:52', null);
INSERT INTO `acesso` VALUES ('1006', '1', '2011-04-14 10:33:22', null);
INSERT INTO `acesso` VALUES ('1007', '1', '2011-04-14 11:16:03', null);
INSERT INTO `acesso` VALUES ('1008', '19', '2011-04-14 11:43:18', null);
INSERT INTO `acesso` VALUES ('1009', '1', '2011-04-14 11:54:03', null);
INSERT INTO `acesso` VALUES ('1010', '19', '2011-04-14 15:29:42', null);
INSERT INTO `acesso` VALUES ('1011', '1', '2011-04-14 17:50:57', null);
INSERT INTO `acesso` VALUES ('1012', '19', '2011-04-15 09:52:08', null);
INSERT INTO `acesso` VALUES ('1013', '5', '2011-04-15 10:59:35', null);
INSERT INTO `acesso` VALUES ('1014', '19', '2011-04-15 11:17:52', null);
INSERT INTO `acesso` VALUES ('1015', '19', '2011-04-15 11:33:48', null);
INSERT INTO `acesso` VALUES ('1016', '1', '2011-04-15 11:46:42', null);
INSERT INTO `acesso` VALUES ('1017', '5', '2011-04-15 11:48:00', null);
INSERT INTO `acesso` VALUES ('1018', '1', '2011-04-15 13:56:45', null);
INSERT INTO `acesso` VALUES ('1019', '19', '2011-04-15 14:24:44', null);
INSERT INTO `acesso` VALUES ('1020', '19', '2011-04-15 15:07:58', null);
INSERT INTO `acesso` VALUES ('1021', '19', '2011-04-18 09:24:46', null);
INSERT INTO `acesso` VALUES ('1022', '19', '2011-04-18 11:35:34', null);
INSERT INTO `acesso` VALUES ('1023', '19', '2011-04-18 11:51:12', null);
INSERT INTO `acesso` VALUES ('1024', '1', '2011-04-18 12:03:38', null);
INSERT INTO `acesso` VALUES ('1025', '19', '2011-04-18 14:19:33', null);
INSERT INTO `acesso` VALUES ('1026', '1', '2011-04-18 14:46:48', null);
INSERT INTO `acesso` VALUES ('1027', '19', '2011-04-18 15:59:23', null);
INSERT INTO `acesso` VALUES ('1028', '1', '2011-04-18 22:11:47', null);
INSERT INTO `acesso` VALUES ('1029', '19', '2011-04-19 08:47:01', null);
INSERT INTO `acesso` VALUES ('1030', '19', '2011-04-19 09:31:27', null);
INSERT INTO `acesso` VALUES ('1031', '19', '2011-04-19 09:56:27', null);
INSERT INTO `acesso` VALUES ('1032', '19', '2011-04-19 11:14:10', null);
INSERT INTO `acesso` VALUES ('1033', '13', '2011-04-19 13:37:41', null);
INSERT INTO `acesso` VALUES ('1034', '19', '2011-04-19 14:17:22', null);
INSERT INTO `acesso` VALUES ('1035', '1', '2011-04-19 14:20:14', null);
INSERT INTO `acesso` VALUES ('1036', '13', '2011-04-19 16:48:50', null);
INSERT INTO `acesso` VALUES ('1037', '19', '2011-04-20 08:40:31', null);
INSERT INTO `acesso` VALUES ('1038', '1', '2011-04-20 09:31:21', null);
INSERT INTO `acesso` VALUES ('1039', '19', '2011-04-20 10:07:36', null);
INSERT INTO `acesso` VALUES ('1040', '13', '2011-04-20 14:00:47', null);
INSERT INTO `acesso` VALUES ('1041', '19', '2011-04-20 14:33:08', null);
INSERT INTO `acesso` VALUES ('1042', '13', '2011-04-20 16:14:05', null);
INSERT INTO `acesso` VALUES ('1043', '19', '2011-04-25 08:31:06', null);
INSERT INTO `acesso` VALUES ('1044', '19', '2011-04-25 14:02:48', null);
INSERT INTO `acesso` VALUES ('1045', '19', '2011-04-25 15:08:54', null);
INSERT INTO `acesso` VALUES ('1046', '19', '2011-04-25 16:08:41', null);
INSERT INTO `acesso` VALUES ('1047', '19', '2011-04-26 09:14:51', null);
INSERT INTO `acesso` VALUES ('1048', '1', '2011-04-26 09:44:54', null);
INSERT INTO `acesso` VALUES ('1049', '1', '2011-04-26 10:49:14', null);
INSERT INTO `acesso` VALUES ('1050', '1', '2011-04-26 11:02:22', null);
INSERT INTO `acesso` VALUES ('1051', '20', '2011-04-26 11:23:05', null);
INSERT INTO `acesso` VALUES ('1052', '19', '2011-04-26 11:37:02', null);
INSERT INTO `acesso` VALUES ('1053', '1', '2011-04-26 13:45:06', null);
INSERT INTO `acesso` VALUES ('1054', '20', '2011-04-26 13:47:25', null);
INSERT INTO `acesso` VALUES ('1055', '19', '2011-04-26 14:02:26', null);
INSERT INTO `acesso` VALUES ('1056', '1', '2011-04-26 14:50:51', null);
INSERT INTO `acesso` VALUES ('1057', '19', '2011-04-27 10:22:42', null);
INSERT INTO `acesso` VALUES ('1058', '1', '2011-04-27 11:09:58', null);
INSERT INTO `acesso` VALUES ('1059', '5', '2011-04-27 12:17:27', null);
INSERT INTO `acesso` VALUES ('1060', '5', '2011-04-27 12:22:26', null);
INSERT INTO `acesso` VALUES ('1061', '5', '2011-04-27 12:37:34', null);
INSERT INTO `acesso` VALUES ('1062', '5', '2011-04-27 12:40:12', null);
INSERT INTO `acesso` VALUES ('1063', '20', '2011-04-27 12:41:17', null);
INSERT INTO `acesso` VALUES ('1064', '1', '2011-04-27 13:39:04', null);
INSERT INTO `acesso` VALUES ('1065', '19', '2011-04-28 09:47:51', null);
INSERT INTO `acesso` VALUES ('1066', '19', '2011-04-28 11:14:46', null);
INSERT INTO `acesso` VALUES ('1067', '19', '2011-04-28 13:38:22', null);
INSERT INTO `acesso` VALUES ('1068', '19', '2011-04-28 14:56:38', null);
INSERT INTO `acesso` VALUES ('1069', '19', '2011-04-28 15:49:07', null);
INSERT INTO `acesso` VALUES ('1070', '19', '2011-05-03 09:51:57', null);
INSERT INTO `acesso` VALUES ('1071', '19', '2011-05-03 10:42:58', null);
INSERT INTO `acesso` VALUES ('1072', '19', '2011-05-04 10:48:27', null);
INSERT INTO `acesso` VALUES ('1073', '1', '2011-05-04 10:52:54', null);
INSERT INTO `acesso` VALUES ('1074', '19', '2011-05-04 11:47:18', null);
INSERT INTO `acesso` VALUES ('1075', '1', '2011-05-04 13:29:14', null);
INSERT INTO `acesso` VALUES ('1076', '19', '2011-05-04 13:59:03', null);
INSERT INTO `acesso` VALUES ('1077', '19', '2011-05-04 14:51:26', null);
INSERT INTO `acesso` VALUES ('1078', '19', '2011-05-04 15:59:29', null);
INSERT INTO `acesso` VALUES ('1079', '19', '2011-05-05 10:32:17', null);
INSERT INTO `acesso` VALUES ('1080', '19', '2011-05-06 09:00:11', null);
INSERT INTO `acesso` VALUES ('1081', '1', '2011-05-06 09:50:38', null);
INSERT INTO `acesso` VALUES ('1082', '1', '2011-05-07 11:04:53', null);
INSERT INTO `acesso` VALUES ('1083', '19', '2011-05-09 11:39:45', null);
INSERT INTO `acesso` VALUES ('1084', '19', '2011-05-09 15:26:59', null);
INSERT INTO `acesso` VALUES ('1085', '19', '2011-05-10 12:03:48', null);
INSERT INTO `acesso` VALUES ('1086', '19', '2011-05-10 13:04:15', null);
INSERT INTO `acesso` VALUES ('1087', '1', '2011-05-10 13:37:10', null);
INSERT INTO `acesso` VALUES ('1088', '19', '2011-05-10 13:49:49', null);
INSERT INTO `acesso` VALUES ('1089', '19', '2011-05-11 10:47:56', null);
INSERT INTO `acesso` VALUES ('1090', '1', '2011-05-11 12:53:40', null);
INSERT INTO `acesso` VALUES ('1091', '1', '2011-05-11 15:08:07', null);
INSERT INTO `acesso` VALUES ('1092', '19', '2011-05-12 11:46:24', null);
INSERT INTO `acesso` VALUES ('1093', '1', '2011-05-12 13:38:01', null);
INSERT INTO `acesso` VALUES ('1094', '19', '2011-05-12 16:07:16', null);
INSERT INTO `acesso` VALUES ('1095', '19', '2011-05-12 16:39:39', null);
INSERT INTO `acesso` VALUES ('1096', '19', '2011-05-13 11:22:01', null);
INSERT INTO `acesso` VALUES ('1097', '19', '2011-05-13 15:53:32', null);
INSERT INTO `acesso` VALUES ('1098', '1', '2011-05-16 09:47:36', null);
INSERT INTO `acesso` VALUES ('1099', '5', '2011-05-19 08:15:48', null);
INSERT INTO `acesso` VALUES ('1100', '5', '2011-05-19 09:11:24', null);
INSERT INTO `acesso` VALUES ('1101', '1', '2011-05-21 00:40:14', null);
INSERT INTO `acesso` VALUES ('1102', '3', '2011-05-23 08:40:55', null);
INSERT INTO `acesso` VALUES ('1103', '3', '2011-05-23 09:42:10', null);
INSERT INTO `acesso` VALUES ('1104', '1', '2011-05-24 10:46:56', null);
INSERT INTO `acesso` VALUES ('1105', '1', '2011-05-26 11:20:48', null);
INSERT INTO `acesso` VALUES ('1106', '1', '2011-05-30 14:37:08', null);
INSERT INTO `acesso` VALUES ('1107', '21', '2011-05-30 14:41:20', null);
INSERT INTO `acesso` VALUES ('1108', '21', '2011-05-30 14:43:12', null);
INSERT INTO `acesso` VALUES ('1109', '5', '2011-05-31 15:58:43', null);
INSERT INTO `acesso` VALUES ('1110', '5', '2011-06-01 07:57:46', null);
INSERT INTO `acesso` VALUES ('1111', '5', '2011-06-01 08:26:04', null);
INSERT INTO `acesso` VALUES ('1112', '5', '2011-06-01 08:33:23', null);
INSERT INTO `acesso` VALUES ('1113', '5', '2011-06-01 09:09:44', null);
INSERT INTO `acesso` VALUES ('1114', '1', '2011-06-01 17:33:26', null);
INSERT INTO `acesso` VALUES ('1115', '1', '2011-06-01 21:38:35', null);
INSERT INTO `acesso` VALUES ('1116', '1', '2011-06-02 13:21:21', null);
INSERT INTO `acesso` VALUES ('1117', '1', '2011-06-02 19:31:47', null);
INSERT INTO `acesso` VALUES ('1118', '1', '2011-06-03 13:11:13', null);
INSERT INTO `acesso` VALUES ('1119', '1', '2011-06-03 13:34:20', null);
INSERT INTO `acesso` VALUES ('1120', '1', '2011-06-03 14:51:43', null);
INSERT INTO `acesso` VALUES ('1121', '1', '2011-06-03 14:52:33', null);
INSERT INTO `acesso` VALUES ('1122', '1', '2011-06-03 15:10:41', null);
INSERT INTO `acesso` VALUES ('1123', '1', '2011-06-03 15:13:02', null);
INSERT INTO `acesso` VALUES ('1124', '1', '2011-06-03 18:09:56', null);
INSERT INTO `acesso` VALUES ('1125', '1', '2011-06-03 23:01:29', null);
INSERT INTO `acesso` VALUES ('1126', '1', '2011-06-06 11:35:06', null);
INSERT INTO `acesso` VALUES ('1127', '1', '2011-06-06 12:22:44', null);
INSERT INTO `acesso` VALUES ('1128', '1', '2011-06-06 12:52:37', null);
INSERT INTO `acesso` VALUES ('1129', '1', '2011-06-06 14:05:40', null);
INSERT INTO `acesso` VALUES ('1130', '1', '2011-06-06 14:06:34', null);
INSERT INTO `acesso` VALUES ('1131', '1', '2011-06-06 18:46:06', null);
INSERT INTO `acesso` VALUES ('1132', '1', '2011-06-06 22:24:34', null);
INSERT INTO `acesso` VALUES ('1133', '1', '2011-06-06 23:50:04', null);
INSERT INTO `acesso` VALUES ('1134', '1', '2011-06-07 00:01:44', null);
INSERT INTO `acesso` VALUES ('1135', '1', '2011-06-07 17:46:01', null);
INSERT INTO `acesso` VALUES ('1136', '1', '2011-06-08 13:08:20', null);
INSERT INTO `acesso` VALUES ('1137', '1', '2011-06-08 17:57:16', null);
INSERT INTO `acesso` VALUES ('1138', '1', '2011-06-08 18:53:51', null);
INSERT INTO `acesso` VALUES ('1139', '1', '2011-06-09 12:07:21', null);
INSERT INTO `acesso` VALUES ('1140', '1', '2011-06-09 12:07:47', null);
INSERT INTO `acesso` VALUES ('1141', '1', '2011-06-09 17:47:35', null);
INSERT INTO `acesso` VALUES ('1142', '1', '2011-06-13 14:02:06', null);
INSERT INTO `acesso` VALUES ('1143', '1', '2011-06-14 21:13:46', null);
INSERT INTO `acesso` VALUES ('1144', '1', '2011-06-14 23:25:34', null);
INSERT INTO `acesso` VALUES ('1145', '1', '2011-06-14 23:44:20', null);
INSERT INTO `acesso` VALUES ('1146', '1', '2011-06-14 23:47:34', null);
INSERT INTO `acesso` VALUES ('1147', '1', '2011-06-14 23:48:11', null);
INSERT INTO `acesso` VALUES ('1148', '1', '2011-06-15 00:04:40', null);
INSERT INTO `acesso` VALUES ('1149', '1', '2011-06-15 00:10:28', null);
INSERT INTO `acesso` VALUES ('1150', '1', '2011-06-16 12:51:41', null);
INSERT INTO `acesso` VALUES ('1151', '1', '2011-06-16 13:10:14', null);
INSERT INTO `acesso` VALUES ('1152', '1', '2011-06-16 16:32:53', null);
INSERT INTO `acesso` VALUES ('1153', '1', '2011-06-17 12:35:25', null);
INSERT INTO `acesso` VALUES ('1154', '1', '2011-06-20 14:07:11', null);
INSERT INTO `acesso` VALUES ('1155', '1', '2011-06-21 11:56:20', null);
INSERT INTO `acesso` VALUES ('1156', '1', '2011-06-22 15:16:15', null);
INSERT INTO `acesso` VALUES ('1157', '1', '2011-06-28 17:53:21', null);
INSERT INTO `acesso` VALUES ('1158', '1', '2011-06-29 12:41:11', null);
INSERT INTO `acesso` VALUES ('1159', '1', '2011-06-29 12:59:47', null);
INSERT INTO `acesso` VALUES ('1160', '1', '2011-06-30 12:47:21', null);
INSERT INTO `acesso` VALUES ('1161', '1', '2011-06-30 23:01:10', null);
INSERT INTO `acesso` VALUES ('1162', '1', '2011-07-01 13:46:42', null);
INSERT INTO `acesso` VALUES ('1164', '1', '2011-07-01 20:37:33', null);
INSERT INTO `acesso` VALUES ('1165', '1', '2011-07-01 20:55:20', null);
INSERT INTO `acesso` VALUES ('1166', '5', '2011-07-01 22:56:39', null);
INSERT INTO `acesso` VALUES ('1167', '1', '2011-07-01 23:04:40', null);
INSERT INTO `acesso` VALUES ('1168', '1', '2011-07-01 23:05:56', null);

-- ----------------------------
-- Table structure for `configuracao`
-- ----------------------------
DROP TABLE IF EXISTS `configuracao`;
CREATE TABLE `configuracao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_projeto` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of configuracao
-- ----------------------------
INSERT INTO `configuracao` VALUES ('1', 'Modelo de Projetos');

-- ----------------------------
-- Table structure for `erro`
-- ----------------------------
DROP TABLE IF EXISTS `erro`;
CREATE TABLE `erro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) DEFAULT NULL,
  `codigo_erro` int(11) NOT NULL,
  `mensagem_erro` text NOT NULL,
  `url` text,
  `arquivo_erro` varchar(255) DEFAULT NULL,
  `linha_erro` varchar(80) DEFAULT NULL,
  `get` text,
  `post` text,
  `session` text,
  `stacktrace` mediumtext,
  `datahora_DATETIME` datetime NOT NULL,
  `status_BOOLEAN` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of erro
-- ----------------------------

-- ----------------------------
-- Table structure for `operacao_sistema`
-- ----------------------------
DROP TABLE IF EXISTS `operacao_sistema`;
CREATE TABLE `operacao_sistema` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) NOT NULL,
  `tipo_operacao` varchar(50) DEFAULT NULL,
  `pagina_operacao` varchar(255) DEFAULT NULL,
  `entidade_operacao` varchar(50) DEFAULT NULL,
  `chave_registro_operacao_INT` int(11) DEFAULT NULL,
  `descricao_operacao` varchar(255) DEFAULT NULL,
  `data_operacao_DATETIME` datetime NOT NULL,
  `url_completa` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario` (`usuario_id_INT`),
  CONSTRAINT `operacao_sistema_ibfk_1` FOREIGN KEY (`usuario_id_INT`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of operacao_sistema
-- ----------------------------

-- ----------------------------
-- Table structure for `uf`
-- ----------------------------
DROP TABLE IF EXISTS `uf`;
CREATE TABLE `uf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `sigla` char(2) NOT NULL,
  `dataCadastro` datetime DEFAULT NULL,
  `dataEdicao` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sigla` (`sigla`),
  UNIQUE KEY `nome` (`nome`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of uf
-- ----------------------------
INSERT INTO `uf` VALUES ('1', 'São Paulo', 'SP', '2009-10-18 16:05:17', '2009-10-18 16:05:17');
INSERT INTO `uf` VALUES ('2', 'Minas Gerais', 'MG', '2009-10-18 16:08:39', '2009-10-18 16:08:39');
INSERT INTO `uf` VALUES ('3', 'Pará', 'PA', '2009-10-23 11:20:19', '2009-10-23 11:20:19');
INSERT INTO `uf` VALUES ('4', 'Espirito Santo', 'ES', '2009-11-04 19:22:18', '2009-11-04 19:22:18');
INSERT INTO `uf` VALUES ('5', 'Rio de Janeiro', 'RJ', '2010-01-01 12:03:54', '2010-01-01 12:03:54');
INSERT INTO `uf` VALUES ('6', 'Rio Grande do Sul', 'RS', '2010-01-01 12:04:12', '2010-01-01 12:04:12');
INSERT INTO `uf` VALUES ('7', 'Bahia', 'BA', '2010-01-01 12:04:23', '2010-01-01 12:04:23');
INSERT INTO `uf` VALUES ('8', 'Tocantins', 'TO', '2010-01-01 12:04:38', '2010-01-01 12:04:38');
INSERT INTO `uf` VALUES ('9', 'Maranhão', 'MA', '2010-01-01 12:06:00', '2010-01-01 12:06:00');
INSERT INTO `uf` VALUES ('10', 'Acre', 'AC', '2010-01-01 12:06:16', '2010-01-01 12:06:16');
INSERT INTO `uf` VALUES ('11', 'Alagoas', 'AL', '2010-01-01 12:06:31', '2010-01-01 12:06:31');
INSERT INTO `uf` VALUES ('12', 'Amapá', 'AP', '2010-01-01 12:06:50', '2010-01-01 12:06:50');
INSERT INTO `uf` VALUES ('13', 'Amazonas', 'AM', '2010-01-01 12:07:01', '2010-01-01 12:07:01');
INSERT INTO `uf` VALUES ('14', 'Ceará', 'CE', '2010-01-01 12:07:21', '2010-01-01 12:07:21');
INSERT INTO `uf` VALUES ('15', 'Distrito Federal', 'DF', '2010-01-01 12:07:40', '2010-01-01 12:07:40');
INSERT INTO `uf` VALUES ('16', 'Goiás', 'GO', '2010-01-01 12:08:08', '2010-01-01 12:08:08');
INSERT INTO `uf` VALUES ('17', 'Mato Grosso', 'MT', '2010-01-01 12:08:30', '2010-01-01 12:08:30');
INSERT INTO `uf` VALUES ('18', 'Mato Grosso do Sul', 'MS', '2010-01-01 12:08:54', '2010-01-01 12:08:54');
INSERT INTO `uf` VALUES ('19', 'Paraíba', 'PB', '2010-01-01 12:09:15', '2010-01-01 12:09:15');
INSERT INTO `uf` VALUES ('20', 'Paraná', 'PR', '2010-01-01 12:09:43', '2010-01-01 12:09:43');
INSERT INTO `uf` VALUES ('21', 'Pernambuco', 'PE', '2010-01-01 12:10:09', '2010-01-01 12:10:09');
INSERT INTO `uf` VALUES ('22', 'Piauí', 'PI', '2010-01-01 12:10:36', '2010-01-01 12:10:36');
INSERT INTO `uf` VALUES ('23', 'Rio Grande do Norte', 'RN', '2010-01-01 12:10:55', '2010-01-01 12:10:55');
INSERT INTO `uf` VALUES ('24', 'Rondônia', 'RO', '2010-01-01 12:11:15', '2010-01-01 12:11:15');
INSERT INTO `uf` VALUES ('25', 'Roraima', 'RR', '2010-01-01 12:11:45', '2010-01-01 12:11:45');
INSERT INTO `uf` VALUES ('26', 'Santa Catarina', 'SC', '2010-01-01 12:12:03', '2010-01-01 12:12:03');
INSERT INTO `uf` VALUES ('27', 'Sergipe', 'SE', '2010-01-01 12:12:20', '2010-01-01 12:12:20');

-- ----------------------------
-- Table structure for `usuario`
-- ----------------------------
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `senha` varchar(100) NOT NULL,
  `usuario_tipo_id_INT` int(11) NOT NULL,
  `status_BOOLEAN` int(1) NOT NULL,
  `pagina_inicial` text,
  PRIMARY KEY (`id`),
  KEY `tipo_usuario_usuario` (`usuario_tipo_id_INT`),
  CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`usuario_tipo_id_INT`) REFERENCES `usuario_tipo` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario
-- ----------------------------
INSERT INTO `usuario` VALUES ('1', 'Desenvolvimento', 'desenvol@vimento', '18563035b358c4f559f4bbc16004bf79', '1', '1', null);

-- ----------------------------
-- Table structure for `usuario_menu`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_menu`;
CREATE TABLE `usuario_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) NOT NULL,
  `area_menu` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=174 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_menu
-- ----------------------------
INSERT INTO `usuario_menu` VALUES ('1', '21', 'Mobilização');
INSERT INTO `usuario_menu` VALUES ('2', '21', 'Mobilização');
INSERT INTO `usuario_menu` VALUES ('3', '21', 'Gráfico de Mobilização');
INSERT INTO `usuario_menu` VALUES ('4', '21', 'Organogramas');
INSERT INTO `usuario_menu` VALUES ('5', '21', 'Organograma de Trabalho');
INSERT INTO `usuario_menu` VALUES ('6', '21', 'Organograma para Impressão');
INSERT INTO `usuario_menu` VALUES ('7', '21', 'Alterar Estrutura Organizacional');
INSERT INTO `usuario_menu` VALUES ('8', '21', 'Visualizar Pendências');
INSERT INTO `usuario_menu` VALUES ('9', '21', 'Planejamento');
INSERT INTO `usuario_menu` VALUES ('10', '21', 'Cadastrar Milestone');
INSERT INTO `usuario_menu` VALUES ('11', '21', 'Gerenciar Milestones');
INSERT INTO `usuario_menu` VALUES ('12', '21', 'Associar Milestones a Vagas');
INSERT INTO `usuario_menu` VALUES ('13', '21', 'Seleção');
INSERT INTO `usuario_menu` VALUES ('14', '21', 'Cadastrar Currículo');
INSERT INTO `usuario_menu` VALUES ('15', '21', 'Currículos em Seleção');
INSERT INTO `usuario_menu` VALUES ('16', '21', 'Acompanhamento dos Pedidos de Vaga');
INSERT INTO `usuario_menu` VALUES ('17', '21', 'Relatórios');
INSERT INTO `usuario_menu` VALUES ('18', '21', 'Resumo do Processo de Mobilização');
INSERT INTO `usuario_menu` VALUES ('19', '21', 'Relatório de Pessoas Associadas');
INSERT INTO `usuario_menu` VALUES ('20', '21', 'Relatório de Pessoas Desassociadas');
INSERT INTO `usuario_menu` VALUES ('21', '21', 'Gestão do Tácito');
INSERT INTO `usuario_menu` VALUES ('22', '21', 'Mix de Similaridades');
INSERT INTO `usuario_menu` VALUES ('23', '21', 'Coletivos de Trabalho');
INSERT INTO `usuario_menu` VALUES ('24', '21', 'Criar Coletivos');
INSERT INTO `usuario_menu` VALUES ('25', '21', 'Criar Coletivos: Força Tarefa');
INSERT INTO `usuario_menu` VALUES ('26', '21', 'Planejar Mix');
INSERT INTO `usuario_menu` VALUES ('27', '21', 'Acompanhar Mix');
INSERT INTO `usuario_menu` VALUES ('28', '21', 'Padrão de Níveis de Similaridade');
INSERT INTO `usuario_menu` VALUES ('29', '21', 'Cadastrar Padrão de NS');
INSERT INTO `usuario_menu` VALUES ('30', '21', 'Gerenciar Padrões de NS');
INSERT INTO `usuario_menu` VALUES ('31', '21', 'Gestão do Tácito');
INSERT INTO `usuario_menu` VALUES ('32', '21', 'Aporte de Conhecimento Tácito');
INSERT INTO `usuario_menu` VALUES ('33', '21', 'Experiência Profissional por NS');
INSERT INTO `usuario_menu` VALUES ('34', '21', 'Experiência Profissional');
INSERT INTO `usuario_menu` VALUES ('35', '21', 'Organograma p/ Análise');
INSERT INTO `usuario_menu` VALUES ('36', '21', 'Redes Relacionais');
INSERT INTO `usuario_menu` VALUES ('37', '21', 'Visualizar Pendências');
INSERT INTO `usuario_menu` VALUES ('38', '21', 'Padrões de NS');
INSERT INTO `usuario_menu` VALUES ('39', '21', 'Experiências Profissionais');
INSERT INTO `usuario_menu` VALUES ('40', '21', 'Treinamento');
INSERT INTO `usuario_menu` VALUES ('41', '21', 'Planejamento de Demanda');
INSERT INTO `usuario_menu` VALUES ('42', '21', 'Unidades Treinamento');
INSERT INTO `usuario_menu` VALUES ('43', '21', 'Cadastrar Unidade de Treinamento');
INSERT INTO `usuario_menu` VALUES ('44', '21', 'Gerenciar Unidades de Treinamento');
INSERT INTO `usuario_menu` VALUES ('45', '21', 'Grupos de Treinamento');
INSERT INTO `usuario_menu` VALUES ('46', '21', 'Cadastrar Grupo de Treinamento');
INSERT INTO `usuario_menu` VALUES ('47', '21', 'Gerenciar Grupos de Treinamento');
INSERT INTO `usuario_menu` VALUES ('48', '21', 'Rotas de Treinamento');
INSERT INTO `usuario_menu` VALUES ('49', '21', 'Cadastrar Rotas de Treinamento');
INSERT INTO `usuario_menu` VALUES ('50', '21', 'Gerenciar Rotas de Treinamento');
INSERT INTO `usuario_menu` VALUES ('51', '21', 'Duração Máxima por Função / NS');
INSERT INTO `usuario_menu` VALUES ('52', '21', 'Planejamento Real');
INSERT INTO `usuario_menu` VALUES ('53', '21', 'Montar Turmas');
INSERT INTO `usuario_menu` VALUES ('54', '21', 'Acompanhamento / Finalização');
INSERT INTO `usuario_menu` VALUES ('55', '21', 'Lista de Chamada');
INSERT INTO `usuario_menu` VALUES ('56', '21', 'Ajustes');
INSERT INTO `usuario_menu` VALUES ('57', '21', 'Relatórios');
INSERT INTO `usuario_menu` VALUES ('58', '21', 'Por Pessoa');
INSERT INTO `usuario_menu` VALUES ('59', '21', 'Por Unidade de Treinamento');
INSERT INTO `usuario_menu` VALUES ('60', '21', 'Metodologia');
INSERT INTO `usuario_menu` VALUES ('61', '21', 'Cadastrar Metodologia');
INSERT INTO `usuario_menu` VALUES ('62', '21', 'Gerenciar Metodologias');
INSERT INTO `usuario_menu` VALUES ('63', '21', 'Tipos de Imersão');
INSERT INTO `usuario_menu` VALUES ('64', '21', 'Cadastrar Tipo de Imersão');
INSERT INTO `usuario_menu` VALUES ('65', '21', 'Gerenciar Tipo de Imersão');
INSERT INTO `usuario_menu` VALUES ('66', '21', 'Start-up');
INSERT INTO `usuario_menu` VALUES ('67', '21', 'Áreas');
INSERT INTO `usuario_menu` VALUES ('68', '21', 'Cadastrar Áreas');
INSERT INTO `usuario_menu` VALUES ('69', '21', 'Gerenciar Áreas');
INSERT INTO `usuario_menu` VALUES ('70', '21', 'Etapas');
INSERT INTO `usuario_menu` VALUES ('71', '21', 'Cadastrar Etapas');
INSERT INTO `usuario_menu` VALUES ('72', '21', 'Gerenciar Etapas');
INSERT INTO `usuario_menu` VALUES ('73', '21', 'Ordenar Etapas');
INSERT INTO `usuario_menu` VALUES ('74', '21', 'Parâmetros de Processo');
INSERT INTO `usuario_menu` VALUES ('75', '21', 'Cadastrar Parâmetros');
INSERT INTO `usuario_menu` VALUES ('76', '21', 'Gerenciar Parâmetros');
INSERT INTO `usuario_menu` VALUES ('77', '21', 'Grupos');
INSERT INTO `usuario_menu` VALUES ('78', '21', 'Cadastrar Grupo');
INSERT INTO `usuario_menu` VALUES ('79', '21', 'Gerenciar Grupos');
INSERT INTO `usuario_menu` VALUES ('80', '21', 'Sistema');
INSERT INTO `usuario_menu` VALUES ('81', '21', 'Backup do Banco de Dados');
INSERT INTO `usuario_menu` VALUES ('82', '21', 'Restaurar um Backup do Banco de Dados');
INSERT INTO `usuario_menu` VALUES ('83', '21', 'Fazer backup do Banco de Dados');
INSERT INTO `usuario_menu` VALUES ('84', '21', 'Gerenciar Rotinas de Backup Automático');
INSERT INTO `usuario_menu` VALUES ('85', '21', 'Textos');
INSERT INTO `usuario_menu` VALUES ('86', '21', 'Alterar Texto do Estudo');
INSERT INTO `usuario_menu` VALUES ('87', '21', 'Alterar Texto do Termo de Consentimento');
INSERT INTO `usuario_menu` VALUES ('88', '21', 'Glossário (Termos)');
INSERT INTO `usuario_menu` VALUES ('89', '21', 'Cadastrar Termo');
INSERT INTO `usuario_menu` VALUES ('90', '21', 'Gerenciar Glossário');
INSERT INTO `usuario_menu` VALUES ('91', '21', 'Outras Ações');
INSERT INTO `usuario_menu` VALUES ('92', '21', 'Gerar Mapa de Mobilização');
INSERT INTO `usuario_menu` VALUES ('93', '21', 'Áreas Participantes');
INSERT INTO `usuario_menu` VALUES ('94', '21', 'Outras Ações');
INSERT INTO `usuario_menu` VALUES ('95', '21', 'Configurações');
INSERT INTO `usuario_menu` VALUES ('96', '21', 'Relatório de Erros');
INSERT INTO `usuario_menu` VALUES ('97', '21', 'Análise Retroativa (Estudo)');
INSERT INTO `usuario_menu` VALUES ('98', '21', 'Assistente para Novo Projeto');
INSERT INTO `usuario_menu` VALUES ('99', '21', 'Cadastros');
INSERT INTO `usuario_menu` VALUES ('100', '21', 'Cadastros Gerais');
INSERT INTO `usuario_menu` VALUES ('101', '21', 'Cadastros Gerais');
INSERT INTO `usuario_menu` VALUES ('102', '21', 'Cargos');
INSERT INTO `usuario_menu` VALUES ('103', '21', 'Cadastrar Cargos');
INSERT INTO `usuario_menu` VALUES ('104', '21', 'Gerenciar Cargos');
INSERT INTO `usuario_menu` VALUES ('105', '21', 'Estados Civis');
INSERT INTO `usuario_menu` VALUES ('106', '21', 'Cadastrar Estado Civil');
INSERT INTO `usuario_menu` VALUES ('107', '21', 'Gerenciar Estados Civis');
INSERT INTO `usuario_menu` VALUES ('108', '21', 'Escolaridade');
INSERT INTO `usuario_menu` VALUES ('109', '21', 'Cadastrar Escolaridade');
INSERT INTO `usuario_menu` VALUES ('110', '21', 'Gerenciar Escolaridades');
INSERT INTO `usuario_menu` VALUES ('111', '21', 'Cadastros Gerais  ');
INSERT INTO `usuario_menu` VALUES ('112', '21', 'Faixas Salariais');
INSERT INTO `usuario_menu` VALUES ('113', '21', 'Cadastrar Faixa Salarial');
INSERT INTO `usuario_menu` VALUES ('114', '21', 'Gerenciar Faixas Salariais');
INSERT INTO `usuario_menu` VALUES ('115', '21', 'Gênero (Sexo)');
INSERT INTO `usuario_menu` VALUES ('116', '21', 'Cadastrar Gênero (Sexo)');
INSERT INTO `usuario_menu` VALUES ('117', '21', 'Gerenciar Gênero (Sexo)');
INSERT INTO `usuario_menu` VALUES ('118', '21', 'Legendas do Organograma');
INSERT INTO `usuario_menu` VALUES ('119', '21', 'Cadastrar Legenda');
INSERT INTO `usuario_menu` VALUES ('120', '21', 'Gerenciar Legendas');
INSERT INTO `usuario_menu` VALUES ('121', '21', 'Cadastros Gerais   ');
INSERT INTO `usuario_menu` VALUES ('122', '21', 'Linhas');
INSERT INTO `usuario_menu` VALUES ('123', '21', 'Cadastrar Linha');
INSERT INTO `usuario_menu` VALUES ('124', '21', 'Gerenciar Linhas');
INSERT INTO `usuario_menu` VALUES ('125', '21', 'Locais de Trabalho');
INSERT INTO `usuario_menu` VALUES ('126', '21', 'Cadastrar Local de Trabalho');
INSERT INTO `usuario_menu` VALUES ('127', '21', 'Gerenciar Locais de Trabalho');
INSERT INTO `usuario_menu` VALUES ('128', '21', 'Motivos de Desassociação');
INSERT INTO `usuario_menu` VALUES ('129', '21', 'Cadastrar Motivo de Desassociação');
INSERT INTO `usuario_menu` VALUES ('130', '21', 'Gerenciar Motivos de Desassociação');
INSERT INTO `usuario_menu` VALUES ('131', '21', 'Cadastros Gerais    ');
INSERT INTO `usuario_menu` VALUES ('132', '21', 'Nível Funcional na Hierarquia');
INSERT INTO `usuario_menu` VALUES ('133', '21', 'Cadastrar Nível');
INSERT INTO `usuario_menu` VALUES ('134', '21', 'Gerenciar Níveis');
INSERT INTO `usuario_menu` VALUES ('135', '21', 'Países');
INSERT INTO `usuario_menu` VALUES ('136', '21', 'Cadastrar País');
INSERT INTO `usuario_menu` VALUES ('137', '21', 'Gerenciar Países');
INSERT INTO `usuario_menu` VALUES ('138', '21', 'Status do Processo dos PVs');
INSERT INTO `usuario_menu` VALUES ('139', '21', 'Cadastrar Status');
INSERT INTO `usuario_menu` VALUES ('140', '21', 'Gerenciar Status');
INSERT INTO `usuario_menu` VALUES ('141', '21', 'Cadastros Gerais     ');
INSERT INTO `usuario_menu` VALUES ('142', '21', 'Unidades de Medida');
INSERT INTO `usuario_menu` VALUES ('143', '21', 'Cadastrar Unidades');
INSERT INTO `usuario_menu` VALUES ('144', '21', 'Gerenciar Unidades');
INSERT INTO `usuario_menu` VALUES ('145', '21', 'Unidades Federativas');
INSERT INTO `usuario_menu` VALUES ('146', '21', 'Cadastrar Estado');
INSERT INTO `usuario_menu` VALUES ('147', '21', 'Gerenciar Estados');
INSERT INTO `usuario_menu` VALUES ('148', '21', 'Nomes dos Níveis Estrutura');
INSERT INTO `usuario_menu` VALUES ('149', '21', 'Cadastrar Nome');
INSERT INTO `usuario_menu` VALUES ('150', '21', 'Gerenciar Nomes');
INSERT INTO `usuario_menu` VALUES ('151', '21', 'Cadastros Gerais      ');
INSERT INTO `usuario_menu` VALUES ('152', '21', 'Rede Relacional');
INSERT INTO `usuario_menu` VALUES ('153', '21', 'Cadastrar Tipo de Relação');
INSERT INTO `usuario_menu` VALUES ('154', '21', 'Gerenciar Tipos de Relação');
INSERT INTO `usuario_menu` VALUES ('155', '21', 'Cadastrar Tipo de Relação entre Colegas de Trabalho');
INSERT INTO `usuario_menu` VALUES ('156', '21', 'Gerenciar Tipos de Relação entre Colegas de Trabalho');
INSERT INTO `usuario_menu` VALUES ('157', '21', 'Sincronização');
INSERT INTO `usuario_menu` VALUES ('158', '21', 'Sincronização');
INSERT INTO `usuario_menu` VALUES ('159', '21', 'Sincronização ');
INSERT INTO `usuario_menu` VALUES ('160', '21', 'Importar Folha de Pagamento');
INSERT INTO `usuario_menu` VALUES ('161', '21', 'Importar Cargos');
INSERT INTO `usuario_menu` VALUES ('162', '21', 'Importar Niveis/Estrutura Org.');
INSERT INTO `usuario_menu` VALUES ('163', '21', 'Sincronização  ');
INSERT INTO `usuario_menu` VALUES ('164', '21', 'Importar Dados Gerais');
INSERT INTO `usuario_menu` VALUES ('165', '21', 'Importar Vagas');
INSERT INTO `usuario_menu` VALUES ('166', '21', 'Importar Dados Pessoais');
INSERT INTO `usuario_menu` VALUES ('167', '21', 'Sincronização   ');
INSERT INTO `usuario_menu` VALUES ('168', '21', 'Importar Experiências Práticas');
INSERT INTO `usuario_menu` VALUES ('169', '21', 'Importar Associações Pessoa-Vaga');
INSERT INTO `usuario_menu` VALUES ('170', '21', 'Importar Pedidos de Vaga');
INSERT INTO `usuario_menu` VALUES ('171', '21', 'Sincronização     ');
INSERT INTO `usuario_menu` VALUES ('172', '21', 'Importar Padrões de Níveis de Similaridade');
INSERT INTO `usuario_menu` VALUES ('173', '21', 'Importar Planejamento de Treinamentos');

-- ----------------------------
-- Table structure for `usuario_privilegio`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_privilegio`;
CREATE TABLE `usuario_privilegio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id_INT` int(11) NOT NULL,
  `identificador_funcionalidade` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_privilegio
-- ----------------------------

-- ----------------------------
-- Table structure for `usuario_tipo`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_tipo`;
CREATE TABLE `usuario_tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `nome_visivel` varchar(255) DEFAULT NULL,
  `status_BOOLEAN` int(11) NOT NULL,
  `pagina_inicial` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_tipo
-- ----------------------------
INSERT INTO `usuario_tipo` VALUES ('1', 'Usuário Administrador', 'Usuário Administrador', '1', '');

-- ----------------------------
-- Table structure for `usuario_tipo_menu`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_tipo_menu`;
CREATE TABLE `usuario_tipo_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_tipo_id_INT` int(11) NOT NULL,
  `area_menu` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10324 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_tipo_menu
-- ----------------------------
INSERT INTO `usuario_tipo_menu` VALUES ('10128', '1', 'Mobilização');
INSERT INTO `usuario_tipo_menu` VALUES ('10129', '1', 'Mobilização');
INSERT INTO `usuario_tipo_menu` VALUES ('10130', '1', 'Gráfico de Mobilização');
INSERT INTO `usuario_tipo_menu` VALUES ('10131', '1', 'Organogramas');
INSERT INTO `usuario_tipo_menu` VALUES ('10132', '1', 'Organograma de Trabalho');
INSERT INTO `usuario_tipo_menu` VALUES ('10133', '1', 'Organograma para Impressão');
INSERT INTO `usuario_tipo_menu` VALUES ('10134', '1', 'Alterar Estrutura Organizacional');
INSERT INTO `usuario_tipo_menu` VALUES ('10135', '1', 'Visualizar Pendências');
INSERT INTO `usuario_tipo_menu` VALUES ('10136', '1', 'Planejamento');
INSERT INTO `usuario_tipo_menu` VALUES ('10137', '1', 'Cadastrar Milestone');
INSERT INTO `usuario_tipo_menu` VALUES ('10138', '1', 'Gerenciar Milestones');
INSERT INTO `usuario_tipo_menu` VALUES ('10139', '1', 'Associar Milestones a Vagas');
INSERT INTO `usuario_tipo_menu` VALUES ('10140', '1', 'Seleção');
INSERT INTO `usuario_tipo_menu` VALUES ('10141', '1', 'Cadastrar Currículo');
INSERT INTO `usuario_tipo_menu` VALUES ('10142', '1', 'Currículos em Seleção');
INSERT INTO `usuario_tipo_menu` VALUES ('10143', '1', 'Acompanhamento dos Pedidos de Vaga');
INSERT INTO `usuario_tipo_menu` VALUES ('10144', '1', 'Relatórios');
INSERT INTO `usuario_tipo_menu` VALUES ('10145', '1', 'Resumo do Processo de Mobilização');
INSERT INTO `usuario_tipo_menu` VALUES ('10146', '1', 'Relatório de Pessoas Associadas');
INSERT INTO `usuario_tipo_menu` VALUES ('10147', '1', 'Relatório de Pessoas Desassociadas');
INSERT INTO `usuario_tipo_menu` VALUES ('10148', '1', 'Gestão do Tácito');
INSERT INTO `usuario_tipo_menu` VALUES ('10149', '1', 'Mix de Similaridades');
INSERT INTO `usuario_tipo_menu` VALUES ('10150', '1', 'Coletivos de Trabalho');
INSERT INTO `usuario_tipo_menu` VALUES ('10151', '1', 'Criar Coletivos');
INSERT INTO `usuario_tipo_menu` VALUES ('10152', '1', 'Criar Coletivos: Força Tarefa');
INSERT INTO `usuario_tipo_menu` VALUES ('10153', '1', 'Planejar Mix');
INSERT INTO `usuario_tipo_menu` VALUES ('10154', '1', 'Acompanhar Mix');
INSERT INTO `usuario_tipo_menu` VALUES ('10155', '1', 'Padrão de Níveis de Similaridade');
INSERT INTO `usuario_tipo_menu` VALUES ('10156', '1', 'Cadastrar Padrão de NS');
INSERT INTO `usuario_tipo_menu` VALUES ('10157', '1', 'Gerenciar Padrões de NS');
INSERT INTO `usuario_tipo_menu` VALUES ('10158', '1', 'Gestão do Tácito');
INSERT INTO `usuario_tipo_menu` VALUES ('10159', '1', 'Aporte de Conhecimento Tácito');
INSERT INTO `usuario_tipo_menu` VALUES ('10160', '1', 'Experiência Profissional por NS');
INSERT INTO `usuario_tipo_menu` VALUES ('10161', '1', 'Organograma p/ Análise');
INSERT INTO `usuario_tipo_menu` VALUES ('10162', '1', 'Redes Relacionais');
INSERT INTO `usuario_tipo_menu` VALUES ('10163', '1', 'Visualizar Pendências');
INSERT INTO `usuario_tipo_menu` VALUES ('10164', '1', 'Padrões de NS');
INSERT INTO `usuario_tipo_menu` VALUES ('10165', '1', 'Experiências Profissionais');
INSERT INTO `usuario_tipo_menu` VALUES ('10166', '1', 'Treinamento');
INSERT INTO `usuario_tipo_menu` VALUES ('10167', '1', 'Planejamento de Demanda');
INSERT INTO `usuario_tipo_menu` VALUES ('10168', '1', 'Unidades Treinamento');
INSERT INTO `usuario_tipo_menu` VALUES ('10169', '1', 'Cadastrar Unidade de Treinamento');
INSERT INTO `usuario_tipo_menu` VALUES ('10170', '1', 'Gerenciar Unidades de Treinamento');
INSERT INTO `usuario_tipo_menu` VALUES ('10171', '1', 'Grupos de Treinamento');
INSERT INTO `usuario_tipo_menu` VALUES ('10172', '1', 'Cadastrar Grupo de Treinamento');
INSERT INTO `usuario_tipo_menu` VALUES ('10173', '1', 'Gerenciar Grupos de Treinamento');
INSERT INTO `usuario_tipo_menu` VALUES ('10174', '1', 'Rotas de Treinamento');
INSERT INTO `usuario_tipo_menu` VALUES ('10175', '1', 'Cadastrar Rotas de Treinamento');
INSERT INTO `usuario_tipo_menu` VALUES ('10176', '1', 'Gerenciar Rotas de Treinamento');
INSERT INTO `usuario_tipo_menu` VALUES ('10177', '1', 'Duração Máxima por Função / NS');
INSERT INTO `usuario_tipo_menu` VALUES ('10178', '1', 'Planejamento Real');
INSERT INTO `usuario_tipo_menu` VALUES ('10179', '1', 'Montar Turmas');
INSERT INTO `usuario_tipo_menu` VALUES ('10180', '1', 'Acompanhamento / Finalização');
INSERT INTO `usuario_tipo_menu` VALUES ('10181', '1', 'Lista de Chamada');
INSERT INTO `usuario_tipo_menu` VALUES ('10182', '1', 'Ajustes');
INSERT INTO `usuario_tipo_menu` VALUES ('10183', '1', 'Relatórios');
INSERT INTO `usuario_tipo_menu` VALUES ('10184', '1', 'Por Pessoa');
INSERT INTO `usuario_tipo_menu` VALUES ('10185', '1', 'Por Unidade de Treinamento');
INSERT INTO `usuario_tipo_menu` VALUES ('10186', '1', 'Metodologia');
INSERT INTO `usuario_tipo_menu` VALUES ('10187', '1', 'Cadastrar Metodologia');
INSERT INTO `usuario_tipo_menu` VALUES ('10188', '1', 'Gerenciar Metodologias');
INSERT INTO `usuario_tipo_menu` VALUES ('10189', '1', 'Tipos de Imersão');
INSERT INTO `usuario_tipo_menu` VALUES ('10190', '1', 'Cadastrar Tipo de Imersão');
INSERT INTO `usuario_tipo_menu` VALUES ('10191', '1', 'Gerenciar Tipo de Imersão');
INSERT INTO `usuario_tipo_menu` VALUES ('10192', '1', 'Pesquisa');
INSERT INTO `usuario_tipo_menu` VALUES ('10193', '1', 'Etapas');
INSERT INTO `usuario_tipo_menu` VALUES ('10194', '1', 'Cadastrar Etapas');
INSERT INTO `usuario_tipo_menu` VALUES ('10195', '1', 'Gerenciar Etapas');
INSERT INTO `usuario_tipo_menu` VALUES ('10196', '1', 'Etapas de Outras Empresas');
INSERT INTO `usuario_tipo_menu` VALUES ('10197', '1', 'Áreas');
INSERT INTO `usuario_tipo_menu` VALUES ('10198', '1', 'Cadastrar Áreas');
INSERT INTO `usuario_tipo_menu` VALUES ('10199', '1', 'Gerenciar Áreas');
INSERT INTO `usuario_tipo_menu` VALUES ('10200', '1', 'Parâmetros de Processo');
INSERT INTO `usuario_tipo_menu` VALUES ('10201', '1', 'Parâmetros de Processo ');
INSERT INTO `usuario_tipo_menu` VALUES ('10202', '1', 'Cadastrar Parâmetros');
INSERT INTO `usuario_tipo_menu` VALUES ('10203', '1', 'Gerenciar Parâmetros');
INSERT INTO `usuario_tipo_menu` VALUES ('10204', '1', 'Parâmetros de Processo  ');
INSERT INTO `usuario_tipo_menu` VALUES ('10205', '1', 'Cadastrar Grupo');
INSERT INTO `usuario_tipo_menu` VALUES ('10206', '1', 'Gerenciar Grupos');
INSERT INTO `usuario_tipo_menu` VALUES ('10207', '1', 'Paradas');
INSERT INTO `usuario_tipo_menu` VALUES ('10208', '1', 'Paradas');
INSERT INTO `usuario_tipo_menu` VALUES ('10209', '1', 'Cadastrar Parada');
INSERT INTO `usuario_tipo_menu` VALUES ('10210', '1', 'Visualizar Paradas');
INSERT INTO `usuario_tipo_menu` VALUES ('10211', '1', 'Medições');
INSERT INTO `usuario_tipo_menu` VALUES ('10212', '1', 'Nova Medição');
INSERT INTO `usuario_tipo_menu` VALUES ('10213', '1', 'Medição Comparativa');
INSERT INTO `usuario_tipo_menu` VALUES ('10214', '1', 'Importar Dados');
INSERT INTO `usuario_tipo_menu` VALUES ('10215', '1', 'Acompanhamento');
INSERT INTO `usuario_tipo_menu` VALUES ('10216', '1', 'Sistema');
INSERT INTO `usuario_tipo_menu` VALUES ('10217', '1', 'Backup do Banco de Dados');
INSERT INTO `usuario_tipo_menu` VALUES ('10218', '1', 'Restaurar um Backup do Banco de Dados');
INSERT INTO `usuario_tipo_menu` VALUES ('10219', '1', 'Fazer backup do Banco de Dados');
INSERT INTO `usuario_tipo_menu` VALUES ('10220', '1', 'Gerenciar Rotinas de Backup Automático');
INSERT INTO `usuario_tipo_menu` VALUES ('10221', '1', 'Textos');
INSERT INTO `usuario_tipo_menu` VALUES ('10222', '1', 'Alterar Texto do Estudo');
INSERT INTO `usuario_tipo_menu` VALUES ('10223', '1', 'Alterar Texto do Termo de Consentimento');
INSERT INTO `usuario_tipo_menu` VALUES ('10224', '1', 'Glossário (Termos)');
INSERT INTO `usuario_tipo_menu` VALUES ('10225', '1', 'Cadastrar Termo');
INSERT INTO `usuario_tipo_menu` VALUES ('10226', '1', 'Gerenciar Glossário');
INSERT INTO `usuario_tipo_menu` VALUES ('10227', '1', 'Outras Ações');
INSERT INTO `usuario_tipo_menu` VALUES ('10228', '1', 'Gerar Mapa de Mobilização');
INSERT INTO `usuario_tipo_menu` VALUES ('10229', '1', 'Áreas Participantes');
INSERT INTO `usuario_tipo_menu` VALUES ('10230', '1', 'Outras Ações');
INSERT INTO `usuario_tipo_menu` VALUES ('10231', '1', 'Configurações');
INSERT INTO `usuario_tipo_menu` VALUES ('10232', '1', 'Relatório de Erros');
INSERT INTO `usuario_tipo_menu` VALUES ('10233', '1', 'Análise Retroativa (Estudo)');
INSERT INTO `usuario_tipo_menu` VALUES ('10234', '1', 'Assistente para Novo Projeto');
INSERT INTO `usuario_tipo_menu` VALUES ('10235', '1', 'Usuários');
INSERT INTO `usuario_tipo_menu` VALUES ('10236', '1', 'Usuários');
INSERT INTO `usuario_tipo_menu` VALUES ('10237', '1', 'Cadastrar Usuário do Sistema');
INSERT INTO `usuario_tipo_menu` VALUES ('10238', '1', 'Gerenciar Usuários do Sistema');
INSERT INTO `usuario_tipo_menu` VALUES ('10239', '1', 'Visualizar Operações de Usuários no sistema');
INSERT INTO `usuario_tipo_menu` VALUES ('10240', '1', 'Classes de Usuários');
INSERT INTO `usuario_tipo_menu` VALUES ('10241', '1', 'Cadastrar Classe de Usuário');
INSERT INTO `usuario_tipo_menu` VALUES ('10242', '1', 'Gerenciar Classes de Usuário');
INSERT INTO `usuario_tipo_menu` VALUES ('10243', '1', 'Sugestões');
INSERT INTO `usuario_tipo_menu` VALUES ('10244', '1', 'Cadastrar Sugestão');
INSERT INTO `usuario_tipo_menu` VALUES ('10245', '1', 'Visualizar Sugestões');
INSERT INTO `usuario_tipo_menu` VALUES ('10246', '1', 'Cadastros');
INSERT INTO `usuario_tipo_menu` VALUES ('10247', '1', 'Cadastros Gerais');
INSERT INTO `usuario_tipo_menu` VALUES ('10248', '1', 'Cadastros Gerais');
INSERT INTO `usuario_tipo_menu` VALUES ('10249', '1', 'Cargos');
INSERT INTO `usuario_tipo_menu` VALUES ('10250', '1', 'Cadastrar Cargos');
INSERT INTO `usuario_tipo_menu` VALUES ('10251', '1', 'Gerenciar Cargos');
INSERT INTO `usuario_tipo_menu` VALUES ('10252', '1', 'Estados Civis');
INSERT INTO `usuario_tipo_menu` VALUES ('10253', '1', 'Cadastrar Estado Civil');
INSERT INTO `usuario_tipo_menu` VALUES ('10254', '1', 'Gerenciar Estados Civis');
INSERT INTO `usuario_tipo_menu` VALUES ('10255', '1', 'Escolaridade');
INSERT INTO `usuario_tipo_menu` VALUES ('10256', '1', 'Cadastrar Escolaridade');
INSERT INTO `usuario_tipo_menu` VALUES ('10257', '1', 'Gerenciar Escolaridades');
INSERT INTO `usuario_tipo_menu` VALUES ('10258', '1', 'Cadastros Gerais  ');
INSERT INTO `usuario_tipo_menu` VALUES ('10259', '1', 'Faixas Salariais');
INSERT INTO `usuario_tipo_menu` VALUES ('10260', '1', 'Cadastrar Faixa Salarial');
INSERT INTO `usuario_tipo_menu` VALUES ('10261', '1', 'Gerenciar Faixas Salariais');
INSERT INTO `usuario_tipo_menu` VALUES ('10262', '1', 'Gênero (Sexo)');
INSERT INTO `usuario_tipo_menu` VALUES ('10263', '1', 'Cadastrar Gênero (Sexo)');
INSERT INTO `usuario_tipo_menu` VALUES ('10264', '1', 'Gerenciar Gênero (Sexo)');
INSERT INTO `usuario_tipo_menu` VALUES ('10265', '1', 'Legendas do Organograma');
INSERT INTO `usuario_tipo_menu` VALUES ('10266', '1', 'Cadastrar Legenda');
INSERT INTO `usuario_tipo_menu` VALUES ('10267', '1', 'Gerenciar Legendas');
INSERT INTO `usuario_tipo_menu` VALUES ('10268', '1', 'Cadastros Gerais   ');
INSERT INTO `usuario_tipo_menu` VALUES ('10269', '1', 'Linhas');
INSERT INTO `usuario_tipo_menu` VALUES ('10270', '1', 'Cadastrar Linha');
INSERT INTO `usuario_tipo_menu` VALUES ('10271', '1', 'Gerenciar Linhas');
INSERT INTO `usuario_tipo_menu` VALUES ('10272', '1', 'Locais de Trabalho');
INSERT INTO `usuario_tipo_menu` VALUES ('10273', '1', 'Cadastrar Local de Trabalho');
INSERT INTO `usuario_tipo_menu` VALUES ('10274', '1', 'Gerenciar Locais de Trabalho');
INSERT INTO `usuario_tipo_menu` VALUES ('10275', '1', 'Motivos de Desassociação');
INSERT INTO `usuario_tipo_menu` VALUES ('10276', '1', 'Cadastrar Motivo de Desassociação');
INSERT INTO `usuario_tipo_menu` VALUES ('10277', '1', 'Gerenciar Motivos de Desassociação');
INSERT INTO `usuario_tipo_menu` VALUES ('10278', '1', 'Cadastros Gerais    ');
INSERT INTO `usuario_tipo_menu` VALUES ('10279', '1', 'Nível Funcional na Hierarquia');
INSERT INTO `usuario_tipo_menu` VALUES ('10280', '1', 'Cadastrar Nível');
INSERT INTO `usuario_tipo_menu` VALUES ('10281', '1', 'Gerenciar Níveis');
INSERT INTO `usuario_tipo_menu` VALUES ('10282', '1', 'Países');
INSERT INTO `usuario_tipo_menu` VALUES ('10283', '1', 'Cadastrar País');
INSERT INTO `usuario_tipo_menu` VALUES ('10284', '1', 'Gerenciar Países');
INSERT INTO `usuario_tipo_menu` VALUES ('10285', '1', 'Status do Processo dos PVs');
INSERT INTO `usuario_tipo_menu` VALUES ('10286', '1', 'Cadastrar Status');
INSERT INTO `usuario_tipo_menu` VALUES ('10287', '1', 'Gerenciar Status');
INSERT INTO `usuario_tipo_menu` VALUES ('10288', '1', 'Cadastros Gerais     ');
INSERT INTO `usuario_tipo_menu` VALUES ('10289', '1', 'Unidades de Medida');
INSERT INTO `usuario_tipo_menu` VALUES ('10290', '1', 'Cadastrar Unidades');
INSERT INTO `usuario_tipo_menu` VALUES ('10291', '1', 'Gerenciar Unidades');
INSERT INTO `usuario_tipo_menu` VALUES ('10292', '1', 'Unidades Federativas');
INSERT INTO `usuario_tipo_menu` VALUES ('10293', '1', 'Cadastrar Estado');
INSERT INTO `usuario_tipo_menu` VALUES ('10294', '1', 'Gerenciar Estados');
INSERT INTO `usuario_tipo_menu` VALUES ('10295', '1', 'Nomes dos Níveis Estrutura');
INSERT INTO `usuario_tipo_menu` VALUES ('10296', '1', 'Cadastrar Nome');
INSERT INTO `usuario_tipo_menu` VALUES ('10297', '1', 'Gerenciar Nomes');
INSERT INTO `usuario_tipo_menu` VALUES ('10298', '1', 'Cadastros Gerais      ');
INSERT INTO `usuario_tipo_menu` VALUES ('10299', '1', 'Rede Relacional');
INSERT INTO `usuario_tipo_menu` VALUES ('10300', '1', 'Cadastrar Tipo de Relação');
INSERT INTO `usuario_tipo_menu` VALUES ('10301', '1', 'Gerenciar Tipos de Relação');
INSERT INTO `usuario_tipo_menu` VALUES ('10302', '1', 'Cadastrar Tipo de Relação entre Colegas de Trabalho');
INSERT INTO `usuario_tipo_menu` VALUES ('10303', '1', 'Gerenciar Tipos de Relação entre Colegas de Trabalho');
INSERT INTO `usuario_tipo_menu` VALUES ('10304', '1', 'Empresas');
INSERT INTO `usuario_tipo_menu` VALUES ('10305', '1', 'Cadastrar Empresa');
INSERT INTO `usuario_tipo_menu` VALUES ('10306', '1', 'Gerenciar Empresas');
INSERT INTO `usuario_tipo_menu` VALUES ('10307', '1', 'Sincronização');
INSERT INTO `usuario_tipo_menu` VALUES ('10308', '1', 'Sincronização');
INSERT INTO `usuario_tipo_menu` VALUES ('10309', '1', 'Sincronização ');
INSERT INTO `usuario_tipo_menu` VALUES ('10310', '1', 'Importar Folha de Pagamento');
INSERT INTO `usuario_tipo_menu` VALUES ('10311', '1', 'Importar Cargos');
INSERT INTO `usuario_tipo_menu` VALUES ('10312', '1', 'Importar Niveis/Estrutura Org.');
INSERT INTO `usuario_tipo_menu` VALUES ('10313', '1', 'Sincronização  ');
INSERT INTO `usuario_tipo_menu` VALUES ('10314', '1', 'Importar Dados Gerais');
INSERT INTO `usuario_tipo_menu` VALUES ('10315', '1', 'Importar Vagas');
INSERT INTO `usuario_tipo_menu` VALUES ('10316', '1', 'Importar Dados Pessoais');
INSERT INTO `usuario_tipo_menu` VALUES ('10317', '1', 'Sincronização   ');
INSERT INTO `usuario_tipo_menu` VALUES ('10318', '1', 'Importar Experiências Práticas');
INSERT INTO `usuario_tipo_menu` VALUES ('10319', '1', 'Importar Associações Pessoa-Vaga');
INSERT INTO `usuario_tipo_menu` VALUES ('10320', '1', 'Importar Pedidos de Vaga');
INSERT INTO `usuario_tipo_menu` VALUES ('10321', '1', 'Sincronização     ');
INSERT INTO `usuario_tipo_menu` VALUES ('10322', '1', 'Importar Padrões de Níveis de Similaridade');
INSERT INTO `usuario_tipo_menu` VALUES ('10323', '1', 'Importar Planejamento de Treinamentos');

-- ----------------------------
-- Table structure for `usuario_tipo_privilegio`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_tipo_privilegio`;
CREATE TABLE `usuario_tipo_privilegio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_tipo_id_INT` int(11) NOT NULL,
  `identificador_funcionalidade` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuario_tipo_privilegio
-- ----------------------------
