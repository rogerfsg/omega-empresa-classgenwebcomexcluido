/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50524
Source Host           : localhost:3306
Source Database       : smartmilhas

Target Server Type    : MYSQL
Target Server Version : 50524
File Encoding         : 65001

Date: 2017-05-11 19:14:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `accesses_promotion`
-- ----------------------------
DROP TABLE IF EXISTS `accesses_promotion`;
CREATE TABLE `accesses_promotion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `promotion_id` int(11) DEFAULT NULL,
  `ip` varchar(30) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `access_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of accesses_promotion
-- ----------------------------

-- ----------------------------
-- Table structure for `accounts`
-- ----------------------------
DROP TABLE IF EXISTS `accounts`;
CREATE TABLE `accounts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) unsigned DEFAULT NULL,
  `bank_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `agency` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `account` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of accounts
-- ----------------------------

-- ----------------------------
-- Table structure for `addresses`
-- ----------------------------
DROP TABLE IF EXISTS `addresses`;
CREATE TABLE `addresses` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) unsigned DEFAULT NULL,
  `zip_code` varchar(20) DEFAULT NULL,
  `address` varchar(300) DEFAULT NULL,
  `number` varchar(20) DEFAULT NULL,
  `neighborhood` varchar(50) DEFAULT NULL,
  `complement` varchar(100) DEFAULT NULL,
  `city_id` int(11) unsigned DEFAULT NULL,
  `ip` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of addresses
-- ----------------------------
INSERT INTO `addresses` VALUES ('1', '1', '2222222', '22222', '22222', '2222', '2222', '1', null, '2013-11-05 15:53:50', null);
INSERT INTO `addresses` VALUES ('2', '2', 'asdsad', 'asdsad', 'qweqwe', 'qqwewqeq', 'qweqwe', null, null, '2013-11-06 14:38:07', '2013-11-06 14:38:07');
INSERT INTO `addresses` VALUES ('3', '2', 'qweqwewqe', 'qwewqe', 'wqeqwe', 'qwewqe', 'qwewqe', null, null, '2013-11-06 14:38:07', '2013-11-06 14:38:07');
INSERT INTO `addresses` VALUES ('4', '3', 'teste', 'teste', 'teste', 'teste', 'teste', '1', null, '2013-11-06 19:05:06', '2013-11-06 19:05:06');
INSERT INTO `addresses` VALUES ('5', '3', 'teste', 'teste', 'teste', 'teste', 'teste', '1', null, '2013-11-06 19:05:06', '2013-11-06 19:05:06');
INSERT INTO `addresses` VALUES ('6', '4', null, null, null, null, null, '2418', null, '2014-01-19 19:47:21', '2014-01-19 19:47:21');
INSERT INTO `addresses` VALUES ('7', '5', null, null, null, null, null, '1565', null, '2014-01-19 21:34:14', '2014-01-19 21:34:14');
INSERT INTO `addresses` VALUES ('8', '6', null, null, null, null, null, '4414', null, '2014-01-19 21:45:14', '2014-01-19 21:45:14');
INSERT INTO `addresses` VALUES ('9', '7', null, null, null, null, null, '1568', null, '2014-01-19 21:49:51', '2014-01-19 21:49:51');
INSERT INTO `addresses` VALUES ('10', '8', null, null, null, null, null, '2419', null, '2014-01-20 18:34:05', '2014-01-20 18:34:05');
INSERT INTO `addresses` VALUES ('11', '16', null, null, null, null, null, '883', null, '2014-01-28 12:49:47', '2014-01-28 12:49:47');
INSERT INTO `addresses` VALUES ('12', '17', null, null, null, null, null, '883', null, '2014-01-29 21:44:16', '2014-01-29 21:44:16');
INSERT INTO `addresses` VALUES ('13', '18', null, null, null, null, null, '883', null, '2014-01-30 19:01:41', '2014-01-30 19:01:41');

-- ----------------------------
-- Table structure for `airports`
-- ----------------------------
DROP TABLE IF EXISTS `airports`;
CREATE TABLE `airports` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `acronym` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `nick` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `city` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `management` int(11) unsigned DEFAULT NULL,
  `azul` tinyint(1) DEFAULT NULL,
  `gol` tinyint(1) DEFAULT NULL,
  `gol_parceiros` tinyint(1) DEFAULT NULL,
  `tam` tinyint(1) DEFAULT NULL,
  `show` tinyint(1) DEFAULT NULL,
  `category` int(1) DEFAULT NULL,
  `combination` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `tam_comercial_fare_money` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `tam_comercial_fare_miles` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=260 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of airports
-- ----------------------------
INSERT INTO `airports` VALUES ('1', 'USH', 'Ushuaia (USH)', '', '', '', 'AR', '3', '0', '0', '0', '1', '0', '0', 'USH', 'JJINTECO', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('2', 'SLA', 'Salta (SLA)', '', '', '', 'AR', '3', '0', '0', '0', '1', '0', '0', 'SLA', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('3', 'RGL', 'Rio Gallegos (RGL)', '', '', '', 'AR', '3', '0', '0', '0', '1', '0', '0', 'RGL', 'JJINTECO', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('4', 'MDZ', 'Mendoza (MDZ)', '', '', '', 'AR', '3', '0', '0', '0', '1', '0', '0', 'MDZ', 'JJINTECO', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('5', 'FTE', 'El Calafate (FTE)', '', '', '', 'AR', '3', '0', '0', '0', '1', '0', '0', 'FTE', 'JJINTECO', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('6', 'COR', 'Cordoba (COR)', '', '', '', 'AR', '3', '0', '1', '0', '1', '0', '0', 'COR', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('7', 'CRD', 'Comodoro Rivadavia (CRD)', '', '', '', 'AR', '3', '0', '0', '0', '1', '0', '0', 'CRD', 'JJINTECO', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('8', 'EZE', 'Buenos Aires   Ezeiza Intl (EZE)', 'Buenos Aires', 'Buenos Aires', '', 'AR', '3', '0', '1', '0', '1', '0', '0', 'EZE', 'JJINTECO', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('9', 'BUE', 'Buenos Aires   Todos (BUE)', '', '', '', 'AR', '3', '0', '1', '0', '1', '0', '0', 'BUE,EZE,AEP', 'JJINTECO', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('10', 'AEP', 'Buenos Aires   Aeroparque (AEP)', 'Buenos Aires - Aeroparque', '', '', 'AR', '3', '0', '1', '0', '1', '0', '0', 'AEP', 'JJINTECO', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('11', 'VIE', 'Vienna (VIE)', '', '', '', 'AT', '3', '0', '0', '0', '1', '0', '0', 'VIE', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('12', 'BRU', 'Bruxelas (BRU)', '', '', '', 'BE', '3', '0', '0', '0', '1', '0', '0', 'BRU', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('13', 'VVI', 'Santa Cruz de la Sierra (VVI)', 'Santa Cruz de la Sierra', 'Santa Cruz de la Sierra', '', 'BO', '3', '0', '1', '0', '1', '0', '0', 'VVI', 'JJINTECO', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('14', 'FBE', 'Francisco Beltrao (FBE)', 'Francisco Beltrao', 'Francisco Beltrao', 'PR', 'BR', '1', '0', '0', '0', '0', '0', '4', 'FBE', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('15', 'VDC', 'Vitoria da Conquista (VDC)', 'Vitoria da Conquista', 'Vitoria Conquista', 'BA', 'BR', '1', '1', '0', '0', '0', '0', '3', 'VDC', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('16', 'VIX', 'Vitoria (VIX)', 'Vitoria', 'Vitoria', 'ES', 'BR', '1', '1', '1', '0', '1', '0', '2', 'VIX', '', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('17', 'BVH', 'Vilhena (BVH)', 'Vilhena', 'Vilhena', 'RO', 'BR', '1', '1', '0', '0', '0', '0', '4', 'BVH', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('18', 'URG', 'Uruguaiana (URG)', 'Uruguaiana', 'Uruguaiana', 'RS', 'BR', '1', '0', '0', '0', '0', '0', '3', 'URG', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('19', 'UNA', 'Comandatuba (UNA)', 'Comandatuba', 'Comandatuba', 'BA', 'BR', '1', '0', '0', '0', '0', '0', '0', 'UNA', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('20', 'UDI', 'Uberlandia (UDI)', 'Uberlandia', 'Uberlandia', 'MG', 'BR', '1', '1', '1', '0', '1', '0', '2', 'UDI', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('21', 'UBA', 'Uberaba (UBA)', 'Uberaba', 'Uberaba', 'MG', 'BR', '1', '1', '1', '0', '0', '0', '2', 'UBA', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('22', 'TUR', 'Tucurui (TUR)', 'Tucurui', 'Tucurui', 'PA', 'BR', '1', '1', '0', '0', '0', '0', '0', 'TUR', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('23', 'THE', 'Teresina (THE)', 'Teresina', 'Teresina', 'PI', 'BR', '1', '1', '1', '0', '1', '0', '2', 'THE', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('24', 'OPS', 'Sinop (OPS)', 'Sinop', 'Sinop', 'MT', 'BR', '1', '1', '0', '0', '0', '0', '3', 'OPS', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('25', 'SAO', 'Sao Paulo - Todos (SAO)', '', '', 'SP', 'BR', '1', '0', '1', '0', '1', '0', '0', 'GRU,CGH,VCP,CPQ', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('26', 'VCP', 'Campinas   Viracopos (VCP)', 'Viracopos', 'Viracopos', 'SP', 'BR', '1', '1', '0', '0', '1', '0', '5', 'VCP,CPQ', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('27', 'GRU', 'Sao Paulo - Guarulhos Intl (GRU)', 'Guarulhos', 'Sao Paulo', 'SP', 'BR', '1', '1', '1', '0', '1', '0', '5', 'GRU', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('28', 'CGH', 'Sao Paulo - Congonhas (CGH)', 'Congonhas', 'Sao Paulo', 'SP', 'BR', '1', '1', '1', '0', '1', '0', '1', 'CGH', '', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('29', 'SLZ', 'Sao Luis (SLZ)', 'Sao Luis', 'Sao Luis', 'MA', 'BR', '1', '1', '1', '0', '1', '0', '1', 'SLZ', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('30', 'SJK', 'Sao Jose dos Campos (SJK)', 'Sao Jose dos Campos', 'Sao Jose dos Campos', 'SP', 'BR', '1', '1', '0', '0', '0', '0', '2', 'SJK', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('31', 'SJP', 'Sao Jose do Rio Preto (SJP)', 'Sao Jose do Rio Preto', 'Sao Jose do Rio Preto', 'SP', 'BR', '1', '1', '0', '0', '1', '0', '2', 'SJP', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('32', 'GEL', 'Santo Angelo (GEL)', 'Santo Angelo', 'Santo Angelo', 'RS', 'BR', '1', '0', '0', '0', '0', '0', '4', 'GEL', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('33', 'STM', 'Santarem (STM)', 'Santarem', 'Santarem', 'PA', 'BR', '1', '1', '1', '0', '1', '0', '2', 'STM', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('34', 'SRA', 'Santa Rosa (SRA)', 'Santa Rosa', 'Santa Rosa', 'RS', 'BR', '1', '0', '0', '0', '0', '0', '4', 'SRA', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('35', 'RIA', 'Santa Maria (RIA)', 'Santa Maria', 'Santa Maria', 'RS', 'BR', '1', '1', '0', '0', '0', '0', '2', 'RIA', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('36', 'SSA', 'Salvador (SSA)', 'Sao Salvador', 'Salvador', 'BA', 'BR', '1', '1', '1', '0', '1', '0', '1', 'SSA', '', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('37', 'ROO', 'Rondonopolis (ROO)', 'Rondonopolis', 'Rondonopolis', 'MT', 'BR', '1', '1', '0', '0', '0', '0', '3', 'ROO', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('38', 'RIG', 'Rio Grande (RIG)', 'Rio Grande', 'Rio Grande', 'RS', 'BR', '1', '0', '0', '0', '0', '0', '3', 'RIG', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('39', 'RIO', 'Rio de Janeiro - Todos (RIO)', '', 'Rio de Janeiro', 'RJ', 'BR', '1', '0', '1', '0', '1', '0', '0', 'RIO,SDU,GIG', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('40', 'SDU', 'Rio de Janeiro - Santos Dumont (SDU)', 'Santos Drumont', 'Rio de Janeiro', 'RJ', 'BR', '1', '1', '1', '0', '1', '0', '1', 'SDU', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('41', 'GIG', 'Rio de Janeiro - Galeao Intl (GIG)', 'Galeao', 'Rio de Janeiro', 'RJ', 'BR', '1', '1', '1', '0', '1', '0', '1', 'GIG', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('42', 'RBR', 'Rio Branco (RBR)', 'Rio Branco', 'Rio Branco', 'AC', 'BR', '1', '1', '1', '0', '1', '0', '2', 'RBR', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('43', 'RAO', 'Ribeirao Preto (RAO)', 'Ribeirao Preto', 'Ribeirao Preto', 'SP', 'BR', '1', '1', '0', '0', '1', '0', '2', 'RAO', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('44', 'REC', 'Recife (REC)', 'Recife', 'Recife', 'PE', 'BR', '1', '1', '1', '0', '1', '0', '1', 'REC', '', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('45', 'PPB', 'Presidente Prudente (PPB)', 'Presidente Prudente', 'Presidente Prudente', 'SP', 'BR', '1', '1', '1', '0', '0', '0', '3', 'PPB', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('46', 'PVH', 'Porto Velho (PVH)', 'Porto Velho', 'Porto Velho', 'RO', 'BR', '1', '1', '1', '0', '1', '0', '2', 'PVH', '', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('47', 'TMT', 'Trombetas (TMT)', 'Trombetas', 'Trombetas', 'PA', 'BR', '1', '1', '0', '0', '0', '0', '0', 'TMT', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('48', 'BPS', 'Porto Seguro (BPS)', 'Porto Seguro', 'Porto Seguro', 'BA', 'BR', '1', '1', '1', '0', '1', '0', '2', 'BPS', '', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('49', 'POA', 'Porto Alegre (POA)', 'Porto Alegre', 'Porto Alegre', 'RS', 'BR', '1', '1', '1', '0', '1', '0', '1', 'POA', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('50', 'PNZ', 'Petrolina (PNZ)', 'Petrolina', 'Petrolina', 'PE', 'BR', '1', '1', '1', '0', '0', '0', '2', 'PNZ', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('51', 'PET', 'Pelotas (PET)', 'Pelotas', 'Pelotas', 'RS', 'BR', '1', '1', '0', '0', '0', '0', '3', 'PET', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('52', 'PFB', 'Passo Fundo (PFB)', 'Passo Fundo', 'Passo Fundo', 'RS', 'BR', '1', '1', '0', '0', '0', '0', '4', 'PFB', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('53', 'PIN', 'Parintins (PIN)', 'Parintins', 'Parintins', 'AM', 'BR', '1', '1', '0', '0', '0', '0', '3', 'PIN', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('54', 'PMW', 'Palmas (PMW)', 'Palmas', 'Palmas', 'TO', 'BR', '1', '1', '1', '0', '1', '0', '2', 'PMW', '', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('55', 'NVT', 'Navegantes (NVT)', 'Navegantes', 'Navegantes', 'SC', 'BR', '1', '1', '1', '0', '1', '0', '2', 'NVT', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('56', 'NAT', 'Natal (NAT)', 'Natal', 'Natal', 'RN', 'BR', '1', '1', '1', '0', '1', '0', '1', 'NAT', '', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('57', 'MOC', 'Montes Claros (MOC)', 'Montes Claros', 'Montes Claros', 'MG', 'BR', '1', '1', '1', '0', '0', '0', '2', 'MOC', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('58', 'MQH', 'Minacu (MQH)', 'Minacu', 'Minacu', 'GO', 'BR', '1', '0', '0', '0', '0', '0', '6', 'MQH', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('59', 'MGF', 'Maringa (MGF)', 'Maringa', 'Maringa', 'PR', 'BR', '1', '1', '1', '0', '0', '0', '2', 'MGF', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('60', 'MII', 'Marilia (MII)', 'Marilia', 'Marilia', 'SP', 'BR', '1', '1', '0', '0', '0', '0', '3', 'MII', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('61', 'MAB', 'Maraba (MAB)', 'Maraba', 'Maraba', 'PA', 'BR', '1', '1', '1', '0', '1', '0', '2', 'MAB', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('62', 'MAO', 'Manaus (MAO)', 'Manaus', 'Manaus', 'AM', 'BR', '1', '1', '1', '0', '1', '0', '1', 'MAO', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('63', 'MCZ', 'Maceio (MCZ)', 'Maceio', 'Maceio', 'AL', 'BR', '1', '1', '1', '0', '1', '0', '1', 'MCZ', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('64', 'MCP', 'Macapa (MCP)', 'Macapa', 'Macapa', 'AP', 'BR', '1', '1', '1', '0', '1', '0', '2', 'MCP', '', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('65', 'MEA', 'Macae (MEA)', 'Macae', 'Macae', 'RJ', 'BR', '1', '1', '0', '0', '0', '0', '2', 'MEA', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('66', 'LDB', 'Londrina (LDB)', 'Londrina', 'Londrina', 'PA', 'BR', '1', '1', '1', '0', '1', '0', '2', 'LDB', '', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('67', 'LEC', 'Lencois Chapada Diamantina (LEC)', 'Lencois', 'Lencois', 'BA', 'BR', '1', '1', '0', '0', '0', '0', '3', 'LEC', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('68', 'JDF', 'Juiz de Fora (JDF)', 'Juiz de Fora', 'Juiz de Fora', 'MG', 'BR', '1', '1', '0', '0', '0', '0', '2', 'JDF', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('69', 'JOI', 'Joinville (JOI)', 'Joinville', 'Joinville', 'SC', 'BR', '1', '1', '1', '0', '1', '0', '2', 'JOI', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('70', 'JPA', 'Joao Pessoa (JPA)', 'Joao Pessoa', 'Joao Pessoa', 'PB', 'BR', '1', '1', '1', '0', '1', '0', '2', 'JPA', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('71', 'JCB', 'Joacaba (JCB)', 'Joacaba', 'Joacaba', 'SC', 'BR', '1', '0', '0', '0', '0', '0', '4', 'JCB', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('72', 'JPR', 'Ji-Parana (JPR)', 'Ji-Parana', 'Ji-Parana', 'RO', 'BR', '1', '1', '0', '0', '0', '0', '3', 'JPR', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('73', 'ITB', 'Itaituba (ITB)', 'Itaituba', 'Itaituba', 'PA', 'BR', '1', '1', '0', '0', '0', '0', '3', 'ITB', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('74', 'IPN', 'Ipatinga (IPN)', 'Ipatinga', 'Ipatinga', 'MG', 'BR', '1', '1', '0', '0', '0', '0', '3', 'IPN', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('75', 'IMP', 'Imperatriz (IMP)', 'Imperatriz', 'Imperatriz', 'MA', 'BR', '1', '0', '1', '0', '1', '0', '2', 'IMP', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('76', 'IOS', 'Ilheus (IOS)', 'Ilheus', 'Ilheus', 'BA', 'BR', '1', '1', '1', '0', '1', '0', '2', 'IOS', '', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('77', 'GPB', 'Guarapuava (GPB)', 'Guarapuava', 'Guarapuava', 'PR', 'BR', '1', '0', '0', '0', '0', '0', '4', 'GPB', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('78', 'GVR', 'Gov. Valadares (GVR)', 'Governador Valadares', 'Governador Valadares', 'MG', 'BR', '1', '1', '0', '0', '0', '0', '3', 'GVR', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('79', 'GYN', 'Goiania (GYN)', 'Goiania', 'Goiania', 'GO', 'BR', '1', '1', '1', '0', '1', '0', '2', 'GYN', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('80', 'FRC', 'Franca (FRC)', 'Franca', 'Franca', 'SP', 'BR', '1', '0', '0', '0', '0', '0', '3', 'FRC', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('81', 'IGU', 'Foz do Iguacu (IGU)', 'Foz do Iguacu', 'Foz do Iguacu', 'PR', 'BR', '1', '1', '1', '0', '1', '0', '2', 'IGU', '', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('82', 'FOR', 'Fortaleza (FOR)', 'Fortaleza', 'Fortaleza', 'CE', 'BR', '1', '1', '1', '0', '1', '0', '1', 'FOR', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('83', 'FLN', 'Florianopolis (FLN)', 'Florianopolis', 'Florianopolis', 'SC', 'BR', '1', '1', '1', '0', '1', '0', '1', 'FLN', '', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('84', 'FEN', 'Fernando de Noronha (FEN)', 'Fernando de Noronha', 'Fernando de Noronha', 'PE', 'BR', '1', '1', '1', '0', '0', '0', '3', 'FEN', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('85', 'ERM', 'Erechim (ERM)', 'Erechim', 'Erechim', 'RS', 'BR', '1', '0', '0', '0', '0', '0', '0', 'ERM', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('86', 'DOU', 'Dourados (DOU)', 'Dourados', 'Dourados', 'MS', 'BR', '1', '1', '1', '0', '0', '0', '0', 'DOU', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('87', 'CWB', 'Curitiba (CWB)', 'Curitiba', 'Curitiba', 'PR', 'BR', '1', '1', '1', '0', '1', '0', '1', 'CWB', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('88', 'CGB', 'Cuiaba (CGB)', 'Cuiaba', 'Cuiaba', 'MS', 'BR', '1', '1', '1', '0', '1', '0', '2', 'CGB', '', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('89', 'CCM', 'Criciuma (CCM)', 'Criciuma', 'Criciuma', 'SC', 'BR', '1', '1', '0', '0', '0', '0', '3', 'CCM', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('90', 'CMG', 'Corumba (CMG)', 'Corumba', 'Corumba', 'MS', 'BR', '1', '1', '0', '0', '0', '0', '2', 'CMG', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('91', 'XAP', 'Chapeco (XAP)', 'Chapeco', 'Chapeco', 'SC', 'BR', '1', '1', '1', '0', '0', '0', '3', 'XAP', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('92', 'CAC', 'Cascavel (CAC)', 'Cascavel', 'Cascavel', 'PR', 'BR', '1', '1', '0', '0', '0', '0', '3', 'CAC', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('93', 'CKS', 'Carajas (CKS)', 'Carajas', 'Carajas', 'PA', 'BR', '1', '1', '0', '0', '0', '0', '0', 'CKS', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('94', 'CGR', 'Campo Grande (CGR)', 'Campo Grande', 'Campo Grande', 'MS', 'BR', '1', '1', '1', '0', '1', '0', '4', 'CGR', '', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('95', 'OAL', 'Cacoal (OAL)', 'Cacoal', 'Cacoal', 'RO', 'BR', '1', '1', '0', '0', '0', '0', '0', 'OAL', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('96', 'CFC', 'Cacador (CFC)', 'Cacador', 'Cacador', 'SC', 'BR', '1', '0', '0', '0', '0', '0', '0', 'CFC', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('97', 'BSB', 'Brasilia (BSB)', 'Brasilia', 'Brasilia', 'DF', 'BR', '1', '1', '1', '0', '1', '0', '5', 'BSB', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('98', 'BYO', 'Bonito (BYO)', 'Bonito', 'Bonito', 'MS', 'BR', '1', '1', '0', '0', '0', '0', '3', 'BYO', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('99', 'BVB', 'Boa Vista (BVB)', 'Boa Vista', 'Boa Vista', 'RO', 'BR', '1', '1', '1', '0', '1', '0', '2', 'BVB', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('100', 'BHZ', 'Belo Horizonte - Todos (BHZ)', '', '', 'MG', 'BR', '1', '0', '1', '0', '1', '0', '0', 'BHZ,PLU,CNF', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('101', 'PLU', 'Belo Horizonte - Pampulha (PLU)', 'Pampulha', 'Belo Horizonte', 'MG', 'BR', '1', '1', '0', '0', '0', '0', '2', 'PLU', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('102', 'CNF', 'Belo Horizonte - Confins Intl (CNF)', 'Confins', 'Belo Horizonte', 'MG', 'BR', '1', '1', '1', '0', '1', '0', '1', 'CNF', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('103', 'BEL', 'Belem (BEL)', 'Val-de-Cans', 'Belem', 'PA', 'BR', '1', '1', '1', '0', '1', '0', '1', 'BEL', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('104', 'JTC', 'Bauru (JTC)', 'Bauru', 'Bauru', 'SP', 'BR', '1', '1', '1', '0', '0', '0', '3', 'JTC', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('105', 'BRA', 'Barreiras (BRA)', 'Barreiras', 'Barreiras', 'BA', 'BR', '1', '1', '0', '0', '0', '0', '4', 'BRA', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('106', 'AAX', 'Araxa (AAX)', 'Araxa', 'Araxa', 'MG', 'BR', '1', '1', '0', '0', '0', '0', '3', 'AAX', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('107', 'AQA', 'Araraquara (AQA)', 'Araraquara', 'Araraquara', 'SP', 'BR', '1', '0', '0', '0', '0', '0', '3', 'AQA', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('108', 'AUX', 'Araguaina (AUX)', 'Araguaina', 'Araguaina', 'TO', 'BR', '1', '1', '0', '0', '0', '0', '4', 'AUX', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('109', 'ARU', 'Aracatuba (ARU)', 'Aracatuba', 'Aracatuba', 'SP', 'BR', '1', '1', '1', '0', '0', '0', '3', 'ARU', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('110', 'AJU', 'Aracaju (AJU)', 'Aracaju', 'Santa Maria', 'SE', 'BR', '1', '1', '1', '0', '1', '0', '2', 'AJU', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('111', 'ATM', 'Altamira (ATM)', 'Altamira', 'Altamira', 'PA', 'BR', '1', '1', '0', '0', '0', '0', '2', 'ATM', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('112', 'AFL', 'Alta Floresta (AFL)', 'Alta Floresta', 'Alta Floresta', 'MT', 'BR', '1', '1', '0', '0', '0', '0', '3', 'AFL', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('113', 'TFF', 'Tefe (TFF)', 'Tefe', 'Tefe', 'AM', 'BR', '1', '1', '0', '0', '0', '0', '3', 'TFF', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('114', 'TBT', 'Tabatinga (TBT)', 'Tabatinga', 'Tabatinga', 'AM', 'BR', '1', '1', '0', '0', '0', '0', '2', 'TBT', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('115', 'RVD', 'Rio Verde (RVD)', 'Rio Verde', 'Rio Verde', 'GO', 'BR', '1', '1', '0', '0', '0', '0', '6', 'RVD', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('116', 'POJ', 'Patos de Minas (POJ)', 'Patos de Minas', 'Patos de Minas', 'MG', 'BR', '1', '1', '0', '0', '0', '0', '0', 'POJ', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('117', 'CZS', 'Cruzeiro do Sul (CZS)', 'Cruzeiro do Sul', 'Cruzeiro do Sul', 'AC', 'BR', '1', '0', '1', '0', '0', '0', '2', 'CZS', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('118', 'CAW', 'Campos dos Goytacazes (CAW)', 'Campos dos Goytacazes', 'Campos dos Goytacazes', 'RJ', 'BR', '1', '1', '0', '0', '0', '0', '2', 'CAW', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('119', 'VAG', 'Varginha (VAG)', '', 'Varginha', 'MG', 'BR', '0', '1', '0', '0', '0', '0', '0', 'VAG', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('120', 'YWG', 'Winnipeg (YWG)', '', 'Winnipeg', '', 'CA', '3', '0', '0', '0', '1', '0', '0', 'YWG', '', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('121', 'YVR', 'Vancouver (YVR)', '', 'Vancouver', '', 'CA', '3', '0', '0', '0', '1', '0', '0', 'YVR', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('122', 'YYZ', 'Toronto (YYZ)', '', 'Toronto', '', 'CA', '3', '0', '0', '0', '1', '0', '0', 'YYZ', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('123', 'YQB', 'Quebec (YQB)', '', 'Quebec', '', 'CA', '3', '0', '0', '0', '1', '0', '0', 'YQB', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('124', 'YOW', 'Ottawa (YOW)', '', 'Ottawa', '', 'CA', '3', '0', '0', '0', '1', '0', '0', 'YOW', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('125', 'YMQ', 'Montreal (YMQ)', '', 'Montreal', '', 'CA', '3', '0', '0', '0', '1', '0', '0', 'YMQ', '', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('126', 'YEG', 'Edmonton (YEG)', '', 'Edmonton', '', 'CA', '3', '0', '0', '0', '1', '0', '0', 'YEG', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('127', 'YYC', 'Calgary (YYC)', '', 'Calgary', '', 'CA', '3', '0', '0', '0', '1', '0', '0', 'YYC', '', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('128', 'YUL', 'Montreal (YUL)', '', 'Montreal', '', 'CA', '3', '0', '0', '0', '1', '0', '0', 'YUL', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('129', 'ZRH', 'Zurique (ZRH)', '', 'Zurique', '', 'CH', '3', '0', '0', '0', '1', '0', '0', 'ZRH', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('130', 'GVA', 'Geneva (GVA)', '', 'Geneva', '', 'CH', '3', '0', '0', '0', '1', '0', '0', 'GVA', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('131', 'ZAL', 'Valdivia (ZAL)', '', 'Valdivia', '', 'CL', '3', '0', '0', '0', '1', '0', '0', 'ZAL', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('132', 'ZCO', 'Temuco (ZCO)', '', 'Temuco', '', 'CL', '3', '0', '0', '0', '1', '0', '0', 'ZCO', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('133', 'SCL', 'Santiago (SCL)', '', 'Santiago', '', 'CL', '3', '0', '0', '0', '1', '0', '0', 'SCL', 'JJINTECO', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('134', 'PUQ', 'Punta Arenas (PUQ)', '', 'Punta Arenas', '', 'CL', '3', '0', '0', '0', '1', '0', '0', 'PUQ', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('135', 'PMC', 'Puerto Montt (PMC)', '', 'Puerto Montt', '', 'CL', '3', '0', '0', '0', '1', '0', '0', 'PMC', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('136', 'ZOS', 'Osorno (ZOS)', '', 'Osorno', '', 'CL', '3', '0', '0', '0', '1', '0', '0', 'ZOS', 'JJINTECO', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('137', 'LSC', 'La Serena (LSC)', '', 'La Serena', '', 'CL', '3', '0', '0', '0', '1', '0', '0', 'LSC', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('138', 'ESR', 'El Salvador (ESR)', '', 'El Salvador', '', 'CL', '3', '0', '0', '0', '1', '0', '0', 'ESR', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('139', 'CPO', 'Copiapo (CPO)', '', 'Copiapo', '', 'CL', '3', '0', '0', '0', '1', '0', '0', 'CPO', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('140', 'CCP', 'Concepcion (CCP)', '', 'Concepcion', '', 'CL', '3', '0', '0', '0', '1', '0', '0', 'CCP', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('141', 'CJC', 'Calama (CJC)', '', 'Calama', '', 'CL', '3', '0', '0', '0', '1', '0', '0', 'CJC', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('142', 'BBA', 'Balmaceda (BBA)', '', 'Balmaceda', '', 'CL', '3', '0', '0', '0', '1', '0', '0', 'BBA', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('143', 'ANF', 'Antofagasta (ANF)', '', 'Antofagasta', '', 'CL', '3', '0', '0', '0', '1', '0', '0', 'ANF', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('144', 'IQQ', 'Iquique (IQQ)', '', 'Iquique', '', 'CL', '3', '0', '0', '0', '1', '0', '0', 'IQQ', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('145', 'ARI', 'Arica (ARI)', '', 'Arica', '', 'CL', '3', '0', '0', '0', '1', '0', '0', 'ARI', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('146', 'PEK', 'Pequim (PEK)', '', 'Pequim', '', 'CN', '3', '0', '0', '0', '1', '0', '0', 'PEK', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('147', 'BOG', 'Bogota (BOG)', '', 'Bogota', '', 'CO', '3', '0', '0', '0', '1', '0', '0', 'BOG', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('148', 'STR', 'Stuttgart   Stuttgart Intl (STR)', '', '', '', 'DE', '3', '0', '0', '0', '1', '0', '0', 'STR', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('149', 'NUE', 'Nuremberg (NUE)', '', '', '', 'DE', '3', '0', '0', '0', '1', '0', '0', 'NUE', '', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('150', 'MUC', 'Munique (MUC)', '', '', '', 'DE', '3', '0', '0', '0', '1', '0', '0', 'MUC', '', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('151', 'HAJ', 'Hanover (HAJ)', '', '', '', 'DE', '3', '0', '0', '0', '1', '0', '0', 'HAJ', '', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('152', 'HAM', 'Hamburgo (HAM)', '', '', '', 'DE', '3', '0', '0', '0', '1', '0', '0', 'HAM', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('153', 'FRA', 'Frankfurt (FRA)', '', '', '', 'DE', '3', '0', '0', '0', '1', '0', '0', 'FRA', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('154', 'DUS', 'Dusseldorf (DUS)', '', '', '', 'DE', '3', '0', '0', '0', '1', '0', '0', 'DUS', '', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('155', 'BRE', 'Bremen (BRE)', '', '', '', 'DE', '3', '0', '0', '0', '1', '0', '0', 'BRE', '', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('156', 'BER', 'Berlin   Todos (BER)', '', '', '', 'DE', '3', '0', '0', '0', '0', '0', '0', 'BER,TXL', 'JJINTECO', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('157', 'TXL', 'Berlim   Berlin-Tegel Intl (TXL)', '', '', '', 'DE', '3', '0', '0', '0', '1', '0', '0', 'TXL', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('158', 'CPH', 'Copenhagen (CPH)', '', '', '', 'DK', '3', '0', '0', '0', '1', '0', '0', 'CPH', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('159', 'MAD', 'Madri   Barajas Intl (MAD)', '', '', '', 'ES', '3', '0', '0', '0', '1', '0', '0', 'MAD', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('160', 'BCN', 'Barcelona (BCN)', '', '', '', 'ES', '3', '0', '0', '0', '0', '0', '0', 'BCN', 'JJINTECO', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('161', 'BIO', 'Bilbao (BIO)', '', '', '', 'ES', '3', '0', '0', '0', '0', '0', '0', 'BIO', 'JJINTECO', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('162', 'AGP', 'Malaga (AGP)', '', '', '', 'ES', '3', '0', '0', '0', '0', '0', '0', 'AGP', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('163', 'SCQ', 'Santiago De Compostela (SCQ)', '', '', '', 'ES', '3', '0', '0', '0', '0', '0', '0', 'SCQ', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('164', 'TFN', 'Tenerife (TFN)', '', '', '', 'ES', '3', '0', '0', '0', '0', '0', '0', 'TFN', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('165', 'CDG', 'Paris   Charles de Gaulle Intl (CDG)', '', '', '', 'FR', '3', '0', '0', '0', '1', '0', '0', 'CDG', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('166', 'MAN', 'Manchester (MAN)', '', '', '', 'GB', '3', '0', '0', '0', '0', '0', '0', 'MAN', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('167', 'LHR', 'Londres   Heathrow Intl (LHR)', '', '', '', 'GB', '3', '0', '0', '0', '1', '0', '0', 'LHR', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('168', 'EDI', 'Edinburgo (EDI)', '', '', '', 'GB', '3', '0', '0', '0', '0', '0', '0', 'EDI', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('169', 'BFS', 'Belfast (BFS)', '', '', '', 'GB', '3', '0', '0', '0', '0', '0', '0', 'BFS', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('170', 'ABZ', 'Aberdeen (ABZ)', '', '', '', 'GB', '3', '0', '0', '0', '0', '0', '0', 'ABZ', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('171', 'DUB', 'Dublin (DUB)', '', '', '', 'IE', '3', '0', '0', '0', '0', '0', '0', 'DUB', 'JJINTECO', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('172', 'MXP', 'Milao   Malpensa Intl (MXP)', 'Milão-Malpensa', 'Milão', '', 'IT', '3', '0', '0', '0', '1', '0', '0', 'MXP', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('173', 'NRT', 'Toquio   Narita Intl (NRT)', '', '', '', 'JP', '3', '0', '0', '0', '1', '0', '0', 'NRT', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('174', 'MEX', 'Cidade do Mexico (MEX)', '', '', '', 'MX', '3', '0', '0', '0', '1', '0', '0', 'MEX', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('175', 'AMS', 'Amsterdam (AMS)', '', '', '', 'NE', '3', '0', '0', '0', '1', '0', '0', 'AMS', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('176', 'OSL', 'Oslo (OSL)', '', '', '', 'NO', '3', '0', '0', '0', '1', '0', '0', 'OSL', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('177', 'TRU', 'Trujillo (TRU)', '', '', '', 'PE', '3', '0', '0', '0', '1', '0', '0', 'TRU', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('178', 'TPP', 'Tarapoto (TPP)', '', '', '', 'PE', '3', '0', '0', '0', '1', '0', '0', 'TPP', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('179', 'TCQ', 'Tacna (TCQ)', '', '', '', 'PE', '3', '0', '0', '0', '1', '0', '0', 'TCQ', 'JJINTECO', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('180', 'PEM', 'Puerto Maldonado (PEM)', '', '', '', 'PE', '3', '0', '0', '0', '1', '0', '0', 'PEM', 'JJINTECO', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('181', 'PCL', 'Pucallpa (PCL)', '', '', '', 'PE', '3', '0', '0', '0', '1', '0', '0', 'PCL', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('182', 'PIU', 'Piura (PIU)', '', '', '', 'PE', '3', '0', '0', '0', '1', '0', '0', 'PIU', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('183', 'LIM', 'Lima (LIM)', '', '', '', 'PE', '3', '0', '0', '0', '1', '0', '0', 'LIM', 'JJINTECO', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('184', 'JUL', 'Juliaca (JUL)', '', '', '', 'PE', '3', '0', '0', '0', '1', '0', '0', 'JUL', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('185', 'CUZ', 'Cuzco (CUZ)', '', '', '', 'PE', '3', '0', '0', '0', '1', '0', '0', 'CUZ', 'JJINTECO', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('186', 'CIX', 'Chiclayo (CIX)', '', '', '', 'PE', '3', '0', '0', '0', '1', '0', '0', 'CIX', 'JJINTECO', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('187', 'AQP', 'Arequipa (AQP)', '', '', '', 'PE', '3', '0', '0', '0', '1', '0', '0', 'AQP', 'JJINTECO', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('188', 'IQT', 'Iquitos (IQT)', '', '', '', 'PE', '3', '0', '0', '0', '1', '0', '0', 'IQT', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('189', 'PXO', 'Porto Santo (PXO)', '', '', '', 'PT', '3', '0', '0', '0', '1', '0', '0', 'PXO', '', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('190', 'OPO', 'Porto (OPO)', '', '', '', 'PT', '3', '0', '0', '0', '1', '0', '0', 'OPO', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('191', 'LIS', 'Lisboa (LIS)', '', '', '', 'PT', '3', '0', '0', '0', '1', '0', '0', 'LIS', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('192', 'FNC', 'Funchal (FNC)', '', '', '', 'PT', '3', '0', '0', '0', '1', '0', '0', 'FNC', '', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('193', 'FAO', 'Faro (FAO)', '', '', '', 'PT', '3', '0', '0', '0', '1', '0', '0', 'FAO', '', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('194', 'AGT', 'Ciudad del Leste (AGT)', '', 'Ciudad del Este', '', 'PY', '3', '0', '0', '0', '1', '0', '0', 'AGT', 'JJINTECO', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('195', 'ASU', 'Assuncao (ASU)', '', '', '', 'PY', '3', '0', '1', '0', '1', '0', '0', 'ASU', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('196', 'STO', 'Estocolmo (STO)', '', '', '', 'SE', '3', '0', '0', '0', '1', '0', '0', 'STO', '', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('197', 'ARN', 'Estocolmo (ARN)', '', '', '', 'SE', '3', '0', '0', '0', '1', '0', '0', 'ARN', '', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('198', 'IAD', 'Washington   Dulles Intl (IAD)', '', '', '', 'US', '3', '0', '0', '0', '1', '0', '0', 'IAD', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('199', 'EGE', 'Eagle County-Vail (EGE)', '', '', '', 'US', '3', '0', '0', '0', '1', '0', '0', 'EGE', '', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('200', 'TPA', 'Tampa (TPA)', '', '', '', 'US', '3', '0', '0', '0', '1', '0', '0', 'TPA', '', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('201', 'SEA', 'Seattle   Tacoma Intl (SEA)', '', '', '', 'US', '3', '0', '0', '0', '1', '0', '0', 'SEA', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('202', 'SJC', 'San Jose (SJC)', '', '', '', 'US', '3', '0', '0', '0', '1', '0', '0', 'SJC', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('203', 'SFO', 'San Francisco (SFO)', '', '', '', 'US', '3', '0', '0', '0', '1', '0', '0', 'SFO', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('204', 'MYF', 'San Diego   Montgomery Field (MYF)', '', '', '', 'US', '3', '0', '0', '0', '0', '0', '0', 'MYF', 'JJINTECO', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('205', 'SLC', 'Salt Lake City (SLC)', '', '', '', 'US', '3', '0', '0', '0', '1', '0', '0', 'SLC', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('206', 'RDU', 'Raleigh Durham (RDU)', '', '', '', 'US', '3', '0', '0', '0', '1', '0', '0', 'RDU', '', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('207', 'PDX', 'Portland (PDX)', '', '', '', 'US', '3', '0', '1', '0', '1', '0', '0', 'PDX', '', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('208', 'PHX', 'Phoenix (PHX)', '', '', '', 'US', '3', '0', '0', '0', '1', '0', '0', 'PHX', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('209', 'MCO', 'Orlando (MCO)', '', '', '', 'US', '3', '0', '1', '0', '1', '0', '0', 'MCO', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('210', 'JFK', 'Nova Iorque   J.F. Kennedy Intl (JFK)', '', '', '', 'US', '3', '0', '0', '0', '1', '0', '0', 'JFK', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('211', 'MSP', 'Minneapolis (MSP)', '', '', '', 'US', '3', '0', '0', '0', '1', '0', '0', 'MSP', '', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('212', 'MIA', 'Miami   Miami Intl (MIA)', '', '', '', 'US', '3', '0', '1', '0', '1', '0', '0', 'MIA', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('213', 'LAX', 'Los Angeles (LAX)', '', '', '', 'US', '3', '0', '0', '0', '1', '0', '0', 'LAX', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('214', 'LAS', 'Las Vegas (LAS)', '', '', '', 'US', '3', '0', '0', '0', '1', '0', '0', 'LAS', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('215', 'JAX', 'Jacksonville (JAX)', '', '', '', 'US', '3', '0', '0', '0', '1', '0', '0', 'JAX', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('216', 'IAH', 'Houston   George Bush Intl (IAH)', '', '', '', 'US', '3', '0', '0', '0', '1', '0', '0', 'IAH', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('217', 'DEN', 'Denver (DEN)', '', '', '', 'US', '3', '0', '0', '0', '1', '0', '0', 'DEN', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('218', 'DFW', 'Dallas   Fort Worth Intl (DFW)', '', '', '', 'US', '3', '0', '0', '0', '1', '0', '0', 'DFW', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('219', 'CGF', 'Cleveland (CGF)', '', '', '', 'US', '3', '0', '0', '0', '0', '0', '0', 'CGF', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('220', 'CLE', 'Cleveland   Hopkins Intl (CLE)', '', '', '', 'US', '3', '0', '0', '0', '1', '0', '0', 'CLE', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('221', 'ORD', 'Chicago   O`Hare Intl (ORD)', '', '', '', 'US', '3', '0', '0', '0', '1', '0', '0', 'ORD', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('222', 'BOS', 'Boston   Logan Intl (BOS)', '', '', '', 'US', '3', '0', '0', '0', '1', '0', '0', 'BOS', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('223', 'AUS', 'Austin (AUS)', '', '', '', 'US', '3', '0', '0', '0', '1', '0', '0', 'AUS', '', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('224', 'ATL', 'Atlanta (ATL)', '', '', '', 'US', '3', '0', '0', '0', '1', '0', '0', 'ATL', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('225', 'PHL', 'Filadelfia (PHL)', '', '', '', 'US', '3', '0', '0', '0', '1', '0', '0', 'PHL', '', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('226', 'SAN', 'San Diego   San Diego Intl (SAN)', '', '', '', 'US', '3', '0', '0', '0', '1', '0', '0', 'SAN', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('227', 'DCA', 'Washington   Ronald Reagan National (DCA)', '', '', '', 'US', '3', '0', '0', '0', '1', '0', '0', 'DCA', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('228', 'LGA', 'Nova Iorque   La Guardia (LGA)', '', '', '', 'US', '3', '0', '0', '0', '1', '0', '0', 'LGA', 'JJINTECO', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('229', 'PDP', 'Punta Del Este (PDP)', '', '', '', 'UY', '3', '0', '0', '0', '1', '0', '0', 'PDP', 'JJINTECO', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('230', 'MVD', 'Montevideo (MVD)', '', '', '', 'UY', '3', '0', '1', '0', '1', '0', '0', 'MVD', 'JJINTECO', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('231', 'CCS', 'Caracas (CCS)', '', '', '', 'VE', '3', '0', '1', '0', '1', '0', '0', 'CCS', 'JJINTECO', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('232', 'AUA', 'Aruba (AUA)', '', '', '', 'AW', '3', '0', '1', '0', '0', '0', '0', 'AUA', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('233', 'BGI', 'Barbados (BGI)', '', '', '', 'BB', '3', '0', '1', '0', '0', '0', '0', 'BGI', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('234', 'CPV', 'Campina Grande (CPV)', '', '', '', 'BR', '1', '1', '1', '0', '0', '0', '2', 'CPV', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('235', 'CXJ', 'Caxias do Sul (CXJ)', '', '', '', 'BR', '1', '1', '1', '0', '0', '0', '3', 'CXJ', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('236', 'CUR', 'Curacao (CUR)', '', '', '', 'CW', '3', '0', '0', '0', '0', '0', '0', 'CUR', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('237', 'JDO', 'Juazeiro do Norte (JDO)', '', '', '', 'BR', '1', '1', '1', '0', '0', '0', '2', 'JDO', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('238', 'PTY', 'Panama City (PTY)', '', '', '', 'PA', '3', '0', '0', '0', '0', '0', '0', 'PTY', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('239', 'PUJ', 'Punta Cana  (PUJ)', '', '', '', 'DO', '3', '0', '1', '0', '1', '0', '0', 'PUJ', '', 'TAMAW3', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('240', 'ROS', 'Rosario (ROS)', '', '', '', 'AR', '3', '0', '1', '0', '1', '0', '0', 'ROS', '', 'TAMAWFLX', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('241', 'SDQ', 'Santo Domingo (SDQ)', '', '', '', 'DO', '3', '0', '1', '0', '0', '0', '0', 'SDQ', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('242', 'CPQ', 'Sao Paulo - Campinas (CPQ)', 'Viracopos', 'Campinas', 'SP', 'BR', '1', '0', '1', '0', '1', '0', '5', 'CPQ,VCP', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('243', 'CFB', 'Cabo Frio (CFB)', 'Cabo Frio', 'Cabo Frio', 'RJ', 'BR', '1', '1', '0', '0', '1', '0', '2', 'CFB', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('244', 'SBCJ', 'Aeroporto de Parauapenas', 'Carajas', '', '', '', '1', '0', '0', '0', '0', '0', '2', 'SBCJ', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('245', 'BAZ', 'Barcelos', 'Barcelos', '', '', '', '1', '1', '0', '0', '1', '0', '2', 'SBCJ', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('246', 'BAZ', 'Barcelos (BAZ)', 'Barcelos', 'Barcloes', 'AM', 'BR', '1', '1', '0', '0', '1', '0', '2', 'BAZ', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('247', 'CLV', 'Caldas Novas (CLV)', 'Caldas Novas', 'Caldas Novas', 'MG', 'BR', '1', '1', '0', '0', '0', '0', '2', 'CLV', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('248', 'CIZ', 'Coari (CIZ)', 'Coari', 'Coari', 'AM', 'BR', '1', '1', '0', '0', '0', '0', '2', 'CIZ', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('249', 'ERN', 'Eirunepe (ERN)', 'Eirunepe', 'Eirunepe', 'AM', 'BR', '1', '1', '0', '0', '0', '0', '2', 'ERN', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('250', 'FBA', 'Fonte Boa (FBA)', 'Fonte Boa', 'Fonte Boa', 'AM', 'BR', '1', '0', '0', '0', '0', '0', '2', 'FBA', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('251', 'HUW', 'Humaita (HUW)', 'Humaita', 'Humaita', 'AM', 'BR', '1', '1', '0', '0', '0', '0', '2', 'HUW', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('252', 'TVT', 'Porto Seguro - Terravista (TVT)', 'Porto Seguro - Terravista', 'Porto Seguro - Terravista', 'BA', 'BR', '1', '0', '0', '0', '0', '0', '2', 'TVT', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('253', 'REZ', 'Resende (REZ)', 'Resende', 'Resende', 'RJ', 'BR', '1', '1', '0', '0', '0', '0', '2', 'REZ', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('254', 'IRZ', 'Santa Isabel do Rio Negro (IRZ)', 'Santa Isabel do Rio Negro', 'Santa Isabel do Rio Negro', 'AM', 'BR', '1', '1', '0', '0', '0', '0', '2', 'IRZ', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('255', 'SJL', 'Sao Gabriel da Cachoeira (SJL)', 'Sao Gabriel da Cachoeira', 'Sao Gabriel da Cachoeira', 'AM', 'BR', '1', '1', '0', '0', '0', '0', '2', 'SJL', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('256', 'JDR', 'Sao Joao del-Rei (JDR)', 'Sao Joao del-Rei', 'Sao Joao del-Rei', 'MG', 'BR', '1', '1', '0', '0', '0', '0', '2', 'JDR', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('257', 'OLC', 'Sao Paulo de Olivenca (OLC)', 'Sao Paulo de Olivenca', 'Sao Paulo de Olivenca', 'AM', 'BR', '1', '1', '0', '0', '0', '0', '2', 'OLC', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('258', 'VAG', 'Varginha (VAG)', 'Varginha', 'Varginha', 'MG', 'BR', '1', '1', '0', '0', '0', '0', '2', 'VAG', '', '', '2013-10-29 02:07:41', null);
INSERT INTO `airports` VALUES ('259', 'DTW', 'Aeroporto de Detroit', 'Detroit', '', '', '', '3', '0', '0', '0', '0', '0', '0', 'DTW', '', '', '2013-10-29 02:07:41', null);

-- ----------------------------
-- Table structure for `banks`
-- ----------------------------
DROP TABLE IF EXISTS `banks`;
CREATE TABLE `banks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `country_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `number` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of banks
-- ----------------------------

-- ----------------------------
-- Table structure for `cards`
-- ----------------------------
DROP TABLE IF EXISTS `cards`;
CREATE TABLE `cards` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) unsigned DEFAULT NULL,
  `flag` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `number` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `validate` datetime DEFAULT NULL,
  `name` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `checker` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of cards
-- ----------------------------

-- ----------------------------
-- Table structure for `carriers`
-- ----------------------------
DROP TABLE IF EXISTS `carriers`;
CREATE TABLE `carriers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of carriers
-- ----------------------------
INSERT INTO `carriers` VALUES ('1', 'OI', '31', '2013-11-05 15:57:26', null);

-- ----------------------------
-- Table structure for `cities`
-- ----------------------------
DROP TABLE IF EXISTS `cities`;
CREATE TABLE `cities` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `state_id` int(11) unsigned DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5565 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cities
-- ----------------------------
INSERT INTO `cities` VALUES ('1', 'Afonso Cláudio', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2', 'Água Doce do Norte', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3', 'Águia Branca', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4', 'Alegre', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5', 'Alfredo Chaves', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('6', 'Alto Rio Novo', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('7', 'Anchieta', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('8', 'Apiacá', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('9', 'Aracruz', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('10', 'Atilio Vivacqua', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('11', 'Baixo Guandu', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('12', 'Barra de São Francisco', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('13', 'Boa Esperança', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('14', 'Bom Jesus do Norte', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('15', 'Brejetuba', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('16', 'Cachoeiro de Itapemirim', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('17', 'Cariacica', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('18', 'Castelo', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('19', 'Colatina', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('20', 'Conceição da Barra', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('21', 'Conceição do Castelo', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('22', 'Divino de São Lourenço', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('23', 'Domingos Martins', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('24', 'Dores do Rio Preto', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('25', 'Ecoporanga', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('26', 'Fundão', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('27', 'Governador Lindenberg', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('28', 'Guaçuí', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('29', 'Guarapari', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('30', 'Ibatiba', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('31', 'Ibiraçu', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('32', 'Ibitirama', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('33', 'Iconha', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('34', 'Irupi', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('35', 'Itaguaçu', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('36', 'Itapemirim', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('37', 'Itarana', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('38', 'Iúna', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('39', 'Jaguaré', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('40', 'Jerônimo Monteiro', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('41', 'João Neiva', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('42', 'Laranja da Terra', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('43', 'Linhares', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('44', 'Mantenópolis', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('45', 'Marataízes', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('46', 'Marechal Floriano', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('47', 'Marilândia', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('48', 'Mimoso do Sul', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('49', 'Montanha', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('50', 'Mucurici', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('51', 'Muniz Freire', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('52', 'Muqui', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('53', 'Nova Venécia', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('54', 'Pancas', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('55', 'Pedro Canário', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('56', 'Pinheiros', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('57', 'Piúma', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('58', 'Ponto Belo', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('59', 'Presidente Kennedy', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('60', 'Rio Bananal', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('61', 'Rio Novo do Sul', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('62', 'Santa Leopoldina', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('63', 'Santa Maria de Jetibá', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('64', 'Santa Teresa', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('65', 'São Domingos do Norte', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('66', 'São Gabriel da Palha', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('67', 'São José do Calçado', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('68', 'São Mateus', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('69', 'São Roque do Canaã', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('70', 'Serra', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('71', 'Sooretama', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('72', 'Vargem Alta', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('73', 'Venda Nova do Imigrante', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('74', 'Viana', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('75', 'Vila Pavão', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('76', 'Vila Valério', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('77', 'Vila Velha', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('78', 'Vitória', '8', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('79', 'Acrelândia', '1', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('80', 'Assis Brasil', '1', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('81', 'Brasiléia', '1', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('82', 'Bujari', '1', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('83', 'Capixaba', '1', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('84', 'Cruzeiro do Sul', '1', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('85', 'Epitaciolândia', '1', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('86', 'Feijó', '1', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('87', 'Jordão', '1', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('88', 'Mâncio Lima', '1', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('89', 'Manoel Urbano', '1', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('90', 'Marechal Thaumaturgo', '1', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('91', 'Plácido de Castro', '1', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('92', 'Porto Acre', '1', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('93', 'Porto Walter', '1', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('94', 'Rio Branco', '1', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('95', 'Rodrigues Alves', '1', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('96', 'Santa Rosa do Purus', '1', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('97', 'Sena Madureira', '1', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('98', 'Senador Guiomard', '1', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('99', 'Tarauacá', '1', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('100', 'Xapuri', '1', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('101', 'Água Branca', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('102', 'Anadia', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('103', 'Arapiraca', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('104', 'Atalaia', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('105', 'Barra de Santo Antônio', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('106', 'Barra de São Miguel', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('107', 'Batalha', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('108', 'Belém', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('109', 'Belo Monte', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('110', 'Boca da Mata', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('111', 'Branquinha', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('112', 'Cacimbinhas', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('113', 'Cajueiro', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('114', 'Campestre', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('115', 'Campo Alegre', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('116', 'Campo Grande', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('117', 'Canapi', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('118', 'Capela', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('119', 'Carneiros', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('120', 'Chã Preta', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('121', 'Coité do Nóia', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('122', 'Colônia Leopoldina', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('123', 'Coqueiro Seco', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('124', 'Coruripe', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('125', 'Craíbas', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('126', 'Delmiro Gouveia', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('127', 'Dois Riachos', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('128', 'Estrela de Alagoas', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('129', 'Feira Grande', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('130', 'Feliz Deserto', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('131', 'Flexeiras', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('132', 'Girau do Ponciano', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('133', 'Ibateguara', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('134', 'Igaci', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('135', 'Igreja Nova', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('136', 'Inhapi', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('137', 'Jacaré dos Homens', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('138', 'Jacuípe', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('139', 'Japaratinga', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('140', 'Jaramataia', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('141', 'Jequiá da Praia', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('142', 'Joaquim Gomes', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('143', 'Jundiá', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('144', 'Junqueiro', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('145', 'Lagoa da Canoa', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('146', 'Limoeiro de Anadia', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('147', 'Maceió', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('148', 'Major Isidoro', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('149', 'Mar Vermelho', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('150', 'Maragogi', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('151', 'Maravilha', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('152', 'Marechal Deodoro', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('153', 'Maribondo', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('154', 'Mata Grande', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('155', 'Matriz de Camaragibe', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('156', 'Messias', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('157', 'Minador do Negrão', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('158', 'Monteirópolis', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('159', 'Murici', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('160', 'Novo Lino', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('161', 'Olho d`Água das Flores', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('162', 'Olho d`Água do Casado', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('163', 'Olho d`Água Grande', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('164', 'Olivença', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('165', 'Ouro Branco', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('166', 'Palestina', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('167', 'Palmeira dos Índios', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('168', 'Pão de Açúcar', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('169', 'Pariconha', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('170', 'Paripueira', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('171', 'Passo de Camaragibe', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('172', 'Paulo Jacinto', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('173', 'Penedo', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('174', 'Piaçabuçu', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('175', 'Pilar', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('176', 'Pindoba', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('177', 'Piranhas', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('178', 'Poço das Trincheiras', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('179', 'Porto Calvo', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('180', 'Porto de Pedras', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('181', 'Porto Real do Colégio', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('182', 'Quebrangulo', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('183', 'Rio Largo', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('184', 'Roteiro', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('185', 'Santa Luzia do Norte', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('186', 'Santana do Ipanema', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('187', 'Santana do Mundaú', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('188', 'São Brás', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('189', 'São José da Laje', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('190', 'São José da Tapera', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('191', 'São Luís do Quitunde', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('192', 'São Miguel dos Campos', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('193', 'São Miguel dos Milagres', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('194', 'São Sebastião', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('195', 'Satuba', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('196', 'Senador Rui Palmeira', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('197', 'Tanque d`Arca', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('198', 'Taquarana', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('199', 'Teotônio Vilela', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('200', 'Traipu', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('201', 'União dos Palmares', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('202', 'Viçosa', '2', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('203', 'Amapá', '4', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('204', 'Calçoene', '4', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('205', 'Cutias', '4', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('206', 'Ferreira Gomes', '4', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('207', 'Itaubal', '4', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('208', 'Laranjal do Jari', '4', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('209', 'Macapá', '4', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('210', 'Mazagão', '4', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('211', 'Oiapoque', '4', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('212', 'Pedra Branca do Amaparí', '4', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('213', 'Porto Grande', '4', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('214', 'Pracuúba', '4', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('215', 'Santana', '4', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('216', 'Serra do Navio', '4', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('217', 'Tartarugalzinho', '4', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('218', 'Vitória do Jari', '4', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('219', 'Alvarães', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('220', 'Amaturá', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('221', 'Anamã', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('222', 'Anori', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('223', 'Apuí', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('224', 'Atalaia do Norte', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('225', 'Autazes', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('226', 'Barcelos', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('227', 'Barreirinha', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('228', 'Benjamin Constant', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('229', 'Beruri', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('230', 'Boa Vista do Ramos', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('231', 'Boca do Acre', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('232', 'Borba', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('233', 'Caapiranga', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('234', 'Canutama', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('235', 'Carauari', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('236', 'Careiro', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('237', 'Careiro da Várzea', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('238', 'Coari', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('239', 'Codajás', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('240', 'Eirunepé', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('241', 'Envira', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('242', 'Fonte Boa', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('243', 'Guajará', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('244', 'Humaitá', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('245', 'Ipixuna', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('246', 'Iranduba', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('247', 'Itacoatiara', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('248', 'Itamarati', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('249', 'Itapiranga', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('250', 'Japurá', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('251', 'Juruá', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('252', 'Jutaí', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('253', 'Lábrea', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('254', 'Manacapuru', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('255', 'Manaquiri', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('256', 'Manaus', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('257', 'Manicoré', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('258', 'Maraã', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('259', 'Maués', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('260', 'Nhamundá', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('261', 'Nova Olinda do Norte', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('262', 'Novo Airão', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('263', 'Novo Aripuanã', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('264', 'Parintins', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('265', 'Pauini', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('266', 'Presidente Figueiredo', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('267', 'Rio Preto da Eva', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('268', 'Santa Isabel do Rio Negro', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('269', 'Santo Antônio do Içá', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('270', 'São Gabriel da Cachoeira', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('271', 'São Paulo de Olivença', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('272', 'São Sebastião do Uatumã', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('273', 'Silves', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('274', 'Tabatinga', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('275', 'Tapauá', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('276', 'Tefé', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('277', 'Tonantins', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('278', 'Uarini', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('279', 'Urucará', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('280', 'Urucurituba', '3', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('281', 'Abaíra', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('282', 'Abaré', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('283', 'Acajutiba', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('284', 'Adustina', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('285', 'Água Fria', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('286', 'Aiquara', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('287', 'Alagoinhas', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('288', 'Alcobaça', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('289', 'Almadina', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('290', 'Amargosa', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('291', 'Amélia Rodrigues', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('292', 'América Dourada', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('293', 'Anagé', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('294', 'Andaraí', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('295', 'Andorinha', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('296', 'Angical', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('297', 'Anguera', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('298', 'Antas', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('299', 'Antônio Cardoso', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('300', 'Antônio Gonçalves', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('301', 'Aporá', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('302', 'Apuarema', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('303', 'Araças', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('304', 'Aracatu', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('305', 'Araci', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('306', 'Aramari', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('307', 'Arataca', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('308', 'Aratuípe', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('309', 'Aurelino Leal', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('310', 'Baianópolis', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('311', 'Baixa Grande', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('312', 'Banzaê', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('313', 'Barra', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('314', 'Barra da Estiva', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('315', 'Barra do Choça', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('316', 'Barra do Mendes', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('317', 'Barra do Rocha', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('318', 'Barreiras', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('319', 'Barro Alto', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('320', 'Barro Preto (antigo Gov. Lomanto Jr.)', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('321', 'Barrocas', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('322', 'Belmonte', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('323', 'Belo Campo', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('324', 'Biritinga', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('325', 'Boa Nova', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('326', 'Boa Vista do Tupim', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('327', 'Bom Jesus da Lapa', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('328', 'Bom Jesus da Serra', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('329', 'Boninal', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('330', 'Bonito', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('331', 'Boquira', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('332', 'Botuporã', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('333', 'Brejões', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('334', 'Brejolândia', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('335', 'Brotas de Macaúbas', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('336', 'Brumado', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('337', 'Buerarema', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('338', 'Buritirama', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('339', 'Caatiba', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('340', 'Cabaceiras do Paraguaçu', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('341', 'Cachoeira', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('342', 'Caculé', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('343', 'Caém', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('344', 'Caetanos', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('345', 'Caetité', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('346', 'Cafarnaum', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('347', 'Cairu', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('348', 'Caldeirão Grande', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('349', 'Camacan', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('350', 'Camaçari', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('351', 'Camamu', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('352', 'Campo Alegre de Lourdes', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('353', 'Campo Formoso', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('354', 'Canápolis', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('355', 'Canarana', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('356', 'Canavieiras', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('357', 'Candeal', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('358', 'Candeias', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('359', 'Candiba', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('360', 'Cândido Sales', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('361', 'Cansanção', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('362', 'Canudos', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('363', 'Capela do Alto Alegre', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('364', 'Capim Grosso', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('365', 'Caraíbas', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('366', 'Caravelas', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('367', 'Cardeal da Silva', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('368', 'Carinhanha', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('369', 'Casa Nova', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('370', 'Castro Alves', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('371', 'Catolândia', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('372', 'Catu', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('373', 'Caturama', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('374', 'Central', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('375', 'Chorrochó', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('376', 'Cícero Dantas', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('377', 'Cipó', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('378', 'Coaraci', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('379', 'Cocos', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('380', 'Conceição da Feira', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('381', 'Conceição do Almeida', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('382', 'Conceição do Coité', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('383', 'Conceição do Jacuípe', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('384', 'Conde', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('385', 'Condeúba', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('386', 'Contendas do Sincorá', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('387', 'Coração de Maria', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('388', 'Cordeiros', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('389', 'Coribe', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('390', 'Coronel João Sá', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('391', 'Correntina', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('392', 'Cotegipe', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('393', 'Cravolândia', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('394', 'Crisópolis', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('395', 'Cristópolis', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('396', 'Cruz das Almas', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('397', 'Curaçá', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('398', 'Dário Meira', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('399', 'Dias d`Ávila', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('400', 'Dom Basílio', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('401', 'Dom Macedo Costa', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('402', 'Elísio Medrado', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('403', 'Encruzilhada', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('404', 'Entre Rios', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('405', 'Érico Cardoso', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('406', 'Esplanada', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('407', 'Euclides da Cunha', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('408', 'Eunápolis', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('409', 'Fátima', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('410', 'Feira da Mata', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('411', 'Feira de Santana', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('412', 'Filadélfia', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('413', 'Firmino Alves', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('414', 'Floresta Azul', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('415', 'Formosa do Rio Preto', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('416', 'Gandu', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('417', 'Gavião', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('418', 'Gentio do Ouro', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('419', 'Glória', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('420', 'Gongogi', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('421', 'Governador Mangabeira', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('422', 'Guajeru', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('423', 'Guanambi', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('424', 'Guaratinga', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('425', 'Heliópolis', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('426', 'Iaçu', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('427', 'Ibiassucê', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('428', 'Ibicaraí', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('429', 'Ibicoara', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('430', 'Ibicuí', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('431', 'Ibipeba', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('432', 'Ibipitanga', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('433', 'Ibiquera', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('434', 'Ibirapitanga', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('435', 'Ibirapuã', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('436', 'Ibirataia', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('437', 'Ibitiara', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('438', 'Ibititá', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('439', 'Ibotirama', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('440', 'Ichu', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('441', 'Igaporã', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('442', 'Igrapiúna', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('443', 'Iguaí', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('444', 'Ilhéus', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('445', 'Inhambupe', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('446', 'Ipecaetá', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('447', 'Ipiaú', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('448', 'Ipirá', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('449', 'Ipupiara', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('450', 'Irajuba', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('451', 'Iramaia', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('452', 'Iraquara', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('453', 'Irará', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('454', 'Irecê', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('455', 'Itabela', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('456', 'Itaberaba', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('457', 'Itabuna', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('458', 'Itacaré', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('459', 'Itaeté', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('460', 'Itagi', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('461', 'Itagibá', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('462', 'Itagimirim', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('463', 'Itaguaçu da Bahia', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('464', 'Itaju do Colônia', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('465', 'Itajuípe', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('466', 'Itamaraju', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('467', 'Itamari', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('468', 'Itambé', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('469', 'Itanagra', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('470', 'Itanhém', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('471', 'Itaparica', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('472', 'Itapé', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('473', 'Itapebi', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('474', 'Itapetinga', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('475', 'Itapicuru', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('476', 'Itapitanga', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('477', 'Itaquara', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('478', 'Itarantim', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('479', 'Itatim', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('480', 'Itiruçu', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('481', 'Itiúba', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('482', 'Itororó', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('483', 'Ituaçu', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('484', 'Ituberá', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('485', 'Iuiú', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('486', 'Jaborandi', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('487', 'Jacaraci', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('488', 'Jacobina', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('489', 'Jaguaquara', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('490', 'Jaguarari', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('491', 'Jaguaripe', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('492', 'Jandaíra', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('493', 'Jequié', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('494', 'Jeremoabo', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('495', 'Jiquiriçá', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('496', 'Jitaúna', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('497', 'João Dourado', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('498', 'Juazeiro', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('499', 'Jucuruçu', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('500', 'Jussara', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('501', 'Jussari', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('502', 'Jussiape', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('503', 'Lafaiete Coutinho', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('504', 'Lagoa Real', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('505', 'Laje', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('506', 'Lajedão', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('507', 'Lajedinho', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('508', 'Lajedo do Tabocal', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('509', 'Lamarão', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('510', 'Lapão', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('511', 'Lauro de Freitas', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('512', 'Lençóis', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('513', 'Licínio de Almeida', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('514', 'Livramento de Nossa Senhora', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('515', 'Luís Eduardo Magalhães', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('516', 'Macajuba', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('517', 'Macarani', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('518', 'Macaúbas', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('519', 'Macururé', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('520', 'Madre de Deus', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('521', 'Maetinga', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('522', 'Maiquinique', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('523', 'Mairi', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('524', 'Malhada', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('525', 'Malhada de Pedras', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('526', 'Manoel Vitorino', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('527', 'Mansidão', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('528', 'Maracás', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('529', 'Maragogipe', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('530', 'Maraú', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('531', 'Marcionílio Souza', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('532', 'Mascote', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('533', 'Mata de São João', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('534', 'Matina', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('535', 'Medeiros Neto', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('536', 'Miguel Calmon', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('537', 'Milagres', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('538', 'Mirangaba', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('539', 'Mirante', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('540', 'Monte Santo', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('541', 'Morpará', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('542', 'Morro do Chapéu', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('543', 'Mortugaba', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('544', 'Mucugê', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('545', 'Mucuri', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('546', 'Mulungu do Morro', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('547', 'Mundo Novo', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('548', 'Muniz Ferreira', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('549', 'Muquém de São Francisco', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('550', 'Muritiba', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('551', 'Mutuípe', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('552', 'Nazaré', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('553', 'Nilo Peçanha', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('554', 'Nordestina', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('555', 'Nova Canaã', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('556', 'Nova Fátima', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('557', 'Nova Ibiá', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('558', 'Nova Itarana', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('559', 'Nova Redenção', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('560', 'Nova Soure', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('561', 'Nova Viçosa', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('562', 'Novo Horizonte', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('563', 'Novo Triunfo', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('564', 'Olindina', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('565', 'Oliveira dos Brejinhos', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('566', 'Ouriçangas', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('567', 'Ourolândia', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('568', 'Palmas de Monte Alto', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('569', 'Palmeiras', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('570', 'Paramirim', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('571', 'Paratinga', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('572', 'Paripiranga', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('573', 'Pau Brasil', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('574', 'Paulo Afonso', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('575', 'Pé de Serra', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('576', 'Pedrão', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('577', 'Pedro Alexandre', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('578', 'Piatã', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('579', 'Pilão Arcado', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('580', 'Pindaí', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('581', 'Pindobaçu', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('582', 'Pintadas', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('583', 'Piraí do Norte', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('584', 'Piripá', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('585', 'Piritiba', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('586', 'Planaltino', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('587', 'Planalto', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('588', 'Poções', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('589', 'Pojuca', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('590', 'Ponto Novo', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('591', 'Porto Seguro', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('592', 'Potiraguá', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('593', 'Prado', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('594', 'Presidente Dutra', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('595', 'Presidente Jânio Quadros', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('596', 'Presidente Tancredo Neves', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('597', 'Queimadas', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('598', 'Quijingue', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('599', 'Quixabeira', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('600', 'Rafael Jambeiro', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('601', 'Remanso', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('602', 'Retirolândia', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('603', 'Riachão das Neves', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('604', 'Riachão do Jacuípe', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('605', 'Riacho de Santana', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('606', 'Ribeira do Amparo', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('607', 'Ribeira do Pombal', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('608', 'Ribeirão do Largo', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('609', 'Rio de Contas', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('610', 'Rio do Antônio', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('611', 'Rio do Pires', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('612', 'Rio Real', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('613', 'Rodelas', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('614', 'Ruy Barbosa', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('615', 'Salinas da Margarida', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('616', 'Salvador', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('617', 'Santa Bárbara', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('618', 'Santa Brígida', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('619', 'Santa Cruz Cabrália', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('620', 'Santa Cruz da Vitória', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('621', 'Santa Inês', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('622', 'Santa Luzia', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('623', 'Santa Maria da Vitória', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('624', 'Santa Rita de Cássia', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('625', 'Santa Teresinha', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('626', 'Santaluz', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('627', 'Santana', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('628', 'Santanópolis', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('629', 'Santo Amaro', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('630', 'Santo Antônio de Jesus', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('631', 'Santo Estêvão', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('632', 'São Desidério', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('633', 'São Domingos', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('634', 'São Felipe', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('635', 'São Félix', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('636', 'São Félix do Coribe', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('637', 'São Francisco do Conde', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('638', 'São Gabriel', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('639', 'São Gonçalo dos Campos', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('640', 'São José da Vitória', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('641', 'São José do Jacuípe', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('642', 'São Miguel das Matas', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('643', 'São Sebastião do Passé', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('644', 'Sapeaçu', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('645', 'Sátiro Dias', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('646', 'Saubara', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('647', 'Saúde', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('648', 'Seabra', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('649', 'Sebastião Laranjeiras', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('650', 'Senhor do Bonfim', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('651', 'Sento Sé', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('652', 'Serra do Ramalho', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('653', 'Serra Dourada', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('654', 'Serra Preta', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('655', 'Serrinha', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('656', 'Serrolândia', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('657', 'Simões Filho', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('658', 'Sítio do Mato', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('659', 'Sítio do Quinto', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('660', 'Sobradinho', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('661', 'Souto Soares', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('662', 'Tabocas do Brejo Velho', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('663', 'Tanhaçu', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('664', 'Tanque Novo', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('665', 'Tanquinho', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('666', 'Taperoá', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('667', 'Tapiramutá', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('668', 'Teixeira de Freitas', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('669', 'Teodoro Sampaio', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('670', 'Teofilândia', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('671', 'Teolândia', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('672', 'Terra Nova', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('673', 'Tremedal', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('674', 'Tucano', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('675', 'Uauá', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('676', 'Ubaíra', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('677', 'Ubaitaba', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('678', 'Ubatã', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('679', 'Uibaí', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('680', 'Umburanas', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('681', 'Una', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('682', 'Urandi', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('683', 'Uruçuca', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('684', 'Utinga', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('685', 'Valença', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('686', 'Valente', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('687', 'Várzea da Roça', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('688', 'Várzea do Poço', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('689', 'Várzea Nova', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('690', 'Varzedo', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('691', 'Vera Cruz', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('692', 'Vereda', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('693', 'Vitória da Conquista', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('694', 'Wagner', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('695', 'Wanderley', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('696', 'Wenceslau Guimarães', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('697', 'Xique-Xique', '5', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('698', 'Abaiara', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('699', 'Acarape', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('700', 'Acaraú', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('701', 'Acopiara', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('702', 'Aiuaba', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('703', 'Alcântaras', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('704', 'Altaneira', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('705', 'Alto Santo', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('706', 'Amontada', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('707', 'Antonina do Norte', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('708', 'Apuiarés', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('709', 'Aquiraz', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('710', 'Aracati', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('711', 'Aracoiaba', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('712', 'Ararendá', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('713', 'Araripe', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('714', 'Aratuba', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('715', 'Arneiroz', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('716', 'Assaré', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('717', 'Aurora', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('718', 'Baixio', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('719', 'Banabuiú', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('720', 'Barbalha', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('721', 'Barreira', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('722', 'Barro', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('723', 'Barroquinha', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('724', 'Baturité', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('725', 'Beberibe', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('726', 'Bela Cruz', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('727', 'Boa Viagem', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('728', 'Brejo Santo', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('729', 'Camocim', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('730', 'Campos Sales', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('731', 'Canindé', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('732', 'Capistrano', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('733', 'Caridade', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('734', 'Cariré', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('735', 'Caririaçu', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('736', 'Cariús', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('737', 'Carnaubal', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('738', 'Cascavel', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('739', 'Catarina', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('740', 'Catunda', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('741', 'Caucaia', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('742', 'Cedro', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('743', 'Chaval', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('744', 'Choró', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('745', 'Chorozinho', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('746', 'Coreaú', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('747', 'Crateús', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('748', 'Crato', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('749', 'Croatá', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('750', 'Cruz', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('751', 'Deputado Irapuan Pinheiro', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('752', 'Ererê', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('753', 'Eusébio', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('754', 'Farias Brito', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('755', 'Forquilha', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('756', 'Fortaleza', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('757', 'Fortim', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('758', 'Frecheirinha', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('759', 'General Sampaio', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('760', 'Graça', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('761', 'Granja', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('762', 'Granjeiro', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('763', 'Groaíras', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('764', 'Guaiúba', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('765', 'Guaraciaba do Norte', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('766', 'Guaramiranga', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('767', 'Hidrolândia', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('768', 'Horizonte', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('769', 'Ibaretama', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('770', 'Ibiapina', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('771', 'Ibicuitinga', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('772', 'Icapuí', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('773', 'Icó', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('774', 'Iguatu', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('775', 'Independência', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('776', 'Ipaporanga', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('777', 'Ipaumirim', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('778', 'Ipu', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('779', 'Ipueiras', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('780', 'Iracema', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('781', 'Irauçuba', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('782', 'Itaiçaba', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('783', 'Itaitinga', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('784', 'Itapagé', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('785', 'Itapipoca', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('786', 'Itapiúna', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('787', 'Itarema', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('788', 'Itatira', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('789', 'Jaguaretama', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('790', 'Jaguaribara', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('791', 'Jaguaribe', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('792', 'Jaguaruana', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('793', 'Jardim', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('794', 'Jati', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('795', 'Jijoca de Jericoacoara', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('796', 'Juazeiro do Norte', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('797', 'Jucás', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('798', 'Lavras da Mangabeira', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('799', 'Limoeiro do Norte', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('800', 'Madalena', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('801', 'Maracanaú', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('802', 'Maranguape', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('803', 'Marco', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('804', 'Martinópole', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('805', 'Massapê', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('806', 'Mauriti', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('807', 'Meruoca', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('808', 'Milagres', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('809', 'Milhã', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('810', 'Miraíma', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('811', 'Missão Velha', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('812', 'Mombaça', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('813', 'Monsenhor Tabosa', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('814', 'Morada Nova', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('815', 'Moraújo', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('816', 'Morrinhos', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('817', 'Mucambo', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('818', 'Mulungu', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('819', 'Nova Olinda', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('820', 'Nova Russas', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('821', 'Novo Oriente', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('822', 'Ocara', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('823', 'Orós', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('824', 'Pacajus', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('825', 'Pacatuba', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('826', 'Pacoti', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('827', 'Pacujá', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('828', 'Palhano', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('829', 'Palmácia', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('830', 'Paracuru', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('831', 'Paraipaba', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('832', 'Parambu', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('833', 'Paramoti', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('834', 'Pedra Branca', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('835', 'Penaforte', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('836', 'Pentecoste', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('837', 'Pereiro', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('838', 'Pindoretama', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('839', 'Piquet Carneiro', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('840', 'Pires Ferreira', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('841', 'Poranga', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('842', 'Porteiras', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('843', 'Potengi', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('844', 'Potiretama', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('845', 'Quiterianópolis', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('846', 'Quixadá', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('847', 'Quixelô', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('848', 'Quixeramobim', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('849', 'Quixeré', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('850', 'Redenção', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('851', 'Reriutaba', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('852', 'Russas', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('853', 'Saboeiro', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('854', 'Salitre', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('855', 'Santa Quitéria', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('856', 'Santana do Acaraú', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('857', 'Santana do Cariri', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('858', 'São Benedito', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('859', 'São Gonçalo do Amarante', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('860', 'São João do Jaguaribe', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('861', 'São Luís do Curu', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('862', 'Senador Pompeu', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('863', 'Senador Sá', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('864', 'Sobral', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('865', 'Solonópole', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('866', 'Tabuleiro do Norte', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('867', 'Tamboril', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('868', 'Tarrafas', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('869', 'Tauá', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('870', 'Tejuçuoca', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('871', 'Tianguá', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('872', 'Trairi', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('873', 'Tururu', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('874', 'Ubajara', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('875', 'Umari', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('876', 'Umirim', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('877', 'Uruburetama', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('878', 'Uruoca', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('879', 'Varjota', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('880', 'Várzea Alegre', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('881', 'Viçosa do Ceará', '6', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('882', 'Brasília', '7', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('883', 'Abadia de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('884', 'Abadiânia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('885', 'Acreúna', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('886', 'Adelândia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('887', 'Água Fria de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('888', 'Água Limpa', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('889', 'Águas Lindas de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('890', 'Alexânia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('891', 'Aloândia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('892', 'Alto Horizonte', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('893', 'Alto Paraíso de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('894', 'Alvorada do Norte', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('895', 'Amaralina', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('896', 'Americano do Brasil', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('897', 'Amorinópolis', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('898', 'Anápolis', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('899', 'Anhanguera', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('900', 'Anicuns', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('901', 'Aparecida de Goiânia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('902', 'Aparecida do Rio Doce', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('903', 'Aporé', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('904', 'Araçu', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('905', 'Aragarças', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('906', 'Aragoiânia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('907', 'Araguapaz', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('908', 'Arenópolis', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('909', 'Aruanã', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('910', 'Aurilândia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('911', 'Avelinópolis', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('912', 'Baliza', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('913', 'Barro Alto', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('914', 'Bela Vista de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('915', 'Bom Jardim de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('916', 'Bom Jesus de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('917', 'Bonfinópolis', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('918', 'Bonópolis', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('919', 'Brazabrantes', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('920', 'Britânia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('921', 'Buriti Alegre', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('922', 'Buriti de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('923', 'Buritinópolis', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('924', 'Cabeceiras', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('925', 'Cachoeira Alta', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('926', 'Cachoeira de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('927', 'Cachoeira Dourada', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('928', 'Caçu', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('929', 'Caiapônia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('930', 'Caldas Novas', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('931', 'Caldazinha', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('932', 'Campestre de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('933', 'Campinaçu', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('934', 'Campinorte', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('935', 'Campo Alegre de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('936', 'Campo Limpo de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('937', 'Campos Belos', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('938', 'Campos Verdes', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('939', 'Carmo do Rio Verde', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('940', 'Castelândia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('941', 'Catalão', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('942', 'Caturaí', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('943', 'Cavalcante', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('944', 'Ceres', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('945', 'Cezarina', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('946', 'Chapadão do Céu', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('947', 'cities Ocidental', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('948', 'Cocalzinho de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('949', 'Colinas do Sul', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('950', 'Córrego do Ouro', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('951', 'Corumbá de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('952', 'Corumbaíba', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('953', 'Cristalina', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('954', 'Cristianópolis', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('955', 'Crixás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('956', 'Cromínia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('957', 'Cumari', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('958', 'Damianópolis', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('959', 'Damolândia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('960', 'Davinópolis', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('961', 'Diorama', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('962', 'Divinópolis de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('963', 'Doverlândia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('964', 'Edealina', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('965', 'Edéia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('966', 'Estrela do Norte', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('967', 'Faina', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('968', 'Fazenda Nova', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('969', 'Firminópolis', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('970', 'Flores de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('971', 'Formosa', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('972', 'Formoso', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('973', 'Gameleira de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('974', 'Goianápolis', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('975', 'Goiandira', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('976', 'Goianésia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('977', 'Goiânia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('978', 'Goianira', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('979', 'Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('980', 'Goiatuba', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('981', 'Gouvelândia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('982', 'Guapó', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('983', 'Guaraíta', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('984', 'Guarani de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('985', 'Guarinos', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('986', 'Heitoraí', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('987', 'Hidrolândia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('988', 'Hidrolina', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('989', 'Iaciara', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('990', 'Inaciolândia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('991', 'Indiara', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('992', 'Inhumas', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('993', 'Ipameri', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('994', 'Ipiranga de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('995', 'Iporá', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('996', 'Israelândia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('997', 'Itaberaí', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('998', 'Itaguari', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('999', 'Itaguaru', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1000', 'Itajá', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1001', 'Itapaci', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1002', 'Itapirapuã', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1003', 'Itapuranga', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1004', 'Itarumã', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1005', 'Itauçu', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1006', 'Itumbiara', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1007', 'Ivolândia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1008', 'Jandaia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1009', 'Jaraguá', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1010', 'Jataí', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1011', 'Jaupaci', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1012', 'Jesúpolis', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1013', 'Joviânia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1014', 'Jussara', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1015', 'Lagoa Santa', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1016', 'Leopoldo de Bulhões', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1017', 'Luziânia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1018', 'Mairipotaba', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1019', 'Mambaí', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1020', 'Mara Rosa', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1021', 'Marzagão', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1022', 'Matrinchã', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1023', 'Maurilândia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1024', 'Mimoso de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1025', 'Minaçu', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1026', 'Mineiros', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1027', 'Moiporá', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1028', 'Monte Alegre de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1029', 'Montes Claros de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1030', 'Montividiu', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1031', 'Montividiu do Norte', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1032', 'Morrinhos', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1033', 'Morro Agudo de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1034', 'Mossâmedes', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1035', 'Mozarlândia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1036', 'Mundo Novo', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1037', 'Mutunópolis', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1038', 'Nazário', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1039', 'Nerópolis', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1040', 'Niquelândia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1041', 'Nova América', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1042', 'Nova Aurora', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1043', 'Nova Crixás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1044', 'Nova Glória', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1045', 'Nova Iguaçu de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1046', 'Nova Roma', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1047', 'Nova Veneza', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1048', 'Novo Brasil', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1049', 'Novo Gama', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1050', 'Novo Planalto', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1051', 'Orizona', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1052', 'Ouro Verde de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1053', 'Ouvidor', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1054', 'Padre Bernardo', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1055', 'Palestina de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1056', 'Palmeiras de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1057', 'Palmelo', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1058', 'Palminópolis', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1059', 'Panamá', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1060', 'Paranaiguara', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1061', 'Paraúna', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1062', 'Perolândia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1063', 'Petrolina de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1064', 'Pilar de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1065', 'Piracanjuba', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1066', 'Piranhas', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1067', 'Pirenópolis', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1068', 'Pires do Rio', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1069', 'Planaltina', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1070', 'Pontalina', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1071', 'Porangatu', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1072', 'Porteirão', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1073', 'Portelândia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1074', 'Posse', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1075', 'Professor Jamil', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1076', 'Quirinópolis', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1077', 'Rialma', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1078', 'Rianápolis', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1079', 'Rio Quente', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1080', 'Rio Verde', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1081', 'Rubiataba', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1082', 'Sanclerlândia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1083', 'Santa Bárbara de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1084', 'Santa Cruz de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1085', 'Santa Fé de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1086', 'Santa Helena de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1087', 'Santa Isabel', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1088', 'Santa Rita do Araguaia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1089', 'Santa Rita do Novo Destino', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1090', 'Santa Rosa de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1091', 'Santa Tereza de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1092', 'Santa Terezinha de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1093', 'Santo Antônio da Barra', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1094', 'Santo Antônio de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1095', 'Santo Antônio do Descoberto', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1096', 'São Domingos', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1097', 'São Francisco de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1098', 'São João d`Aliança', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1099', 'São João da Paraúna', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1100', 'São Luís de Montes Belos', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1101', 'São Luíz do Norte', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1102', 'São Miguel do Araguaia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1103', 'São Miguel do Passa Quatro', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1104', 'São Patrício', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1105', 'São Simão', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1106', 'Senador Canedo', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1107', 'Serranópolis', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1108', 'Silvânia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1109', 'Simolândia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1110', 'Sítio d`Abadia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1111', 'Taquaral de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1112', 'Teresina de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1113', 'Terezópolis de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1114', 'Três Ranchos', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1115', 'Trindade', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1116', 'Trombas', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1117', 'Turvânia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1118', 'Turvelândia', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1119', 'Uirapuru', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1120', 'Uruaçu', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1121', 'Uruana', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1122', 'Urutaí', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1123', 'Valparaíso de Goiás', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1124', 'Varjão', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1125', 'Vianópolis', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1126', 'Vicentinópolis', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1127', 'Vila Boa', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1128', 'Vila Propício', '9', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1129', 'Açailândia', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1130', 'Afonso Cunha', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1131', 'Água Doce do Maranhão', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1132', 'Alcântara', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1133', 'Aldeias Altas', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1134', 'Altamira do Maranhão', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1135', 'Alto Alegre do Maranhão', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1136', 'Alto Alegre do Pindaré', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1137', 'Alto Parnaíba', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1138', 'Amapá do Maranhão', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1139', 'Amarante do Maranhão', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1140', 'Anajatuba', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1141', 'Anapurus', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1142', 'Apicum-Açu', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1143', 'Araguanã', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1144', 'Araioses', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1145', 'Arame', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1146', 'Arari', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1147', 'Axixá', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1148', 'Bacabal', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1149', 'Bacabeira', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1150', 'Bacuri', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1151', 'Bacurituba', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1152', 'Balsas', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1153', 'Barão de Grajaú', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1154', 'Barra do Corda', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1155', 'Barreirinhas', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1156', 'Bela Vista do Maranhão', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1157', 'Belágua', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1158', 'Benedito Leite', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1159', 'Bequimão', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1160', 'Bernardo do Mearim', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1161', 'Boa Vista do Gurupi', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1162', 'Bom Jardim', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1163', 'Bom Jesus das Selvas', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1164', 'Bom Lugar', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1165', 'Brejo', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1166', 'Brejo de Areia', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1167', 'Buriti', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1168', 'Buriti Bravo', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1169', 'Buriticupu', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1170', 'Buritirana', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1171', 'Cachoeira Grande', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1172', 'Cajapió', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1173', 'Cajari', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1174', 'Campestre do Maranhão', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1175', 'Cândido Mendes', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1176', 'Cantanhede', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1177', 'Capinzal do Norte', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1178', 'Carolina', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1179', 'Carutapera', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1180', 'Caxias', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1181', 'Cedral', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1182', 'Central do Maranhão', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1183', 'Centro do Guilherme', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1184', 'Centro Novo do Maranhão', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1185', 'Chapadinha', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1186', 'Cidelândia', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1187', 'Codó', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1188', 'Coelho Neto', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1189', 'Colinas', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1190', 'Conceição do Lago-Açu', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1191', 'Coroatá', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1192', 'Cururupu', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1193', 'Davinópolis', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1194', 'Dom Pedro', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1195', 'Duque Bacelar', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1196', 'Esperantinópolis', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1197', 'Estreito', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1198', 'Feira Nova do Maranhão', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1199', 'Fernando Falcão', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1200', 'Formosa da Serra Negra', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1201', 'Fortaleza dos Nogueiras', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1202', 'Fortuna', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1203', 'Godofredo Viana', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1204', 'Gonçalves Dias', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1205', 'Governador Archer', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1206', 'Governador Edison Lobão', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1207', 'Governador Eugênio Barros', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1208', 'Governador Luiz Rocha', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1209', 'Governador Newton Bello', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1210', 'Governador Nunes Freire', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1211', 'Graça Aranha', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1212', 'Grajaú', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1213', 'Guimarães', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1214', 'Humberto de Campos', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1215', 'Icatu', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1216', 'Igarapé do Meio', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1217', 'Igarapé Grande', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1218', 'Imperatriz', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1219', 'Itaipava do Grajaú', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1220', 'Itapecuru Mirim', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1221', 'Itinga do Maranhão', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1222', 'Jatobá', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1223', 'Jenipapo dos Vieiras', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1224', 'João Lisboa', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1225', 'Joselândia', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1226', 'Junco do Maranhão', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1227', 'Lago da Pedra', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1228', 'Lago do Junco', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1229', 'Lago dos Rodrigues', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1230', 'Lago Verde', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1231', 'Lagoa do Mato', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1232', 'Lagoa Grande do Maranhão', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1233', 'Lajeado Novo', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1234', 'Lima Campos', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1235', 'Loreto', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1236', 'Luís Domingues', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1237', 'Magalhães de Almeida', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1238', 'Maracaçumé', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1239', 'Marajá do Sena', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1240', 'Maranhãozinho', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1241', 'Mata Roma', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1242', 'Matinha', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1243', 'Matões', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1244', 'Matões do Norte', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1245', 'Milagres do Maranhão', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1246', 'Mirador', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1247', 'Miranda do Norte', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1248', 'Mirinzal', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1249', 'Monção', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1250', 'Montes Altos', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1251', 'Morros', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1252', 'Nina Rodrigues', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1253', 'Nova Colinas', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1254', 'Nova Iorque', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1255', 'Nova Olinda do Maranhão', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1256', 'Olho d`Água das Cunhãs', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1257', 'Olinda Nova do Maranhão', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1258', 'Paço do Lumiar', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1259', 'Palmeirândia', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1260', 'Paraibano', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1261', 'Parnarama', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1262', 'Passagem Franca', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1263', 'Pastos Bons', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1264', 'Paulino Neves', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1265', 'Paulo Ramos', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1266', 'Pedreiras', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1267', 'Pedro do Rosário', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1268', 'Penalva', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1269', 'Peri Mirim', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1270', 'Peritoró', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1271', 'Pindaré-Mirim', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1272', 'Pinheiro', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1273', 'Pio XII', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1274', 'Pirapemas', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1275', 'Poção de Pedras', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1276', 'Porto Franco', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1277', 'Porto Rico do Maranhão', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1278', 'Presidente Dutra', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1279', 'Presidente Juscelino', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1280', 'Presidente Médici', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1281', 'Presidente Sarney', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1282', 'Presidente Vargas', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1283', 'Primeira Cruz', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1284', 'Raposa', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1285', 'Riachão', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1286', 'Ribamar Fiquene', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1287', 'Rosário', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1288', 'Sambaíba', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1289', 'Santa Filomena do Maranhão', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1290', 'Santa Helena', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1291', 'Santa Inês', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1292', 'Santa Luzia', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1293', 'Santa Luzia do Paruá', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1294', 'Santa Quitéria do Maranhão', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1295', 'Santa Rita', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1296', 'Santana do Maranhão', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1297', 'Santo Amaro do Maranhão', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1298', 'Santo Antônio dos Lopes', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1299', 'São Benedito do Rio Preto', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1300', 'São Bento', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1301', 'São Bernardo', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1302', 'São Domingos do Azeitão', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1303', 'São Domingos do Maranhão', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1304', 'São Félix de Balsas', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1305', 'São Francisco do Brejão', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1306', 'São Francisco do Maranhão', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1307', 'São João Batista', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1308', 'São João do Carú', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1309', 'São João do Paraíso', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1310', 'São João do Soter', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1311', 'São João dos Patos', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1312', 'São José de Ribamar', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1313', 'São José dos Basílios', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1314', 'São Luís', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1315', 'São Luís Gonzaga do Maranhão', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1316', 'São Mateus do Maranhão', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1317', 'São Pedro da Água Branca', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1318', 'São Pedro dos Crentes', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1319', 'São Raimundo das Mangabeiras', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1320', 'São Raimundo do Doca Bezerra', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1321', 'São Roberto', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1322', 'São Vicente Ferrer', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1323', 'Satubinha', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1324', 'Senador Alexandre Costa', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1325', 'Senador La Rocque', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1326', 'Serrano do Maranhão', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1327', 'Sítio Novo', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1328', 'Sucupira do Norte', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1329', 'Sucupira do Riachão', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1330', 'Tasso Fragoso', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1331', 'Timbiras', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1332', 'Timon', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1333', 'Trizidela do Vale', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1334', 'Tufilândia', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1335', 'Tuntum', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1336', 'Turiaçu', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1337', 'Turilândia', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1338', 'Tutóia', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1339', 'Urbano Santos', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1340', 'Vargem Grande', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1341', 'Viana', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1342', 'Vila Nova dos Martírios', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1343', 'Vitória do Mearim', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1344', 'Vitorino Freire', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1345', 'Zé Doca', '10', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1346', 'Acorizal', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1347', 'Água Boa', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1348', 'Alta Floresta', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1349', 'Alto Araguaia', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1350', 'Alto Boa Vista', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1351', 'Alto Garças', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1352', 'Alto Paraguai', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1353', 'Alto Taquari', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1354', 'Apiacás', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1355', 'Araguaiana', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1356', 'Araguainha', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1357', 'Araputanga', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1358', 'Arenápolis', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1359', 'Aripuanã', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1360', 'Barão de Melgaço', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1361', 'Barra do Bugres', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1362', 'Barra do Garças', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1363', 'Bom Jesus do Araguaia', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1364', 'Brasnorte', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1365', 'Cáceres', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1366', 'Campinápolis', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1367', 'Campo Novo do Parecis', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1368', 'Campo Verde', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1369', 'Campos de Júlio', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1370', 'Canabrava do Norte', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1371', 'Canarana', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1372', 'Carlinda', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1373', 'Castanheira', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1374', 'Chapada dos Guimarães', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1375', 'Cláudia', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1376', 'Cocalinho', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1377', 'Colíder', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1378', 'Colniza', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1379', 'Comodoro', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1380', 'Confresa', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1381', 'Conquista d`Oeste', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1382', 'Cotriguaçu', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1383', 'Cuiabá', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1384', 'Curvelândia', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1385', 'Curvelândia', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1386', 'Denise', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1387', 'Diamantino', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1388', 'Dom Aquino', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1389', 'Feliz Natal', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1390', 'Figueirópolis d`Oeste', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1391', 'Gaúcha do Norte', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1392', 'General Carneiro', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1393', 'Glória d`Oeste', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1394', 'Guarantã do Norte', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1395', 'Guiratinga', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1396', 'Indiavaí', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1397', 'Ipiranga do Norte', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1398', 'Itanhangá', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1399', 'Itaúba', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1400', 'Itiquira', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1401', 'Jaciara', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1402', 'Jangada', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1403', 'Jauru', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1404', 'Juara', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1405', 'Juína', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1406', 'Juruena', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1407', 'Juscimeira', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1408', 'Lambari d`Oeste', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1409', 'Lucas do Rio Verde', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1410', 'Luciára', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1411', 'Marcelândia', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1412', 'Matupá', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1413', 'Mirassol d`Oeste', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1414', 'Nobres', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1415', 'Nortelândia', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1416', 'Nossa Senhora do Livramento', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1417', 'Nova Bandeirantes', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1418', 'Nova Brasilândia', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1419', 'Nova Canaã do Norte', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1420', 'Nova Guarita', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1421', 'Nova Lacerda', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1422', 'Nova Marilândia', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1423', 'Nova Maringá', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1424', 'Nova Monte verde', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1425', 'Nova Mutum', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1426', 'Nova Olímpia', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1427', 'Nova Santa Helena', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1428', 'Nova Ubiratã', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1429', 'Nova Xavantina', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1430', 'Novo Horizonte do Norte', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1431', 'Novo Mundo', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1432', 'Novo Santo Antônio', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1433', 'Novo São Joaquim', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1434', 'Paranaíta', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1435', 'Paranatinga', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1436', 'Pedra Preta', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1437', 'Peixoto de Azevedo', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1438', 'Planalto da Serra', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1439', 'Poconé', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1440', 'Pontal do Araguaia', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1441', 'Ponte Branca', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1442', 'Pontes e Lacerda', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1443', 'Porto Alegre do Norte', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1444', 'Porto dos Gaúchos', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1445', 'Porto Esperidião', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1446', 'Porto Estrela', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1447', 'Poxoréo', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1448', 'Primavera do Leste', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1449', 'Querência', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1450', 'Reserva do Cabaçal', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1451', 'Ribeirão Cascalheira', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1452', 'Ribeirãozinho', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1453', 'Rio Branco', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1454', 'Rondolândia', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1455', 'Rondonópolis', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1456', 'Rosário Oeste', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1457', 'Salto do Céu', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1458', 'Santa Carmem', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1459', 'Santa Cruz do Xingu', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1460', 'Santa Rita do Trivelato', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1461', 'Santa Terezinha', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1462', 'Santo Afonso', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1463', 'Santo Antônio do Leste', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1464', 'Santo Antônio do Leverger', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1465', 'São Félix do Araguaia', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1466', 'São José do Povo', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1467', 'São José do Rio Claro', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1468', 'São José do Xingu', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1469', 'São José dos Quatro Marcos', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1470', 'São Pedro da Cipa', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1471', 'Sapezal', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1472', 'Serra Nova Dourada', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1473', 'Sinop', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1474', 'Sorriso', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1475', 'Tabaporã', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1476', 'Tangará da Serra', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1477', 'Tapurah', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1478', 'Terra Nova do Norte', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1479', 'Tesouro', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1480', 'Torixoréu', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1481', 'União do Sul', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1482', 'Vale de São Domingos', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1483', 'Várzea Grande', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1484', 'Vera', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1485', 'Vila Bela da Santíssima Trindade', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1486', 'Vila Rica', '13', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1487', 'Água Clara', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1488', 'Alcinópolis', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1489', 'Amambaí', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1490', 'Anastácio', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1491', 'Anaurilândia', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1492', 'Angélica', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1493', 'Antônio João', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1494', 'Aparecida do Taboado', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1495', 'Aquidauana', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1496', 'Aral Moreira', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1497', 'Bandeirantes', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1498', 'Bataguassu', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1499', 'Bataiporã', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1500', 'Bela Vista', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1501', 'Bodoquena', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1502', 'Bonito', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1503', 'Brasilândia', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1504', 'Caarapó', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1505', 'Camapuã', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1506', 'Campo Grande', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1507', 'Caracol', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1508', 'Cassilândia', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1509', 'Chapadão do Sul', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1510', 'Corguinho', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1511', 'Coronel Sapucaia', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1512', 'Corumbá', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1513', 'Costa Rica', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1514', 'Coxim', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1515', 'Deodápolis', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1516', 'Dois Irmãos do Buriti', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1517', 'Douradina', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1518', 'Dourados', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1519', 'Eldorado', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1520', 'Fátima do Sul', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1521', 'Figueirão', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1522', 'Glória de Dourados', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1523', 'Guia Lopes da Laguna', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1524', 'Iguatemi', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1525', 'Inocência', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1526', 'Itaporã', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1527', 'Itaquiraí', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1528', 'Ivinhema', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1529', 'Japorã', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1530', 'Jaraguari', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1531', 'Jardim', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1532', 'Jateí', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1533', 'Juti', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1534', 'Ladário', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1535', 'Laguna Carapã', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1536', 'Maracaju', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1537', 'Miranda', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1538', 'Mundo Novo', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1539', 'Naviraí', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1540', 'Nioaque', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1541', 'Nova Alvorada do Sul', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1542', 'Nova Andradina', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1543', 'Novo Horizonte do Sul', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1544', 'Paranaíba', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1545', 'Paranhos', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1546', 'Pedro Gomes', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1547', 'Ponta Porã', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1548', 'Porto Murtinho', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1549', 'Ribas do Rio Pardo', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1550', 'Rio Brilhante', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1551', 'Rio Negro', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1552', 'Rio Verde de Mato Grosso', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1553', 'Rochedo', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1554', 'Santa Rita do Pardo', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1555', 'São Gabriel do Oeste', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1556', 'Selvíria', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1557', 'Sete Quedas', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1558', 'Sidrolândia', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1559', 'Sonora', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1560', 'Tacuru', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1561', 'Taquarussu', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1562', 'Terenos', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1563', 'Três Lagoas', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1564', 'Vicentina', '12', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1565', 'Abadia dos Dourados', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1566', 'Abaeté', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1567', 'Abre Campo', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1568', 'Acaiaca', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1569', 'Açucena', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1570', 'Água Boa', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1571', 'Água Comprida', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1572', 'Aguanil', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1573', 'Águas Formosas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1574', 'Águas Vermelhas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1575', 'Aimorés', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1576', 'Aiuruoca', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1577', 'Alagoa', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1578', 'Albertina', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1579', 'Além Paraíba', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1580', 'Alfenas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1581', 'Alfredo Vasconcelos', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1582', 'Almenara', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1583', 'Alpercata', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1584', 'Alpinópolis', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1585', 'Alterosa', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1586', 'Alto Caparaó', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1587', 'Alto Jequitibá', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1588', 'Alto Rio Doce', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1589', 'Alvarenga', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1590', 'Alvinópolis', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1591', 'Alvorada de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1592', 'Amparo do Serra', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1593', 'Andradas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1594', 'Andrelândia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1595', 'Angelândia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1596', 'Antônio Carlos', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1597', 'Antônio Dias', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1598', 'Antônio Prado de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1599', 'Araçaí', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1600', 'Aracitaba', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1601', 'Araçuaí', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1602', 'Araguari', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1603', 'Arantina', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1604', 'Araponga', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1605', 'Araporã', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1606', 'Arapuá', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1607', 'Araújos', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1608', 'Araxá', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1609', 'Arceburgo', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1610', 'Arcos', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1611', 'Areado', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1612', 'Argirita', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1613', 'Aricanduva', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1614', 'Arinos', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1615', 'Astolfo Dutra', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1616', 'Ataléia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1617', 'Augusto de Lima', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1618', 'Baependi', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1619', 'Baldim', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1620', 'Bambuí', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1621', 'Bandeira', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1622', 'Bandeira do Sul', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1623', 'Barão de Cocais', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1624', 'Barão de Monte Alto', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1625', 'Barbacena', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1626', 'Barra Longa', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1627', 'Barroso', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1628', 'Bela Vista de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1629', 'Belmiro Braga', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1630', 'Belo Horizonte', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1631', 'Belo Oriente', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1632', 'Belo Vale', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1633', 'Berilo', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1634', 'Berizal', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1635', 'Bertópolis', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1636', 'Betim', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1637', 'Bias Fortes', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1638', 'Bicas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1639', 'Biquinhas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1640', 'Boa Esperança', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1641', 'Bocaina de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1642', 'Bocaiúva', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1643', 'Bom Despacho', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1644', 'Bom Jardim de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1645', 'Bom Jesus da Penha', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1646', 'Bom Jesus do Amparo', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1647', 'Bom Jesus do Galho', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1648', 'Bom Repouso', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1649', 'Bom Sucesso', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1650', 'Bonfim', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1651', 'Bonfinópolis de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1652', 'Bonito de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1653', 'Borda da Mata', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1654', 'Botelhos', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1655', 'Botumirim', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1656', 'Brás Pires', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1657', 'Brasilândia de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1658', 'Brasília de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1659', 'Brasópolis', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1660', 'Braúnas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1661', 'Brumadinho', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1662', 'Bueno Brandão', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1663', 'Buenópolis', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1664', 'Bugre', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1665', 'Buritis', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1666', 'Buritizeiro', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1667', 'Cabeceira Grande', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1668', 'Cabo Verde', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1669', 'Cachoeira da Prata', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1670', 'Cachoeira de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1671', 'Cachoeira de Pajeú', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1672', 'Cachoeira Dourada', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1673', 'Caetanópolis', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1674', 'Caeté', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1675', 'Caiana', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1676', 'Cajuri', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1677', 'Caldas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1678', 'Camacho', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1679', 'Camanducaia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1680', 'Cambuí', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1681', 'Cambuquira', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1682', 'Campanário', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1683', 'Campanha', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1684', 'Campestre', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1685', 'Campina Verde', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1686', 'Campo Azul', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1687', 'Campo Belo', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1688', 'Campo do Meio', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1689', 'Campo Florido', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1690', 'Campos Altos', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1691', 'Campos Gerais', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1692', 'Cana Verde', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1693', 'Canaã', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1694', 'Canápolis', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1695', 'Candeias', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1696', 'Cantagalo', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1697', 'Caparaó', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1698', 'Capela Nova', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1699', 'Capelinha', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1700', 'Capetinga', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1701', 'Capim Branco', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1702', 'Capinópolis', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1703', 'Capitão Andrade', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1704', 'Capitão Enéas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1705', 'Capitólio', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1706', 'Caputira', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1707', 'Caraí', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1708', 'Caranaíba', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1709', 'Carandaí', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1710', 'Carangola', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1711', 'Caratinga', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1712', 'Carbonita', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1713', 'Careaçu', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1714', 'Carlos Chagas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1715', 'Carmésia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1716', 'Carmo da Cachoeira', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1717', 'Carmo da Mata', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1718', 'Carmo de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1719', 'Carmo do Cajuru', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1720', 'Carmo do Paranaíba', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1721', 'Carmo do Rio Claro', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1722', 'Carmópolis de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1723', 'Carneirinho', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1724', 'Carrancas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1725', 'Carvalhópolis', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1726', 'Carvalhos', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1727', 'Casa Grande', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1728', 'Cascalho Rico', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1729', 'Cássia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1730', 'Cataguases', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1731', 'Catas Altas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1732', 'Catas Altas da Noruega', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1733', 'Catuji', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1734', 'Catuti', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1735', 'Caxambu', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1736', 'Cedro do Abaeté', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1737', 'Central de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1738', 'Centralina', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1739', 'Chácara', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1740', 'Chalé', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1741', 'Chapada do Norte', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1742', 'Chapada Gaúcha', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1743', 'Chiador', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1744', 'Cipotânea', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1745', 'Claraval', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1746', 'Claro dos Poções', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1747', 'Cláudio', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1748', 'Coimbra', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1749', 'Coluna', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1750', 'Comendador Gomes', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1751', 'Comercinho', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1752', 'Conceição da Aparecida', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1753', 'Conceição da Barra de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1754', 'Conceição das Alagoas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1755', 'Conceição das Pedras', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1756', 'Conceição de Ipanema', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1757', 'Conceição do Mato Dentro', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1758', 'Conceição do Pará', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1759', 'Conceição do Rio Verde', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1760', 'Conceição dos Ouros', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1761', 'Cônego Marinho', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1762', 'Confins', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1763', 'Congonhal', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1764', 'Congonhas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1765', 'Congonhas do Norte', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1766', 'Conquista', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1767', 'Conselheiro Lafaiete', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1768', 'Conselheiro Pena', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1769', 'Consolação', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1770', 'Contagem', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1771', 'Coqueiral', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1772', 'Coração de Jesus', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1773', 'Cordisburgo', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1774', 'Cordislândia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1775', 'Corinto', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1776', 'Coroaci', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1777', 'Coromandel', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1778', 'Coronel Fabriciano', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1779', 'Coronel Murta', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1780', 'Coronel Pacheco', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1781', 'Coronel Xavier Chaves', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1782', 'Córrego Danta', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1783', 'Córrego do Bom Jesus', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1784', 'Córrego Fundo', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1785', 'Córrego Novo', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1786', 'Couto de Magalhães de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1787', 'Crisólita', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1788', 'Cristais', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1789', 'Cristália', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1790', 'Cristiano Otoni', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1791', 'Cristina', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1792', 'Crucilândia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1793', 'Cruzeiro da Fortaleza', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1794', 'Cruzília', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1795', 'Cuparaque', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1796', 'Curral de Dentro', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1797', 'Curvelo', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1798', 'Datas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1799', 'Delfim Moreira', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1800', 'Delfinópolis', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1801', 'Delta', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1802', 'Descoberto', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1803', 'Desterro de Entre Rios', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1804', 'Desterro do Melo', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1805', 'Diamantina', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1806', 'Diogo de Vasconcelos', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1807', 'Dionísio', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1808', 'Divinésia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1809', 'Divino', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1810', 'Divino das Laranjeiras', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1811', 'Divinolândia de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1812', 'Divinópolis', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1813', 'Divisa Alegre', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1814', 'Divisa Nova', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1815', 'Divisópolis', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1816', 'Dom Bosco', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1817', 'Dom Cavati', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1818', 'Dom Joaquim', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1819', 'Dom Silvério', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1820', 'Dom Viçoso', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1821', 'Dona Eusébia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1822', 'Dores de Campos', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1823', 'Dores de Guanhães', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1824', 'Dores do Indaiá', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1825', 'Dores do Turvo', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1826', 'Doresópolis', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1827', 'Douradoquara', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1828', 'Durandé', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1829', 'Elói Mendes', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1830', 'Engenheiro Caldas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1831', 'Engenheiro Navarro', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1832', 'Entre Folhas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1833', 'Entre Rios de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1834', 'Ervália', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1835', 'Esmeraldas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1836', 'Espera Feliz', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1837', 'Espinosa', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1838', 'Espírito Santo do Dourado', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1839', 'Estiva', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1840', 'Estrela Dalva', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1841', 'Estrela do Indaiá', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1842', 'Estrela do Sul', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1843', 'Eugenópolis', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1844', 'Ewbank da Câmara', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1845', 'Extrema', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1846', 'Fama', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1847', 'Faria Lemos', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1848', 'Felício dos Santos', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1849', 'Felisburgo', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1850', 'Felixlândia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1851', 'Fernandes Tourinho', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1852', 'Ferros', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1853', 'Fervedouro', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1854', 'Florestal', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1855', 'Formiga', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1856', 'Formoso', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1857', 'Fortaleza de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1858', 'Fortuna de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1859', 'Francisco Badaró', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1860', 'Francisco Dumont', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1861', 'Francisco Sá', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1862', 'Franciscópolis', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1863', 'Frei Gaspar', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1864', 'Frei Inocêncio', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1865', 'Frei Lagonegro', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1866', 'Fronteira', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1867', 'Fronteira dos Vales', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1868', 'Fruta de Leite', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1869', 'Frutal', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1870', 'Funilândia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1871', 'Galiléia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1872', 'Gameleiras', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1873', 'Glaucilândia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1874', 'Goiabeira', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1875', 'Goianá', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1876', 'Gonçalves', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1877', 'Gonzaga', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1878', 'Gouveia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1879', 'Governador Valadares', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1880', 'Grão Mogol', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1881', 'Grupiara', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1882', 'Guanhães', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1883', 'Guapé', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1884', 'Guaraciaba', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1885', 'Guaraciama', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1886', 'Guaranésia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1887', 'Guarani', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1888', 'Guarará', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1889', 'Guarda-Mor', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1890', 'Guaxupé', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1891', 'Guidoval', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1892', 'Guimarânia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1893', 'Guiricema', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1894', 'Gurinhatã', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1895', 'Heliodora', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1896', 'Iapu', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1897', 'Ibertioga', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1898', 'Ibiá', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1899', 'Ibiaí', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1900', 'Ibiracatu', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1901', 'Ibiraci', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1902', 'Ibirité', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1903', 'Ibitiúra de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1904', 'Ibituruna', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1905', 'Icaraí de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1906', 'Igarapé', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1907', 'Igaratinga', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1908', 'Iguatama', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1909', 'Ijaci', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1910', 'Ilicínea', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1911', 'Imbé de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1912', 'Inconfidentes', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1913', 'Indaiabira', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1914', 'Indianópolis', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1915', 'Ingaí', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1916', 'Inhapim', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1917', 'Inhaúma', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1918', 'Inimutaba', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1919', 'Ipaba', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1920', 'Ipanema', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1921', 'Ipatinga', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1922', 'Ipiaçu', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1923', 'Ipuiúna', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1924', 'Iraí de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1925', 'Itabira', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1926', 'Itabirinha de Mantena', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1927', 'Itabirito', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1928', 'Itacambira', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1929', 'Itacarambi', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1930', 'Itaguara', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1931', 'Itaipé', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1932', 'Itajubá', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1933', 'Itamarandiba', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1934', 'Itamarati de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1935', 'Itambacuri', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1936', 'Itambé do Mato Dentro', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1937', 'Itamogi', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1938', 'Itamonte', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1939', 'Itanhandu', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1940', 'Itanhomi', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1941', 'Itaobim', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1942', 'Itapagipe', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1943', 'Itapecerica', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1944', 'Itapeva', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1945', 'Itatiaiuçu', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1946', 'Itaú de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1947', 'Itaúna', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1948', 'Itaverava', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1949', 'Itinga', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1950', 'Itueta', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1951', 'Ituiutaba', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1952', 'Itumirim', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1953', 'Iturama', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1954', 'Itutinga', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1955', 'Jaboticatubas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1956', 'Jacinto', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1957', 'Jacuí', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1958', 'Jacutinga', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1959', 'Jaguaraçu', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1960', 'Jaíba', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1961', 'Jampruca', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1962', 'Janaúba', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1963', 'Januária', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1964', 'Japaraíba', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1965', 'Japonvar', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1966', 'Jeceaba', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1967', 'Jenipapo de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1968', 'Jequeri', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1969', 'Jequitaí', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1970', 'Jequitibá', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1971', 'Jequitinhonha', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1972', 'Jesuânia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1973', 'Joaíma', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1974', 'Joanésia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1975', 'João Monlevade', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1976', 'João Pinheiro', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1977', 'Joaquim Felício', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1978', 'Jordânia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1979', 'José Gonçalves de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1980', 'José Raydan', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1981', 'Josenópolis', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1982', 'Juatuba', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1983', 'Juiz de Fora', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1984', 'Juramento', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1985', 'Juruaia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1986', 'Juvenília', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1987', 'Ladainha', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1988', 'Lagamar', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1989', 'Lagoa da Prata', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1990', 'Lagoa dos Patos', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1991', 'Lagoa Dourada', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1992', 'Lagoa Formosa', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1993', 'Lagoa Grande', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1994', 'Lagoa Santa', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1995', 'Lajinha', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1996', 'Lambari', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1997', 'Lamim', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1998', 'Laranjal', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('1999', 'Lassance', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2000', 'Lavras', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2001', 'Leandro Ferreira', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2002', 'Leme do Prado', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2003', 'Leopoldina', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2004', 'Liberdade', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2005', 'Lima Duarte', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2006', 'Limeira do Oeste', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2007', 'Lontra', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2008', 'Luisburgo', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2009', 'Luislândia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2010', 'Luminárias', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2011', 'Luz', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2012', 'Machacalis', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2013', 'Machado', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2014', 'Madre de Deus de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2015', 'Malacacheta', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2016', 'Mamonas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2017', 'Manga', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2018', 'Manhuaçu', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2019', 'Manhumirim', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2020', 'Mantena', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2021', 'Mar de Espanha', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2022', 'Maravilhas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2023', 'Maria da Fé', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2024', 'Mariana', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2025', 'Marilac', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2026', 'Mário Campos', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2027', 'Maripá de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2028', 'Marliéria', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2029', 'Marmelópolis', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2030', 'Martinho Campos', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2031', 'Martins Soares', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2032', 'Mata Verde', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2033', 'Materlândia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2034', 'Mateus Leme', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2035', 'Mathias Lobato', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2036', 'Matias Barbosa', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2037', 'Matias Cardoso', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2038', 'Matipó', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2039', 'Mato Verde', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2040', 'Matozinhos', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2041', 'Matutina', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2042', 'Medeiros', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2043', 'Medina', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2044', 'Mendes Pimentel', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2045', 'Mercês', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2046', 'Mesquita', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2047', 'Minas Novas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2048', 'Minduri', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2049', 'Mirabela', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2050', 'Miradouro', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2051', 'Miraí', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2052', 'Miravânia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2053', 'Moeda', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2054', 'Moema', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2055', 'Monjolos', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2056', 'Monsenhor Paulo', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2057', 'Montalvânia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2058', 'Monte Alegre de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2059', 'Monte Azul', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2060', 'Monte Belo', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2061', 'Monte Carmelo', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2062', 'Monte Formoso', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2063', 'Monte Santo de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2064', 'Monte Sião', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2065', 'Montes Claros', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2066', 'Montezuma', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2067', 'Morada Nova de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2068', 'Morro da Garça', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2069', 'Morro do Pilar', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2070', 'Munhoz', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2071', 'Muriaé', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2072', 'Mutum', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2073', 'Muzambinho', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2074', 'Nacip Raydan', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2075', 'Nanuque', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2076', 'Naque', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2077', 'Natalândia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2078', 'Natércia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2079', 'Nazareno', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2080', 'Nepomuceno', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2081', 'Ninheira', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2082', 'Nova Belém', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2083', 'Nova Era', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2084', 'Nova Lima', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2085', 'Nova Módica', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2086', 'Nova Ponte', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2087', 'Nova Porteirinha', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2088', 'Nova Resende', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2089', 'Nova Serrana', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2090', 'Nova União', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2091', 'Novo Cruzeiro', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2092', 'Novo Oriente de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2093', 'Novorizonte', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2094', 'Olaria', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2095', 'Olhos-d`Água', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2096', 'Olímpio Noronha', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2097', 'Oliveira', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2098', 'Oliveira Fortes', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2099', 'Onça de Pitangui', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2100', 'Oratórios', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2101', 'Orizânia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2102', 'Ouro Branco', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2103', 'Ouro Fino', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2104', 'Ouro Preto', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2105', 'Ouro Verde de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2106', 'Padre Carvalho', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2107', 'Padre Paraíso', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2108', 'Pai Pedro', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2109', 'Paineiras', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2110', 'Pains', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2111', 'Paiva', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2112', 'Palma', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2113', 'Palmópolis', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2114', 'Papagaios', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2115', 'Pará de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2116', 'Paracatu', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2117', 'Paraguaçu', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2118', 'Paraisópolis', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2119', 'Paraopeba', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2120', 'Passa Quatro', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2121', 'Passa Tempo', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2122', 'Passabém', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2123', 'Passa-Vinte', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2124', 'Passos', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2125', 'Patis', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2126', 'Patos de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2127', 'Patrocínio', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2128', 'Patrocínio do Muriaé', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2129', 'Paula Cândido', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2130', 'Paulistas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2131', 'Pavão', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2132', 'Peçanha', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2133', 'Pedra Azul', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2134', 'Pedra Bonita', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2135', 'Pedra do Anta', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2136', 'Pedra do Indaiá', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2137', 'Pedra Dourada', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2138', 'Pedralva', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2139', 'Pedras de Maria da Cruz', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2140', 'Pedrinópolis', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2141', 'Pedro Leopoldo', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2142', 'Pedro Teixeira', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2143', 'Pequeri', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2144', 'Pequi', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2145', 'Perdigão', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2146', 'Perdizes', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2147', 'Perdões', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2148', 'Periquito', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2149', 'Pescador', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2150', 'Piau', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2151', 'Piedade de Caratinga', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2152', 'Piedade de Ponte Nova', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2153', 'Piedade do Rio Grande', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2154', 'Piedade dos Gerais', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2155', 'Pimenta', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2156', 'Pingo-d`Água', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2157', 'Pintópolis', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2158', 'Piracema', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2159', 'Pirajuba', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2160', 'Piranga', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2161', 'Piranguçu', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2162', 'Piranguinho', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2163', 'Pirapetinga', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2164', 'Pirapora', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2165', 'Piraúba', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2166', 'Pitangui', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2167', 'Piumhi', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2168', 'Planura', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2169', 'Poço Fundo', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2170', 'Poços de Caldas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2171', 'Pocrane', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2172', 'Pompéu', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2173', 'Ponte Nova', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2174', 'Ponto Chique', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2175', 'Ponto dos Volantes', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2176', 'Porteirinha', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2177', 'Porto Firme', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2178', 'Poté', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2179', 'Pouso Alegre', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2180', 'Pouso Alto', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2181', 'Prados', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2182', 'Prata', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2183', 'Pratápolis', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2184', 'Pratinha', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2185', 'Presidente Bernardes', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2186', 'Presidente Juscelino', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2187', 'Presidente Kubitschek', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2188', 'Presidente Olegário', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2189', 'Prudente de Morais', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2190', 'Quartel Geral', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2191', 'Queluzito', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2192', 'Raposos', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2193', 'Raul Soares', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2194', 'Recreio', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2195', 'Reduto', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2196', 'Resende Costa', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2197', 'Resplendor', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2198', 'Ressaquinha', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2199', 'Riachinho', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2200', 'Riacho dos Machados', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2201', 'Ribeirão das Neves', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2202', 'Ribeirão Vermelho', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2203', 'Rio Acima', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2204', 'Rio Casca', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2205', 'Rio do Prado', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2206', 'Rio Doce', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2207', 'Rio Espera', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2208', 'Rio Manso', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2209', 'Rio Novo', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2210', 'Rio Paranaíba', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2211', 'Rio Pardo de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2212', 'Rio Piracicaba', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2213', 'Rio Pomba', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2214', 'Rio Preto', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2215', 'Rio Vermelho', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2216', 'Ritápolis', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2217', 'Rochedo de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2218', 'Rodeiro', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2219', 'Romaria', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2220', 'Rosário da Limeira', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2221', 'Rubelita', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2222', 'Rubim', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2223', 'Sabará', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2224', 'Sabinópolis', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2225', 'Sacramento', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2226', 'Salinas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2227', 'Salto da Divisa', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2228', 'Santa Bárbara', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2229', 'Santa Bárbara do Leste', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2230', 'Santa Bárbara do Monte Verde', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2231', 'Santa Bárbara do Tugúrio', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2232', 'Santa Cruz de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2233', 'Santa Cruz de Salinas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2234', 'Santa Cruz do Escalvado', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2235', 'Santa Efigênia de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2236', 'Santa Fé de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2237', 'Santa Helena de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2238', 'Santa Juliana', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2239', 'Santa Luzia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2240', 'Santa Margarida', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2241', 'Santa Maria de Itabira', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2242', 'Santa Maria do Salto', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2243', 'Santa Maria do Suaçuí', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2244', 'Santa Rita de Caldas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2245', 'Santa Rita de Ibitipoca', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2246', 'Santa Rita de Jacutinga', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2247', 'Santa Rita de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2248', 'Santa Rita do Itueto', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2249', 'Santa Rita do Sapucaí', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2250', 'Santa Rosa da Serra', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2251', 'Santa Vitória', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2252', 'Santana da Vargem', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2253', 'Santana de Cataguases', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2254', 'Santana de Pirapama', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2255', 'Santana do Deserto', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2256', 'Santana do Garambéu', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2257', 'Santana do Jacaré', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2258', 'Santana do Manhuaçu', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2259', 'Santana do Paraíso', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2260', 'Santana do Riacho', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2261', 'Santana dos Montes', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2262', 'Santo Antônio do Amparo', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2263', 'Santo Antônio do Aventureiro', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2264', 'Santo Antônio do Grama', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2265', 'Santo Antônio do Itambé', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2266', 'Santo Antônio do Jacinto', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2267', 'Santo Antônio do Monte', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2268', 'Santo Antônio do Retiro', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2269', 'Santo Antônio do Rio Abaixo', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2270', 'Santo Hipólito', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2271', 'Santos Dumont', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2272', 'São Bento Abade', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2273', 'São Brás do Suaçuí', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2274', 'São Domingos das Dores', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2275', 'São Domingos do Prata', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2276', 'São Félix de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2277', 'São Francisco', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2278', 'São Francisco de Paula', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2279', 'São Francisco de Sales', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2280', 'São Francisco do Glória', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2281', 'São Geraldo', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2282', 'São Geraldo da Piedade', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2283', 'São Geraldo do Baixio', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2284', 'São Gonçalo do Abaeté', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2285', 'São Gonçalo do Pará', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2286', 'São Gonçalo do Rio Abaixo', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2287', 'São Gonçalo do Rio Preto', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2288', 'São Gonçalo do Sapucaí', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2289', 'São Gotardo', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2290', 'São João Batista do Glória', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2291', 'São João da Lagoa', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2292', 'São João da Mata', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2293', 'São João da Ponte', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2294', 'São João das Missões', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2295', 'São João del Rei', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2296', 'São João do Manhuaçu', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2297', 'São João do Manteninha', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2298', 'São João do Oriente', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2299', 'São João do Pacuí', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2300', 'São João do Paraíso', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2301', 'São João Evangelista', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2302', 'São João Nepomuceno', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2303', 'São Joaquim de Bicas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2304', 'São José da Barra', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2305', 'São José da Lapa', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2306', 'São José da Safira', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2307', 'São José da Varginha', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2308', 'São José do Alegre', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2309', 'São José do Divino', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2310', 'São José do Goiabal', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2311', 'São José do Jacuri', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2312', 'São José do Mantimento', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2313', 'São Lourenço', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2314', 'São Miguel do Anta', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2315', 'São Pedro da União', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2316', 'São Pedro do Suaçuí', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2317', 'São Pedro dos Ferros', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2318', 'São Romão', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2319', 'São Roque de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2320', 'São Sebastião da Bela Vista', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2321', 'São Sebastião da Vargem Alegre', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2322', 'São Sebastião do Anta', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2323', 'São Sebastião do Maranhão', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2324', 'São Sebastião do Oeste', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2325', 'São Sebastião do Paraíso', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2326', 'São Sebastião do Rio Preto', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2327', 'São Sebastião do Rio Verde', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2328', 'São Thomé das Letras', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2329', 'São Tiago', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2330', 'São Tomás de Aquino', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2331', 'São Vicente de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2332', 'Sapucaí-Mirim', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2333', 'Sardoá', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2334', 'Sarzedo', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2335', 'Sem-Peixe', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2336', 'Senador Amaral', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2337', 'Senador Cortes', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2338', 'Senador Firmino', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2339', 'Senador José Bento', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2340', 'Senador Modestino Gonçalves', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2341', 'Senhora de Oliveira', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2342', 'Senhora do Porto', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2343', 'Senhora dos Remédios', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2344', 'Sericita', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2345', 'Seritinga', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2346', 'Serra Azul de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2347', 'Serra da Saudade', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2348', 'Serra do Salitre', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2349', 'Serra dos Aimorés', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2350', 'Serrania', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2351', 'Serranópolis de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2352', 'Serranos', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2353', 'Serro', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2354', 'Sete Lagoas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2355', 'Setubinha', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2356', 'Silveirânia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2357', 'Silvianópolis', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2358', 'Simão Pereira', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2359', 'Simonésia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2360', 'Sobrália', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2361', 'Soledade de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2362', 'Tabuleiro', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2363', 'Taiobeiras', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2364', 'Taparuba', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2365', 'Tapira', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2366', 'Tapiraí', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2367', 'Taquaraçu de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2368', 'Tarumirim', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2369', 'Teixeiras', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2370', 'Teófilo Otoni', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2371', 'Timóteo', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2372', 'Tiradentes', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2373', 'Tiros', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2374', 'Tocantins', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2375', 'Tocos do Moji', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2376', 'Toledo', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2377', 'Tombos', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2378', 'Três Corações', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2379', 'Três Marias', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2380', 'Três Pontas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2381', 'Tumiritinga', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2382', 'Tupaciguara', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2383', 'Turmalina', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2384', 'Turvolândia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2385', 'Ubá', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2386', 'Ubaí', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2387', 'Ubaporanga', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2388', 'Uberaba', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2389', 'Uberlândia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2390', 'Umburatiba', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2391', 'Unaí', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2392', 'União de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2393', 'Uruana de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2394', 'Urucânia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2395', 'Urucuia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2396', 'Vargem Alegre', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2397', 'Vargem Bonita', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2398', 'Vargem Grande do Rio Pardo', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2399', 'Varginha', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2400', 'Varjão de Minas', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2401', 'Várzea da Palma', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2402', 'Varzelândia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2403', 'Vazante', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2404', 'Verdelândia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2405', 'Veredinha', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2406', 'Veríssimo', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2407', 'Vermelho Novo', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2408', 'Vespasiano', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2409', 'Viçosa', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2410', 'Vieiras', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2411', 'Virgem da Lapa', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2412', 'Virgínia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2413', 'Virginópolis', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2414', 'Virgolândia', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2415', 'Visconde do Rio Branco', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2416', 'Volta Grande', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2417', 'Wenceslau Braz', '11', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2418', 'Abaetetuba', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2419', 'Abel Figueiredo', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2420', 'Acará', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2421', 'Afuá', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2422', 'Água Azul do Norte', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2423', 'Alenquer', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2424', 'Almeirim', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2425', 'Altamira', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2426', 'Anajás', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2427', 'Ananindeua', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2428', 'Anapu', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2429', 'Augusto Corrêa', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2430', 'Aurora do Pará', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2431', 'Aveiro', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2432', 'Bagre', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2433', 'Baião', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2434', 'Bannach', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2435', 'Barcarena', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2436', 'Belém', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2437', 'Belterra', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2438', 'Benevides', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2439', 'Bom Jesus do Tocantins', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2440', 'Bonito', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2441', 'Bragança', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2442', 'Brasil Novo', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2443', 'Brejo Grande do Araguaia', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2444', 'Breu Branco', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2445', 'Breves', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2446', 'Bujaru', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2447', 'Cachoeira do Arari', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2448', 'Cachoeira do Piriá', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2449', 'Cametá', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2450', 'Canaã dos Carajás', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2451', 'Capanema', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2452', 'Capitão Poço', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2453', 'Castanhal', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2454', 'Chaves', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2455', 'Colares', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2456', 'Conceição do Araguaia', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2457', 'Concórdia do Pará', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2458', 'Cumaru do Norte', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2459', 'Curionópolis', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2460', 'Curralinho', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2461', 'Curuá', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2462', 'Curuçá', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2463', 'Dom Eliseu', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2464', 'Eldorado dos Carajás', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2465', 'Faro', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2466', 'Floresta do Araguaia', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2467', 'Garrafão do Norte', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2468', 'Goianésia do Pará', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2469', 'Gurupá', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2470', 'Igarapé-Açu', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2471', 'Igarapé-Miri', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2472', 'Inhangapi', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2473', 'Ipixuna do Pará', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2474', 'Irituia', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2475', 'Itaituba', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2476', 'Itupiranga', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2477', 'Jacareacanga', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2478', 'Jacundá', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2479', 'Juruti', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2480', 'Limoeiro do Ajuru', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2481', 'Mãe do Rio', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2482', 'Magalhães Barata', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2483', 'Marabá', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2484', 'Maracanã', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2485', 'Marapanim', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2486', 'Marituba', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2487', 'Medicilândia', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2488', 'Melgaço', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2489', 'Mocajuba', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2490', 'Moju', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2491', 'Monte Alegre', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2492', 'Muaná', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2493', 'Nova Esperança do Piriá', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2494', 'Nova Ipixuna', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2495', 'Nova Timboteua', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2496', 'Novo Progresso', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2497', 'Novo Repartimento', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2498', 'Óbidos', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2499', 'Oeiras do Pará', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2500', 'Oriximiná', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2501', 'Ourém', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2502', 'Ourilândia do Norte', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2503', 'Pacajá', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2504', 'Palestina do Pará', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2505', 'Paragominas', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2506', 'Parauapebas', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2507', 'Pau d`Arco', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2508', 'Peixe-Boi', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2509', 'Piçarra', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2510', 'Placas', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2511', 'Ponta de Pedras', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2512', 'Portel', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2513', 'Porto de Moz', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2514', 'Prainha', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2515', 'Primavera', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2516', 'Quatipuru', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2517', 'Redenção', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2518', 'Rio Maria', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2519', 'Rondon do Pará', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2520', 'Rurópolis', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2521', 'Salinópolis', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2522', 'Salvaterra', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2523', 'Santa Bárbara do Pará', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2524', 'Santa Cruz do Arari', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2525', 'Santa Isabel do Pará', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2526', 'Santa Luzia do Pará', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2527', 'Santa Maria das Barreiras', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2528', 'Santa Maria do Pará', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2529', 'Santana do Araguaia', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2530', 'Santarém', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2531', 'Santarém Novo', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2532', 'Santo Antônio do Tauá', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2533', 'São Caetano de Odivelas', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2534', 'São Domingos do Araguaia', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2535', 'São Domingos do Capim', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2536', 'São Félix do Xingu', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2537', 'São Francisco do Pará', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2538', 'São Geraldo do Araguaia', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2539', 'São João da Ponta', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2540', 'São João de Pirabas', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2541', 'São João do Araguaia', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2542', 'São Miguel do Guamá', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2543', 'São Sebastião da Boa Vista', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2544', 'Sapucaia', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2545', 'Senador José Porfírio', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2546', 'Soure', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2547', 'Tailândia', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2548', 'Terra Alta', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2549', 'Terra Santa', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2550', 'Tomé-Açu', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2551', 'Tracuateua', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2552', 'Trairão', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2553', 'Tucumã', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2554', 'Tucuruí', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2555', 'Ulianópolis', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2556', 'Uruará', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2557', 'Vigia', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2558', 'Viseu', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2559', 'Vitória do Xingu', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2560', 'Xinguara', '14', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2561', 'Água Branca', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2562', 'Aguiar', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2563', 'Alagoa Grande', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2564', 'Alagoa Nova', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2565', 'Alagoinha', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2566', 'Alcantil', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2567', 'Algodão de Jandaíra', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2568', 'Alhandra', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2569', 'Amparo', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2570', 'Aparecida', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2571', 'Araçagi', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2572', 'Arara', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2573', 'Araruna', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2574', 'Areia', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2575', 'Areia de Baraúnas', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2576', 'Areial', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2577', 'Aroeiras', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2578', 'Assunção', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2579', 'Baía da Traição', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2580', 'Bananeiras', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2581', 'Baraúna', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2582', 'Barra de Santa Rosa', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2583', 'Barra de Santana', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2584', 'Barra de São Miguel', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2585', 'Bayeux', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2586', 'Belém', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2587', 'Belém do Brejo do Cruz', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2588', 'Bernardino Batista', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2589', 'Boa Ventura', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2590', 'Boa Vista', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2591', 'Bom Jesus', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2592', 'Bom Sucesso', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2593', 'Bonito de Santa Fé', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2594', 'Boqueirão', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2595', 'Borborema', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2596', 'Brejo do Cruz', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2597', 'Brejo dos Santos', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2598', 'Caaporã', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2599', 'Cabaceiras', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2600', 'Cabedelo', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2601', 'Cachoeira dos Índios', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2602', 'Cacimba de Areia', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2603', 'Cacimba de Dentro', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2604', 'Cacimbas', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2605', 'Caiçara', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2606', 'Cajazeiras', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2607', 'Cajazeirinhas', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2608', 'Caldas Brandão', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2609', 'Camalaú', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2610', 'Campina Grande', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2611', 'Campo de Santana', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2612', 'Capim', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2613', 'Caraúbas', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2614', 'Carrapateira', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2615', 'Casserengue', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2616', 'Catingueira', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2617', 'Catolé do Rocha', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2618', 'Caturité', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2619', 'Conceição', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2620', 'Condado', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2621', 'Conde', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2622', 'Congo', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2623', 'Coremas', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2624', 'Coxixola', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2625', 'Cruz do Espírito Santo', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2626', 'Cubati', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2627', 'Cuité', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2628', 'Cuité de Mamanguape', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2629', 'Cuitegi', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2630', 'Curral de Cima', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2631', 'Curral Velho', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2632', 'Damião', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2633', 'Desterro', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2634', 'Diamante', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2635', 'Dona Inês', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2636', 'Duas Estradas', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2637', 'Emas', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2638', 'Esperança', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2639', 'Fagundes', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2640', 'Frei Martinho', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2641', 'Gado Bravo', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2642', 'Guarabira', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2643', 'Gurinhém', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2644', 'Gurjão', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2645', 'Ibiara', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2646', 'Igaracy', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2647', 'Imaculada', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2648', 'Ingá', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2649', 'Itabaiana', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2650', 'Itaporanga', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2651', 'Itapororoca', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2652', 'Itatuba', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2653', 'Jacaraú', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2654', 'Jericó', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2655', 'João Pessoa', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2656', 'Juarez Távora', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2657', 'Juazeirinho', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2658', 'Junco do Seridó', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2659', 'Juripiranga', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2660', 'Juru', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2661', 'Lagoa', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2662', 'Lagoa de Dentro', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2663', 'Lagoa Seca', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2664', 'Lastro', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2665', 'Livramento', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2666', 'Logradouro', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2667', 'Lucena', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2668', 'Mãe d`Água', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2669', 'Malta', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2670', 'Mamanguape', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2671', 'Manaíra', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2672', 'Marcação', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2673', 'Mari', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2674', 'Marizópolis', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2675', 'Massaranduba', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2676', 'Mataraca', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2677', 'Matinhas', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2678', 'Mato Grosso', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2679', 'Maturéia', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2680', 'Mogeiro', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2681', 'Montadas', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2682', 'Monte Horebe', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2683', 'Monteiro', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2684', 'Mulungu', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2685', 'Natuba', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2686', 'Nazarezinho', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2687', 'Nova Floresta', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2688', 'Nova Olinda', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2689', 'Nova Palmeira', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2690', 'Olho d`Água', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2691', 'Olivedos', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2692', 'Ouro Velho', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2693', 'Parari', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2694', 'Passagem', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2695', 'Patos', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2696', 'Paulista', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2697', 'Pedra Branca', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2698', 'Pedra Lavrada', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2699', 'Pedras de Fogo', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2700', 'Pedro Régis', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2701', 'Piancó', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2702', 'Picuí', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2703', 'Pilar', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2704', 'Pilões', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2705', 'Pilõezinhos', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2706', 'Pirpirituba', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2707', 'Pitimbu', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2708', 'Pocinhos', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2709', 'Poço Dantas', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2710', 'Poço de José de Moura', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2711', 'Pombal', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2712', 'Prata', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2713', 'Princesa Isabel', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2714', 'Puxinanã', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2715', 'Queimadas', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2716', 'Quixabá', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2717', 'Remígio', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2718', 'Riachão', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2719', 'Riachão do Bacamarte', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2720', 'Riachão do Poço', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2721', 'Riacho de Santo Antônio', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2722', 'Riacho dos Cavalos', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2723', 'Rio Tinto', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2724', 'Salgadinho', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2725', 'Salgado de São Félix', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2726', 'Santa Cecília', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2727', 'Santa Cruz', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2728', 'Santa Helena', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2729', 'Santa Inês', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2730', 'Santa Luzia', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2731', 'Santa Rita', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2732', 'Santa Teresinha', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2733', 'Santana de Mangueira', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2734', 'Santana dos Garrotes', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2735', 'Santarém', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2736', 'Santo André', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2737', 'São Bentinho', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2738', 'São Bento', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2739', 'São Domingos de Pombal', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2740', 'São Domingos do Cariri', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2741', 'São Francisco', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2742', 'São João do Cariri', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2743', 'São João do Rio do Peixe', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2744', 'São João do Tigre', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2745', 'São José da Lagoa Tapada', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2746', 'São José de Caiana', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2747', 'São José de Espinharas', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2748', 'São José de Piranhas', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2749', 'São José de Princesa', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2750', 'São José do Bonfim', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2751', 'São José do Brejo do Cruz', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2752', 'São José do Sabugi', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2753', 'São José dos Cordeiros', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2754', 'São José dos Ramos', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2755', 'São Mamede', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2756', 'São Miguel de Taipu', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2757', 'São Sebastião de Lagoa de Roça', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2758', 'São Sebastião do Umbuzeiro', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2759', 'Sapé', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2760', 'Seridó', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2761', 'Serra Branca', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2762', 'Serra da Raiz', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2763', 'Serra Grande', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2764', 'Serra Redonda', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2765', 'Serraria', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2766', 'Sertãozinho', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2767', 'Sobrado', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2768', 'Solânea', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2769', 'Soledade', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2770', 'Sossêgo', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2771', 'Sousa', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2772', 'Sumé', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2773', 'Taperoá', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2774', 'Tavares', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2775', 'Teixeira', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2776', 'Tenório', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2777', 'Triunfo', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2778', 'Uiraúna', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2779', 'Umbuzeiro', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2780', 'Várzea', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2781', 'Vieirópolis', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2782', 'Vista Serrana', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2783', 'Zabelê', '15', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2784', 'Abatiá', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2785', 'Adrianópolis', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2786', 'Agudos do Sul', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2787', 'Almirante Tamandaré', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2788', 'Altamira do Paraná', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2789', 'Alto Paraíso', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2790', 'Alto Paraná', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2791', 'Alto Piquiri', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2792', 'Altônia', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2793', 'Alvorada do Sul', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2794', 'Amaporã', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2795', 'Ampére', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2796', 'Anahy', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2797', 'Andirá', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2798', 'Ângulo', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2799', 'Antonina', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2800', 'Antônio Olinto', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2801', 'Apucarana', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2802', 'Arapongas', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2803', 'Arapoti', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2804', 'Arapuã', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2805', 'Araruna', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2806', 'Araucária', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2807', 'Ariranha do Ivaí', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2808', 'Assaí', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2809', 'Assis Chateaubriand', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2810', 'Astorga', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2811', 'Atalaia', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2812', 'Balsa Nova', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2813', 'Bandeirantes', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2814', 'Barbosa Ferraz', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2815', 'Barra do Jacaré', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2816', 'Barracão', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2817', 'Bela Vista da Caroba', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2818', 'Bela Vista do Paraíso', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2819', 'Bituruna', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2820', 'Boa Esperança', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2821', 'Boa Esperança do Iguaçu', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2822', 'Boa Ventura de São Roque', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2823', 'Boa Vista da Aparecida', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2824', 'Bocaiúva do Sul', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2825', 'Bom Jesus do Sul', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2826', 'Bom Sucesso', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2827', 'Bom Sucesso do Sul', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2828', 'Borrazópolis', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2829', 'Braganey', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2830', 'Brasilândia do Sul', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2831', 'Cafeara', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2832', 'Cafelândia', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2833', 'Cafezal do Sul', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2834', 'Califórnia', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2835', 'Cambará', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2836', 'Cambé', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2837', 'Cambira', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2838', 'Campina da Lagoa', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2839', 'Campina do Simão', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2840', 'Campina Grande do Sul', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2841', 'Campo Bonito', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2842', 'Campo do Tenente', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2843', 'Campo Largo', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2844', 'Campo Magro', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2845', 'Campo Mourão', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2846', 'Cândido de Abreu', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2847', 'Candói', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2848', 'Cantagalo', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2849', 'Capanema', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2850', 'Capitão Leônidas Marques', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2851', 'Carambeí', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2852', 'Carlópolis', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2853', 'Cascavel', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2854', 'Castro', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2855', 'Catanduvas', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2856', 'Centenário do Sul', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2857', 'Cerro Azul', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2858', 'Céu Azul', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2859', 'Chopinzinho', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2860', 'Cianorte', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2861', 'cities Gaúcha', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2862', 'Clevelândia', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2863', 'Colombo', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2864', 'Colorado', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2865', 'Congonhinhas', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2866', 'Conselheiro Mairinck', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2867', 'Contenda', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2868', 'Corbélia', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2869', 'Cornélio Procópio', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2870', 'Coronel Domingos Soares', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2871', 'Coronel Vivida', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2872', 'Corumbataí do Sul', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2873', 'Cruz Machado', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2874', 'Cruzeiro do Iguaçu', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2875', 'Cruzeiro do Oeste', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2876', 'Cruzeiro do Sul', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2877', 'Cruzmaltina', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2878', 'Curitiba', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2879', 'Curiúva', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2880', 'Diamante d`Oeste', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2881', 'Diamante do Norte', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2882', 'Diamante do Sul', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2883', 'Dois Vizinhos', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2884', 'Douradina', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2885', 'Doutor Camargo', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2886', 'Doutor Ulysses', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2887', 'Enéas Marques', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2888', 'Engenheiro Beltrão', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2889', 'Entre Rios do Oeste', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2890', 'Esperança Nova', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2891', 'Espigão Alto do Iguaçu', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2892', 'Farol', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2893', 'Faxinal', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2894', 'Fazenda Rio Grande', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2895', 'Fênix', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2896', 'Fernandes Pinheiro', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2897', 'Figueira', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2898', 'Flor da Serra do Sul', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2899', 'Floraí', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2900', 'Floresta', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2901', 'Florestópolis', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2902', 'Flórida', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2903', 'Formosa do Oeste', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2904', 'Foz do Iguaçu', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2905', 'Foz do Jordão', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2906', 'Francisco Alves', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2907', 'Francisco Beltrão', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2908', 'General Carneiro', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2909', 'Godoy Moreira', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2910', 'Goioerê', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2911', 'Goioxim', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2912', 'Grandes Rios', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2913', 'Guaíra', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2914', 'Guairaçá', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2915', 'Guamiranga', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2916', 'Guapirama', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2917', 'Guaporema', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2918', 'Guaraci', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2919', 'Guaraniaçu', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2920', 'Guarapuava', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2921', 'Guaraqueçaba', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2922', 'Guaratuba', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2923', 'Honório Serpa', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2924', 'Ibaiti', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2925', 'Ibema', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2926', 'Ibiporã', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2927', 'Icaraíma', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2928', 'Iguaraçu', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2929', 'Iguatu', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2930', 'Imbaú', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2931', 'Imbituva', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2932', 'Inácio Martins', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2933', 'Inajá', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2934', 'Indianópolis', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2935', 'Ipiranga', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2936', 'Iporã', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2937', 'Iracema do Oeste', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2938', 'Irati', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2939', 'Iretama', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2940', 'Itaguajé', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2941', 'Itaipulândia', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2942', 'Itambaracá', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2943', 'Itambé', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2944', 'Itapejara d`Oeste', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2945', 'Itaperuçu', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2946', 'Itaúna do Sul', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2947', 'Ivaí', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2948', 'Ivaiporã', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2949', 'Ivaté', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2950', 'Ivatuba', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2951', 'Jaboti', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2952', 'Jacarezinho', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2953', 'Jaguapitã', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2954', 'Jaguariaíva', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2955', 'Jandaia do Sul', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2956', 'Janiópolis', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2957', 'Japira', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2958', 'Japurá', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2959', 'Jardim Alegre', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2960', 'Jardim Olinda', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2961', 'Jataizinho', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2962', 'Jesuítas', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2963', 'Joaquim Távora', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2964', 'Jundiaí do Sul', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2965', 'Juranda', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2966', 'Jussara', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2967', 'Kaloré', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2968', 'Lapa', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2969', 'Laranjal', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2970', 'Laranjeiras do Sul', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2971', 'Leópolis', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2972', 'Lidianópolis', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2973', 'Lindoeste', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2974', 'Loanda', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2975', 'Lobato', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2976', 'Londrina', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2977', 'Luiziana', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2978', 'Lunardelli', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2979', 'Lupionópolis', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2980', 'Mallet', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2981', 'Mamborê', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2982', 'Mandaguaçu', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2983', 'Mandaguari', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2984', 'Mandirituba', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2985', 'Manfrinópolis', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2986', 'Mangueirinha', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2987', 'Manoel Ribas', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2988', 'Marechal Cândido Rondon', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2989', 'Maria Helena', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2990', 'Marialva', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2991', 'Marilândia do Sul', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2992', 'Marilena', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2993', 'Mariluz', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2994', 'Maringá', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2995', 'Mariópolis', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2996', 'Maripá', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2997', 'Marmeleiro', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2998', 'Marquinho', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('2999', 'Marumbi', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3000', 'Matelândia', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3001', 'Matinhos', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3002', 'Mato Rico', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3003', 'Mauá da Serra', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3004', 'Medianeira', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3005', 'Mercedes', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3006', 'Mirador', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3007', 'Miraselva', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3008', 'Missal', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3009', 'Moreira Sales', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3010', 'Morretes', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3011', 'Munhoz de Melo', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3012', 'Nossa Senhora das Graças', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3013', 'Nova Aliança do Ivaí', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3014', 'Nova América da Colina', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3015', 'Nova Aurora', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3016', 'Nova Cantu', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3017', 'Nova Esperança', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3018', 'Nova Esperança do Sudoeste', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3019', 'Nova Fátima', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3020', 'Nova Laranjeiras', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3021', 'Nova Londrina', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3022', 'Nova Olímpia', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3023', 'Nova Prata do Iguaçu', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3024', 'Nova Santa Bárbara', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3025', 'Nova Santa Rosa', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3026', 'Nova Tebas', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3027', 'Novo Itacolomi', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3028', 'Ortigueira', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3029', 'Ourizona', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3030', 'Ouro Verde do Oeste', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3031', 'Paiçandu', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3032', 'Palmas', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3033', 'Palmeira', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3034', 'Palmital', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3035', 'Palotina', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3036', 'Paraíso do Norte', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3037', 'Paranacity', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3038', 'Paranaguá', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3039', 'Paranapoema', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3040', 'Paranavaí', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3041', 'Pato Bragado', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3042', 'Pato Branco', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3043', 'Paula Freitas', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3044', 'Paulo Frontin', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3045', 'Peabiru', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3046', 'Perobal', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3047', 'Pérola', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3048', 'Pérola d`Oeste', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3049', 'Piên', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3050', 'Pinhais', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3051', 'Pinhal de São Bento', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3052', 'Pinhalão', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3053', 'Pinhão', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3054', 'Piraí do Sul', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3055', 'Piraquara', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3056', 'Pitanga', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3057', 'Pitangueiras', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3058', 'Planaltina do Paraná', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3059', 'Planalto', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3060', 'Ponta Grossa', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3061', 'Pontal do Paraná', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3062', 'Porecatu', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3063', 'Porto Amazonas', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3064', 'Porto Barreiro', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3065', 'Porto Rico', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3066', 'Porto Vitória', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3067', 'Prado Ferreira', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3068', 'Pranchita', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3069', 'Presidente Castelo Branco', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3070', 'Primeiro de Maio', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3071', 'Prudentópolis', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3072', 'Quarto Centenário', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3073', 'Quatiguá', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3074', 'Quatro Barras', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3075', 'Quatro Pontes', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3076', 'Quedas do Iguaçu', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3077', 'Querência do Norte', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3078', 'Quinta do Sol', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3079', 'Quitandinha', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3080', 'Ramilândia', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3081', 'Rancho Alegre', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3082', 'Rancho Alegre d`Oeste', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3083', 'Realeza', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3084', 'Rebouças', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3085', 'Renascença', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3086', 'Reserva', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3087', 'Reserva do Iguaçu', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3088', 'Ribeirão Claro', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3089', 'Ribeirão do Pinhal', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3090', 'Rio Azul', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3091', 'Rio Bom', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3092', 'Rio Bonito do Iguaçu', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3093', 'Rio Branco do Ivaí', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3094', 'Rio Branco do Sul', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3095', 'Rio Negro', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3096', 'Rolândia', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3097', 'Roncador', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3098', 'Rondon', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3099', 'Rosário do Ivaí', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3100', 'Sabáudia', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3101', 'Salgado Filho', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3102', 'Salto do Itararé', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3103', 'Salto do Lontra', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3104', 'Santa Amélia', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3105', 'Santa Cecília do Pavão', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3106', 'Santa Cruz de Monte Castelo', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3107', 'Santa Fé', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3108', 'Santa Helena', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3109', 'Santa Inês', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3110', 'Santa Isabel do Ivaí', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3111', 'Santa Izabel do Oeste', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3112', 'Santa Lúcia', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3113', 'Santa Maria do Oeste', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3114', 'Santa Mariana', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3115', 'Santa Mônica', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3116', 'Santa Tereza do Oeste', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3117', 'Santa Terezinha de Itaipu', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3118', 'Santana do Itararé', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3119', 'Santo Antônio da Platina', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3120', 'Santo Antônio do Caiuá', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3121', 'Santo Antônio do Paraíso', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3122', 'Santo Antônio do Sudoeste', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3123', 'Santo Inácio', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3124', 'São Carlos do Ivaí', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3125', 'São Jerônimo da Serra', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3126', 'São João', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3127', 'São João do Caiuá', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3128', 'São João do Ivaí', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3129', 'São João do Triunfo', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3130', 'São Jorge d`Oeste', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3131', 'São Jorge do Ivaí', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3132', 'São Jorge do Patrocínio', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3133', 'São José da Boa Vista', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3134', 'São José das Palmeiras', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3135', 'São José dos Pinhais', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3136', 'São Manoel do Paraná', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3137', 'São Mateus do Sul', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3138', 'São Miguel do Iguaçu', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3139', 'São Pedro do Iguaçu', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3140', 'São Pedro do Ivaí', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3141', 'São Pedro do Paraná', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3142', 'São Sebastião da Amoreira', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3143', 'São Tomé', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3144', 'Sapopema', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3145', 'Sarandi', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3146', 'Saudade do Iguaçu', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3147', 'Sengés', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3148', 'Serranópolis do Iguaçu', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3149', 'Sertaneja', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3150', 'Sertanópolis', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3151', 'Siqueira Campos', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3152', 'Sulina', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3153', 'Tamarana', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3154', 'Tamboara', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3155', 'Tapejara', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3156', 'Tapira', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3157', 'Teixeira Soares', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3158', 'Telêmaco Borba', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3159', 'Terra Boa', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3160', 'Terra Rica', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3161', 'Terra Roxa', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3162', 'Tibagi', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3163', 'Tijucas do Sul', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3164', 'Toledo', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3165', 'Tomazina', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3166', 'Três Barras do Paraná', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3167', 'Tunas do Paraná', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3168', 'Tuneiras do Oeste', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3169', 'Tupãssi', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3170', 'Turvo', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3171', 'Ubiratã', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3172', 'Umuarama', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3173', 'União da Vitória', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3174', 'Uniflor', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3175', 'Uraí', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3176', 'Ventania', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3177', 'Vera Cruz do Oeste', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3178', 'Verê', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3179', 'Virmond', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3180', 'Vitorino', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3181', 'Wenceslau Braz', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3182', 'Xambrê', '18', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3183', 'Abreu e Lima', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3184', 'Afogados da Ingazeira', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3185', 'Afrânio', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3186', 'Agrestina', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3187', 'Água Preta', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3188', 'Águas Belas', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3189', 'Alagoinha', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3190', 'Aliança', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3191', 'Altinho', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3192', 'Amaraji', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3193', 'Angelim', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3194', 'Araçoiaba', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3195', 'Araripina', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3196', 'Arcoverde', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3197', 'Barra de Guabiraba', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3198', 'Barreiros', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3199', 'Belém de Maria', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3200', 'Belém de São Francisco', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3201', 'Belo Jardim', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3202', 'Betânia', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3203', 'Bezerros', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3204', 'Bodocó', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3205', 'Bom Conselho', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3206', 'Bom Jardim', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3207', 'Bonito', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3208', 'Brejão', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3209', 'Brejinho', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3210', 'Brejo da Madre de Deus', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3211', 'Buenos Aires', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3212', 'Buíque', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3213', 'Cabo de Santo Agostinho', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3214', 'Cabrobó', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3215', 'Cachoeirinha', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3216', 'Caetés', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3217', 'Calçado', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3218', 'Calumbi', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3219', 'Camaragibe', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3220', 'Camocim de São Félix', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3221', 'Camutanga', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3222', 'Canhotinho', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3223', 'Capoeiras', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3224', 'Carnaíba', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3225', 'Carnaubeira da Penha', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3226', 'Carpina', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3227', 'Caruaru', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3228', 'Casinhas', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3229', 'Catende', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3230', 'Cedro', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3231', 'Chã de Alegria', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3232', 'Chã Grande', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3233', 'Condado', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3234', 'Correntes', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3235', 'Cortês', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3236', 'Cumaru', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3237', 'Cupira', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3238', 'Custódia', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3239', 'Dormentes', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3240', 'Escada', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3241', 'Exu', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3242', 'Feira Nova', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3243', 'Fernando de Noronha', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3244', 'Ferreiros', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3245', 'Flores', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3246', 'Floresta', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3247', 'Frei Miguelinho', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3248', 'Gameleira', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3249', 'Garanhuns', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3250', 'Glória do Goitá', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3251', 'Goiana', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3252', 'Granito', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3253', 'Gravatá', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3254', 'Iati', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3255', 'Ibimirim', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3256', 'Ibirajuba', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3257', 'Igarassu', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3258', 'Iguaraci', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3259', 'Ilha de Itamaracá', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3260', 'Inajá', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3261', 'Ingazeira', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3262', 'Ipojuca', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3263', 'Ipubi', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3264', 'Itacuruba', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3265', 'Itaíba', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3266', 'Itambé', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3267', 'Itapetim', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3268', 'Itapissuma', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3269', 'Itaquitinga', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3270', 'Jaboatão dos Guararapes', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3271', 'Jaqueira', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3272', 'Jataúba', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3273', 'Jatobá', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3274', 'João Alfredo', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3275', 'Joaquim Nabuco', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3276', 'Jucati', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3277', 'Jupi', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3278', 'Jurema', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3279', 'Lagoa do Carro', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3280', 'Lagoa do Itaenga', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3281', 'Lagoa do Ouro', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3282', 'Lagoa dos Gatos', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3283', 'Lagoa Grande', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3284', 'Lajedo', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3285', 'Limoeiro', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3286', 'Macaparana', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3287', 'Machados', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3288', 'Manari', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3289', 'Maraial', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3290', 'Mirandiba', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3291', 'Moreilândia', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3292', 'Moreno', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3293', 'Nazaré da Mata', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3294', 'Olinda', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3295', 'Orobó', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3296', 'Orocó', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3297', 'Ouricuri', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3298', 'Palmares', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3299', 'Palmeirina', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3300', 'Panelas', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3301', 'Paranatama', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3302', 'Parnamirim', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3303', 'Passira', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3304', 'Paudalho', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3305', 'Paulista', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3306', 'Pedra', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3307', 'Pesqueira', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3308', 'Petrolândia', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3309', 'Petrolina', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3310', 'Poção', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3311', 'Pombos', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3312', 'Primavera', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3313', 'Quipapá', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3314', 'Quixaba', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3315', 'Recife', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3316', 'Riacho das Almas', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3317', 'Ribeirão', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3318', 'Rio Formoso', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3319', 'Sairé', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3320', 'Salgadinho', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3321', 'Salgueiro', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3322', 'Saloá', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3323', 'Sanharó', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3324', 'Santa Cruz', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3325', 'Santa Cruz da Baixa Verde', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3326', 'Santa Cruz do Capibaribe', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3327', 'Santa Filomena', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3328', 'Santa Maria da Boa Vista', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3329', 'Santa Maria do Cambucá', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3330', 'Santa Terezinha', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3331', 'São Benedito do Sul', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3332', 'São Bento do Una', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3333', 'São Caitano', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3334', 'São João', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3335', 'São Joaquim do Monte', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3336', 'São José da Coroa Grande', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3337', 'São José do Belmonte', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3338', 'São José do Egito', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3339', 'São Lourenço da Mata', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3340', 'São Vicente Ferrer', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3341', 'Serra Talhada', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3342', 'Serrita', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3343', 'Sertânia', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3344', 'Sirinhaém', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3345', 'Solidão', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3346', 'Surubim', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3347', 'Tabira', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3348', 'Tacaimbó', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3349', 'Tacaratu', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3350', 'Tamandaré', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3351', 'Taquaritinga do Norte', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3352', 'Terezinha', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3353', 'Terra Nova', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3354', 'Timbaúba', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3355', 'Toritama', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3356', 'Tracunhaém', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3357', 'Trindade', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3358', 'Triunfo', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3359', 'Tupanatinga', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3360', 'Tuparetama', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3361', 'Venturosa', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3362', 'Verdejante', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3363', 'Vertente do Lério', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3364', 'Vertentes', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3365', 'Vicência', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3366', 'Vitória de Santo Antão', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3367', 'Xexéu', '16', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3368', 'Acauã', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3369', 'Agricolândia', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3370', 'Água Branca', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3371', 'Alagoinha do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3372', 'Alegrete do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3373', 'Alto Longá', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3374', 'Altos', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3375', 'Alvorada do Gurguéia', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3376', 'Amarante', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3377', 'Angical do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3378', 'Anísio de Abreu', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3379', 'Antônio Almeida', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3380', 'Aroazes', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3381', 'Aroeiras do Itaim', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3382', 'Arraial', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3383', 'Assunção do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3384', 'Avelino Lopes', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3385', 'Baixa Grande do Ribeiro', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3386', 'Barra d`Alcântara', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3387', 'Barras', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3388', 'Barreiras do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3389', 'Barro Duro', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3390', 'Batalha', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3391', 'Bela Vista do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3392', 'Belém do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3393', 'Beneditinos', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3394', 'Bertolínia', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3395', 'Betânia do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3396', 'Boa Hora', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3397', 'Bocaina', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3398', 'Bom Jesus', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3399', 'Bom Princípio do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3400', 'Bonfim do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3401', 'Boqueirão do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3402', 'Brasileira', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3403', 'Brejo do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3404', 'Buriti dos Lopes', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3405', 'Buriti dos Montes', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3406', 'Cabeceiras do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3407', 'Cajazeiras do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3408', 'Cajueiro da Praia', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3409', 'Caldeirão Grande do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3410', 'Campinas do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3411', 'Campo Alegre do Fidalgo', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3412', 'Campo Grande do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3413', 'Campo Largo do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3414', 'Campo Maior', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3415', 'Canavieira', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3416', 'Canto do Buriti', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3417', 'Capitão de Campos', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3418', 'Capitão Gervásio Oliveira', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3419', 'Caracol', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3420', 'Caraúbas do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3421', 'Caridade do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3422', 'Castelo do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3423', 'Caxingó', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3424', 'Cocal', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3425', 'Cocal de Telha', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3426', 'Cocal dos Alves', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3427', 'Coivaras', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3428', 'Colônia do Gurguéia', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3429', 'Colônia do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3430', 'Conceição do Canindé', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3431', 'Coronel José Dias', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3432', 'Corrente', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3433', 'Cristalândia do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3434', 'Cristino Castro', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3435', 'Curimatá', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3436', 'Currais', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3437', 'Curral Novo do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3438', 'Curralinhos', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3439', 'Demerval Lobão', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3440', 'Dirceu Arcoverde', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3441', 'Dom Expedito Lopes', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3442', 'Dom Inocêncio', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3443', 'Domingos Mourão', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3444', 'Elesbão Veloso', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3445', 'Eliseu Martins', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3446', 'Esperantina', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3447', 'Fartura do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3448', 'Flores do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3449', 'Floresta do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3450', 'Floriano', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3451', 'Francinópolis', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3452', 'Francisco Ayres', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3453', 'Francisco Macedo', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3454', 'Francisco Santos', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3455', 'Fronteiras', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3456', 'Geminiano', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3457', 'Gilbués', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3458', 'Guadalupe', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3459', 'Guaribas', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3460', 'Hugo Napoleão', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3461', 'Ilha Grande', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3462', 'Inhuma', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3463', 'Ipiranga do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3464', 'Isaías Coelho', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3465', 'Itainópolis', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3466', 'Itaueira', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3467', 'Jacobina do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3468', 'Jaicós', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3469', 'Jardim do Mulato', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3470', 'Jatobá do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3471', 'Jerumenha', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3472', 'João Costa', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3473', 'Joaquim Pires', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3474', 'Joca Marques', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3475', 'José de Freitas', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3476', 'Juazeiro do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3477', 'Júlio Borges', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3478', 'Jurema', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3479', 'Lagoa Alegre', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3480', 'Lagoa de São Francisco', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3481', 'Lagoa do Barro do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3482', 'Lagoa do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3483', 'Lagoa do Sítio', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3484', 'Lagoinha do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3485', 'Landri Sales', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3486', 'Luís Correia', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3487', 'Luzilândia', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3488', 'Madeiro', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3489', 'Manoel Emídio', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3490', 'Marcolândia', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3491', 'Marcos Parente', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3492', 'Massapê do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3493', 'Matias Olímpio', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3494', 'Miguel Alves', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3495', 'Miguel Leão', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3496', 'Milton Brandão', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3497', 'Monsenhor Gil', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3498', 'Monsenhor Hipólito', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3499', 'Monte Alegre do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3500', 'Morro Cabeça no Tempo', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3501', 'Morro do Chapéu do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3502', 'Murici dos Portelas', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3503', 'Nazaré do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3504', 'Nossa Senhora de Nazaré', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3505', 'Nossa Senhora dos Remédios', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3506', 'Nova Santa Rita', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3507', 'Novo Oriente do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3508', 'Novo Santo Antônio', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3509', 'Oeiras', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3510', 'Olho d`Água do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3511', 'Padre Marcos', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3512', 'Paes Landim', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3513', 'Pajeú do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3514', 'Palmeira do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3515', 'Palmeirais', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3516', 'Paquetá', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3517', 'Parnaguá', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3518', 'Parnaíba', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3519', 'Passagem Franca do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3520', 'Patos do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3521', 'Pau d`Arco do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3522', 'Paulistana', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3523', 'Pavussu', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3524', 'Pedro II', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3525', 'Pedro Laurentino', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3526', 'Picos', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3527', 'Pimenteiras', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3528', 'Pio IX', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3529', 'Piracuruca', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3530', 'Piripiri', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3531', 'Porto', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3532', 'Porto Alegre do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3533', 'Prata do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3534', 'Queimada Nova', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3535', 'Redenção do Gurguéia', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3536', 'Regeneração', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3537', 'Riacho Frio', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3538', 'Ribeira do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3539', 'Ribeiro Gonçalves', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3540', 'Rio Grande do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3541', 'Santa Cruz do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3542', 'Santa Cruz dos Milagres', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3543', 'Santa Filomena', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3544', 'Santa Luz', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3545', 'Santa Rosa do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3546', 'Santana do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3547', 'Santo Antônio de Lisboa', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3548', 'Santo Antônio dos Milagres', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3549', 'Santo Inácio do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3550', 'São Braz do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3551', 'São Félix do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3552', 'São Francisco de Assis do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3553', 'São Francisco do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3554', 'São Gonçalo do Gurguéia', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3555', 'São Gonçalo do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3556', 'São João da Canabrava', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3557', 'São João da Fronteira', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3558', 'São João da Serra', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3559', 'São João da Varjota', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3560', 'São João do Arraial', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3561', 'São João do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3562', 'São José do Divino', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3563', 'São José do Peixe', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3564', 'São José do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3565', 'São Julião', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3566', 'São Lourenço do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3567', 'São Luis do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3568', 'São Miguel da Baixa Grande', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3569', 'São Miguel do Fidalgo', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3570', 'São Miguel do Tapuio', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3571', 'São Pedro do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3572', 'São Raimundo Nonato', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3573', 'Sebastião Barros', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3574', 'Sebastião Leal', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3575', 'Sigefredo Pacheco', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3576', 'Simões', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3577', 'Simplício Mendes', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3578', 'Socorro do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3579', 'Sussuapara', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3580', 'Tamboril do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3581', 'Tanque do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3582', 'Teresina', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3583', 'União', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3584', 'Uruçuí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3585', 'Valença do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3586', 'Várzea Branca', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3587', 'Várzea Grande', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3588', 'Vera Mendes', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3589', 'Vila Nova do Piauí', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3590', 'Wall Ferraz', '17', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3591', 'Angra dos Reis', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3592', 'Aperibé', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3593', 'Araruama', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3594', 'Areal', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3595', 'Armação dos Búzios', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3596', 'Arraial do Cabo', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3597', 'Barra do Piraí', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3598', 'Barra Mansa', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3599', 'Belford Roxo', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3600', 'Bom Jardim', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3601', 'Bom Jesus do Itabapoana', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3602', 'Cabo Frio', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3603', 'Cachoeiras de Macacu', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3604', 'Cambuci', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3605', 'Campos dos Goytacazes', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3606', 'Cantagalo', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3607', 'Carapebus', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3608', 'Cardoso Moreira', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3609', 'Carmo', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3610', 'Casimiro de Abreu', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3611', 'Comendador Levy Gasparian', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3612', 'Conceição de Macabu', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3613', 'Cordeiro', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3614', 'Duas Barras', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3615', 'Duque de Caxias', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3616', 'Engenheiro Paulo de Frontin', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3617', 'Guapimirim', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3618', 'Iguaba Grande', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3619', 'Itaboraí', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3620', 'Itaguaí', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3621', 'Italva', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3622', 'Itaocara', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3623', 'Itaperuna', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3624', 'Itatiaia', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3625', 'Japeri', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3626', 'Laje do Muriaé', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3627', 'Macaé', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3628', 'Macuco', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3629', 'Magé', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3630', 'Mangaratiba', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3631', 'Maricá', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3632', 'Mendes', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3633', 'Mesquita', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3634', 'Miguel Pereira', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3635', 'Miracema', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3636', 'Natividade', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3637', 'Nilópolis', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3638', 'Niterói', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3639', 'Nova Friburgo', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3640', 'Nova Iguaçu', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3641', 'Paracambi', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3642', 'Paraíba do Sul', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3643', 'Parati', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3644', 'Paty do Alferes', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3645', 'Petrópolis', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3646', 'Pinheiral', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3647', 'Piraí', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3648', 'Porciúncula', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3649', 'Porto Real', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3650', 'Quatis', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3651', 'Queimados', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3652', 'Quissamã', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3653', 'Resende', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3654', 'Rio Bonito', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3655', 'Rio Claro', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3656', 'Rio das Flores', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3657', 'Rio das Ostras', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3658', 'Rio de Janeiro', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3659', 'Santa Maria Madalena', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3660', 'Santo Antônio de Pádua', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3661', 'São Fidélis', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3662', 'São Francisco de Itabapoana', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3663', 'São Gonçalo', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3664', 'São João da Barra', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3665', 'São João de Meriti', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3666', 'São José de Ubá', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3667', 'São José do Vale do Rio Pret', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3668', 'São Pedro da Aldeia', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3669', 'São Sebastião do Alto', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3670', 'Sapucaia', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3671', 'Saquarema', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3672', 'Seropédica', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3673', 'Silva Jardim', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3674', 'Sumidouro', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3675', 'Tanguá', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3676', 'Teresópolis', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3677', 'Trajano de Morais', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3678', 'Três Rios', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3679', 'Valença', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3680', 'Varre-Sai', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3681', 'Vassouras', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3682', 'Volta Redonda', '19', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3683', 'Acari', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3684', 'Açu', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3685', 'Afonso Bezerra', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3686', 'Água Nova', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3687', 'Alexandria', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3688', 'Almino Afonso', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3689', 'Alto do Rodrigues', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3690', 'Angicos', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3691', 'Antônio Martins', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3692', 'Apodi', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3693', 'Areia Branca', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3694', 'Arês', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3695', 'Augusto Severo', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3696', 'Baía Formosa', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3697', 'Baraúna', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3698', 'Barcelona', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3699', 'Bento Fernandes', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3700', 'Bodó', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3701', 'Bom Jesus', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3702', 'Brejinho', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3703', 'Caiçara do Norte', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3704', 'Caiçara do Rio do Vento', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3705', 'Caicó', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3706', 'Campo Redondo', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3707', 'Canguaretama', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3708', 'Caraúbas', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3709', 'Carnaúba dos Dantas', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3710', 'Carnaubais', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3711', 'Ceará-Mirim', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3712', 'Cerro Corá', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3713', 'Coronel Ezequiel', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3714', 'Coronel João Pessoa', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3715', 'Cruzeta', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3716', 'Currais Novos', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3717', 'Doutor Severiano', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3718', 'Encanto', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3719', 'Equador', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3720', 'Espírito Santo', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3721', 'Extremoz', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3722', 'Felipe Guerra', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3723', 'Fernando Pedroza', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3724', 'Florânia', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3725', 'Francisco Dantas', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3726', 'Frutuoso Gomes', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3727', 'Galinhos', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3728', 'Goianinha', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3729', 'Governador Dix-Sept Rosado', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3730', 'Grossos', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3731', 'Guamaré', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3732', 'Ielmo Marinho', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3733', 'Ipanguaçu', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3734', 'Ipueira', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3735', 'Itajá', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3736', 'Itaú', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3737', 'Jaçanã', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3738', 'Jandaíra', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3739', 'Janduís', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3740', 'Januário Cicco', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3741', 'Japi', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3742', 'Jardim de Angicos', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3743', 'Jardim de Piranhas', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3744', 'Jardim do Seridó', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3745', 'João Câmara', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3746', 'João Dias', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3747', 'José da Penha', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3748', 'Jucurutu', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3749', 'Jundiá', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3750', 'Lagoa d`Anta', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3751', 'Lagoa de Pedras', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3752', 'Lagoa de Velhos', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3753', 'Lagoa Nova', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3754', 'Lagoa Salgada', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3755', 'Lajes', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3756', 'Lajes Pintadas', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3757', 'Lucrécia', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3758', 'Luís Gomes', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3759', 'Macaíba', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3760', 'Macau', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3761', 'Major Sales', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3762', 'Marcelino Vieira', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3763', 'Martins', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3764', 'Maxaranguape', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3765', 'Messias Targino', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3766', 'Montanhas', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3767', 'Monte Alegre', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3768', 'Monte das Gameleiras', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3769', 'Mossoró', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3770', 'Natal', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3771', 'Nísia Floresta', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3772', 'Nova Cruz', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3773', 'Olho-d`Água do Borges', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3774', 'Ouro Branco', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3775', 'Paraná', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3776', 'Paraú', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3777', 'Parazinho', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3778', 'Parelhas', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3779', 'Parnamirim', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3780', 'Passa e Fica', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3781', 'Passagem', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3782', 'Patu', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3783', 'Pau dos Ferros', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3784', 'Pedra Grande', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3785', 'Pedra Preta', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3786', 'Pedro Avelino', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3787', 'Pedro Velho', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3788', 'Pendências', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3789', 'Pilões', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3790', 'Poço Branco', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3791', 'Portalegre', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3792', 'Porto do Mangue', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3793', 'Presidente Juscelino', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3794', 'Pureza', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3795', 'Rafael Fernandes', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3796', 'Rafael Godeiro', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3797', 'Riacho da Cruz', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3798', 'Riacho de Santana', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3799', 'Riachuelo', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3800', 'Rio do Fogo', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3801', 'Rodolfo Fernandes', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3802', 'Ruy Barbosa', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3803', 'Santa Cruz', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3804', 'Santa Maria', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3805', 'Santana do Matos', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3806', 'Santana do Seridó', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3807', 'Santo Antônio', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3808', 'São Bento do Norte', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3809', 'São Bento do Trairí', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3810', 'São Fernando', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3811', 'São Francisco do Oeste', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3812', 'São Gonçalo do Amarante', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3813', 'São João do Sabugi', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3814', 'São José de Mipibu', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3815', 'São José do Campestre', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3816', 'São José do Seridó', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3817', 'São Miguel', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3818', 'São Miguel do Gostoso', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3819', 'São Paulo do Potengi', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3820', 'São Pedro', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3821', 'São Rafael', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3822', 'São Tomé', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3823', 'São Vicente', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3824', 'Senador Elói de Souza', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3825', 'Senador Georgino Avelino', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3826', 'Serra de São Bento', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3827', 'Serra do Mel', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3828', 'Serra Negra do Norte', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3829', 'Serrinha', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3830', 'Serrinha dos Pintos', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3831', 'Severiano Melo', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3832', 'Sítio Novo', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3833', 'Taboleiro Grande', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3834', 'Taipu', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3835', 'Tangará', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3836', 'Tenente Ananias', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3837', 'Tenente Laurentino Cruz', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3838', 'Tibau', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3839', 'Tibau do Sul', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3840', 'Timbaúba dos Batistas', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3841', 'Touros', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3842', 'Triunfo Potiguar', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3843', 'Umarizal', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3844', 'Upanema', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3845', 'Várzea', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3846', 'Venha-Ver', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3847', 'Vera Cruz', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3848', 'Viçosa', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3849', 'Vila Flor', '20', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3850', 'Aceguá', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3851', 'Água Santa', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3852', 'Agudo', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3853', 'Ajuricaba', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3854', 'Alecrim', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3855', 'Alegrete', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3856', 'Alegria', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3857', 'Almirante Tamandaré do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3858', 'Alpestre', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3859', 'Alto Alegre', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3860', 'Alto Feliz', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3861', 'Alvorada', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3862', 'Amaral Ferrador', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3863', 'Ametista do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3864', 'André da Rocha', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3865', 'Anta Gorda', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3866', 'Antônio Prado', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3867', 'Arambaré', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3868', 'Araricá', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3869', 'Aratiba', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3870', 'Arroio do Meio', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3871', 'Arroio do Padre', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3872', 'Arroio do Sal', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3873', 'Arroio do Tigre', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3874', 'Arroio dos Ratos', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3875', 'Arroio Grande', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3876', 'Arvorezinha', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3877', 'Augusto Pestana', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3878', 'Áurea', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3879', 'Bagé', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3880', 'Balneário Pinhal', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3881', 'Barão', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3882', 'Barão de Cotegipe', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3883', 'Barão do Triunfo', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3884', 'Barra do Guarita', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3885', 'Barra do Quaraí', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3886', 'Barra do Ribeiro', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3887', 'Barra do Rio Azul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3888', 'Barra Funda', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3889', 'Barracão', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3890', 'Barros Cassal', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3891', 'Benjamin Constant do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3892', 'Bento Gonçalves', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3893', 'Boa Vista das Missões', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3894', 'Boa Vista do Buricá', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3895', 'Boa Vista do Cadeado', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3896', 'Boa Vista do Incra', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3897', 'Boa Vista do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3898', 'Bom Jesus', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3899', 'Bom Princípio', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3900', 'Bom Progresso', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3901', 'Bom Retiro do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3902', 'Boqueirão do Leão', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3903', 'Bossoroca', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3904', 'Bozano', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3905', 'Braga', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3906', 'Brochier', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3907', 'Butiá', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3908', 'Caçapava do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3909', 'Cacequi', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3910', 'Cachoeira do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3911', 'Cachoeirinha', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3912', 'Cacique Doble', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3913', 'Caibaté', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3914', 'Caiçara', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3915', 'Camaquã', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3916', 'Camargo', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3917', 'Cambará do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3918', 'Campestre da Serra', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3919', 'Campina das Missões', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3920', 'Campinas do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3921', 'Campo Bom', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3922', 'Campo Novo', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3923', 'Campos Borges', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3924', 'Candelária', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3925', 'Cândido Godói', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3926', 'Candiota', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3927', 'Canela', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3928', 'Canguçu', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3929', 'Canoas', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3930', 'Canudos do Vale', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3931', 'Capão Bonito do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3932', 'Capão da Canoa', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3933', 'Capão do Cipó', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3934', 'Capão do Leão', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3935', 'Capela de Santana', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3936', 'Capitão', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3937', 'Capivari do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3938', 'Caraá', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3939', 'Carazinho', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3940', 'Carlos Barbosa', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3941', 'Carlos Gomes', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3942', 'Casca', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3943', 'Caseiros', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3944', 'Catuípe', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3945', 'Caxias do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3946', 'Centenário', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3947', 'Cerrito', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3948', 'Cerro Branco', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3949', 'Cerro Grande', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3950', 'Cerro Grande do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3951', 'Cerro Largo', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3952', 'Chapada', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3953', 'Charqueadas', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3954', 'Charrua', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3955', 'Chiapeta', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3956', 'Chuí', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3957', 'Chuvisca', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3958', 'Cidreira', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3959', 'Ciríaco', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3960', 'Colinas', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3961', 'Colorado', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3962', 'Condor', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3963', 'Constantina', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3964', 'Coqueiro Baixo', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3965', 'Coqueiros do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3966', 'Coronel Barros', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3967', 'Coronel Bicaco', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3968', 'Coronel Pilar', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3969', 'Cotiporã', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3970', 'Coxilha', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3971', 'Crissiumal', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3972', 'Cristal', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3973', 'Cristal do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3974', 'Cruz Alta', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3975', 'Cruzaltense', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3976', 'Cruzeiro do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3977', 'David Canabarro', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3978', 'Derrubadas', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3979', 'Dezesseis de Novembro', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3980', 'Dilermando de Aguiar', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3981', 'Dois Irmãos', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3982', 'Dois Irmãos das Missões', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3983', 'Dois Lajeados', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3984', 'Dom Feliciano', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3985', 'Dom Pedrito', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3986', 'Dom Pedro de Alcântara', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3987', 'Dona Francisca', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3988', 'Doutor Maurício Cardoso', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3989', 'Doutor Ricardo', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3990', 'Eldorado do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3991', 'Encantado', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3992', 'Encruzilhada do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3993', 'Engenho Velho', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3994', 'Entre Rios do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3995', 'Entre-Ijuís', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3996', 'Erebango', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3997', 'Erechim', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3998', 'Ernestina', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('3999', 'Erval Grande', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4000', 'Erval Seco', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4001', 'Esmeralda', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4002', 'Esperança do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4003', 'Espumoso', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4004', 'Estação', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4005', 'Estância Velha', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4006', 'Esteio', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4007', 'Estrela', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4008', 'Estrela Velha', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4009', 'Eugênio de Castro', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4010', 'Fagundes Varela', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4011', 'Farroupilha', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4012', 'Faxinal do Soturno', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4013', 'Faxinalzinho', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4014', 'Fazenda Vilanova', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4015', 'Feliz', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4016', 'Flores da Cunha', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4017', 'Floriano Peixoto', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4018', 'Fontoura Xavier', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4019', 'Formigueiro', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4020', 'Forquetinha', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4021', 'Fortaleza dos Valos', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4022', 'Frederico Westphalen', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4023', 'Garibaldi', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4024', 'Garruchos', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4025', 'Gaurama', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4026', 'General Câmara', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4027', 'Gentil', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4028', 'Getúlio Vargas', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4029', 'Giruá', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4030', 'Glorinha', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4031', 'Gramado', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4032', 'Gramado dos Loureiros', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4033', 'Gramado Xavier', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4034', 'Gravataí', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4035', 'Guabiju', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4036', 'Guaíba', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4037', 'Guaporé', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4038', 'Guarani das Missões', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4039', 'Harmonia', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4040', 'Herval', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4041', 'Herveiras', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4042', 'Horizontina', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4043', 'Hulha Negra', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4044', 'Humaitá', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4045', 'Ibarama', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4046', 'Ibiaçá', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4047', 'Ibiraiaras', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4048', 'Ibirapuitã', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4049', 'Ibirubá', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4050', 'Igrejinha', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4051', 'Ijuí', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4052', 'Ilópolis', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4053', 'Imbé', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4054', 'Imigrante', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4055', 'Independência', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4056', 'Inhacorá', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4057', 'Ipê', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4058', 'Ipiranga do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4059', 'Iraí', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4060', 'Itaara', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4061', 'Itacurubi', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4062', 'Itapuca', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4063', 'Itaqui', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4064', 'Itati', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4065', 'Itatiba do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4066', 'Ivorá', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4067', 'Ivoti', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4068', 'Jaboticaba', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4069', 'Jacuizinho', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4070', 'Jacutinga', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4071', 'Jaguarão', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4072', 'Jaguari', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4073', 'Jaquirana', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4074', 'Jari', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4075', 'Jóia', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4076', 'Júlio de Castilhos', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4077', 'Lagoa Bonita do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4078', 'Lagoa dos Três Cantos', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4079', 'Lagoa Vermelha', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4080', 'Lagoão', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4081', 'Lajeado', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4082', 'Lajeado do Bugre', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4083', 'Lavras do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4084', 'Liberato Salzano', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4085', 'Lindolfo Collor', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4086', 'Linha Nova', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4087', 'Maçambara', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4088', 'Machadinho', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4089', 'Mampituba', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4090', 'Manoel Viana', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4091', 'Maquiné', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4092', 'Maratá', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4093', 'Marau', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4094', 'Marcelino Ramos', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4095', 'Mariana Pimentel', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4096', 'Mariano Moro', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4097', 'Marques de Souza', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4098', 'Mata', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4099', 'Mato Castelhano', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4100', 'Mato Leitão', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4101', 'Mato Queimado', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4102', 'Maximiliano de Almeida', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4103', 'Minas do Leão', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4104', 'Miraguaí', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4105', 'Montauri', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4106', 'Monte Alegre dos Campos', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4107', 'Monte Belo do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4108', 'Montenegro', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4109', 'Mormaço', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4110', 'Morrinhos do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4111', 'Morro Redondo', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4112', 'Morro Reuter', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4113', 'Mostardas', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4114', 'Muçum', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4115', 'Muitos Capões', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4116', 'Muliterno', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4117', 'Não-Me-Toque', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4118', 'Nicolau Vergueiro', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4119', 'Nonoai', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4120', 'Nova Alvorada', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4121', 'Nova Araçá', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4122', 'Nova Bassano', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4123', 'Nova Boa Vista', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4124', 'Nova Bréscia', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4125', 'Nova Candelária', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4126', 'Nova Esperança do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4127', 'Nova Hartz', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4128', 'Nova Pádua', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4129', 'Nova Palma', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4130', 'Nova Petrópolis', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4131', 'Nova Prata', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4132', 'Nova Ramada', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4133', 'Nova Roma do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4134', 'Nova Santa Rita', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4135', 'Novo Barreiro', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4136', 'Novo Cabrais', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4137', 'Novo Hamburgo', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4138', 'Novo Machado', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4139', 'Novo Tiradentes', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4140', 'Novo Xingu', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4141', 'Osório', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4142', 'Paim Filho', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4143', 'Palmares do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4144', 'Palmeira das Missões', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4145', 'Palmitinho', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4146', 'Panambi', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4147', 'Pantano Grande', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4148', 'Paraí', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4149', 'Paraíso do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4150', 'Pareci Novo', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4151', 'Parobé', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4152', 'Passa Sete', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4153', 'Passo do Sobrado', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4154', 'Passo Fundo', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4155', 'Paulo Bento', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4156', 'Paverama', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4157', 'Pedras Altas', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4158', 'Pedro Osório', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4159', 'Pejuçara', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4160', 'Pelotas', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4161', 'Picada Café', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4162', 'Pinhal', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4163', 'Pinhal da Serra', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4164', 'Pinhal Grande', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4165', 'Pinheirinho do Vale', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4166', 'Pinheiro Machado', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4167', 'Pirapó', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4168', 'Piratini', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4169', 'Planalto', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4170', 'Poço das Antas', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4171', 'Pontão', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4172', 'Ponte Preta', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4173', 'Portão', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4174', 'Porto Alegre', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4175', 'Porto Lucena', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4176', 'Porto Mauá', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4177', 'Porto Vera Cruz', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4178', 'Porto Xavier', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4179', 'Pouso Novo', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4180', 'Presidente Lucena', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4181', 'Progresso', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4182', 'Protásio Alves', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4183', 'Putinga', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4184', 'Quaraí', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4185', 'Quatro Irmãos', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4186', 'Quevedos', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4187', 'Quinze de Novembro', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4188', 'Redentora', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4189', 'Relvado', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4190', 'Restinga Seca', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4191', 'Rio dos Índios', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4192', 'Rio Grande', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4193', 'Rio Pardo', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4194', 'Riozinho', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4195', 'Roca Sales', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4196', 'Rodeio Bonito', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4197', 'Rolador', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4198', 'Rolante', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4199', 'Ronda Alta', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4200', 'Rondinha', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4201', 'Roque Gonzales', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4202', 'Rosário do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4203', 'Sagrada Família', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4204', 'Saldanha Marinho', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4205', 'Salto do Jacuí', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4206', 'Salvador das Missões', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4207', 'Salvador do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4208', 'Sananduva', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4209', 'Santa Bárbara do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4210', 'Santa Cecília do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4211', 'Santa Clara do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4212', 'Santa Cruz do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4213', 'Santa Margarida do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4214', 'Santa Maria', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4215', 'Santa Maria do Herval', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4216', 'Santa Rosa', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4217', 'Santa Tereza', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4218', 'Santa Vitória do Palmar', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4219', 'Santana da Boa Vista', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4220', 'Santana do Livramento', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4221', 'Santiago', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4222', 'Santo Ângelo', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4223', 'Santo Antônio da Patrulha', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4224', 'Santo Antônio das Missões', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4225', 'Santo Antônio do Palma', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4226', 'Santo Antônio do Planalto', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4227', 'Santo Augusto', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4228', 'Santo Cristo', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4229', 'Santo Expedito do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4230', 'São Borja', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4231', 'São Domingos do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4232', 'São Francisco de Assis', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4233', 'São Francisco de Paula', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4234', 'São Gabriel', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4235', 'São Jerônimo', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4236', 'São João da Urtiga', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4237', 'São João do Polêsine', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4238', 'São Jorge', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4239', 'São José das Missões', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4240', 'São José do Herval', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4241', 'São José do Hortêncio', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4242', 'São José do Inhacorá', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4243', 'São José do Norte', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4244', 'São José do Ouro', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4245', 'São José do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4246', 'São José dos Ausentes', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4247', 'São Leopoldo', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4248', 'São Lourenço do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4249', 'São Luiz Gonzaga', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4250', 'São Marcos', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4251', 'São Martinho', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4252', 'São Martinho da Serra', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4253', 'São Miguel das Missões', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4254', 'São Nicolau', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4255', 'São Paulo das Missões', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4256', 'São Pedro da Serra', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4257', 'São Pedro das Missões', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4258', 'São Pedro do Butiá', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4259', 'São Pedro do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4260', 'São Sebastião do Caí', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4261', 'São Sepé', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4262', 'São Valentim', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4263', 'São Valentim do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4264', 'São Valério do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4265', 'São Vendelino', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4266', 'São Vicente do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4267', 'Sapiranga', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4268', 'Sapucaia do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4269', 'Sarandi', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4270', 'Seberi', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4271', 'Sede Nova', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4272', 'Segredo', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4273', 'Selbach', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4274', 'Senador Salgado Filho', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4275', 'Sentinela do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4276', 'Serafina Corrêa', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4277', 'Sério', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4278', 'Sertão', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4279', 'Sertão Santana', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4280', 'Sete de Setembro', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4281', 'Severiano de Almeida', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4282', 'Silveira Martins', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4283', 'Sinimbu', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4284', 'Sobradinho', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4285', 'Soledade', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4286', 'Tabaí', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4287', 'Tapejara', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4288', 'Tapera', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4289', 'Tapes', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4290', 'Taquara', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4291', 'Taquari', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4292', 'Taquaruçu do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4293', 'Tavares', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4294', 'Tenente Portela', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4295', 'Terra de Areia', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4296', 'Teutônia', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4297', 'Tio Hugo', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4298', 'Tiradentes do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4299', 'Toropi', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4300', 'Torres', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4301', 'Tramandaí', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4302', 'Travesseiro', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4303', 'Três Arroios', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4304', 'Três Cachoeiras', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4305', 'Três Coroas', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4306', 'Três de Maio', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4307', 'Três Forquilhas', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4308', 'Três Palmeiras', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4309', 'Três Passos', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4310', 'Trindade do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4311', 'Triunfo', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4312', 'Tucunduva', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4313', 'Tunas', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4314', 'Tupanci do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4315', 'Tupanciretã', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4316', 'Tupandi', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4317', 'Tuparendi', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4318', 'Turuçu', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4319', 'Ubiretama', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4320', 'União da Serra', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4321', 'Unistalda', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4322', 'Uruguaiana', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4323', 'Vacaria', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4324', 'Vale do Sol', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4325', 'Vale Real', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4326', 'Vale Verde', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4327', 'Vanini', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4328', 'Venâncio Aires', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4329', 'Vera Cruz', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4330', 'Veranópolis', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4331', 'Vespasiano Correa', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4332', 'Viadutos', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4333', 'Viamão', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4334', 'Vicente Dutra', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4335', 'Victor Graeff', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4336', 'Vila Flores', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4337', 'Vila Lângaro', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4338', 'Vila Maria', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4339', 'Vila Nova do Sul', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4340', 'Vista Alegre', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4341', 'Vista Alegre do Prata', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4342', 'Vista Gaúcha', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4343', 'Vitória das Missões', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4344', 'Westfália', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4345', 'Xangri-lá', '23', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4346', 'Alta Floresta d`Oeste', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4347', 'Alto Alegre dos Parecis', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4348', 'Alto Paraíso', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4349', 'Alvorada d`Oeste', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4350', 'Ariquemes', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4351', 'Buritis', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4352', 'Cabixi', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4353', 'Cacaulândia', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4354', 'Cacoal', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4355', 'Campo Novo de Rondônia', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4356', 'Candeias do Jamari', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4357', 'Castanheiras', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4358', 'Cerejeiras', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4359', 'Chupinguaia', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4360', 'Colorado do Oeste', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4361', 'Corumbiara', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4362', 'Costa Marques', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4363', 'Cujubim', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4364', 'Espigão d`Oeste', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4365', 'Governador Jorge Teixeira', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4366', 'Guajará-Mirim', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4367', 'Itapuã do Oeste', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4368', 'Jaru', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4369', 'Ji-Paraná', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4370', 'Machadinho d`Oeste', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4371', 'Ministro Andreazza', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4372', 'Mirante da Serra', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4373', 'Monte Negro', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4374', 'Nova Brasilândia d`Oeste', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4375', 'Nova Mamoré', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4376', 'Nova União', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4377', 'Novo Horizonte do Oeste', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4378', 'Ouro Preto do Oeste', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4379', 'Parecis', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4380', 'Pimenta Bueno', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4381', 'Pimenteiras do Oeste', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4382', 'Porto Velho', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4383', 'Presidente Médici', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4384', 'Primavera de Rondônia', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4385', 'Rio Crespo', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4386', 'Rolim de Moura', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4387', 'Santa Luzia d`Oeste', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4388', 'São Felipe d`Oeste', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4389', 'São Francisco do Guaporé', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4390', 'São Miguel do Guaporé', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4391', 'Seringueiras', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4392', 'Teixeirópolis', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4393', 'Theobroma', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4394', 'Urupá', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4395', 'Vale do Anari', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4396', 'Vale do Paraíso', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4397', 'Vilhena', '21', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4398', 'Alto Alegre', '22', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4399', 'Amajari', '22', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4400', 'Boa Vista', '22', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4401', 'Bonfim', '22', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4402', 'Cantá', '22', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4403', 'Caracaraí', '22', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4404', 'Caroebe', '22', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4405', 'Iracema', '22', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4406', 'Mucajaí', '22', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4407', 'Normandia', '22', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4408', 'Pacaraima', '22', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4409', 'Rorainópolis', '22', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4410', 'São João da Baliza', '22', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4411', 'São Luiz', '22', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4412', 'Uiramutã', '22', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4413', 'Abdon Batista', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4414', 'Abelardo Luz', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4415', 'Agrolândia', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4416', 'Agronômica', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4417', 'Água Doce', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4418', 'Águas de Chapecó', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4419', 'Águas Frias', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4420', 'Águas Mornas', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4421', 'Alfredo Wagner', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4422', 'Alto Bela Vista', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4423', 'Anchieta', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4424', 'Angelina', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4425', 'Anita Garibaldi', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4426', 'Anitápolis', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4427', 'Antônio Carlos', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4428', 'Apiúna', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4429', 'Arabutã', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4430', 'Araquari', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4431', 'Araranguá', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4432', 'Armazém', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4433', 'Arroio Trinta', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4434', 'Arvoredo', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4435', 'Ascurra', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4436', 'Atalanta', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4437', 'Aurora', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4438', 'Balneário Arroio do Silva', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4439', 'Balneário Barra do Sul', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4440', 'Balneário Camboriú', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4441', 'Balneário Gaivota', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4442', 'Bandeirante', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4443', 'Barra Bonita', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4444', 'Barra Velha', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4445', 'Bela Vista do Toldo', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4446', 'Belmonte', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4447', 'Benedito Novo', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4448', 'Biguaçu', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4449', 'Blumenau', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4450', 'Bocaina do Sul', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4451', 'Bom Jardim da Serra', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4452', 'Bom Jesus', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4453', 'Bom Jesus do Oeste', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4454', 'Bom Retiro', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4455', 'Bombinhas', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4456', 'Botuverá', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4457', 'Braço do Norte', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4458', 'Braço do Trombudo', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4459', 'Brunópolis', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4460', 'Brusque', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4461', 'Caçador', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4462', 'Caibi', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4463', 'Calmon', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4464', 'Camboriú', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4465', 'Campo Alegre', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4466', 'Campo Belo do Sul', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4467', 'Campo Erê', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4468', 'Campos Novos', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4469', 'Canelinha', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4470', 'Canoinhas', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4471', 'Capão Alto', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4472', 'Capinzal', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4473', 'Capivari de Baixo', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4474', 'Catanduvas', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4475', 'Caxambu do Sul', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4476', 'Celso Ramos', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4477', 'Cerro Negro', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4478', 'Chapadão do Lageado', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4479', 'Chapecó', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4480', 'Cocal do Sul', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4481', 'Concórdia', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4482', 'Cordilheira Alta', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4483', 'Coronel Freitas', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4484', 'Coronel Martins', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4485', 'Correia Pinto', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4486', 'Corupá', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4487', 'Criciúma', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4488', 'Cunha Porã', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4489', 'Cunhataí', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4490', 'Curitibanos', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4491', 'Descanso', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4492', 'Dionísio Cerqueira', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4493', 'Dona Emma', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4494', 'Doutor Pedrinho', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4495', 'Entre Rios', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4496', 'Ermo', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4497', 'Erval Velho', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4498', 'Faxinal dos Guedes', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4499', 'Flor do Sertão', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4500', 'Florianópolis', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4501', 'Formosa do Sul', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4502', 'Forquilhinha', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4503', 'Fraiburgo', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4504', 'Frei Rogério', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4505', 'Galvão', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4506', 'Garopaba', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4507', 'Garuva', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4508', 'Gaspar', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4509', 'Governador Celso Ramos', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4510', 'Grão Pará', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4511', 'Gravatal', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4512', 'Guabiruba', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4513', 'Guaraciaba', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4514', 'Guaramirim', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4515', 'Guarujá do Sul', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4516', 'Guatambú', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4517', 'Herval d`Oeste', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4518', 'Ibiam', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4519', 'Ibicaré', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4520', 'Ibirama', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4521', 'Içara', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4522', 'Ilhota', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4523', 'Imaruí', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4524', 'Imbituba', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4525', 'Imbuia', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4526', 'Indaial', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4527', 'Iomerê', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4528', 'Ipira', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4529', 'Iporã do Oeste', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4530', 'Ipuaçu', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4531', 'Ipumirim', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4532', 'Iraceminha', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4533', 'Irani', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4534', 'Irati', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4535', 'Irineópolis', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4536', 'Itá', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4537', 'Itaiópolis', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4538', 'Itajaí', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4539', 'Itapema', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4540', 'Itapiranga', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4541', 'Itapoá', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4542', 'Ituporanga', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4543', 'Jaborá', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4544', 'Jacinto Machado', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4545', 'Jaguaruna', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4546', 'Jaraguá do Sul', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4547', 'Jardinópolis', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4548', 'Joaçaba', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4549', 'Joinville', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4550', 'José Boiteux', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4551', 'Jupiá', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4552', 'Lacerdópolis', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4553', 'Lages', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4554', 'Laguna', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4555', 'Lajeado Grande', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4556', 'Laurentino', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4557', 'Lauro Muller', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4558', 'Lebon Régis', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4559', 'Leoberto Leal', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4560', 'Lindóia do Sul', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4561', 'Lontras', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4562', 'Luiz Alves', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4563', 'Luzerna', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4564', 'Macieira', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4565', 'Mafra', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4566', 'Major Gercino', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4567', 'Major Vieira', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4568', 'Maracajá', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4569', 'Maravilha', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4570', 'Marema', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4571', 'Massaranduba', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4572', 'Matos Costa', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4573', 'Meleiro', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4574', 'Mirim Doce', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4575', 'Modelo', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4576', 'Mondaí', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4577', 'Monte Carlo', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4578', 'Monte Castelo', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4579', 'Morro da Fumaça', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4580', 'Morro Grande', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4581', 'Navegantes', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4582', 'Nova Erechim', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4583', 'Nova Itaberaba', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4584', 'Nova Trento', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4585', 'Nova Veneza', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4586', 'Novo Horizonte', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4587', 'Orleans', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4588', 'Otacílio Costa', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4589', 'Ouro', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4590', 'Ouro Verde', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4591', 'Paial', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4592', 'Painel', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4593', 'Palhoça', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4594', 'Palma Sola', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4595', 'Palmeira', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4596', 'Palmitos', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4597', 'Papanduva', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4598', 'Paraíso', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4599', 'Passo de Torres', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4600', 'Passos Maia', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4601', 'Paulo Lopes', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4602', 'Pedras Grandes', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4603', 'Penha', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4604', 'Peritiba', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4605', 'Petrolândia', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4606', 'Piçarras', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4607', 'Pinhalzinho', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4608', 'Pinheiro Preto', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4609', 'Piratuba', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4610', 'Planalto Alegre', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4611', 'Pomerode', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4612', 'Ponte Alta', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4613', 'Ponte Alta do Norte', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4614', 'Ponte Serrada', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4615', 'Porto Belo', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4616', 'Porto União', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4617', 'Pouso Redondo', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4618', 'Praia Grande', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4619', 'Presidente Castelo Branco', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4620', 'Presidente Getúlio', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4621', 'Presidente Nereu', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4622', 'Princesa', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4623', 'Quilombo', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4624', 'Rancho Queimado', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4625', 'Rio das Antas', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4626', 'Rio do Campo', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4627', 'Rio do Oeste', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4628', 'Rio do Sul', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4629', 'Rio dos Cedros', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4630', 'Rio Fortuna', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4631', 'Rio Negrinho', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4632', 'Rio Rufino', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4633', 'Riqueza', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4634', 'Rodeio', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4635', 'Romelândia', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4636', 'Salete', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4637', 'Saltinho', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4638', 'Salto Veloso', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4639', 'Sangão', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4640', 'Santa Cecília', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4641', 'Santa Helena', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4642', 'Santa Rosa de Lima', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4643', 'Santa Rosa do Sul', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4644', 'Santa Terezinha', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4645', 'Santa Terezinha do Progresso', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4646', 'Santiago do Sul', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4647', 'Santo Amaro da Imperatriz', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4648', 'São Bento do Sul', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4649', 'São Bernardino', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4650', 'São Bonifácio', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4651', 'São Carlos', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4652', 'São Cristovão do Sul', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4653', 'São Domingos', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4654', 'São Francisco do Sul', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4655', 'São João Batista', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4656', 'São João do Itaperiú', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4657', 'São João do Oeste', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4658', 'São João do Sul', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4659', 'São Joaquim', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4660', 'São José', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4661', 'São José do Cedro', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4662', 'São José do Cerrito', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4663', 'São Lourenço do Oeste', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4664', 'São Ludgero', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4665', 'São Martinho', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4666', 'São Miguel da Boa Vista', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4667', 'São Miguel do Oeste', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4668', 'São Pedro de Alcântara', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4669', 'Saudades', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4670', 'Schroeder', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4671', 'Seara', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4672', 'Serra Alta', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4673', 'Siderópolis', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4674', 'Sombrio', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4675', 'Sul Brasil', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4676', 'Taió', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4677', 'Tangará', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4678', 'Tigrinhos', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4679', 'Tijucas', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4680', 'Timbé do Sul', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4681', 'Timbó', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4682', 'Timbó Grande', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4683', 'Três Barras', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4684', 'Treviso', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4685', 'Treze de Maio', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4686', 'Treze Tílias', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4687', 'Trombudo Central', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4688', 'Tubarão', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4689', 'Tunápolis', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4690', 'Turvo', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4691', 'União do Oeste', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4692', 'Urubici', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4693', 'Urupema', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4694', 'Urussanga', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4695', 'Vargeão', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4696', 'Vargem', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4697', 'Vargem Bonita', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4698', 'Vidal Ramos', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4699', 'Videira', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4700', 'Vitor Meireles', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4701', 'Witmarsum', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4702', 'Xanxerê', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4703', 'Xavantina', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4704', 'Xaxim', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4705', 'Zortéa', '24', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4706', 'Adamantina', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4707', 'Adolfo', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4708', 'Aguaí', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4709', 'Águas da Prata', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4710', 'Águas de Lindóia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4711', 'Águas de Santa Bárbara', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4712', 'Águas de São Pedro', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4713', 'Agudos', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4714', 'Alambari', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4715', 'Alfredo Marcondes', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4716', 'Altair', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4717', 'Altinópolis', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4718', 'Alto Alegre', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4719', 'Alumínio', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4720', 'Álvares Florence', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4721', 'Álvares Machado', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4722', 'Álvaro de Carvalho', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4723', 'Alvinlândia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4724', 'Americana', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4725', 'Américo Brasiliense', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4726', 'Américo de Campos', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4727', 'Amparo', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4728', 'Analândia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4729', 'Andradina', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4730', 'Angatuba', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4731', 'Anhembi', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4732', 'Anhumas', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4733', 'Aparecida', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4734', 'Aparecida d`Oeste', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4735', 'Apiaí', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4736', 'Araçariguama', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4737', 'Araçatuba', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4738', 'Araçoiaba da Serra', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4739', 'Aramina', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4740', 'Arandu', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4741', 'Arapeí', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4742', 'Araraquara', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4743', 'Araras', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4744', 'Arco-Íris', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4745', 'Arealva', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4746', 'Areias', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4747', 'Areiópolis', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4748', 'Ariranha', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4749', 'Artur Nogueira', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4750', 'Arujá', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4751', 'Aspásia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4752', 'Assis', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4753', 'Atibaia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4754', 'Auriflama', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4755', 'Avaí', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4756', 'Avanhandava', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4757', 'Avaré', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4758', 'Bady Bassitt', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4759', 'Balbinos', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4760', 'Bálsamo', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4761', 'Bananal', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4762', 'Barão de Antonina', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4763', 'Barbosa', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4764', 'Bariri', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4765', 'Barra Bonita', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4766', 'Barra do Chapéu', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4767', 'Barra do Turvo', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4768', 'Barretos', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4769', 'Barrinha', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4770', 'Barueri', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4771', 'Bastos', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4772', 'Batatais', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4773', 'Bauru', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4774', 'Bebedouro', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4775', 'Bento de Abreu', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4776', 'Bernardino de Campos', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4777', 'Bertioga', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4778', 'Bilac', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4779', 'Birigui', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4780', 'Biritiba-Mirim', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4781', 'Boa Esperança do Sul', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4782', 'Bocaina', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4783', 'Bofete', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4784', 'Boituva', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4785', 'Bom Jesus dos Perdões', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4786', 'Bom Sucesso de Itararé', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4787', 'Borá', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4788', 'Boracéia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4789', 'Borborema', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4790', 'Borebi', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4791', 'Botucatu', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4792', 'Bragança Paulista', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4793', 'Braúna', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4794', 'Brejo Alegre', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4795', 'Brodowski', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4796', 'Brotas', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4797', 'Buri', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4798', 'Buritama', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4799', 'Buritizal', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4800', 'Cabrália Paulista', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4801', 'Cabreúva', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4802', 'Caçapava', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4803', 'Cachoeira Paulista', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4804', 'Caconde', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4805', 'Cafelândia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4806', 'Caiabu', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4807', 'Caieiras', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4808', 'Caiuá', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4809', 'Cajamar', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4810', 'Cajati', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4811', 'Cajobi', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4812', 'Cajuru', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4813', 'Campina do Monte Alegre', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4814', 'Campinas', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4815', 'Campo Limpo Paulista', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4816', 'Campos do Jordão', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4817', 'Campos Novos Paulista', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4818', 'Cananéia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4819', 'Canas', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4820', 'Cândido Mota', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4821', 'Cândido Rodrigues', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4822', 'Canitar', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4823', 'Capão Bonito', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4824', 'Capela do Alto', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4825', 'Capivari', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4826', 'Caraguatatuba', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4827', 'Carapicuíba', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4828', 'Cardoso', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4829', 'Casa Branca', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4830', 'Cássia dos Coqueiros', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4831', 'Castilho', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4832', 'Catanduva', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4833', 'Catiguá', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4834', 'Cedral', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4835', 'Cerqueira César', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4836', 'Cerquilho', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4837', 'Cesário Lange', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4838', 'Charqueada', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4839', 'Chavantes', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4840', 'Clementina', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4841', 'Colina', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4842', 'Colômbia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4843', 'Conchal', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4844', 'Conchas', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4845', 'Cordeirópolis', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4846', 'Coroados', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4847', 'Coronel Macedo', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4848', 'Corumbataí', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4849', 'Cosmópolis', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4850', 'Cosmorama', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4851', 'Cotia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4852', 'Cravinhos', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4853', 'Cristais Paulista', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4854', 'Cruzália', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4855', 'Cruzeiro', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4856', 'Cubatão', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4857', 'Cunha', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4858', 'Descalvado', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4859', 'Diadema', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4860', 'Dirce Reis', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4861', 'Divinolândia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4862', 'Dobrada', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4863', 'Dois Córregos', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4864', 'Dolcinópolis', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4865', 'Dourado', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4866', 'Dracena', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4867', 'Duartina', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4868', 'Dumont', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4869', 'Echaporã', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4870', 'Eldorado', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4871', 'Elias Fausto', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4872', 'Elisiário', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4873', 'Embaúba', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4874', 'Embu', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4875', 'Embu-Guaçu', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4876', 'Emilianópolis', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4877', 'Engenheiro Coelho', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4878', 'Espírito Santo do Pinhal', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4879', 'Espírito Santo do Turvo', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4880', 'Estiva Gerbi', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4881', 'Estrela d`Oeste', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4882', 'Estrela do Norte', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4883', 'Euclides da Cunha Paulista', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4884', 'Fartura', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4885', 'Fernando Prestes', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4886', 'Fernandópolis', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4887', 'Fernão', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4888', 'Ferraz de Vasconcelos', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4889', 'Flora Rica', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4890', 'Floreal', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4891', 'Flórida Paulista', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4892', 'Florínia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4893', 'Franca', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4894', 'Francisco Morato', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4895', 'Franco da Rocha', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4896', 'Gabriel Monteiro', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4897', 'Gália', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4898', 'Garça', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4899', 'Gastão Vidigal', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4900', 'Gavião Peixoto', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4901', 'General Salgado', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4902', 'Getulina', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4903', 'Glicério', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4904', 'Guaiçara', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4905', 'Guaimbê', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4906', 'Guaíra', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4907', 'Guapiaçu', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4908', 'Guapiara', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4909', 'Guará', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4910', 'Guaraçaí', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4911', 'Guaraci', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4912', 'Guarani d`Oeste', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4913', 'Guarantã', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4914', 'Guararapes', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4915', 'Guararema', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4916', 'Guaratinguetá', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4917', 'Guareí', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4918', 'Guariba', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4919', 'Guarujá', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4920', 'Guarulhos', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4921', 'Guatapará', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4922', 'Guzolândia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4923', 'Herculândia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4924', 'Holambra', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4925', 'Hortolândia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4926', 'Iacanga', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4927', 'Iacri', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4928', 'Iaras', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4929', 'Ibaté', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4930', 'Ibirá', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4931', 'Ibirarema', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4932', 'Ibitinga', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4933', 'Ibiúna', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4934', 'Icém', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4935', 'Iepê', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4936', 'Igaraçu do Tietê', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4937', 'Igarapava', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4938', 'Igaratá', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4939', 'Iguape', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4940', 'Ilha Comprida', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4941', 'Ilha Solteira', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4942', 'Ilhabela', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4943', 'Indaiatuba', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4944', 'Indiana', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4945', 'Indiaporã', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4946', 'Inúbia Paulista', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4947', 'Ipaussu', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4948', 'Iperó', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4949', 'Ipeúna', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4950', 'Ipiguá', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4951', 'Iporanga', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4952', 'Ipuã', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4953', 'Iracemápolis', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4954', 'Irapuã', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4955', 'Irapuru', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4956', 'Itaberá', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4957', 'Itaí', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4958', 'Itajobi', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4959', 'Itaju', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4960', 'Itanhaém', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4961', 'Itaóca', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4962', 'Itapecerica da Serra', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4963', 'Itapetininga', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4964', 'Itapeva', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4965', 'Itapevi', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4966', 'Itapira', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4967', 'Itapirapuã Paulista', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4968', 'Itápolis', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4969', 'Itaporanga', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4970', 'Itapuí', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4971', 'Itapura', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4972', 'Itaquaquecetuba', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4973', 'Itararé', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4974', 'Itariri', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4975', 'Itatiba', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4976', 'Itatinga', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4977', 'Itirapina', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4978', 'Itirapuã', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4979', 'Itobi', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4980', 'Itu', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4981', 'Itupeva', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4982', 'Ituverava', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4983', 'Jaborandi', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4984', 'Jaboticabal', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4985', 'Jacareí', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4986', 'Jaci', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4987', 'Jacupiranga', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4988', 'Jaguariúna', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4989', 'Jales', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4990', 'Jambeiro', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4991', 'Jandira', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4992', 'Jardinópolis', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4993', 'Jarinu', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4994', 'Jaú', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4995', 'Jeriquara', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4996', 'Joanópolis', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4997', 'João Ramalho', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4998', 'José Bonifácio', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('4999', 'Júlio Mesquita', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5000', 'Jumirim', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5001', 'Jundiaí', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5002', 'Junqueirópolis', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5003', 'Juquiá', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5004', 'Juquitiba', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5005', 'Lagoinha', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5006', 'Laranjal Paulista', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5007', 'Lavínia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5008', 'Lavrinhas', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5009', 'Leme', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5010', 'Lençóis Paulista', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5011', 'Limeira', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5012', 'Lindóia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5013', 'Lins', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5014', 'Lorena', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5015', 'Lourdes', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5016', 'Louveira', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5017', 'Lucélia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5018', 'Lucianópolis', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5019', 'Luís Antônio', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5020', 'Luiziânia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5021', 'Lupércio', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5022', 'Lutécia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5023', 'Macatuba', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5024', 'Macaubal', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5025', 'Macedônia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5026', 'Magda', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5027', 'Mairinque', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5028', 'Mairiporã', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5029', 'Manduri', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5030', 'Marabá Paulista', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5031', 'Maracaí', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5032', 'Marapoama', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5033', 'Mariápolis', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5034', 'Marília', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5035', 'Marinópolis', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5036', 'Martinópolis', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5037', 'Matão', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5038', 'Mauá', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5039', 'Mendonça', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5040', 'Meridiano', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5041', 'Mesópolis', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5042', 'Miguelópolis', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5043', 'Mineiros do Tietê', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5044', 'Mira Estrela', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5045', 'Miracatu', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5046', 'Mirandópolis', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5047', 'Mirante do Paranapanema', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5048', 'Mirassol', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5049', 'Mirassolândia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5050', 'Mococa', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5051', 'Mogi das Cruzes', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5052', 'Mogi Guaçu', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5053', 'Moji Mirim', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5054', 'Mombuca', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5055', 'Monções', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5056', 'Mongaguá', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5057', 'Monte Alegre do Sul', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5058', 'Monte Alto', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5059', 'Monte Aprazível', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5060', 'Monte Azul Paulista', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5061', 'Monte Castelo', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5062', 'Monte Mor', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5063', 'Monteiro Lobato', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5064', 'Morro Agudo', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5065', 'Morungaba', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5066', 'Motuca', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5067', 'Murutinga do Sul', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5068', 'Nantes', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5069', 'Narandiba', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5070', 'Natividade da Serra', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5071', 'Nazaré Paulista', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5072', 'Neves Paulista', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5073', 'Nhandeara', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5074', 'Nipoã', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5075', 'Nova Aliança', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5076', 'Nova Campina', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5077', 'Nova Canaã Paulista', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5078', 'Nova Castilho', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5079', 'Nova Europa', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5080', 'Nova Granada', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5081', 'Nova Guataporanga', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5082', 'Nova Independência', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5083', 'Nova Luzitânia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5084', 'Nova Odessa', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5085', 'Novais', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5086', 'Novo Horizonte', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5087', 'Nuporanga', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5088', 'Ocauçu', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5089', 'Óleo', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5090', 'Olímpia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5091', 'Onda Verde', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5092', 'Oriente', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5093', 'Orindiúva', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5094', 'Orlândia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5095', 'Osasco', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5096', 'Oscar Bressane', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5097', 'Osvaldo Cruz', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5098', 'Ourinhos', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5099', 'Ouro Verde', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5100', 'Ouroeste', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5101', 'Pacaembu', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5102', 'Palestina', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5103', 'Palmares Paulista', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5104', 'Palmeira d`Oeste', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5105', 'Palmital', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5106', 'Panorama', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5107', 'Paraguaçu Paulista', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5108', 'Paraibuna', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5109', 'Paraíso', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5110', 'Paranapanema', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5111', 'Paranapuã', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5112', 'Parapuã', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5113', 'Pardinho', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5114', 'Pariquera-Açu', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5115', 'Parisi', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5116', 'Patrocínio Paulista', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5117', 'Paulicéia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5118', 'Paulínia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5119', 'Paulistânia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5120', 'Paulo de Faria', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5121', 'Pederneiras', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5122', 'Pedra Bela', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5123', 'Pedranópolis', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5124', 'Pedregulho', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5125', 'Pedreira', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5126', 'Pedrinhas Paulista', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5127', 'Pedro de Toledo', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5128', 'Penápolis', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5129', 'Pereira Barreto', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5130', 'Pereiras', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5131', 'Peruíbe', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5132', 'Piacatu', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5133', 'Piedade', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5134', 'Pilar do Sul', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5135', 'Pindamonhangaba', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5136', 'Pindorama', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5137', 'Pinhalzinho', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5138', 'Piquerobi', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5139', 'Piquete', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5140', 'Piracaia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5141', 'Piracicaba', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5142', 'Piraju', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5143', 'Pirajuí', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5144', 'Pirangi', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5145', 'Pirapora do Bom Jesus', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5146', 'Pirapozinho', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5147', 'Pirassununga', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5148', 'Piratininga', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5149', 'Pitangueiras', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5150', 'Planalto', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5151', 'Platina', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5152', 'Poá', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5153', 'Poloni', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5154', 'Pompéia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5155', 'Pongaí', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5156', 'Pontal', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5157', 'Pontalinda', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5158', 'Pontes Gestal', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5159', 'Populina', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5160', 'Porangaba', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5161', 'Porto Feliz', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5162', 'Porto Ferreira', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5163', 'Potim', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5164', 'Potirendaba', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5165', 'Pracinha', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5166', 'Pradópolis', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5167', 'Praia Grande', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5168', 'Pratânia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5169', 'Presidente Alves', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5170', 'Presidente Bernardes', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5171', 'Presidente Epitácio', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5172', 'Presidente Prudente', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5173', 'Presidente Venceslau', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5174', 'Promissão', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5175', 'Quadra', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5176', 'Quatá', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5177', 'Queiroz', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5178', 'Queluz', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5179', 'Quintana', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5180', 'Rafard', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5181', 'Rancharia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5182', 'Redenção da Serra', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5183', 'Regente Feijó', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5184', 'Reginópolis', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5185', 'Registro', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5186', 'Restinga', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5187', 'Ribeira', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5188', 'Ribeirão Bonito', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5189', 'Ribeirão Branco', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5190', 'Ribeirão Corrente', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5191', 'Ribeirão do Sul', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5192', 'Ribeirão dos Índios', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5193', 'Ribeirão Grande', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5194', 'Ribeirão Pires', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5195', 'Ribeirão Preto', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5196', 'Rifaina', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5197', 'Rincão', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5198', 'Rinópolis', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5199', 'Rio Claro', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5200', 'Rio das Pedras', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5201', 'Rio Grande da Serra', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5202', 'Riolândia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5203', 'Riversul', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5204', 'Rosana', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5205', 'Roseira', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5206', 'Rubiácea', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5207', 'Rubinéia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5208', 'Sabino', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5209', 'Sagres', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5210', 'Sales', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5211', 'Sales Oliveira', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5212', 'Salesópolis', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5213', 'Salmourão', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5214', 'Saltinho', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5215', 'Salto', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5216', 'Salto de Pirapora', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5217', 'Salto Grande', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5218', 'Sandovalina', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5219', 'Santa Adélia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5220', 'Santa Albertina', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5221', 'Santa Bárbara d`Oeste', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5222', 'Santa Branca', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5223', 'Santa Clara d`Oeste', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5224', 'Santa Cruz da Conceição', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5225', 'Santa Cruz da Esperança', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5226', 'Santa Cruz das Palmeiras', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5227', 'Santa Cruz do Rio Pardo', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5228', 'Santa Ernestina', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5229', 'Santa Fé do Sul', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5230', 'Santa Gertrudes', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5231', 'Santa Isabel', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5232', 'Santa Lúcia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5233', 'Santa Maria da Serra', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5234', 'Santa Mercedes', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5235', 'Santa Rita d`Oeste', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5236', 'Santa Rita do Passa Quatro', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5237', 'Santa Rosa de Viterbo', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5238', 'Santa Salete', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5239', 'Santana da Ponte Pensa', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5240', 'Santana de Parnaíba', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5241', 'Santo Anastácio', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5242', 'Santo André', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5243', 'Santo Antônio da Alegria', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5244', 'Santo Antônio de Posse', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5245', 'Santo Antônio do Aracanguá', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5246', 'Santo Antônio do Jardim', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5247', 'Santo Antônio do Pinhal', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5248', 'Santo Expedito', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5249', 'Santópolis do Aguapeí', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5250', 'Santos', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5251', 'São Bento do Sapucaí', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5252', 'São Bernardo do Campo', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5253', 'São Caetano do Sul', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5254', 'São Carlos', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5255', 'São Francisco', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5256', 'São João da Boa Vista', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5257', 'São João das Duas Pontes', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5258', 'São João de Iracema', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5259', 'São João do Pau d`Alho', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5260', 'São Joaquim da Barra', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5261', 'São José da Bela Vista', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5262', 'São José do Barreiro', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5263', 'São José do Rio Pardo', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5264', 'São José do Rio Preto', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5265', 'São José dos Campos', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5266', 'São Lourenço da Serra', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5267', 'São Luís do Paraitinga', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5268', 'São Manuel', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5269', 'São Miguel Arcanjo', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5270', 'São Paulo', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5271', 'São Pedro', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5272', 'São Pedro do Turvo', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5273', 'São Roque', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5274', 'São Sebastião', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5275', 'São Sebastião da Grama', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5276', 'São Simão', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5277', 'São Vicente', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5278', 'Sarapuí', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5279', 'Sarutaiá', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5280', 'Sebastianópolis do Sul', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5281', 'Serra Azul', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5282', 'Serra Negra', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5283', 'Serrana', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5284', 'Sertãozinho', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5285', 'Sete Barras', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5286', 'Severínia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5287', 'Silveiras', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5288', 'Socorro', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5289', 'Sorocaba', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5290', 'Sud Mennucci', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5291', 'Sumaré', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5292', 'Suzanápolis', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5293', 'Suzano', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5294', 'Tabapuã', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5295', 'Tabatinga', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5296', 'Taboão da Serra', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5297', 'Taciba', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5298', 'Taguaí', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5299', 'Taiaçu', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5300', 'Taiúva', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5301', 'Tambaú', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5302', 'Tanabi', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5303', 'Tapiraí', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5304', 'Tapiratiba', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5305', 'Taquaral', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5306', 'Taquaritinga', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5307', 'Taquarituba', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5308', 'Taquarivaí', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5309', 'Tarabai', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5310', 'Tarumã', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5311', 'Tatuí', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5312', 'Taubaté', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5313', 'Tejupá', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5314', 'Teodoro Sampaio', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5315', 'Terra Roxa', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5316', 'Tietê', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5317', 'Timburi', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5318', 'Torre de Pedra', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5319', 'Torrinha', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5320', 'Trabiju', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5321', 'Tremembé', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5322', 'Três Fronteiras', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5323', 'Tuiuti', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5324', 'Tupã', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5325', 'Tupi Paulista', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5326', 'Turiúba', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5327', 'Turmalina', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5328', 'Ubarana', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5329', 'Ubatuba', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5330', 'Ubirajara', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5331', 'Uchoa', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5332', 'União Paulista', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5333', 'Urânia', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5334', 'Uru', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5335', 'Urupês', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5336', 'Valentim Gentil', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5337', 'Valinhos', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5338', 'Valparaíso', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5339', 'Vargem', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5340', 'Vargem Grande do Sul', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5341', 'Vargem Grande Paulista', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5342', 'Várzea Paulista', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5343', 'Vera Cruz', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5344', 'Vinhedo', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5345', 'Viradouro', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5346', 'Vista Alegre do Alto', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5347', 'Vitória Brasil', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5348', 'Votorantim', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5349', 'Votuporanga', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5350', 'Zacarias', '26', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5351', 'Amparo de São Francisco', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5352', 'Aquidabã', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5353', 'Aracaju', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5354', 'Arauá', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5355', 'Areia Branca', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5356', 'Barra dos Coqueiros', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5357', 'Boquim', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5358', 'Brejo Grande', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5359', 'Campo do Brito', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5360', 'Canhoba', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5361', 'Canindé de São Francisco', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5362', 'Capela', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5363', 'Carira', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5364', 'Carmópolis', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5365', 'Cedro de São João', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5366', 'Cristinápolis', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5367', 'Cumbe', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5368', 'Divina Pastora', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5369', 'Estância', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5370', 'Feira Nova', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5371', 'Frei Paulo', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5372', 'Gararu', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5373', 'General Maynard', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5374', 'Gracho Cardoso', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5375', 'Ilha das Flores', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5376', 'Indiaroba', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5377', 'Itabaiana', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5378', 'Itabaianinha', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5379', 'Itabi', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5380', 'Itaporanga d`Ajuda', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5381', 'Japaratuba', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5382', 'Japoatã', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5383', 'Lagarto', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5384', 'Laranjeiras', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5385', 'Macambira', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5386', 'Malhada dos Bois', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5387', 'Malhador', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5388', 'Maruim', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5389', 'Moita Bonita', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5390', 'Monte Alegre de Sergipe', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5391', 'Muribeca', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5392', 'Neópolis', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5393', 'Nossa Senhora Aparecida', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5394', 'Nossa Senhora da Glória', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5395', 'Nossa Senhora das Dores', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5396', 'Nossa Senhora de Lourdes', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5397', 'Nossa Senhora do Socorro', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5398', 'Pacatuba', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5399', 'Pedra Mole', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5400', 'Pedrinhas', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5401', 'Pinhão', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5402', 'Pirambu', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5403', 'Poço Redondo', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5404', 'Poço Verde', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5405', 'Porto da Folha', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5406', 'Propriá', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5407', 'Riachão do Dantas', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5408', 'Riachuelo', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5409', 'Ribeirópolis', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5410', 'Rosário do Catete', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5411', 'Salgado', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5412', 'Santa Luzia do Itanhy', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5413', 'Santa Rosa de Lima', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5414', 'Santana do São Francisco', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5415', 'Santo Amaro das Brotas', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5416', 'São Cristóvão', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5417', 'São Domingos', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5418', 'São Francisco', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5419', 'São Miguel do Aleixo', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5420', 'Simão Dias', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5421', 'Siriri', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5422', 'Telha', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5423', 'Tobias Barreto', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5424', 'Tomar do Geru', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5425', 'Umbaúba', '25', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5426', 'Abreulândia', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5427', 'Aguiarnópolis', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5428', 'Aliança do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5429', 'Almas', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5430', 'Alvorada', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5431', 'Ananás', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5432', 'Angico', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5433', 'Aparecida do Rio Negro', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5434', 'Aragominas', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5435', 'Araguacema', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5436', 'Araguaçu', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5437', 'Araguaína', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5438', 'Araguanã', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5439', 'Araguatins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5440', 'Arapoema', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5441', 'Arraias', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5442', 'Augustinópolis', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5443', 'Aurora do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5444', 'Axixá do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5445', 'Babaçulândia', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5446', 'Bandeirantes do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5447', 'Barra do Ouro', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5448', 'Barrolândia', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5449', 'Bernardo Sayão', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5450', 'Bom Jesus do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5451', 'Brasilândia do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5452', 'Brejinho de Nazaré', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5453', 'Buriti do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5454', 'Cachoeirinha', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5455', 'Campos Lindos', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5456', 'Cariri do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5457', 'Carmolândia', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5458', 'Carrasco Bonito', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5459', 'Caseara', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5460', 'Centenário', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5461', 'Chapada da Natividade', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5462', 'Chapada de Areia', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5463', 'Colinas do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5464', 'Colméia', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5465', 'Combinado', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5466', 'Conceição do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5467', 'Couto de Magalhães', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5468', 'Cristalândia', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5469', 'Crixás do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5470', 'Darcinópolis', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5471', 'Dianópolis', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5472', 'Divinópolis do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5473', 'Dois Irmãos do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5474', 'Dueré', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5475', 'Esperantina', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5476', 'Fátima', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5477', 'Figueirópolis', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5478', 'Filadélfia', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5479', 'Formoso do Araguaia', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5480', 'Fortaleza do Tabocão', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5481', 'Goianorte', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5482', 'Goiatins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5483', 'Guaraí', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5484', 'Gurupi', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5485', 'Ipueiras', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5486', 'Itacajá', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5487', 'Itaguatins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5488', 'Itapiratins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5489', 'Itaporã do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5490', 'Jaú do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5491', 'Juarina', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5492', 'Lagoa da Confusão', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5493', 'Lagoa do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5494', 'Lajeado', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5495', 'Lavandeira', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5496', 'Lizarda', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5497', 'Luzinópolis', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5498', 'Marianópolis do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5499', 'Mateiros', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5500', 'Maurilândia do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5501', 'Miracema do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5502', 'Miranorte', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5503', 'Monte do Carmo', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5504', 'Monte Santo do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5505', 'Muricilândia', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5506', 'Natividade', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5507', 'Nazaré', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5508', 'Nova Olinda', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5509', 'Nova Rosalândia', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5510', 'Novo Acordo', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5511', 'Novo Alegre', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5512', 'Novo Jardim', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5513', 'Oliveira de Fátima', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5514', 'Palmas', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5515', 'Palmeirante', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5516', 'Palmeiras do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5517', 'Palmeirópolis', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5518', 'Paraíso do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5519', 'Paranã', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5520', 'Pau d`Arco', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5521', 'Pedro Afonso', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5522', 'Peixe', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5523', 'Pequizeiro', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5524', 'Pindorama do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5525', 'Piraquê', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5526', 'Pium', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5527', 'Ponte Alta do Bom Jesus', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5528', 'Ponte Alta do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5529', 'Porto Alegre do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5530', 'Porto Nacional', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5531', 'Praia Norte', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5532', 'Presidente Kennedy', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5533', 'Pugmil', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5534', 'Recursolândia', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5535', 'Riachinho', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5536', 'Rio da Conceição', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5537', 'Rio dos Bois', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5538', 'Rio Sono', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5539', 'Sampaio', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5540', 'Sandolândia', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5541', 'Santa Fé do Araguaia', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5542', 'Santa Maria do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5543', 'Santa Rita do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5544', 'Santa Rosa do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5545', 'Santa Tereza do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5546', 'Santa Terezinha do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5547', 'São Bento do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5548', 'São Félix do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5549', 'São Miguel do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5550', 'São Salvador do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5551', 'São Sebastião do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5552', 'São Valério da Natividade', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5553', 'Silvanópolis', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5554', 'Sítio Novo do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5555', 'Sucupira', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5556', 'Taguatinga', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5557', 'Taipas do Tocantins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5558', 'Talismã', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5559', 'Tocantínia', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5560', 'Tocantinópolis', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5561', 'Tupirama', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5562', 'Tupiratins', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5563', 'Wanderlândia', '27', '2013-10-29 14:32:45', null);
INSERT INTO `cities` VALUES ('5564', 'Xambioá', '27', '2013-10-29 14:32:45', null);

-- ----------------------------
-- Table structure for `clients`
-- ----------------------------
DROP TABLE IF EXISTS `clients`;
CREATE TABLE `clients` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `kind` int(11) DEFAULT NULL,
  `name` varchar(300) DEFAULT NULL,
  `fancy_name` varchar(300) DEFAULT NULL,
  `email` varchar(300) DEFAULT NULL,
  `gender` enum('1','2') DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `ssn` varchar(255) DEFAULT NULL,
  `registro_geral` varchar(255) DEFAULT NULL,
  `inscricao_estadual` varchar(255) DEFAULT NULL,
  `titulo_eleitor` varchar(255) DEFAULT NULL,
  `carteira_motorista` varchar(255) DEFAULT NULL,
  `dad` varchar(300) DEFAULT NULL,
  `mon` varchar(300) DEFAULT NULL,
  `occupation` varchar(200) DEFAULT NULL,
  `ip` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of clients
-- ----------------------------
INSERT INTO `clients` VALUES ('2', '1', 'asdas', null, 'asd@gmail.com', '', '2013-11-06', 'asdasd', 'asdsa', null, null, null, null, null, '1', null, '2013-11-06 14:38:07', '2013-11-06 14:38:07');
INSERT INTO `clients` VALUES ('3', '1', 'teste', null, 'teste@gmail.com', '', '2013-11-06', 'teste', 'teste', null, null, null, null, null, '1', null, '2013-11-06 19:05:06', '2013-11-06 19:05:06');
INSERT INTO `clients` VALUES ('4', null, 'Fabiano Ishiy Zaparoli', null, null, null, null, '696.328.252-84', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `clients` VALUES ('5', null, 'teste asdsadsa', null, null, null, null, '482.520.424-49', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `clients` VALUES ('6', null, 'carlitos tevez', null, null, null, null, '631.188.854-83', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `clients` VALUES ('7', null, 'brasdsa sadqrwe', null, null, null, null, '484.462.406-70', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `clients` VALUES ('8', null, 'brasdsa sadqrwe', null, null, null, null, '575.315.653-31', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `clients` VALUES ('15', '1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2014-01-26 20:24:16', '2014-01-31 02:37:11');
INSERT INTO `clients` VALUES ('16', null, 'teste', null, null, null, null, '082.403.476-73', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `clients` VALUES ('17', null, 'Renato', null, null, null, null, '977.272.618-15', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `clients` VALUES ('18', null, 'teste4', null, null, null, null, '145.825.951-03', null, null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for `countries`
-- ----------------------------
DROP TABLE IF EXISTS `countries`;
CREATE TABLE `countries` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=256 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of countries
-- ----------------------------
INSERT INTO `countries` VALUES ('1', 'África do Sul', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('2', 'Akrotiri', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('3', 'Albânia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('4', 'Alemanha', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('5', 'Andorra', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('6', 'Angola', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('7', 'Anguila', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('8', 'Antárctida', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('9', 'Antígua e Barbuda', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('10', 'Antilhas Neerlandesas', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('11', 'Arábia Saudita', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('12', 'Arctic Ocean', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('13', 'Argélia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('14', 'Argentina', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('15', 'Arménia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('16', 'Aruba', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('17', 'Ashmore and Cartier Islands', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('18', 'Atlantic Ocean', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('19', 'Austrália', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('20', 'Áustria', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('21', 'Azerbaijão', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('22', 'Baamas', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('23', 'Bangladeche', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('24', 'Barbados', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('25', 'Barém', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('26', 'Bélgica', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('27', 'Belize', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('28', 'Benim', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('29', 'Bermudas', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('30', 'Bielorrússia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('31', 'Birmânia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('32', 'Bolívia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('33', 'Bósnia e Herzegovina', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('34', 'Botsuana', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('35', 'Brasil', '55', '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('36', 'Brunei', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('37', 'Bulgária', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('38', 'Burquina Faso', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('39', 'Burúndi', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('40', 'Butão', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('41', 'Cabo Verde', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('42', 'Camarões', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('43', 'Camboja', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('44', 'Canadá', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('45', 'Catar', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('46', 'Cazaquistão', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('47', 'Chade', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('48', 'Chile', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('49', 'China', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('50', 'Chipre', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('51', 'Clipperton Island', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('52', 'Colômbia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('53', 'Comores', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('54', 'Congo-Brazzaville', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('55', 'Congo-Kinshasa', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('56', 'Coral Sea Islands', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('57', 'Coreia do Norte', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('58', 'Coreia do Sul', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('59', 'Costa do Marfim', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('60', 'Costa Rica', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('61', 'Croácia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('62', 'Cuba', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('63', 'Dhekelia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('64', 'Dinamarca', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('65', 'Domínica', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('66', 'Egipto', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('67', 'Emiratos Árabes Unidos', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('68', 'Equador', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('69', 'Eritreia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('70', 'Eslováquia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('71', 'Eslovénia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('72', 'Espanha', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('73', 'Estados Unidos', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('74', 'Estónia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('75', 'Etiópia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('76', 'Faroé', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('77', 'Fiji', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('78', 'Filipinas', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('79', 'Finlândia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('80', 'França', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('81', 'Gabão', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('82', 'Gâmbia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('83', 'Gana', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('84', 'Gaza Strip', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('85', 'Geórgia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('86', 'Geórgia do Sul e Sandwich do Sul', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('87', 'Gibraltar', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('88', 'Granada', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('89', 'Grécia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('90', 'Gronelândia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('91', 'Guame', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('92', 'Guatemala', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('93', 'Guernsey', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('94', 'Guiana', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('95', 'Guiné', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('96', 'Guiné Equatorial', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('97', 'Guiné-Bissau', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('98', 'Haiti', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('99', 'Honduras', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('100', 'Hong Kong', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('101', 'Hungria', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('102', 'Iémen', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('103', 'Ilha Bouvet', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('104', 'Ilha do Natal', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('105', 'Ilha Norfolk', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('106', 'Ilhas Caimão', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('107', 'Ilhas Cook', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('108', 'Ilhas dos Cocos', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('109', 'Ilhas Falkland', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('110', 'Ilhas Heard e McDonald', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('111', 'Ilhas Marshall', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('112', 'Ilhas Salomão', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('113', 'Ilhas Turcas e Caicos', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('114', 'Ilhas Virgens Americanas', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('115', 'Ilhas Virgens Britânicas', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('116', 'Índia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('117', 'Indian Ocean', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('118', 'Indonésia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('119', 'Irão', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('120', 'Iraque', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('121', 'Irlanda', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('122', 'Islândia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('123', 'Israel', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('124', 'Itália', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('125', 'Jamaica', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('126', 'Jan Mayen', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('127', 'Japão', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('128', 'Jersey', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('129', 'Jibuti', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('130', 'Jordânia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('131', 'Kuwait', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('132', 'Laos', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('133', 'Lesoto', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('134', 'Letónia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('135', 'Líbano', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('136', 'Libéria', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('137', 'Líbia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('138', 'Listenstaine', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('139', 'Lituânia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('140', 'Luxemburgo', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('141', 'Macau', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('142', 'Macedónia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('143', 'Madagáscar', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('144', 'Malásia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('145', 'Malávi', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('146', 'Maldivas', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('147', 'Mali', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('148', 'Malta', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('149', 'Man, Isle of', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('150', 'Marianas do Norte', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('151', 'Marrocos', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('152', 'Maurícia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('153', 'Mauritânia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('154', 'Mayotte', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('155', 'México', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('156', 'Micronésia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('157', 'Moçambique', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('158', 'Moldávia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('159', 'Mónaco', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('160', 'Mongólia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('161', 'Monserrate', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('162', 'Montenegro', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('163', 'Mundo', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('164', 'Namíbia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('165', 'Nauru', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('166', 'Navassa Island', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('167', 'Nepal', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('168', 'Nicarágua', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('169', 'Níger', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('170', 'Nigéria', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('171', 'Niue', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('172', 'Noruega', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('173', 'Nova Caledónia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('174', 'Nova Zelândia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('175', 'Omã', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('176', 'Pacific Ocean', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('177', 'Países Baixos', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('178', 'Palau', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('179', 'Panamá', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('180', 'Papua-Nova Guiné', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('181', 'Paquistão', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('182', 'Paracel Islands', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('183', 'Paraguai', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('184', 'Peru', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('185', 'Pitcairn', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('186', 'Polinésia Francesa', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('187', 'Polónia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('188', 'Porto Rico', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('189', 'Portugal', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('190', 'Quénia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('191', 'Quirguizistão', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('192', 'Quiribáti', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('193', 'Reino Unido', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('194', 'República Centro-Africana', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('195', 'República Checa', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('196', 'República Dominicana', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('197', 'Roménia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('198', 'Ruanda', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('199', 'Rússia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('200', 'Salvador', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('201', 'Samoa', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('202', 'Samoa Americana', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('203', 'Santa Helena', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('204', 'Santa Lúcia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('205', 'São Cristóvão e Neves', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('206', 'São Marinho', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('207', 'São Pedro e Miquelon', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('208', 'São Tomé e Príncipe', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('209', 'São Vicente e Granadinas', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('210', 'Sara Ocidental', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('211', 'Seicheles', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('212', 'Senegal', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('213', 'Serra Leoa', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('214', 'Sérvia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('215', 'Singapura', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('216', 'Síria', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('217', 'Somália', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('218', 'Southern Ocean', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('219', 'Spratly Islands', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('220', 'Sri Lanca', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('221', 'Suazilândia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('222', 'Sudão', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('223', 'Suécia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('224', 'Suíça', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('225', 'Suriname', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('226', 'Svalbard e Jan Mayen', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('227', 'Tailândia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('228', 'Taiwan', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('229', 'Tajiquistão', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('230', 'Tanzânia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('231', 'Território Britânico do Oceano Índico', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('232', 'Territórios Austrais Franceses', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('233', 'Timor Leste', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('234', 'Togo', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('235', 'Tokelau', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('236', 'Tonga', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('237', 'Trindade e Tobago', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('238', 'Tunísia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('239', 'Turquemenistão', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('240', 'Turquia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('241', 'Tuvalu', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('242', 'Ucrânia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('243', 'Uganda', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('244', 'União Europeia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('245', 'Uruguai', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('246', 'Usbequistão', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('247', 'Vanuatu', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('248', 'Vaticano', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('249', 'Venezuela', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('250', 'Vietname', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('251', 'Wake Island', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('252', 'Wallis e Futuna', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('253', 'West Bank', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('254', 'Zâmbia', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');
INSERT INTO `countries` VALUES ('255', 'Zimbabué', null, '2013-10-29 13:56:46', '2013-10-29 13:56:46');

-- ----------------------------
-- Table structure for `emails`
-- ----------------------------
DROP TABLE IF EXISTS `emails`;
CREATE TABLE `emails` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) unsigned DEFAULT NULL,
  `indication` int(11) unsigned DEFAULT NULL,
  `email` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of emails
-- ----------------------------

-- ----------------------------
-- Table structure for `fees`
-- ----------------------------
DROP TABLE IF EXISTS `fees`;
CREATE TABLE `fees` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` float(8,3) DEFAULT NULL,
  `percent` float(8,3) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of fees
-- ----------------------------

-- ----------------------------
-- Table structure for `fidelities`
-- ----------------------------
DROP TABLE IF EXISTS `fidelities`;
CREATE TABLE `fidelities` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `program_id` int(11) unsigned DEFAULT NULL,
  `login` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `password` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `tam_miles_token_1` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `tam_miles_token_2` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `automatic_checked` tinyint(4) DEFAULT NULL,
  `checked` tinyint(4) DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of fidelities
-- ----------------------------
INSERT INTO `fidelities` VALUES ('53', '2', '4', '', '', '', '', null, null, null, '2014-01-12 16:01:45', '2014-01-19 14:32:57');
INSERT INTO `fidelities` VALUES ('54', '2', '1', '', '', '', '', null, null, null, '2014-01-12 16:01:46', '2014-01-19 14:32:58');

-- ----------------------------
-- Table structure for `flights`
-- ----------------------------
DROP TABLE IF EXISTS `flights`;
CREATE TABLE `flights` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `program_id` int(11) DEFAULT NULL,
  `transaction_id` int(11) unsigned DEFAULT NULL,
  `package_id` int(11) unsigned DEFAULT NULL,
  `airline_id` int(11) unsigned DEFAULT NULL,
  `trip` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kind` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `number` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stops` int(11) DEFAULT '0',
  `from` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `to` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `departure` timestamp NULL DEFAULT NULL,
  `arrival` timestamp NULL DEFAULT NULL,
  `price_children` float(10,2) DEFAULT '0.00',
  `price_adults` float(10,2) DEFAULT '0.00',
  `miles_children` int(11) DEFAULT '0',
  `miles_adults` int(11) DEFAULT '0',
  `pre_fee` float(10,2) DEFAULT NULL,
  `fee_arrival` float(10,2) DEFAULT '0.00',
  `fee_departure` float(10,2) DEFAULT '0.00',
  `seat` int(11) unsigned DEFAULT NULL,
  `passangers_multiplicator` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of flights
-- ----------------------------
INSERT INTO `flights` VALUES ('52', '2', null, '78', null, '1', null, '123-0', '0', 'CNF', 'IPN', '2014-01-30 13:00:00', '2034-07-07 13:00:00', '150.00', '250.00', '4000', '5000', '0.00', '50.00', '0.00', null, null, '2014-01-12 20:46:56', '2014-01-12 20:46:56');
INSERT INTO `flights` VALUES ('53', '2', null, '79', null, '2', null, '123-0', '0', 'IPN', 'CNF', '2014-01-31 13:00:00', '2035-07-07 13:00:00', '150.00', '250.00', '6000', '6000', '0.00', '50.00', '0.00', null, null, '2014-01-12 20:46:56', '2014-01-12 20:46:56');
INSERT INTO `flights` VALUES ('54', '2', null, '80', null, '1', null, '123-0', '0', 'CNF', 'IPN', '2034-07-07 13:00:00', '2034-07-07 13:00:00', '150.00', '250.00', '4000', '5000', '0.00', '50.00', '0.00', null, null, '2014-01-12 21:07:42', '2014-01-12 21:07:42');
INSERT INTO `flights` VALUES ('55', '2', null, '81', null, '1', null, '123-0', '0', 'IPN', 'CNF', '2035-07-07 13:00:00', '2035-07-07 13:00:00', '150.00', '250.00', '6000', '6000', '0.00', '50.00', '0.00', null, null, '2014-01-12 21:07:43', '2014-01-12 21:07:43');
INSERT INTO `flights` VALUES ('56', '2', null, '82', null, '1', null, '123-0', '0', 'CNF', 'IPN', '2034-07-07 13:00:00', '2034-07-07 13:00:00', '150.00', '250.00', '4000', '5000', '0.00', '50.00', '0.00', null, null, '2014-01-12 22:48:08', '2014-01-12 22:48:08');
INSERT INTO `flights` VALUES ('57', '2', null, '83', null, '1', null, '123-0', '0', 'IPN', 'CNF', '2035-07-07 13:00:00', '2035-07-07 13:00:00', '150.00', '250.00', '6000', '6000', '0.00', '50.00', '0.00', null, null, '2014-01-12 22:48:09', '2014-01-12 22:48:09');
INSERT INTO `flights` VALUES ('58', '2', null, '84', null, '1', null, '123-0', '0', 'CNF', 'IPN', '2034-07-07 13:00:00', '2034-07-07 13:00:00', '150.00', '250.00', '4000', '5000', '0.00', '50.00', '0.00', null, null, '2014-01-13 20:41:30', '2014-01-13 20:41:30');
INSERT INTO `flights` VALUES ('59', '2', null, '85', null, '1', null, '123-0', '0', 'IPN', 'CNF', '2035-07-07 13:00:00', '2035-07-07 13:00:00', '150.00', '250.00', '6000', '6000', '0.00', '50.00', '0.00', null, null, '2014-01-13 20:41:30', '2014-01-13 20:41:30');
INSERT INTO `flights` VALUES ('60', '2', null, '86', null, '1', null, '123-0', '0', 'CNF', 'IPN', '2034-07-07 13:00:00', '2034-07-07 13:00:00', '150.00', '250.00', '4000', '5000', '0.00', '50.00', '0.00', null, null, '2014-01-15 12:58:18', '2014-01-15 12:58:18');
INSERT INTO `flights` VALUES ('61', '2', null, '86', null, '2', null, '123-0', '0', 'IPN', 'CNF', '2035-07-07 13:00:00', '2035-07-07 13:00:00', '150.00', '250.00', '6000', '6000', '0.00', '50.00', '0.00', null, null, '2014-01-15 12:58:18', '2014-01-15 12:58:18');
INSERT INTO `flights` VALUES ('62', '2', null, '87', null, '1', null, '123-0', '0', 'CNF', 'IPN', '2034-07-07 13:00:00', '2034-07-07 13:00:00', '150.00', '250.00', '4000', '5000', '0.00', '50.00', '0.00', null, null, '2014-01-15 15:30:18', '2014-01-15 15:30:18');
INSERT INTO `flights` VALUES ('63', '2', null, '88', null, '2', null, '123-0', '0', 'IPN', 'CNF', '2035-07-07 13:00:00', '2035-07-07 13:00:00', '150.00', '250.00', '6000', '6000', '0.00', '50.00', '0.00', null, null, '2014-01-15 15:30:18', '2014-01-15 15:30:18');
INSERT INTO `flights` VALUES ('64', '2', null, '89', null, '1', null, '123-0', '0', 'CNF', 'IPN', '2034-07-07 13:00:00', '2034-07-07 13:00:00', '150.00', '250.00', '4000', '5000', '0.00', '50.00', '0.00', null, null, '2014-01-15 15:32:56', '2014-01-15 15:32:56');
INSERT INTO `flights` VALUES ('65', '2', null, '90', null, '2', null, '123-0', '0', 'IPN', 'CNF', '2035-07-07 13:00:00', '2035-07-07 13:00:00', '150.00', '250.00', '6000', '6000', '0.00', '50.00', '0.00', null, null, '2014-01-15 15:32:56', '2014-01-15 15:32:56');

-- ----------------------------
-- Table structure for `groups`
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of groups
-- ----------------------------
INSERT INTO `groups` VALUES ('1', 'Admin', '2013-11-01 19:15:48', null);
INSERT INTO `groups` VALUES ('2', 'Normal', '2013-11-01 19:15:48', null);
INSERT INTO `groups` VALUES ('3', 'Suspended', '2013-11-01 19:15:48', null);
INSERT INTO `groups` VALUES ('4', 'Deleted', '2013-11-01 19:15:48', null);

-- ----------------------------
-- Table structure for `historic_flight`
-- ----------------------------
DROP TABLE IF EXISTS `historic_flight`;
CREATE TABLE `historic_flight` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quotation_id` int(11) DEFAULT NULL,
  `program_id` int(11) DEFAULT NULL,
  `from_airport_acronnym` varchar(10) DEFAULT NULL,
  `to_airport_acronnym` varchar(10) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `carrier_id` int(11) DEFAULT NULL,
  `stops` int(5) DEFAULT NULL,
  `trip` int(5) DEFAULT NULL,
  `arrival` date DEFAULT NULL,
  `departure` date DEFAULT NULL,
  `price_children` float(11,2) DEFAULT NULL,
  `price_adults` float(11,2) DEFAULT NULL,
  `miles_children` int(11) DEFAULT NULL,
  `miles_adults` int(11) DEFAULT NULL,
  `pre_fee` float(11,2) DEFAULT NULL,
  `fee_departure` float(11,2) DEFAULT NULL,
  `fee_arrival` float(11,2) DEFAULT NULL,
  `price_smartmilhas` float(11,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of historic_flight
-- ----------------------------

-- ----------------------------
-- Table structure for `nips`
-- ----------------------------
DROP TABLE IF EXISTS `nips`;
CREATE TABLE `nips` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) unsigned DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `nip` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `situation` int(11) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of nips
-- ----------------------------

-- ----------------------------
-- Table structure for `packages`
-- ----------------------------
DROP TABLE IF EXISTS `packages`;
CREATE TABLE `packages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) unsigned DEFAULT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `program_id` int(11) unsigned DEFAULT NULL,
  `kind` int(11) DEFAULT NULL,
  `points` int(11) DEFAULT NULL,
  `fee` float(10,2) DEFAULT NULL,
  `price` float(10,2) DEFAULT NULL,
  `multiple` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of packages
-- ----------------------------
INSERT INTO `packages` VALUES ('78', '63', '2', '2', null, '9000', '0.00', '315.00', null, '2014-01-12 20:46:56', '2014-01-12 20:46:56');
INSERT INTO `packages` VALUES ('79', '63', '2', '2', null, '12000', '0.00', '420.00', null, '2014-01-12 20:46:56', '2014-01-12 20:46:56');
INSERT INTO `packages` VALUES ('80', '64', '2', '2', null, '9000', '0.00', '315.00', null, '2014-01-12 21:07:42', '2014-01-12 21:07:42');
INSERT INTO `packages` VALUES ('81', '64', '2', '2', null, '12000', '0.00', '420.00', null, '2014-01-12 21:07:43', '2014-01-12 21:07:43');
INSERT INTO `packages` VALUES ('82', '65', '2', '2', null, '9000', '0.00', '315.00', null, '2014-01-12 22:48:08', '2014-01-12 22:48:08');
INSERT INTO `packages` VALUES ('83', '65', '2', '2', null, '12000', '0.00', '420.00', null, '2014-01-12 22:48:08', '2014-01-12 22:48:08');
INSERT INTO `packages` VALUES ('84', '66', '2', '2', null, '9000', '0.00', '315.00', null, '2014-01-13 20:41:29', '2014-01-13 20:41:29');
INSERT INTO `packages` VALUES ('85', '66', '2', '2', null, '12000', '0.00', '420.00', null, '2014-01-13 20:41:30', '2014-01-13 20:41:30');
INSERT INTO `packages` VALUES ('86', '69', '6', '2', null, '21000', '0.00', '598.50', null, '2014-01-15 12:58:18', '2014-01-15 12:58:18');
INSERT INTO `packages` VALUES ('87', '70', '7', '2', null, '9000', '0.00', '256.50', null, '2014-01-15 15:30:18', '2014-01-15 15:30:18');
INSERT INTO `packages` VALUES ('88', '70', '2', '2', null, '12000', '0.00', '342.00', null, '2014-01-15 15:30:18', '2014-01-15 15:30:18');
INSERT INTO `packages` VALUES ('89', '71', '6', '2', null, '9000', '0.00', '256.50', null, '2014-01-15 15:32:56', '2014-01-15 15:32:56');
INSERT INTO `packages` VALUES ('90', '71', '6', '2', null, '12000', '0.00', '342.00', null, '2014-01-15 15:32:56', '2014-01-15 15:32:56');

-- ----------------------------
-- Table structure for `passengers`
-- ----------------------------
DROP TABLE IF EXISTS `passengers`;
CREATE TABLE `passengers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) unsigned DEFAULT NULL,
  `treatment` enum('Bebê','Criança','Senhorita','Senhora','Senhor') CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(400) CHARACTER SET utf8 DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `suffix` enum('Sobrinho','Neto','Junior','Filho','') CHARACTER SET utf8 DEFAULT NULL,
  `ssn` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `passport` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of passengers
-- ----------------------------
INSERT INTO `passengers` VALUES ('25', '44', null, 'Fabiano Ishiy Zaparoli', '2014-01-01', null, '075.933.446-30', '', '2014-01-02 14:38:21', '2014-01-02 14:38:21');
INSERT INTO `passengers` VALUES ('26', '44', null, 'Fabiano Ishiy Zaparoli teste', '2014-01-01', null, '075.933.446-30', '', '2014-01-02 14:39:00', '2014-01-02 14:39:00');
INSERT INTO `passengers` VALUES ('27', '44', null, 'Fabiano Ishiy Zaparoli teste', '2014-01-01', null, '075.933.446-30', '', '2014-01-02 14:47:35', '2014-01-02 14:47:35');
INSERT INTO `passengers` VALUES ('28', '44', null, 'Fabiano Ishiy Zaparoli teste', '2014-01-01', null, '075.933.446-30', '', '2014-01-02 14:50:10', '2014-01-02 14:50:10');
INSERT INTO `passengers` VALUES ('29', '44', null, 'Fabiano Ishiy Zaparoli teste', '2014-01-01', null, '075.933.446-30', '', '2014-01-02 15:05:58', '2014-01-02 15:05:58');
INSERT INTO `passengers` VALUES ('30', '45', null, 'Fabiano Ishiy Zaparoli teste', '2014-01-01', null, '075.933.446-30', '', '2014-01-07 12:23:27', '2014-01-07 12:23:27');
INSERT INTO `passengers` VALUES ('31', '46', null, 'Fabiano Ishiy Zaparoli teste', '2014-01-01', null, '075.933.446-30', '', '2014-01-07 12:25:17', '2014-01-07 12:25:17');
INSERT INTO `passengers` VALUES ('32', '47', null, 'Fabiano Ishiy Zaparoli teste', '2014-01-01', null, '075.933.446-30', '', '2014-01-07 12:25:32', '2014-01-07 12:25:32');
INSERT INTO `passengers` VALUES ('33', '48', null, 'Fabiano Ishiy Zaparoli teste', '2014-01-01', null, '075.933.446-30', '', '2014-01-07 12:25:36', '2014-01-07 12:25:36');
INSERT INTO `passengers` VALUES ('34', '49', null, 'Fabiano Ishiy Zaparoli teste', '2014-01-01', null, '075.933.446-30', '', '2014-01-07 12:26:09', '2014-01-07 12:26:09');
INSERT INTO `passengers` VALUES ('35', '50', null, 'Fabiano Ishiy Zaparoli teste', '2014-01-01', null, '075.933.446-30', '', '2014-01-07 12:26:49', '2014-01-07 12:26:49');
INSERT INTO `passengers` VALUES ('36', '51', null, 'Fabiano Ishiy Zaparoli teste', '2014-01-01', null, '075.933.446-30', '', '2014-01-07 12:32:21', '2014-01-07 12:32:21');
INSERT INTO `passengers` VALUES ('37', '52', null, 'Fabiano Ishiy Zaparoli teste', '2014-01-01', null, '075.933.446-30', '', '2014-01-07 12:36:37', '2014-01-07 12:36:37');
INSERT INTO `passengers` VALUES ('38', '53', null, 'Fabiano Ishiy Zaparoli teste', '2014-01-01', null, '075.933.446-30', '', '2014-01-07 12:47:57', '2014-01-07 12:47:57');
INSERT INTO `passengers` VALUES ('39', '54', null, 'Fabiano Ishiy Zaparoli teste', '2014-01-01', null, '075.933.446-30', '', '2014-01-07 12:58:18', '2014-01-07 12:58:18');
INSERT INTO `passengers` VALUES ('40', '55', null, 'Fabiano Ishiy Zaparoli teste', '2014-01-01', null, '075.933.446-30', '', '2014-01-07 13:00:16', '2014-01-07 13:00:16');
INSERT INTO `passengers` VALUES ('41', '56', null, 'Fabiano Ishiy Zaparoli teste', '2014-01-01', null, '075.933.446-30', '', '2014-01-07 13:02:30', '2014-01-07 13:02:30');
INSERT INTO `passengers` VALUES ('42', '57', null, 'Fabiano Ishiy Zaparoli teste', '2014-01-01', null, '075.933.446-30', '', '2014-01-07 13:03:06', '2014-01-07 13:03:06');
INSERT INTO `passengers` VALUES ('43', '58', null, 'Fabiano Ishiy Zaparoli teste', '2014-01-01', null, '075.933.446-30', '', '2014-01-07 13:03:54', '2014-01-07 13:03:54');
INSERT INTO `passengers` VALUES ('44', '59', null, 'Fabiano Ishiy Zaparoli teste', '2014-01-01', null, '075.933.446-30', '', '2014-01-07 13:07:31', '2014-01-07 13:07:31');
INSERT INTO `passengers` VALUES ('45', '60', null, 'Fabiano Ishiy Zaparoli teste', '2014-01-01', null, '075.933.446-30', '', '2014-01-07 13:18:59', '2014-01-07 13:18:59');
INSERT INTO `passengers` VALUES ('46', '61', null, 'Fabiano Ishiy Zaparoli teste', '2014-01-01', null, '075.933.446-30', '', '2014-01-07 16:07:58', '2014-01-07 16:07:58');
INSERT INTO `passengers` VALUES ('47', '62', null, 'Fabiano Ishiy Zaparoli teste', '2014-01-01', null, '075.933.446-30', '', '2014-01-07 16:08:35', '2014-01-07 16:08:35');
INSERT INTO `passengers` VALUES ('48', '63', null, 'Fabiano Ishiy Zaparoli teste', '2014-01-01', null, '075.933.446-30', '', '2014-01-12 20:46:55', '2014-01-12 20:46:55');
INSERT INTO `passengers` VALUES ('49', '64', null, 'Fabiano Ishiy Zaparoli teste', '2014-01-01', null, '075.933.446-30', '', '2014-01-12 21:07:42', '2014-01-12 21:07:42');
INSERT INTO `passengers` VALUES ('50', '65', null, 'Fabiano Ishiy Zaparoli teste', '2014-01-01', null, '075.933.446-30', '', '2014-01-12 22:48:08', '2014-01-12 22:48:08');

-- ----------------------------
-- Table structure for `phones`
-- ----------------------------
DROP TABLE IF EXISTS `phones`;
CREATE TABLE `phones` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) unsigned DEFAULT NULL,
  `carrier_id` int(11) unsigned DEFAULT NULL,
  `number` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `sms` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of phones
-- ----------------------------
INSERT INTO `phones` VALUES ('1', '1', '1', '33333333', null, '2013-11-05 15:56:58', null);
INSERT INTO `phones` VALUES ('2', '2', null, '', null, '2013-11-06 14:38:07', '2013-11-06 14:38:07');
INSERT INTO `phones` VALUES ('3', '2', null, 'qweqwe', null, '2013-11-06 14:38:07', '2013-11-06 14:38:07');
INSERT INTO `phones` VALUES ('4', '2', null, 'qweeqw', null, '2013-11-06 14:38:07', '2013-11-06 14:38:07');
INSERT INTO `phones` VALUES ('5', '2', null, '', null, '2013-11-06 14:38:07', '2013-11-06 14:38:07');
INSERT INTO `phones` VALUES ('6', '3', '1', 'teste', null, '2013-11-06 19:05:06', '2013-11-06 19:05:06');
INSERT INTO `phones` VALUES ('7', '3', '1', 'teste', null, '2013-11-06 19:05:06', '2013-11-06 19:05:06');
INSERT INTO `phones` VALUES ('8', '3', '1', 'teste', null, '2013-11-06 19:05:06', '2013-11-06 19:05:06');
INSERT INTO `phones` VALUES ('9', '3', '1', 'teste', null, '2013-11-06 19:05:06', '2013-11-06 19:05:06');
INSERT INTO `phones` VALUES ('10', '1', '1', '(31) 9158-9937', null, '2014-01-19 14:48:06', '2014-01-19 14:48:06');
INSERT INTO `phones` VALUES ('11', '1', null, null, null, '2014-01-19 14:48:06', '2014-01-19 14:48:06');
INSERT INTO `phones` VALUES ('12', '1', null, null, null, '2014-01-19 14:48:06', '2014-01-19 14:48:06');
INSERT INTO `phones` VALUES ('13', '1', '1', '(31) 9158-9937', null, '2014-01-19 14:49:41', '2014-01-19 14:49:41');
INSERT INTO `phones` VALUES ('14', '1', null, null, null, '2014-01-19 14:49:41', '2014-01-19 14:49:41');
INSERT INTO `phones` VALUES ('15', '1', null, null, null, '2014-01-19 14:49:41', '2014-01-19 14:49:41');
INSERT INTO `phones` VALUES ('16', '1', '1', '(31) 9158-9937', null, '2014-01-19 14:52:41', '2014-01-19 14:52:41');
INSERT INTO `phones` VALUES ('17', '1', null, null, null, '2014-01-19 14:52:41', '2014-01-19 14:52:41');
INSERT INTO `phones` VALUES ('18', '1', null, null, null, '2014-01-19 14:52:41', '2014-01-19 14:52:41');
INSERT INTO `phones` VALUES ('19', '1', '1', '(31) 9158-9937', null, '2014-01-19 14:53:31', '2014-01-19 14:53:31');
INSERT INTO `phones` VALUES ('20', '1', null, null, null, '2014-01-19 14:53:31', '2014-01-19 14:53:31');
INSERT INTO `phones` VALUES ('21', '1', null, null, null, '2014-01-19 14:53:31', '2014-01-19 14:53:31');
INSERT INTO `phones` VALUES ('22', '1', '1', '(31) 9158-9937', null, '2014-01-19 14:55:39', '2014-01-19 14:55:39');
INSERT INTO `phones` VALUES ('23', '1', null, null, null, '2014-01-19 14:55:39', '2014-01-19 14:55:39');
INSERT INTO `phones` VALUES ('24', '1', null, null, null, '2014-01-19 14:55:39', '2014-01-19 14:55:39');
INSERT INTO `phones` VALUES ('25', '1', '1', '(31) 9158-9937', null, '2014-01-19 14:56:27', '2014-01-19 14:56:27');
INSERT INTO `phones` VALUES ('26', '1', null, null, null, '2014-01-19 14:56:27', '2014-01-19 14:56:27');
INSERT INTO `phones` VALUES ('27', '1', null, null, null, '2014-01-19 14:56:27', '2014-01-19 14:56:27');
INSERT INTO `phones` VALUES ('28', '4', '1', '(11) 1221-2222', null, '2014-01-19 19:47:21', '2014-01-19 19:47:21');
INSERT INTO `phones` VALUES ('29', '4', '0', '', null, '2014-01-19 19:47:21', '2014-01-19 19:47:21');
INSERT INTO `phones` VALUES ('30', '4', null, '', null, '2014-01-19 19:47:21', '2014-01-19 19:47:21');
INSERT INTO `phones` VALUES ('31', '4', null, '', null, '2014-01-19 19:47:21', '2014-01-19 19:47:21');
INSERT INTO `phones` VALUES ('32', '5', '1', '(31) 9158-9937', null, '2014-01-19 21:34:14', '2014-01-19 21:34:14');
INSERT INTO `phones` VALUES ('33', '5', '0', '', null, '2014-01-19 21:34:14', '2014-01-19 21:34:14');
INSERT INTO `phones` VALUES ('34', '5', null, '', null, '2014-01-19 21:34:14', '2014-01-19 21:34:14');
INSERT INTO `phones` VALUES ('35', '5', null, '', null, '2014-01-19 21:34:14', '2014-01-19 21:34:14');
INSERT INTO `phones` VALUES ('36', '6', '1', '(11) 1221-2222', null, '2014-01-19 21:45:14', '2014-01-19 21:45:14');
INSERT INTO `phones` VALUES ('37', '6', '0', '', null, '2014-01-19 21:45:14', '2014-01-19 21:45:14');
INSERT INTO `phones` VALUES ('38', '6', null, '', null, '2014-01-19 21:45:14', '2014-01-19 21:45:14');
INSERT INTO `phones` VALUES ('39', '6', null, '', null, '2014-01-19 21:45:14', '2014-01-19 21:45:14');
INSERT INTO `phones` VALUES ('40', '7', '1', '(31) 9158-9937', null, '2014-01-19 21:49:51', '2014-01-19 21:49:51');
INSERT INTO `phones` VALUES ('41', '7', '0', '', null, '2014-01-19 21:49:51', '2014-01-19 21:49:51');
INSERT INTO `phones` VALUES ('42', '7', null, '', null, '2014-01-19 21:49:51', '2014-01-19 21:49:51');
INSERT INTO `phones` VALUES ('43', '7', null, '', null, '2014-01-19 21:49:51', '2014-01-19 21:49:51');
INSERT INTO `phones` VALUES ('44', '8', '1', '(31) 9158-9937', null, '2014-01-20 18:34:05', '2014-01-20 18:34:05');
INSERT INTO `phones` VALUES ('45', '8', '0', '', null, '2014-01-20 18:34:05', '2014-01-20 18:34:05');
INSERT INTO `phones` VALUES ('46', '8', null, '', null, '2014-01-20 18:34:05', '2014-01-20 18:34:05');
INSERT INTO `phones` VALUES ('47', '8', null, '', null, '2014-01-20 18:34:05', '2014-01-20 18:34:05');
INSERT INTO `phones` VALUES ('48', '16', '1', '(31) 8899-2232', null, '2014-01-28 12:49:47', '2014-01-28 12:49:47');
INSERT INTO `phones` VALUES ('49', '16', '0', '', null, '2014-01-28 12:49:47', '2014-01-28 12:49:47');
INSERT INTO `phones` VALUES ('50', '16', null, '', null, '2014-01-28 12:49:47', '2014-01-28 12:49:47');
INSERT INTO `phones` VALUES ('51', '17', '1', '(31) 99888-8888', null, '2014-01-29 21:44:16', '2014-01-29 21:44:16');
INSERT INTO `phones` VALUES ('52', '17', '0', '', null, '2014-01-29 21:44:16', '2014-01-29 21:44:16');
INSERT INTO `phones` VALUES ('53', '17', null, '', null, '2014-01-29 21:44:16', '2014-01-29 21:44:16');
INSERT INTO `phones` VALUES ('54', '15', '1', '(31) 8898-8988', null, '2014-01-29 23:52:40', '2014-01-29 23:52:40');
INSERT INTO `phones` VALUES ('55', '15', null, null, null, '2014-01-29 23:52:40', '2014-01-29 23:52:40');
INSERT INTO `phones` VALUES ('56', '15', null, null, null, '2014-01-29 23:52:40', '2014-01-29 23:52:40');
INSERT INTO `phones` VALUES ('57', '15', '1', '(31) 8889-9888', null, '2014-01-29 23:56:09', '2014-01-29 23:56:09');
INSERT INTO `phones` VALUES ('58', '15', null, null, null, '2014-01-29 23:56:09', '2014-01-29 23:56:09');
INSERT INTO `phones` VALUES ('59', '15', null, null, null, '2014-01-29 23:56:09', '2014-01-29 23:56:09');
INSERT INTO `phones` VALUES ('60', '18', '1', '(31) 8898-8898', null, '2014-01-30 19:01:41', '2014-01-30 19:01:41');
INSERT INTO `phones` VALUES ('61', '18', '0', '', null, '2014-01-30 19:01:41', '2014-01-30 19:01:41');
INSERT INTO `phones` VALUES ('62', '18', null, '', null, '2014-01-30 19:01:41', '2014-01-30 19:01:41');

-- ----------------------------
-- Table structure for `points`
-- ----------------------------
DROP TABLE IF EXISTS `points`;
CREATE TABLE `points` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `program_id` int(11) unsigned DEFAULT NULL,
  `points` int(11) DEFAULT NULL,
  `reserved` int(11) DEFAULT NULL,
  `fee` float(10,10) DEFAULT NULL,
  `price_unity` float(10,10) DEFAULT NULL,
  `multiple` int(11) DEFAULT '0',
  `program_image` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `negotiable` int(11) unsigned DEFAULT '1',
  `negotiations` int(11) unsigned DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of points
-- ----------------------------
INSERT INTO `points` VALUES ('49', '2', '2', '0', null, null, '0.0000000000', '1000', 'azul_miles13895496209509_20131211114204592768a.jpg', '1', '0', '2014-01-12 16:01:44', '2014-01-26 14:45:13');
INSERT INTO `points` VALUES ('50', '2', '3', '0', null, null, '0.0000000000', '1000', 'azul_dotz13895496280194_142119.jpg', '1', '0', '2014-01-12 16:01:44', '2014-01-26 14:45:13');
INSERT INTO `points` VALUES ('51', '2', '4', '0', null, null, '0.0000000000', '1000', 'gol_miles1389549675425_142119.jpg', '1', '0', '2014-01-12 16:01:44', '2014-01-26 14:45:14');
INSERT INTO `points` VALUES ('52', '2', '1', '0', null, null, '0.0000000000', '1000', 'tam_miles13895496803796_20131211114204592768a.jpg', '1', '0', '2014-01-12 16:01:45', '2014-01-26 14:45:14');
INSERT INTO `points` VALUES ('53', '5', '2', '100000', null, null, '0.0285000000', '1000', 'azul_dotz13895496280194_142119.jpg', '1', '0', '2014-01-12 16:01:45', null);
INSERT INTO `points` VALUES ('54', '6', '2', '100000', null, null, '0.0285000000', '1000', 'azul_dotz13895496280194_142119.jpg', '1', '0', '2014-01-12 16:01:45', null);
INSERT INTO `points` VALUES ('55', '7', '2', '15000', null, null, '0.0285000000', '1000', 'azul_dotz13895496280194_142119.jpg', '1', '0', '2014-01-12 16:01:45', null);
INSERT INTO `points` VALUES ('56', '56', '1', '1000', null, null, '0.0199999996', '1000', null, '1', '0', '2014-01-28 12:45:54', '2014-01-28 12:45:54');
INSERT INTO `points` VALUES ('57', '57', '2', '50000', null, null, '0.0350000001', '1000', null, '1', '0', '2014-01-28 12:50:15', '2014-01-28 12:50:15');
INSERT INTO `points` VALUES ('58', '56', '2', '10000', null, null, '0.0199999996', '1000', null, '1', '0', '2014-01-30 00:00:09', '2014-01-30 00:00:09');

-- ----------------------------
-- Table structure for `programs`
-- ----------------------------
DROP TABLE IF EXISTS `programs`;
CREATE TABLE `programs` (
  `id` int(10) unsigned NOT NULL,
  `hash` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `acronym` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `airline` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `kind_name` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activated` int(11) DEFAULT NULL,
  `img_1_points_register` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `add_activated` int(11) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of programs
-- ----------------------------
INSERT INTO `programs` VALUES ('1', 'tam_miles', 'TAM', 'TAM', 'Tam Milhas', 'milhas', '1', 'points_register_tam_miles.png', '1', '2013-11-01 18:29:16', null);
INSERT INTO `programs` VALUES ('2', 'azul_miles', 'AZUL', 'AZUL', 'Azul Milhas', 'milhas', '1', 'points_register_azul_miles.png', '1', '2013-11-01 18:29:16', null);
INSERT INTO `programs` VALUES ('3', 'azul_dotz', 'DOTZ', 'AZUL', 'Azul Dotz', 'dotz', '0', 'points_register_azul_dotz.png', '1', '2013-11-01 18:29:16', null);
INSERT INTO `programs` VALUES ('4', 'gol_miles', 'SMILES', 'GOL', 'Gol Milhas', 'milhas', '0', 'points_register_gol_miles.png', '1', '2013-11-01 18:29:16', null);

-- ----------------------------
-- Table structure for `promotions`
-- ----------------------------
DROP TABLE IF EXISTS `promotions`;
CREATE TABLE `promotions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `historic_flight_id` int(11) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of promotions
-- ----------------------------

-- ----------------------------
-- Table structure for `quotations`
-- ----------------------------
DROP TABLE IF EXISTS `quotations`;
CREATE TABLE `quotations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stretch_id` int(11) DEFAULT NULL,
  `reference_date` date DEFAULT NULL,
  `last_quotation_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of quotations
-- ----------------------------

-- ----------------------------
-- Table structure for `recent_flights`
-- ----------------------------
DROP TABLE IF EXISTS `recent_flights`;
CREATE TABLE `recent_flights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quotation_id` int(11) DEFAULT NULL,
  `program_id` int(11) DEFAULT NULL,
  `from_airport_acronnym` varchar(10) DEFAULT NULL,
  `to_airport_acronnym` varchar(10) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `carrier_id` int(11) DEFAULT NULL,
  `stops` int(5) DEFAULT NULL,
  `trip` int(5) DEFAULT NULL,
  `arrival` date DEFAULT NULL,
  `departure` date DEFAULT NULL,
  `price_children` float(11,2) DEFAULT NULL,
  `price_adults` float(11,2) DEFAULT NULL,
  `miles_children` int(11) DEFAULT NULL,
  `miles_adults` int(11) DEFAULT NULL,
  `pre_fee` float(11,2) DEFAULT NULL,
  `fee_departure` float(11,2) DEFAULT NULL,
  `fee_arrival` float(11,2) DEFAULT NULL,
  `price_smartmilhas` float(11,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of recent_flights
-- ----------------------------

-- ----------------------------
-- Table structure for `recent_trips`
-- ----------------------------
DROP TABLE IF EXISTS `recent_trips`;
CREATE TABLE `recent_trips` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recent_flight_id` int(11) DEFAULT NULL,
  `number` varchar(10) DEFAULT NULL,
  `airplane` varchar(50) DEFAULT NULL,
  `kind` varchar(10) DEFAULT NULL,
  `departure` date DEFAULT NULL,
  `arrival` date DEFAULT NULL,
  `seq` int(5) DEFAULT NULL,
  `next_recent_trip_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of recent_trips
-- ----------------------------

-- ----------------------------
-- Table structure for `sales`
-- ----------------------------
DROP TABLE IF EXISTS `sales`;
CREATE TABLE `sales` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) unsigned DEFAULT NULL,
  `kind` int(11) DEFAULT NULL,
  `price` float(10,2) DEFAULT NULL,
  `completed` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sales
-- ----------------------------

-- ----------------------------
-- Table structure for `searches`
-- ----------------------------
DROP TABLE IF EXISTS `searches`;
CREATE TABLE `searches` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `trip` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `from` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `to` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `arrival` date DEFAULT NULL,
  `departure` date DEFAULT NULL,
  `adults` int(11) DEFAULT NULL,
  `children` int(11) DEFAULT NULL,
  `babies` int(11) DEFAULT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `ip` char(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=214 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of searches
-- ----------------------------
INSERT INTO `searches` VALUES ('1', 'R', 'CNF', 'BEL', '0000-00-00', '2013-10-31', '1', '0', '0', null, '', '2013-10-31 13:51:36', '2013-10-31 13:51:36');
INSERT INTO `searches` VALUES ('2', 'R', 'CNF', 'BEL', '2013-12-24', '2013-11-04', '1', '0', '0', null, null, '2013-10-31 13:51:59', '2013-10-31 13:51:59');
INSERT INTO `searches` VALUES ('3', 'R', 'CNF', 'BEL', '2013-11-18', '2013-10-31', '1', '0', '0', null, '', '2013-10-31 13:55:42', '2013-10-31 13:55:42');
INSERT INTO `searches` VALUES ('4', 'R', 'CNF', 'BEL', '2013-11-14', '2013-10-31', '1', '0', '0', null, '', '2013-10-31 13:56:23', '2013-10-31 13:56:23');
INSERT INTO `searches` VALUES ('5', 'R', 'CNF', 'BEL', '2013-11-14', '2013-10-31', '1', '0', '0', null, '', '2013-10-31 13:58:52', '2013-10-31 13:58:52');
INSERT INTO `searches` VALUES ('6', 'R', 'CNF', 'BEL', '2013-11-14', '2013-10-31', '1', '0', '0', null, '', '2013-10-31 13:59:05', '2013-10-31 13:59:05');
INSERT INTO `searches` VALUES ('7', 'R', 'CNF', 'BEL', '2013-11-14', '2013-10-31', '1', '0', '0', null, '', '2013-10-31 14:09:28', '2013-10-31 14:09:28');
INSERT INTO `searches` VALUES ('8', 'R', 'CNF', 'BEL', '2013-11-13', '2013-10-31', '1', '0', '0', null, '', '2013-10-31 14:11:37', '2013-10-31 14:11:37');
INSERT INTO `searches` VALUES ('9', 'R', 'BEL', 'CNF', '2013-12-15', '2013-11-10', '1', '0', '0', null, null, '2013-10-31 14:34:28', '2013-10-31 14:34:28');
INSERT INTO `searches` VALUES ('10', 'R', 'BEL', 'CNF', '2013-12-15', '2013-11-10', '1', '0', '0', null, null, '2013-10-31 14:35:54', '2013-10-31 14:35:54');
INSERT INTO `searches` VALUES ('11', 'R', 'BEL', 'CNF', null, null, '1', '0', '0', null, null, '2013-10-31 14:38:20', '2013-10-31 14:38:20');
INSERT INTO `searches` VALUES ('12', 'R', 'BEL', 'CNF', null, null, '1', '0', '0', null, null, '2013-10-31 14:38:46', '2013-10-31 14:38:46');
INSERT INTO `searches` VALUES ('13', 'R', 'BEL', 'CNF', null, null, '1', '0', '0', null, null, '2013-10-31 14:42:23', '2013-10-31 14:42:23');
INSERT INTO `searches` VALUES ('14', 'R', 'BEL', 'CNF', null, null, '1', '0', '0', null, null, '2013-10-31 14:43:32', '2013-10-31 14:43:32');
INSERT INTO `searches` VALUES ('15', 'R', 'BEL', 'CNF', null, null, '1', '0', '0', null, null, '2013-10-31 14:46:05', '2013-10-31 14:46:05');
INSERT INTO `searches` VALUES ('16', 'R', 'BEL', 'CNF', null, null, '1', '0', '0', null, null, '2013-10-31 14:47:47', '2013-10-31 14:47:47');
INSERT INTO `searches` VALUES ('17', 'R', 'BEL', 'CNF', null, null, '1', '0', '0', null, null, '2013-10-31 14:50:56', '2013-10-31 14:50:56');
INSERT INTO `searches` VALUES ('18', 'R', 'BEL', 'CNF', null, null, '1', '0', '0', null, null, '2013-10-31 15:00:21', '2013-10-31 15:00:21');
INSERT INTO `searches` VALUES ('19', 'R', 'BEL', 'CNF', null, null, '1', '0', '0', null, null, '2013-10-31 15:07:13', '2013-10-31 15:07:13');
INSERT INTO `searches` VALUES ('20', 'R', 'BEL', 'CNF', null, null, '1', '0', '0', null, null, '2013-10-31 15:09:13', '2013-10-31 15:09:13');
INSERT INTO `searches` VALUES ('21', 'R', 'BEL', 'CNF', null, null, '1', '0', '0', null, null, '2013-10-31 15:12:02', '2013-10-31 15:12:02');
INSERT INTO `searches` VALUES ('22', 'R', 'BEL', 'CNF', null, null, '1', '0', '0', null, null, '2013-10-31 15:19:30', '2013-10-31 15:19:30');
INSERT INTO `searches` VALUES ('23', 'R', 'BEL', 'CNF', null, null, '1', '0', '0', null, null, '2013-10-31 15:22:01', '2013-10-31 15:22:01');
INSERT INTO `searches` VALUES ('24', 'R', 'BEL', 'CNF', null, null, '1', '0', '0', null, null, '2013-10-31 15:22:46', '2013-10-31 15:22:46');
INSERT INTO `searches` VALUES ('25', 'R', 'BEL', 'CNF', null, null, '1', '0', '0', null, null, '2013-10-31 15:45:33', '2013-10-31 15:45:33');
INSERT INTO `searches` VALUES ('26', 'R', 'BEL', 'CNF', null, null, '1', '0', '0', null, null, '2013-10-31 15:46:25', '2013-10-31 15:46:25');
INSERT INTO `searches` VALUES ('27', 'R', 'BEL', 'CNF', null, null, '1', '0', '0', null, null, '2013-10-31 15:49:17', '2013-10-31 15:49:17');
INSERT INTO `searches` VALUES ('28', 'R', 'BEL', 'CNF', null, null, '1', '0', '0', null, null, '2013-10-31 15:50:11', '2013-10-31 15:50:11');
INSERT INTO `searches` VALUES ('29', 'R', 'BEL', 'CNF', null, null, '1', '0', '0', null, null, '2013-10-31 15:53:15', '2013-10-31 15:53:15');
INSERT INTO `searches` VALUES ('30', 'R', 'BEL', 'CNF', null, null, '1', '0', '0', null, null, '2013-10-31 15:55:14', '2013-10-31 15:55:14');
INSERT INTO `searches` VALUES ('31', 'R', 'BEL', 'CNF', null, null, '1', '0', '0', null, null, '2013-10-31 16:17:28', '2013-10-31 16:17:28');
INSERT INTO `searches` VALUES ('32', 'R', 'BEL', 'CNF', null, null, '1', '0', '0', null, null, '2013-10-31 16:26:34', '2013-10-31 16:26:34');
INSERT INTO `searches` VALUES ('33', 'R', 'BEL', 'CNF', null, null, '1', '0', '0', null, null, '2013-10-31 16:27:43', '2013-10-31 16:27:43');
INSERT INTO `searches` VALUES ('34', 'R', 'BHZ', 'IPN', null, null, '1', '0', '0', null, null, '2013-10-31 16:30:03', '2013-10-31 16:30:03');
INSERT INTO `searches` VALUES ('35', 'R', 'BHZ', 'IPN', null, null, '1', '0', '0', null, null, '2013-10-31 16:30:36', '2013-10-31 16:30:36');
INSERT INTO `searches` VALUES ('36', 'R', 'BHZ', 'IPN', null, null, '1', '0', '0', null, null, '2013-10-31 16:35:16', '2013-10-31 16:35:16');
INSERT INTO `searches` VALUES ('37', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-10-31 16:47:36', '2013-10-31 16:47:36');
INSERT INTO `searches` VALUES ('38', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-01 14:10:02', '2013-11-01 14:10:02');
INSERT INTO `searches` VALUES ('39', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-01 14:12:52', '2013-11-01 14:12:52');
INSERT INTO `searches` VALUES ('40', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-01 14:18:42', '2013-11-01 14:18:42');
INSERT INTO `searches` VALUES ('41', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-01 17:27:02', '2013-11-01 17:27:02');
INSERT INTO `searches` VALUES ('42', 'O', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-01 17:28:09', '2013-11-01 17:28:09');
INSERT INTO `searches` VALUES ('43', 'O', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-01 17:28:57', '2013-11-01 17:28:57');
INSERT INTO `searches` VALUES ('44', 'O', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-01 17:29:15', '2013-11-01 17:29:15');
INSERT INTO `searches` VALUES ('45', 'O', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-01 17:29:46', '2013-11-01 17:29:46');
INSERT INTO `searches` VALUES ('46', 'O', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-01 17:30:02', '2013-11-01 17:30:02');
INSERT INTO `searches` VALUES ('47', 'O', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-01 17:50:50', '2013-11-01 17:50:50');
INSERT INTO `searches` VALUES ('48', null, null, null, null, null, null, null, null, null, null, '2013-11-01 17:50:56', '2013-11-01 17:50:56');
INSERT INTO `searches` VALUES ('49', null, null, null, null, null, null, null, null, null, null, '2013-11-01 18:02:41', '2013-11-01 18:02:41');
INSERT INTO `searches` VALUES ('50', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-01 18:03:02', '2013-11-01 18:03:02');
INSERT INTO `searches` VALUES ('51', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-01 18:12:55', '2013-11-01 18:12:55');
INSERT INTO `searches` VALUES ('52', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-01 18:15:08', '2013-11-01 18:15:08');
INSERT INTO `searches` VALUES ('53', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-01 18:33:34', '2013-11-01 18:33:34');
INSERT INTO `searches` VALUES ('54', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-01 18:33:45', '2013-11-01 18:33:45');
INSERT INTO `searches` VALUES ('55', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-01 18:34:04', '2013-11-01 18:34:04');
INSERT INTO `searches` VALUES ('56', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-02 16:02:28', '2013-11-02 16:02:28');
INSERT INTO `searches` VALUES ('57', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-02 17:13:53', '2013-11-02 17:13:53');
INSERT INTO `searches` VALUES ('58', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-02 18:26:24', '2013-11-02 18:26:24');
INSERT INTO `searches` VALUES ('59', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-02 19:20:42', '2013-11-02 19:20:42');
INSERT INTO `searches` VALUES ('60', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-04 13:58:07', '2013-11-04 13:58:07');
INSERT INTO `searches` VALUES ('61', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-04 14:13:20', '2013-11-04 14:13:20');
INSERT INTO `searches` VALUES ('62', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-04 15:44:25', '2013-11-04 15:44:25');
INSERT INTO `searches` VALUES ('63', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-04 15:47:37', '2013-11-04 15:47:37');
INSERT INTO `searches` VALUES ('64', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-04 16:45:47', '2013-11-04 16:45:47');
INSERT INTO `searches` VALUES ('65', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-04 16:48:34', '2013-11-04 16:48:34');
INSERT INTO `searches` VALUES ('66', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-04 16:50:01', '2013-11-04 16:50:01');
INSERT INTO `searches` VALUES ('67', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-04 16:58:32', '2013-11-04 16:58:32');
INSERT INTO `searches` VALUES ('68', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-04 17:02:56', '2013-11-04 17:02:56');
INSERT INTO `searches` VALUES ('69', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-04 17:04:50', '2013-11-04 17:04:50');
INSERT INTO `searches` VALUES ('70', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-04 17:08:01', '2013-11-04 17:08:01');
INSERT INTO `searches` VALUES ('71', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-04 17:08:56', '2013-11-04 17:08:56');
INSERT INTO `searches` VALUES ('72', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-04 17:12:21', '2013-11-04 17:12:21');
INSERT INTO `searches` VALUES ('73', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-04 17:59:53', '2013-11-04 17:59:53');
INSERT INTO `searches` VALUES ('74', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-04 19:58:14', '2013-11-04 19:58:14');
INSERT INTO `searches` VALUES ('75', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-05 09:31:13', '2013-11-05 09:31:13');
INSERT INTO `searches` VALUES ('76', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-06 17:45:27', '2013-11-06 17:45:27');
INSERT INTO `searches` VALUES ('77', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-06 18:38:46', '2013-11-06 18:38:46');
INSERT INTO `searches` VALUES ('78', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-06 19:23:06', '2013-11-06 19:23:06');
INSERT INTO `searches` VALUES ('79', 'R', 'BHZ', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-06 22:41:11', '2013-11-06 22:41:11');
INSERT INTO `searches` VALUES ('80', 'R', 'BHZ', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-07 09:41:07', '2013-11-07 09:41:07');
INSERT INTO `searches` VALUES ('81', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-07 11:34:09', '2013-11-07 11:34:09');
INSERT INTO `searches` VALUES ('82', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-07 11:52:13', '2013-11-07 11:52:13');
INSERT INTO `searches` VALUES ('83', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-07 11:58:11', '2013-11-07 11:58:11');
INSERT INTO `searches` VALUES ('84', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-07 11:59:00', '2013-11-07 11:59:00');
INSERT INTO `searches` VALUES ('85', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-07 12:29:14', '2013-11-07 12:29:14');
INSERT INTO `searches` VALUES ('86', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-07 12:34:30', '2013-11-07 12:34:30');
INSERT INTO `searches` VALUES ('87', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-07 12:43:38', '2013-11-07 12:43:38');
INSERT INTO `searches` VALUES ('88', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-07 12:52:55', '2013-11-07 12:52:55');
INSERT INTO `searches` VALUES ('89', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-07 12:58:57', '2013-11-07 12:58:57');
INSERT INTO `searches` VALUES ('90', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-07 14:28:51', '2013-11-07 14:28:51');
INSERT INTO `searches` VALUES ('91', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-07 15:00:25', '2013-11-07 15:00:25');
INSERT INTO `searches` VALUES ('92', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-07 15:45:53', '2013-11-07 15:45:53');
INSERT INTO `searches` VALUES ('93', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-07 16:17:04', '2013-11-07 16:17:04');
INSERT INTO `searches` VALUES ('94', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-08 16:23:08', '2013-11-08 16:23:08');
INSERT INTO `searches` VALUES ('95', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-08 17:04:27', '2013-11-08 17:04:27');
INSERT INTO `searches` VALUES ('96', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-11 10:29:59', '2013-11-11 10:29:59');
INSERT INTO `searches` VALUES ('97', 'R', 'CNF', 'IPN', null, null, '1', '0', '0', null, null, '2013-11-11 11:19:53', '2013-11-11 11:19:53');
INSERT INTO `searches` VALUES ('98', null, null, null, null, null, null, null, null, null, null, '2013-11-22 12:09:49', '2013-11-22 12:09:49');
INSERT INTO `searches` VALUES ('99', null, null, null, null, null, null, null, null, null, null, '2013-11-22 12:10:11', '2013-11-22 12:10:11');
INSERT INTO `searches` VALUES ('100', null, null, null, null, null, null, null, null, null, null, '2013-11-22 12:31:51', '2013-11-22 12:31:51');
INSERT INTO `searches` VALUES ('101', null, null, null, null, null, null, null, null, null, null, '2013-11-22 16:14:44', '2013-11-22 16:14:44');
INSERT INTO `searches` VALUES ('102', null, null, null, null, null, null, null, null, null, null, '2013-11-22 16:15:31', '2013-11-22 16:15:31');
INSERT INTO `searches` VALUES ('103', null, null, null, null, null, null, null, null, null, null, '2013-11-22 16:15:57', '2013-11-22 16:15:57');
INSERT INTO `searches` VALUES ('104', null, null, null, null, null, null, null, null, null, null, '2013-11-22 17:00:12', '2013-11-22 17:00:12');
INSERT INTO `searches` VALUES ('105', null, null, null, null, null, null, null, null, null, null, '2013-11-22 18:23:16', '2013-11-22 18:23:16');
INSERT INTO `searches` VALUES ('106', null, null, null, null, null, null, null, null, null, null, '2013-11-22 18:42:25', '2013-11-22 18:42:25');
INSERT INTO `searches` VALUES ('107', null, null, null, null, null, null, null, null, null, null, '2013-11-22 18:43:08', '2013-11-22 18:43:08');
INSERT INTO `searches` VALUES ('108', null, null, null, null, null, null, null, null, null, null, '2013-11-22 19:02:49', '2013-11-22 19:02:49');
INSERT INTO `searches` VALUES ('109', null, null, null, null, null, null, null, null, null, null, '2013-11-22 19:03:09', '2013-11-22 19:03:09');
INSERT INTO `searches` VALUES ('110', null, null, null, null, null, null, null, null, null, null, '2013-12-10 16:14:44', '2013-12-10 16:14:44');
INSERT INTO `searches` VALUES ('111', null, null, null, null, null, null, null, null, null, null, '2013-12-10 16:16:36', '2013-12-10 16:16:36');
INSERT INTO `searches` VALUES ('112', null, null, null, null, null, null, null, null, null, null, '2013-12-10 16:21:44', '2013-12-10 16:21:44');
INSERT INTO `searches` VALUES ('113', null, null, null, null, null, null, null, null, null, null, '2013-12-10 16:26:54', '2013-12-10 16:26:54');
INSERT INTO `searches` VALUES ('114', null, null, null, null, null, null, null, null, null, null, '2013-12-10 16:27:35', '2013-12-10 16:27:35');
INSERT INTO `searches` VALUES ('115', null, null, null, null, null, null, null, null, null, null, '2013-12-10 16:33:24', '2013-12-10 16:33:24');
INSERT INTO `searches` VALUES ('116', null, null, null, null, null, null, null, null, null, null, '2013-12-10 16:50:04', '2013-12-10 16:50:04');
INSERT INTO `searches` VALUES ('117', null, null, null, null, null, null, null, null, null, null, '2013-12-10 16:52:07', '2013-12-10 16:52:07');
INSERT INTO `searches` VALUES ('118', null, null, null, null, null, null, null, null, null, null, '2013-12-10 16:54:23', '2013-12-10 16:54:23');
INSERT INTO `searches` VALUES ('119', null, null, null, null, null, null, null, null, null, null, '2013-12-10 16:56:16', '2013-12-10 16:56:16');
INSERT INTO `searches` VALUES ('120', null, null, null, null, null, null, null, null, null, null, '2013-12-11 19:47:48', '2013-12-11 19:47:48');
INSERT INTO `searches` VALUES ('121', null, null, null, null, null, null, null, null, null, null, '2013-12-12 16:03:31', '2013-12-12 16:03:31');
INSERT INTO `searches` VALUES ('122', null, null, null, null, null, null, null, null, null, null, '2013-12-12 16:04:29', '2013-12-12 16:04:29');
INSERT INTO `searches` VALUES ('123', null, null, null, null, null, null, null, null, null, null, '2013-12-12 16:05:51', '2013-12-12 16:05:51');
INSERT INTO `searches` VALUES ('124', null, null, null, null, null, null, null, null, null, null, '2013-12-12 16:06:48', '2013-12-12 16:06:48');
INSERT INTO `searches` VALUES ('125', null, null, null, null, null, null, null, null, null, null, '2013-12-12 16:07:59', '2013-12-12 16:07:59');
INSERT INTO `searches` VALUES ('126', null, null, null, null, null, null, null, null, null, null, '2013-12-12 16:08:32', '2013-12-12 16:08:32');
INSERT INTO `searches` VALUES ('127', null, null, null, null, null, null, null, null, null, null, '2013-12-12 16:08:53', '2013-12-12 16:08:53');
INSERT INTO `searches` VALUES ('128', null, null, null, null, null, null, null, null, null, null, '2013-12-12 16:14:04', '2013-12-12 16:14:04');
INSERT INTO `searches` VALUES ('129', null, null, null, null, null, null, null, null, null, null, '2013-12-12 16:14:45', '2013-12-12 16:14:45');
INSERT INTO `searches` VALUES ('130', null, null, null, null, null, null, null, null, null, null, '2013-12-12 17:16:38', '2013-12-12 17:16:38');
INSERT INTO `searches` VALUES ('131', null, null, null, null, null, null, null, null, null, null, '2013-12-12 17:30:13', '2013-12-12 17:30:13');
INSERT INTO `searches` VALUES ('132', null, null, null, null, null, null, null, null, null, null, '2013-12-12 17:30:59', '2013-12-12 17:30:59');
INSERT INTO `searches` VALUES ('133', null, null, null, null, null, null, null, null, null, null, '2013-12-12 17:34:22', '2013-12-12 17:34:22');
INSERT INTO `searches` VALUES ('134', null, null, null, null, null, null, null, null, null, null, '2013-12-12 17:37:23', '2013-12-12 17:37:23');
INSERT INTO `searches` VALUES ('135', null, null, null, null, null, null, null, null, null, null, '2013-12-12 17:38:30', '2013-12-12 17:38:30');
INSERT INTO `searches` VALUES ('136', null, null, null, null, null, null, null, null, null, null, '2013-12-12 17:43:25', '2013-12-12 17:43:25');
INSERT INTO `searches` VALUES ('137', null, null, null, null, null, null, null, null, null, null, '2013-12-12 18:21:37', '2013-12-12 18:21:37');
INSERT INTO `searches` VALUES ('138', null, null, null, null, null, null, null, null, null, null, '2013-12-14 15:01:07', '2013-12-14 15:01:07');
INSERT INTO `searches` VALUES ('139', null, null, null, null, null, null, null, null, null, null, '2013-12-14 17:21:15', '2013-12-14 17:21:15');
INSERT INTO `searches` VALUES ('140', null, null, null, null, null, null, null, null, null, null, '2013-12-14 17:24:53', '2013-12-14 17:24:53');
INSERT INTO `searches` VALUES ('141', null, null, null, null, null, null, null, null, null, null, '2013-12-14 17:26:00', '2013-12-14 17:26:00');
INSERT INTO `searches` VALUES ('142', null, null, null, null, null, null, null, null, null, null, '2013-12-14 17:26:10', '2013-12-14 17:26:10');
INSERT INTO `searches` VALUES ('143', null, null, null, null, null, null, null, null, null, null, '2013-12-14 17:27:32', '2013-12-14 17:27:32');
INSERT INTO `searches` VALUES ('144', null, null, null, null, null, null, null, null, null, null, '2013-12-14 17:27:41', '2013-12-14 17:27:41');
INSERT INTO `searches` VALUES ('145', null, null, null, null, null, null, null, null, null, null, '2013-12-14 17:31:42', '2013-12-14 17:31:42');
INSERT INTO `searches` VALUES ('146', null, null, null, null, null, null, null, null, null, null, '2013-12-14 17:32:19', '2013-12-14 17:32:19');
INSERT INTO `searches` VALUES ('147', null, null, null, null, null, null, null, null, null, null, '2013-12-14 17:33:12', '2013-12-14 17:33:12');
INSERT INTO `searches` VALUES ('148', null, null, null, null, null, null, null, null, null, null, '2013-12-14 17:44:14', '2013-12-14 17:44:14');
INSERT INTO `searches` VALUES ('149', null, null, null, null, null, null, null, null, null, null, '2013-12-14 17:44:56', '2013-12-14 17:44:56');
INSERT INTO `searches` VALUES ('150', null, null, null, null, null, null, null, null, null, null, '2013-12-14 20:06:06', '2013-12-14 20:06:06');
INSERT INTO `searches` VALUES ('151', null, null, null, null, null, null, null, null, null, null, '2013-12-14 20:15:09', '2013-12-14 20:15:09');
INSERT INTO `searches` VALUES ('152', null, null, null, null, null, null, null, null, null, null, '2013-12-14 20:18:17', '2013-12-14 20:18:17');
INSERT INTO `searches` VALUES ('153', null, null, null, null, null, null, null, null, null, null, '2013-12-14 20:18:49', '2013-12-14 20:18:49');
INSERT INTO `searches` VALUES ('154', null, null, null, null, null, null, null, null, null, null, '2013-12-14 20:19:24', '2013-12-14 20:19:24');
INSERT INTO `searches` VALUES ('155', null, null, null, null, null, null, null, null, null, null, '2013-12-14 20:20:47', '2013-12-14 20:20:47');
INSERT INTO `searches` VALUES ('156', null, null, null, null, null, null, null, null, null, null, '2013-12-14 20:21:31', '2013-12-14 20:21:31');
INSERT INTO `searches` VALUES ('157', null, null, null, null, null, null, null, null, null, null, '2013-12-14 20:23:04', '2013-12-14 20:23:04');
INSERT INTO `searches` VALUES ('158', null, null, null, null, null, null, null, null, null, null, '2013-12-14 20:23:27', '2013-12-14 20:23:27');
INSERT INTO `searches` VALUES ('159', null, null, null, null, null, null, null, null, null, null, '2013-12-14 20:24:52', '2013-12-14 20:24:52');
INSERT INTO `searches` VALUES ('160', null, null, null, null, null, null, null, null, null, null, '2013-12-14 20:26:52', '2013-12-14 20:26:52');
INSERT INTO `searches` VALUES ('161', null, null, null, null, null, null, null, null, null, null, '2013-12-14 20:29:02', '2013-12-14 20:29:02');
INSERT INTO `searches` VALUES ('162', null, null, null, null, null, null, null, null, null, null, '2013-12-14 20:29:57', '2013-12-14 20:29:57');
INSERT INTO `searches` VALUES ('163', null, null, null, null, null, null, null, null, null, null, '2013-12-14 20:30:42', '2013-12-14 20:30:42');
INSERT INTO `searches` VALUES ('164', null, null, null, null, null, null, null, null, null, null, '2013-12-14 20:31:23', '2013-12-14 20:31:23');
INSERT INTO `searches` VALUES ('165', null, null, null, null, null, null, null, null, null, null, '2013-12-14 20:37:18', '2013-12-14 20:37:18');
INSERT INTO `searches` VALUES ('166', null, null, null, null, null, null, null, null, null, null, '2013-12-14 20:40:43', '2013-12-14 20:40:43');
INSERT INTO `searches` VALUES ('167', null, null, null, null, null, null, null, null, null, null, '2013-12-14 21:07:51', '2013-12-14 21:07:51');
INSERT INTO `searches` VALUES ('168', null, null, null, null, null, null, null, null, null, null, '2013-12-14 21:09:33', '2013-12-14 21:09:33');
INSERT INTO `searches` VALUES ('169', null, null, null, null, null, null, null, null, null, null, '2013-12-14 21:09:58', '2013-12-14 21:09:58');
INSERT INTO `searches` VALUES ('170', null, null, null, null, null, null, null, null, null, null, '2013-12-14 21:10:29', '2013-12-14 21:10:29');
INSERT INTO `searches` VALUES ('171', null, null, null, null, null, null, null, null, null, null, '2013-12-14 21:11:31', '2013-12-14 21:11:31');
INSERT INTO `searches` VALUES ('172', null, null, null, null, null, null, null, null, null, null, '2013-12-14 21:14:42', '2013-12-14 21:14:42');
INSERT INTO `searches` VALUES ('173', null, null, null, null, null, null, null, null, null, null, '2013-12-14 21:14:49', '2013-12-14 21:14:49');
INSERT INTO `searches` VALUES ('174', null, null, null, null, null, null, null, null, null, null, '2013-12-14 21:15:12', '2013-12-14 21:15:12');
INSERT INTO `searches` VALUES ('175', null, null, null, null, null, null, null, null, null, null, '2013-12-14 21:15:40', '2013-12-14 21:15:40');
INSERT INTO `searches` VALUES ('176', null, null, null, null, null, null, null, null, null, null, '2013-12-14 21:16:47', '2013-12-14 21:16:47');
INSERT INTO `searches` VALUES ('177', null, null, null, null, null, null, null, null, null, null, '2013-12-14 21:27:57', '2013-12-14 21:27:57');
INSERT INTO `searches` VALUES ('178', null, null, null, null, null, null, null, null, null, null, '2013-12-14 21:29:34', '2013-12-14 21:29:34');
INSERT INTO `searches` VALUES ('179', null, null, null, null, null, null, null, null, null, null, '2013-12-14 21:31:07', '2013-12-14 21:31:07');
INSERT INTO `searches` VALUES ('180', null, null, null, null, null, null, null, null, null, null, '2013-12-14 21:31:16', '2013-12-14 21:31:16');
INSERT INTO `searches` VALUES ('181', null, null, null, null, null, null, null, null, null, null, '2013-12-14 21:32:01', '2013-12-14 21:32:01');
INSERT INTO `searches` VALUES ('182', null, null, null, null, null, null, null, null, null, null, '2013-12-14 22:42:57', '2013-12-14 22:42:57');
INSERT INTO `searches` VALUES ('183', null, null, null, null, null, null, null, null, null, null, '2013-12-14 22:43:06', '2013-12-14 22:43:06');
INSERT INTO `searches` VALUES ('184', null, null, null, null, null, null, null, null, null, null, '2013-12-14 22:45:45', '2013-12-14 22:45:45');
INSERT INTO `searches` VALUES ('185', null, null, null, null, null, null, null, null, null, null, '2013-12-14 22:50:07', '2013-12-14 22:50:07');
INSERT INTO `searches` VALUES ('186', null, null, null, null, null, null, null, null, null, null, '2013-12-14 22:50:46', '2013-12-14 22:50:46');
INSERT INTO `searches` VALUES ('187', null, null, null, null, null, null, null, null, null, null, '2013-12-14 22:50:50', '2013-12-14 22:50:50');
INSERT INTO `searches` VALUES ('188', null, null, null, null, null, null, null, null, null, null, '2013-12-15 14:33:47', '2013-12-15 14:33:47');
INSERT INTO `searches` VALUES ('189', null, null, null, null, null, null, null, null, null, null, '2013-12-15 14:34:20', '2013-12-15 14:34:20');
INSERT INTO `searches` VALUES ('190', null, null, null, null, null, null, null, null, null, null, '2013-12-15 14:34:30', '2013-12-15 14:34:30');
INSERT INTO `searches` VALUES ('191', null, null, null, null, null, null, null, null, null, null, '2013-12-15 14:34:43', '2013-12-15 14:34:43');
INSERT INTO `searches` VALUES ('192', null, null, null, null, null, null, null, null, null, null, '2013-12-15 14:34:52', '2013-12-15 14:34:52');
INSERT INTO `searches` VALUES ('193', null, null, null, null, null, null, null, null, null, null, '2013-12-15 14:35:34', '2013-12-15 14:35:34');
INSERT INTO `searches` VALUES ('194', null, null, null, null, null, null, null, null, null, null, '2013-12-15 14:35:42', '2013-12-15 14:35:42');
INSERT INTO `searches` VALUES ('195', null, null, null, null, null, null, null, null, null, null, '2013-12-15 14:35:50', '2013-12-15 14:35:50');
INSERT INTO `searches` VALUES ('196', null, null, null, null, null, null, null, null, null, null, '2013-12-15 14:35:56', '2013-12-15 14:35:56');
INSERT INTO `searches` VALUES ('197', null, null, null, null, null, null, null, null, null, null, '2013-12-15 14:36:07', '2013-12-15 14:36:07');
INSERT INTO `searches` VALUES ('198', null, null, null, null, null, null, null, null, null, null, '2013-12-15 14:36:18', '2013-12-15 14:36:18');
INSERT INTO `searches` VALUES ('199', null, null, null, null, null, null, null, null, null, null, '2013-12-15 14:36:25', '2013-12-15 14:36:25');
INSERT INTO `searches` VALUES ('200', null, null, null, null, null, null, null, null, null, null, '2013-12-15 14:36:36', '2013-12-15 14:36:36');
INSERT INTO `searches` VALUES ('201', null, null, null, null, null, null, null, null, null, null, '2013-12-15 14:36:49', '2013-12-15 14:36:49');
INSERT INTO `searches` VALUES ('202', null, null, null, null, null, null, null, null, null, null, '2013-12-16 13:49:08', '2013-12-16 13:49:08');
INSERT INTO `searches` VALUES ('203', null, null, null, null, null, null, null, null, null, null, '2013-12-16 19:53:29', '2013-12-16 19:53:29');
INSERT INTO `searches` VALUES ('204', null, null, null, null, null, null, null, null, null, null, '2013-12-16 20:37:01', '2013-12-16 20:37:01');
INSERT INTO `searches` VALUES ('205', null, null, null, null, null, null, null, null, null, null, '2013-12-16 20:38:56', '2013-12-16 20:38:56');
INSERT INTO `searches` VALUES ('206', null, null, null, null, null, null, null, null, null, null, '2013-12-16 20:39:04', '2013-12-16 20:39:04');
INSERT INTO `searches` VALUES ('207', null, null, null, null, null, null, null, null, null, null, '2013-12-16 21:14:02', '2013-12-16 21:14:02');
INSERT INTO `searches` VALUES ('208', null, null, null, null, null, null, null, null, null, null, '2013-12-16 21:15:19', '2013-12-16 21:15:19');
INSERT INTO `searches` VALUES ('209', null, null, null, null, null, null, null, null, null, null, '2013-12-16 21:23:53', '2013-12-16 21:23:53');
INSERT INTO `searches` VALUES ('210', null, null, null, null, null, null, null, null, null, null, '2013-12-16 21:26:34', '2013-12-16 21:26:34');
INSERT INTO `searches` VALUES ('211', null, null, null, null, null, null, null, null, null, null, '2013-12-16 21:31:20', '2013-12-16 21:31:20');
INSERT INTO `searches` VALUES ('212', null, null, null, null, null, null, null, null, null, null, '2013-12-16 21:31:29', '2013-12-16 21:31:29');
INSERT INTO `searches` VALUES ('213', null, null, null, null, null, null, null, null, null, null, '2013-12-16 21:32:18', '2013-12-16 21:32:18');

-- ----------------------------
-- Table structure for `states`
-- ----------------------------
DROP TABLE IF EXISTS `states`;
CREATE TABLE `states` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `acronym` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_id` int(11) unsigned DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of states
-- ----------------------------
INSERT INTO `states` VALUES ('1', 'Acre', 'AC', '35', '2013-10-29 13:56:46', null);
INSERT INTO `states` VALUES ('2', 'Alagoas', 'AL', '35', '2013-10-29 13:56:46', null);
INSERT INTO `states` VALUES ('3', 'Amazonas', 'AM', '35', '2013-10-29 13:56:46', null);
INSERT INTO `states` VALUES ('4', 'Amapá', 'AP', '35', '2013-10-29 13:56:46', null);
INSERT INTO `states` VALUES ('5', 'Bahia', 'BA', '35', '2013-10-29 13:56:46', null);
INSERT INTO `states` VALUES ('6', 'Ceará', 'CE', '35', '2013-10-29 13:56:46', null);
INSERT INTO `states` VALUES ('7', 'Distrito Federal', 'DF', '35', '2013-10-29 13:56:46', null);
INSERT INTO `states` VALUES ('8', 'Espírito Santo', 'ES', '35', '2013-10-29 13:56:46', null);
INSERT INTO `states` VALUES ('9', 'Goiás', 'GO', '35', '2013-10-29 13:56:46', null);
INSERT INTO `states` VALUES ('10', 'Maranhão', 'MA', '35', '2013-10-29 13:56:46', null);
INSERT INTO `states` VALUES ('11', 'Minas Gerais', 'MG', '35', '2013-10-29 13:56:46', null);
INSERT INTO `states` VALUES ('12', 'Mato Grosso do Sul', 'MS', '35', '2013-10-29 13:56:46', null);
INSERT INTO `states` VALUES ('13', 'Mato Grosso', 'MT', '35', '2013-10-29 13:56:46', null);
INSERT INTO `states` VALUES ('14', 'Pará', 'PA', '35', '2013-10-29 13:56:46', null);
INSERT INTO `states` VALUES ('15', 'Paraíba', 'PB', '35', '2013-10-29 13:56:46', null);
INSERT INTO `states` VALUES ('16', 'Pernambuco', 'PE', '35', '2013-10-29 13:56:46', null);
INSERT INTO `states` VALUES ('17', 'Piauí', 'PI', '35', '2013-10-29 13:56:46', null);
INSERT INTO `states` VALUES ('18', 'Paraná', 'PR', '35', '2013-10-29 13:56:46', null);
INSERT INTO `states` VALUES ('19', 'Rio de Janeiro', 'RJ', '35', '2013-10-29 13:56:46', null);
INSERT INTO `states` VALUES ('20', 'Rio Grande do Norte', 'RN', '35', '2013-10-29 13:56:46', null);
INSERT INTO `states` VALUES ('21', 'Rondônia', 'RO', '35', '2013-10-29 13:56:46', null);
INSERT INTO `states` VALUES ('22', 'Roraima', 'RR', '35', '2013-10-29 13:56:46', null);
INSERT INTO `states` VALUES ('23', 'Rio Grande do Sul', 'RS', '35', '2013-10-29 13:56:46', null);
INSERT INTO `states` VALUES ('24', 'Santa Catarina', 'SC', '35', '2013-10-29 13:56:46', null);
INSERT INTO `states` VALUES ('25', 'Sergipe', 'SE', '35', '2013-10-29 13:56:46', null);
INSERT INTO `states` VALUES ('26', 'São Paulo', 'SP', '35', '2013-10-29 13:56:46', null);
INSERT INTO `states` VALUES ('27', 'Tocantins', 'TO', '35', '2013-10-29 13:56:46', null);

-- ----------------------------
-- Table structure for `stretches`
-- ----------------------------
DROP TABLE IF EXISTS `stretches`;
CREATE TABLE `stretches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_airport_acronnym` varchar(10) DEFAULT NULL,
  `to_airport_acronnym` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of stretches
-- ----------------------------

-- ----------------------------
-- Table structure for `tickets`
-- ----------------------------
DROP TABLE IF EXISTS `tickets`;
CREATE TABLE `tickets` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `package_id` int(11) unsigned DEFAULT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `eticket` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `checked` tinyint(1) DEFAULT NULL,
  `aproved` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tickets
-- ----------------------------

-- ----------------------------
-- Table structure for `track_users`
-- ----------------------------
DROP TABLE IF EXISTS `track_users`;
CREATE TABLE `track_users` (
  `id` int(11) NOT NULL,
  `ip` varchar(30) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `lat` int(11) DEFAULT NULL,
  `lon` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of track_users
-- ----------------------------

-- ----------------------------
-- Table structure for `transactions`
-- ----------------------------
DROP TABLE IF EXISTS `transactions`;
CREATE TABLE `transactions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned DEFAULT NULL,
  `fee_gateway` float(10,2) DEFAULT NULL,
  `fee_smartmilhas` float(10,2) DEFAULT NULL,
  `fee_agency` float(10,2) DEFAULT NULL,
  `fee_company` float(10,2) DEFAULT NULL,
  `price` float(10,2) DEFAULT NULL,
  `total_price` float(10,2) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `last_status_update` datetime DEFAULT NULL,
  `visualized` datetime DEFAULT NULL,
  `status_1` datetime DEFAULT NULL,
  `status_2` datetime DEFAULT NULL,
  `status_3` datetime DEFAULT NULL,
  `status_4` datetime DEFAULT NULL,
  `status_5` datetime DEFAULT NULL,
  `status_6` datetime DEFAULT NULL,
  `status_7` datetime DEFAULT NULL,
  `status_8` datetime DEFAULT NULL,
  `status_9` datetime DEFAULT NULL,
  `status_10` datetime DEFAULT NULL,
  `status_11` datetime DEFAULT NULL,
  `status_ticket` int(11) DEFAULT NULL,
  `paid_seller` datetime DEFAULT NULL,
  `flew_confirmation` datetime DEFAULT NULL,
  `completed` int(11) DEFAULT NULL,
  `comment_buyer` longtext COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of transactions
-- ----------------------------
INSERT INTO `transactions` VALUES ('63', '2', '41.75', '0.00', '0.00', '100.00', '735.00', '876.75', '3', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2014-01-12 20:46:55', '2014-01-12 20:46:55');
INSERT INTO `transactions` VALUES ('64', '2', '41.75', '0.00', '0.00', '100.00', '735.00', '876.75', '3', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2014-01-12 21:07:42', '2014-01-12 21:07:42');
INSERT INTO `transactions` VALUES ('65', '2', '41.75', '0.00', '0.00', '100.00', '735.00', '876.75', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2014-01-12 22:48:08', '2014-01-12 22:48:08');
INSERT INTO `transactions` VALUES ('66', '2', '41.75', '0.00', '0.00', '100.00', '735.00', '876.75', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2014-01-13 20:41:29', '2014-01-13 20:41:29');
INSERT INTO `transactions` VALUES ('67', '2', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2014-01-14 14:11:12', '2014-01-14 14:11:12');
INSERT INTO `transactions` VALUES ('68', '2', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2014-01-14 14:19:41', '2014-01-14 14:19:41');
INSERT INTO `transactions` VALUES ('69', '2', '34.92', '0.00', '0.00', '100.00', '598.50', '733.42', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2014-01-15 12:58:18', '2014-01-15 12:58:18');
INSERT INTO `transactions` VALUES ('70', '2', '34.92', '0.00', '0.00', '100.00', '598.50', '733.42', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2014-01-15 15:30:18', '2014-01-15 15:30:18');
INSERT INTO `transactions` VALUES ('71', '2', '34.92', '0.00', '0.00', '100.00', '598.50', '733.42', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2014-01-15 15:32:56', '2014-01-15 15:32:56');

-- ----------------------------
-- Table structure for `trips`
-- ----------------------------
DROP TABLE IF EXISTS `trips`;
CREATE TABLE `trips` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `index` int(11) DEFAULT NULL,
  `flight_id` int(11) unsigned DEFAULT NULL,
  `airline_id` int(11) unsigned DEFAULT NULL,
  `number` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `carrier` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `airplane` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `kind` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `from` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `to` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `departure` timestamp NULL DEFAULT NULL,
  `arrival` timestamp NULL DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of trips
-- ----------------------------

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `nick` varchar(50) DEFAULT NULL,
  `facebook_id` bigint(20) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `client_id` int(11) unsigned DEFAULT NULL,
  `group_id` int(11) unsigned DEFAULT NULL,
  `condition` int(11) DEFAULT NULL,
  `register_permition` int(11) unsigned DEFAULT '1',
  `reserved` int(11) DEFAULT NULL,
  `reputation` int(11) DEFAULT NULL,
  `total_transactions_done` int(11) DEFAULT NULL,
  `total_transactions_incomplete` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('5', 'saca', null, null, '1033abcb6a989ee5fca4dc1cf2f9b2dfb294e095', '4', '3', '1', '1', '0', null, null, null, '2013-11-04 20:50:46', '2013-11-04 20:50:46');
INSERT INTO `users` VALUES ('6', 'saca@gggg.com', null, null, '1033abcb6a989ee5fca4dc1cf2f9b2dfb294e095', '2', null, null, null, null, null, null, null, '2013-11-06 14:38:07', '2013-11-06 14:38:07');
INSERT INTO `users` VALUES ('7', 'teste', null, null, '1d268a0495f544c85e9ab3e66e496bcccc773667', '3', null, null, null, null, null, null, null, '2013-11-06 19:05:06', '2013-11-06 19:05:06');
INSERT INTO `users` VALUES ('8', 'ze@gmail.com', null, null, '01d78b7889d31eb79e7b0ed7be803b002b8d9498', '4', null, null, '1', null, null, null, null, '2014-01-19 19:47:21', '2014-01-19 19:47:21');
INSERT INTO `users` VALUES ('9', 'ze@gasdsadadas.com', null, null, '01d78b7889d31eb79e7b0ed7be803b002b8d9498', '5', null, null, '1', null, null, null, null, '2014-01-19 21:34:14', '2014-01-19 21:34:14');
INSERT INTO `users` VALUES ('10', 'cartev@gmail.com', null, null, '01d78b7889d31eb79e7b0ed7be803b002b8d9498', '6', null, null, '1', null, null, null, null, '2014-01-19 21:45:14', '2014-01-19 21:45:14');
INSERT INTO `users` VALUES ('11', 'aaa@asfdsfasdf.com.br', null, null, '01d78b7889d31eb79e7b0ed7be803b002b8d9498', '7', null, null, '1', null, null, null, null, '2014-01-19 21:49:51', '2014-01-19 21:49:51');
INSERT INTO `users` VALUES ('12', 'asssssaa@asfdsfasdf.com.br', null, null, '01d78b7889d31eb79e7b0ed7be803b002b8d9498', '8', null, null, '1', null, null, null, null, '2014-01-20 18:34:05', '2014-01-20 18:34:05');
INSERT INTO `users` VALUES ('56', 'roger.gusmao.37', 'rogerfsg', '100005076363113', '94d612d07b6ed513d264c218569b750df6fbfef4', '15', '2', '1', '1', null, null, null, null, '2014-01-26 20:24:16', '2014-01-29 23:56:09');
INSERT INTO `users` VALUES ('57', 'renato@gmail.com', 'renatao', null, '01d78b7889d31eb79e7b0ed7be803b002b8d9498', '16', null, null, '1', null, null, null, null, '2014-01-28 12:49:47', '2014-01-28 12:49:47');
INSERT INTO `users` VALUES ('58', 'renato2@gmail.com', 'renato2', null, '01d78b7889d31eb79e7b0ed7be803b002b8d9498', '17', null, null, '1', null, null, null, null, '2014-01-29 21:44:16', '2014-01-29 21:44:16');
INSERT INTO `users` VALUES ('59', 'teste4@gmail.com', 'teste4', null, '01d78b7889d31eb79e7b0ed7be803b002b8d9498', '18', null, null, '1', null, null, null, null, '2014-01-30 19:01:41', '2014-01-30 19:01:41');

-- ----------------------------
-- Table structure for `user_searches`
-- ----------------------------
DROP TABLE IF EXISTS `user_searches`;
CREATE TABLE `user_searches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(30) DEFAULT NULL,
  `users_id` int(11) DEFAULT NULL,
  `quotation_id` int(11) DEFAULT NULL,
  `date_search` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_searches
-- ----------------------------
