/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50524
Source Host           : localhost:3306
Source Database       : contatos_estetica

Target Server Type    : MYSQL
Target Server Version : 50524
File Encoding         : 65001

Date: 2017-05-11 19:18:20
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `cidade`
-- ----------------------------
DROP TABLE IF EXISTS `cidade`;
CREATE TABLE `cidade` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_normalizado` varchar(100) NOT NULL,
  `latitude_INT` int(6) DEFAULT NULL,
  `longitude_INT` int(6) DEFAULT NULL,
  `pais_id_INT` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome` (`nome_normalizado`,`pais_id_INT`) USING BTREE,
  KEY `pais_id_INT` (`pais_id_INT`),
  CONSTRAINT `cidade_ibfk_1` FOREIGN KEY (`pais_id_INT`) REFERENCES `pais` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cidade
-- ----------------------------
INSERT INTO `cidade` VALUES ('1', 'BELO HORIZONTE', 'BELO HORIZONTE', null, null, '1');

-- ----------------------------
-- Table structure for `coordenada_referencia`
-- ----------------------------
DROP TABLE IF EXISTS `coordenada_referencia`;
CREATE TABLE `coordenada_referencia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `latitude_INT` int(6) NOT NULL,
  `longitude_INT` int(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of coordenada_referencia
-- ----------------------------

-- ----------------------------
-- Table structure for `entidade`
-- ----------------------------
DROP TABLE IF EXISTS `entidade`;
CREATE TABLE `entidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `nome_normalizado` varchar(255) NOT NULL,
  `tipo_entidade_id_INT` int(11) DEFAULT NULL,
  `telefone` varchar(30) DEFAULT NULL,
  `site` varchar(255) DEFAULT NULL,
  `latitude_INT` int(6) DEFAULT NULL,
  `longitude_INT` int(6) DEFAULT NULL,
  `logradouro` varchar(255) DEFAULT NULL,
  `logradouro_normalizado` varchar(255) DEFAULT NULL,
  `cidade_id_INT` int(11) DEFAULT NULL,
  `foto` varchar(50) DEFAULT NULL,
  `is_parser_check_BOOLEAN` int(1) DEFAULT NULL,
  `is_favorito_BOOLEAN` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cidade_id_INT` (`cidade_id_INT`),
  KEY `tipo_entidade_id_INT` (`tipo_entidade_id_INT`),
  CONSTRAINT `entidade_ibfk_1` FOREIGN KEY (`cidade_id_INT`) REFERENCES `cidade` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `entidade_ibfk_2` FOREIGN KEY (`tipo_entidade_id_INT`) REFERENCES `tipo_entidade` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of entidade
-- ----------------------------

-- ----------------------------
-- Table structure for `pais`
-- ----------------------------
DROP TABLE IF EXISTS `pais`;
CREATE TABLE `pais` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `nome_normalizado` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pais
-- ----------------------------
INSERT INTO `pais` VALUES ('1', 'BRASIL', 'BRASIL');

-- ----------------------------
-- Table structure for `permissao`
-- ----------------------------
DROP TABLE IF EXISTS `permissao`;
CREATE TABLE `permissao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `tag` varchar(255) DEFAULT NULL,
  `pai_permissao_id_INT` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pai_permissao_id_INT` (`pai_permissao_id_INT`),
  CONSTRAINT `permissao_ibfk_1` FOREIGN KEY (`pai_permissao_id_INT`) REFERENCES `permissao` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of permissao
-- ----------------------------

-- ----------------------------
-- Table structure for `tipo_entidade`
-- ----------------------------
DROP TABLE IF EXISTS `tipo_entidade`;
CREATE TABLE `tipo_entidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) DEFAULT NULL,
  `tag` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tipo_entidade
-- ----------------------------
INSERT INTO `tipo_entidade` VALUES ('1', 'clinica estetica', null);
INSERT INTO `tipo_entidade` VALUES ('2', 'spa', null);
