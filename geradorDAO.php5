<?php

function gerarDAO($table, $class, $key, $label, $ext, $sobrescrever){
	
    $objBanco = new Database();

    $arrExcessoesRelacionamento = array();

    $idProjeto = $_POST["projeto"];
	$pd = new Padronizador_Database($idProjeto) ;
	$qProjeto = "SELECT p.nomeBanco, c.hostnameBanco, c.usuarioBanco, c.senhaBanco, p.diretorioClassesDAO FROM projetos p, conexoes c WHERE c.id = p.conexaoBanco_INT AND p.id=$idProjeto";
	// echo $qProjeto;
	// exit();
    $objBanco->query($qProjeto);

    $database = new Database(Database::mysqli_result($objBanco->result, 0, 0));

    $database->database = Database::mysqli_result($objBanco->result, 0, 0);
    $database->host = Database::mysqli_result($objBanco->result, 0, 1);
    $database->user = Database::mysqli_result($objBanco->result, 0, 2);
    $database->password = Database::mysqli_result($objBanco->result, 0, 3);
    $diretorio = Database::mysqli_result($objBanco->result, 0, 4);

    $database->OpenLink();

    $dir = dirname(__FILE__);

    $filename = $dir . "/../../" . $diretorio ."/" . $class . ".php";
	$pathResumido = $diretorio ."/" . $class . ".php";

    $sql = "SHOW TABLES LIKE '$table';";

    $database->query($sql);

    if($database->rows < 1){

        return;

    }

    $permissaoSobreescrita = Helper::conferirPermissaoSobreEscrita($filename);

    // if file exists, then delete it
    if($permissaoSobreescrita && $sobrescrever && file_exists($filename))
    {
        unlink($filename);
    }

    if(!file_exists($filename)){

    // open file in insert mode
    $file = fopen($filename, "w+");
    $filedate = date("d.m.Y");

    $c = "";

    $c = "<?php

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:  $class
    * DATA DE GERA��O: $filedate
    * ARQUIVO:         $class.php
    * TABELA MYSQL:    $table
    * BANCO DE DADOS:  $database->database
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class $class extends Generic_DAO
    {


    // *************************
    // DECLARA��O DE ATRIBUTOS
    // *************************

";

    $sql = "SHOW COLUMNS FROM $table;";
    $database->query($sql);
    $result = $database->result;
    $labelUpper = ucfirst($label);
    $keyUpper = ucfirst($key);

    $sqlAutoIncremento = "SHOW COLUMNS FROM $table WHERE `Field` = '$key' AND `Extra` = 'auto_increment';";
    $database->query($sqlAutoIncremento);
    $result2 = $database->result;

    $autoInc = mysqli_num_rows($result2);

    if($autoInc == 1)
        $autoIncremento = "TRUE";
    else
        $autoIncremento = "FALSE";

    $numeroArquivos = 0;
    
    $sqlCamposMarcacaoDelecao = "SHOW COLUMNS FROM $table WHERE `Field` = 'excluido_BOOLEAN'";
    $database->query($sqlCamposMarcacaoDelecao);
    
    if($database->rows == 0){
        
        $database->query("ALTER TABLE {$table} ADD COLUMN excluido_BOOLEAN INT(1) DEFAULT 0 NOT NULL");
        
    }
    
    $sqlCamposMarcacaoDelecao = "SHOW COLUMNS FROM $table WHERE `Field` = 'excluido_DATETIME'";
    $database->query($sqlCamposMarcacaoDelecao);
    
    if($database->rows == 0){
        
        $database->query("ALTER TABLE {$table} ADD COLUMN excluido_DATETIME DATETIME");
        
    }

    $tabelasRelacionamento = $_POST["tabelasrelacionamento"];
    
    foreach($tabelasRelacionamento as $tabelaRelacionamento){

        $databaseAux = new Database();
        $databaseAux->query("SELECT valores_campos_texto FROM configs_salvas WHERE projetos_id_INT={$idProjeto} AND tabela_nome='{$tabelaRelacionamento}'");

        while ($row = mysqli_fetch_array($databaseAux->result)){

            $arrCamposComplemento = unserialize(html_entity_decode($row[0]));

        }
        
        $nomeEntidadePlural = $arrCamposComplemento["entidade_plural"];
        $nomeTabela = $arrCamposComplemento["tablename"];
        $nomeTabelaUC = ucfirst($nomeTabela);
        $classeEXT = $arrCamposComplemento["extname"];
        
        $tabelaBase = $table;
        $tabelaBaseUC = ucfirst($tabelaBase);
        
        $campoChave = $arrCamposComplemento["keyname"];
        $campoChaveUC = ucfirst($campoChave);
        
        //action add
        $conteudoAjaxRelacionamento .= "
        
                //bloco de {$nomeEntidadePlural}
                \$obj{$nomeTabelaUC} = new {$classeEXT}();
                \$obj{$nomeTabelaUC}->set{$tabelaBaseUC}_id_INT(\$this->get{$campoChaveUC}());

                Ajax::persistirEntidadesPresentesNosBlocosEmLista(\$obj{$nomeTabelaUC}, \"{$nomeTabela}_{$campoChave}\", true, array());

        ";
        

    }
    
    $cadastrado[0] = "cadastrado";
    $cadastrado[1] = "cadastrada";
    $cadastrado[2] = "cadastrados";
    $cadastrado[3] = "cadastradas";

    $modificado[0] = "modificado";
    $modificado[1] = "modificada";
    $modificado[2] = "modificados";
    $modificado[3] = "modificadas";

    $excluido[0] = "exclu�do";
    $excluido[1] = "exclu�da";
    $excluido[2] = "exclu�dos";
    $excluido[3] = "exclu�das";

    $foi[0] = "foi";
    $foi[1] = "foi";
    $foi[2] = "foram";
    $foi[3] = "foram";

    $artigo[0] = "O";
    $artigo[1] = "A";
    $artigo[2] = "Os";
    $artigo[3] = "As";

    $genero   = $_POST["genero_entidade"]; //F ou M
    $nomeSing = $_POST["entidade_singular"];
    $nomePlu  = $_POST["entidade_plural"];
    $numeroCadastros = $_POST["numeroCadastros"];

    $index = 0;

    if($genero == "F")
        $index += 1;

    $nomeMsg = $nomeSing;

    $mensagemExclusaoSucesso = $artigo[$index] . " " . $nomeMsg  . " " . $foi[$index] . " " . $excluido[$index] . " com sucesso.";

    if($numeroCadastros > 1){

        $index += 2;
        $nomeMsg = $nomePlu;

    }

    $mensagemCadastroSucesso = $artigo[$index] . " " . $nomeMsg . " " . $foi[$index] . " " . $cadastrado[$index] . " com sucesso.";

    $mensagemEdicaoSucesso = $artigo[$index] . " " . $nomeMsg  . " " . $foi[$index] . " " . $modificado[$index] . " com sucesso.";

    while ($row = mysqli_fetch_row($result))
    {

            $col = $row[0];
            $colUpper = ucfirst($col);
            $nomeColunaFormatado = ucwords($_POST["nomexcampo_" . $col]);
            $strBodyEmptyFunction .= "\t\t\$this->{$col}  = null;\n";
            
            $ocorrencias = array(" De ", " Da ", " Do ", " Das ", " Dos ", " Em ", " No ", " Na ", " Nos ", " Nas ");
            $substituicoes = array(" de ", " da ", " do ", " das ", " dos ", " em ", " no ", " na ", " nos ", " nas ");

            $nomeColunaFormatado = str_replace($ocorrencias, $substituicoes, $nomeColunaFormatado);

            $instLabel .= "\t\t\t\$this->label_{$col} = \"{$nomeColunaFormatado}\";\n";

            if(substr_count($col, "_ARQUIVO") > 0 || substr_count($col, "_IMAGEM") > 0){

               $numeroArquivos++;

               $strUploadFile .= "

        public function __upload$colUpper(\$numRegistro=1, \$urlErro){

            \$dirUpload  = \$this->diretorio_{$col};
            \$labelCampo = \$this->label_{$col};

            \$objUpload = new Upload();

            \$objUpload->arrPermitido = \"\";
            \$objUpload->tamanhoMax = \"\";
            \$objUpload->file = \$_FILES[\"$col{\$numRegistro}\"];
            \$objUpload->nome = \$this->getId() . \"_$numeroArquivos.\" . Helper::getExtensaoDoArquivo(\$objUpload->file[\"name\"]);
            \$objUpload->uploadPath = \$dirUpload;\n";

            if(substr_count($col, "_IMAGEM") > 0){

                $strUploadFile .= "\t\t\t\$success = \$objUpload->uploadImagem(\$this->dimensoes_{$col}[0], \$this->dimensoes_{$col}[1]);";

            }
            else{

                $strUploadFile .= "\t\t\t\$success = \$objUpload->uploadFile();";

            }

            $strUploadFile .= "

            if(!\$success){

                \Helper::setSession(\"erro\", true);
                \$this->createSession();
                return array(\"location: \$urlErro&msgErro=Erro no upload do arquivo \$labelCampo:\" . \$objUpload->erro);
                exit();

            }
            else{

                return array(\$objUpload->nome, \$dirUpload);

            }

        }
           ";

            $strActionAdd .= "
            //UPLOAD DO ARQUIVO $colUpper
            if(Helper::verificarUploadArquivo(\"$col{\$i}\")){

                if(is_array(\$arquivo = \$this->__upload$colUpper(\$i, \$urlErro))){

                    \$this->updateCampo(\$this->get$keyUpper(), \"$col\", \$arquivo[0]);

                }

            }

                ";

            $strActionEdit .= "
            //UPLOAD DO ARQUIVO $colUpper
            if(Helper::verificarUploadArquivo(\"$col{\$i}\")){

                if(is_array(\$arquivo = \$this->__upload$colUpper(\$i, \$urlErro))){

                    \$this->updateCampo(\$this->get$keyUpper(), \"$col\", \$arquivo[0]);

                }

            }

                ";

            $strActionRemove .= "

            //REMO��O DO ARQUIVO $colUpper
            \$pathArquivo = \$this->diretorio_$col . \$this->get$colUpper();

            if(file_exists(\$pathArquivo)){

                unlink(\$pathArquivo);

            }

                ";

            }

        $camposAjax = $_POST["camposajaxmestres"];

		if(is_array($camposAjax)){

                    foreach($camposAjax as $campoAjax){

                        $campoAjaxUpper = ucfirst($campoAjax);

                        $listaCmdsAjax .= "\$this->set{$campoAjaxUpper}(Helper::POST(\"{$campoAjax}1\"));\n";

                    }

		}

        $col=$row[0];
        $colUpper = ucfirst($col);

        $attrLabel .= "\tpublic \$label_{$col};\n";

        if(substr_count($col, "_ARQUIVO") > 0 || substr_count($col, "_IMAGEM") > 0){

            $attrDiretorio .= "\tpublic \$diretorio_$col;\n";

            if(substr_count($col, "_IMAGEM") > 0){

                $attrDimensoesImagem .= "\tpublic \$dimensoes_{$col};\n";

            }

        }
        elseif(substr_count($col, "_HTML") > 0){

            
        }

        if(true)
        {

            if(substr_count($col, "_DATE") > 0){

                $timestamp3 .= "\tpublic $" . $col . "_UNIX;\n";

            }

            $c.= "\tpublic $$col;\n";

            if(substr_count($col, "_id_") > 0 && !in_array($col, $arrExcessoesRelacionamento)){
				$tabela = $pd->getMorePossibleTableOfForeignKeyByNameOfField($col);
                
				$colWithoutIDSufix = ucfirst(substr($col, 0, strpos($col, "_id_")));
               $objTabela = "obj" . $colWithoutIDSufix;
                $nomeMetodo = "All" . $colWithoutIDSufix;
				
                $colUpper = ucfirst($col);
                    $functionGetIdFK = "get" . $colUpper . "()";
                    $functionGetObjFK = "getFkObj{$colWithoutIDSufix}()";
                    $renderGetFk =  "
            public function $functionGetObjFK {
                if(\$this->$objTabela == null){
                    
                    \$this->$objTabela = new EXTDAO_" . ucfirst($tabela) . "(\$this->getConfiguracaoDAO());
                }
                \$idFK = \$this->$functionGetIdFK;
                if(!strlen(\$idFK)){
                    \$this->{$objTabela}->clear();
                }
                else if(\$this->{$objTabela}->getId() != \$idFK){
                    \$this->{$objTabela}->select(\$idFK);
                }
                return \$this->$objTabela;
            }\n";
                $instanciaTabela .= "\t\t\$this->$objTabela = new EXTDAO_" . ucfirst($tabela) . "();\n";
                
                $renderSelect .= $renderGetFk."\n\n";
                    
                $renderSelect .= "public function getComboBox{$nomeMetodo}(\$objArgumentos){\n\n";
                
                $renderSelect .= "\t\t\$objArgumentos->nome=\"$col\";\n";
                $renderSelect .= "\t\t\$objArgumentos->id=\"$col\";\n";

                if(substr_count($col, "_id_value") > 0){

                    $renderSelect .= "\t\t\$objArgumentos->valueReplaceId=true;\n\n";

                }
                else{

                    $renderSelect .= "\t\t\$objArgumentos->valueReplaceId=false;\n\n";

                }
                $renderSelect .= "\t\t\$this->{$objTabela} = \$this->{$functionGetObjFK};\n";
                $renderSelect .= "\t\treturn \$this->{$objTabela}->getComboBox(\$objArgumentos);\n\n";
                $renderSelect .= "\t}\n\n";

                $c.= "\tpublic \$$objTabela;\n";

            }

        } // endif

    } // endwhile

    //$cdb = "\$database";
    $cdb2 = "database";

    $c.="

    public \$nomeEntidade;

$timestamp3

    ";

    $cthis = "\$this->";
    $thisdb = $cthis . $cdb2 . " = new Database();";

    $c.= "

$attrLabel

$attrDiretorio

$attrDimensoesImagem

    // **********************
    // M�TODO CONSTRUTOR
    // **********************
  public function __construct(\$configDAO = null)
        {

        \tparent::__construct(\$configDAO);

        \t\$this->nomeEntidade = \"$nomeSing\";
        \t\$this->nomeTabela = \"$table\";
        \t\$this->campoId = \"$key\";
        \t\$this->campoLabel = \"$label\";\n
            
        
        }


    public function valorCampoLabel(){

    	return \$this->get$labelUpper();

    }

    ";

    $c.="

        {$renderSelect}

	 public function __actionAdd(){

            \$mensagemSucesso = \"{$mensagemCadastroSucesso}\";

            \$numeroRegistros = Helper::POST(\"numeroRegs\");

            \$urlSuccess = Helper::getUrlAction(Helper::POST(\"next_action\"), Helper::POST(\"$key\"));
            \$urlErro = Helper::getUrlAction(Helper::POST(\"origin_action\"), Helper::POST(\"$key\"));

            for(\$i=1; \$i <= \$numeroRegistros; \$i++){

                \$this->setByPost(\$i);
                \$this->formatarParaSQL();

                \$msg = \$this->insert();
				if(\$msg != null && \$msg->erro()) return \$msg;
                \$this->selectUltimoRegistroInserido();

                {$strActionAdd}
                {$conteudoAjaxRelacionamento}
    
            }

            return array(\"location: \$urlSuccess&msgSucesso=\$mensagemSucesso\");

        }

        public function __actionAddAjax(){

            \$mensagemSucesso = \"$mensagemCadastroSucesso\";

            \$numeroRegistros = Helper::POST(\"numero_registros_ajax\");

            \$urlSuccess = Helper::getUrlAction(Helper::POST(\"next_action\"), Helper::POST(\"$key\"));
            \$urlErro = Helper::getUrlAction(Helper::POST(\"origin_action\"), Helper::POST(\"$key\"));

            for(\$i=1; \$i <= \$numeroRegistros; \$i++){

                \$this->setByPost(\$i);

                $listaCmdsAjax

                \$this->formatarParaSQL();

                \$msg = \$this->insert();
				if(\$msg != null && \$msg->erro()) return \$msg;
                \$this->selectUltimoRegistroInserido();

                $strActionAdd
    
        	}

                return array(\"location: \$urlSuccess&msgSucesso=\$mensagemSucesso\");

        }

        public function __actionEdit(){

            \$mensagemSucesso = \"{$mensagemEdicaoSucesso}\";
            \$numeroRegistros = Helper::POST(\"numeroRegs\");

            \$urlSuccess = Helper::getUrlAction(Helper::POST(\"next_action\"), Helper::POST(\"{$key}\"));
            \$urlErro = Helper::getUrlAction(Helper::POST(\"origin_action\"), Helper::POST(\"{$key}\"));

            for(\$i=1; \$i <= \$numeroRegistros; \$i++){

                \$this->setByPost(\$i);
                \$this->formatarParaSQL();

                \$msg = \$this->update(\$this->get{$keyUpper}(), \$_POST, \$i);
				if(\$msg != null && \$msg->erro()) return \$msg;

                \$this->select(\$this->get{$keyUpper}());

                {$strActionEdit}
                {$conteudoAjaxRelacionamento}
    
            }

            return array(\"location: \$urlSuccess&msgSucesso=\$mensagemSucesso\");

        }

        public function __actionRemove(){

            \$mensagemSucesso = \"$mensagemExclusaoSucesso\";

            \$urlSuccess = Helper::getUrlAction(\"list_$table\", Helper::GET(\"$key\"));
            \$urlErro = Helper::getUrlAction(\"list_$table\", Helper::GET(\"$key\"));

            \$registroRemover = Helper::GET(\"$key\");

            \$msg = \$this->delete(\"\$registroRemover\");
			if(\$msg != null && \$msg->erro()) return \$msg;

            $strActionRemove

            return array(\"location: \$urlSuccess&msgSucesso=\$mensagemSucesso\", \$registroRemover);

        }

        $strUploadFile


    // **********************
    // M�TODOS GETTER's
    // **********************

    ";
    // GETTER
    $database->query($sql);
    $result = $database->result;
    while ($row = mysqli_fetch_row($result))
    {

    $col=$row[0];
    $colUpper = ucfirst($col);

        if(substr_count($row[0], "_DATE") > 0){

            $timestamp4 = "get" . $colUpper . "_UNIX()";
            $timestamp5 = "\$this->" . $col . "_UNIX";

    $c.="
    function $timestamp4
    {
    \treturn $timestamp5;
    }
    ";

        }

    $mname = "get" . $colUpper . "()";
    $mthis = "\$this->" . $col;
    $c.="
    public function $mname
    {
    \treturn $mthis;
    }
    ";
    }


    $c.="
    // **********************
    // M�TODOS SETTER's
    // **********************

    ";
    // SETTER
    $database->query($sql);
    $result = $database->result;
    while ($row = mysqli_fetch_row($result))
    {

    $col=$row[0];
    $colUpper = ucfirst($col);

    $val = "\$val";
    $mname = "set" . $colUpper . "(\$val)";
    $mthis = "\$this->" . $col . " = ";
    $c.="
    function $mname
    {
    \t$mthis $val;
    }
    ";


    if(substr_count($row[0], "_DATE") > 0){

    $timestamp .=  "UNIX_TIMESTAMP($col) AS $col" . "_UNIX, ";

    }

    }

    if(strlen($timestamp) > 0)
        $timestamp = ", " . substr($timestamp, 0, strlen($timestamp) -2);
    else
        $timestamp = "";

    $sql = "\$sql = ";
    $id = "\$id";
    $thisdb = "\$this->database";
    $thisdbquery = "\$msg = \$this->database->queryMensagem(\$sql" . ");\n";
    $thisdbquery .= "\tif(\$msg != null && (\$msg->erro() || \$msg->resultadoVazio())){ \n\t\$this->database->closeResult(); \n\treturn \$msg;\n\t}";
    $result = "\$result = ";
    $row = "\$row";
    $result1 = "\$result";
    $res = "\$result = \$this->database->result;";

    $c.="

    // **********************
    // SELECT
    // **********************

    function select($id)
    {

    \t$sql \"SELECT * $timestamp FROM $table WHERE $key = $id;\";
    \t$thisdbquery
    \t$res
    \t$row = \$this->database->fetchObject($result1);
    \$this->database->closeResult();
    ";

    $sql = "SHOW COLUMNS FROM $table;";
    $database->query($sql);
    $result = $database->result;
    while ($row = mysqli_fetch_row($result))
    {

        $col=$row[0];

        $cthis = "\$this->" . $col . " = \$row->" . $col;

        $c.="
        $cthis;
        ";

        if(substr_count($row[0], "_DATE") > 0){

            $timestamp2 = "\$this->" . $col . "_UNIX = \$row->" . $col . "_UNIX;";

            $c.="$timestamp2\n";

        }
        elseif(substr_count($row[0], "_id_")){

            if(substr_count($row[0], "_id_value")){

                $c.="\$this->get{$colUpper}();\n";

            }
            else{

//                $tabela = substr($col, 0, strrpos($col, "_id_"));
//                $objTabela = "obj" . ucfirst($tabela);
//                $c.="if(\$this->$col)\n";
//                $c.="\t\t\t\$this->{$objTabela}->select(\$this->$col);\n";

            }

        }

    }

    $c.="
        
    }
    ";
$c .=  "
        // **********************
        // EMPTY
        // **********************
        public function clear()
        {
            $strBodyEmptyFunction
        }
        ";

    $zeile1 = "\$sql = \"UPDATE $table SET excluido_BOOLEAN=1, excluido_DATETIME=NOW() WHERE $key = $id;\"";
    $zeile2 = "\$msg = \$this->database->queryMensagem(\$sql);\n";
	$zeile2 .= "if(\$msg != null && \$msg->erro()) return \$msg;\n";
    $c.="

    // **********************
    // DELETE
    // **********************

    public function delete($id)
    {
    \t$zeile1;
    \t$zeile2
    ";
    $c.="
    }
    ";

    $zeile1 = "\$this->$key = \"\"";
    $zeile2 = "INSERT INTO $table (";
    $zeile5= ")";
    $zeile3 = "";
    $zeile4 = "";
    $zeile6 = "VALUES (";

    $sql = "SHOW COLUMNS FROM $table;";
    $database->query($sql);
    $result = $database->result;

    while ($row = mysqli_fetch_row($result))
    {
        $col=$row[0];

        if($col!=$key || ($col==$key && $autoIncremento== "FALSE"))
        {
			if(strlen($zeile3)) $zeile3.= ", ";
            $zeile3.= "{$col} ";

            if(strpos($col, "dataCadastro") !== false || strpos($col, "dataEdicao") !== false ){
				if(strlen($zeile4)) $zeile4.= ", ";
                $zeile4.= "NOW() ";

            }
            elseif(strpos($col, "_BOOLEAN") !== false || strpos($col, "_FLOAT") !== false || strpos($col, "_INT") !== false || strpos($col, "_TIME") !== false || strpos($col, "_DATETIME") !== false || strpos($col, "_DATE") !== false){
				if(strlen($zeile4)) $zeile4.= ", ";
                $zeile4.= "{\$this->$col} ";

            }
            else{
				if(strlen($zeile4)) $zeile4.= ", ";
                $zeile4.= "{\$this->$col} ";

            }


        }

    }


    if($autoIncremento == "TRUE")
        $limparChave = $zeile1 . "; //limpar chave com autoincremento";
    else
        $limparChave = "";

    $zeile3 = substr($zeile3, 0, -1);
    $zeile4 = substr($zeile4, 0, -1);
    $sql = "\$sql =";
    $zeile7 = "\$msg = \$this->database->queryMensagem(\$sql);\n";
	$zeile7 .= "if(\$msg != null && \$msg->erro()) return \$msg;\n";
    $zeile8 = "\$row";
    $zeile9 = "\$result";
    //$zeile10 = "\$this->$key = " . "mysqli_insert_id(\$this->database->link);";

    $c.="
    // **********************
    // INSERT
    // **********************

    public function insert()
    {

    \t$limparChave
    \t\$this->excluido_BOOLEAN = \"0\";

    \t$sql \"$zeile2 $zeile3 $zeile5 $zeile6 $zeile4 $zeile5\";
    \t$zeile7
    \t

    }
    ";



    //funcao para formatar parametros com valores para gravar no banco.
    $sql = "SHOW COLUMNS FROM $table;";
    $database->query($sql);
    $result = $database->result;
    while ($row = mysqli_fetch_row($result))
    {
        $col=$row[0];

        $colUpper = ucfirst($col);

        $acumuladorGetNome .= "\tpublic function nomeCampo$colUpper(){ \n\n";
        $acumuladorGetNome .= "\t\treturn \"$col\";\n\n";
        $acumuladorGetNome .= "\t}\n\n";

        if($col!=$key)
        {
            $zeile3.= "$col" . ",";

            $acumuladorFunCampo .= "\tpublic function imprimirCampo$colUpper(\$objArguments){\n\n";
            $acumuladorFunCampo .= "\t\t\$objArguments->nome = \"$col\";\n";
            $acumuladorFunCampo .= "\t\t\$objArguments->id = \"$col\";\n\n";

            if(strpos($col, "_DATETIME") !== false || $col == "dataCadastro" || $col == "dataEdicao"){

                $acumulador.= "\t\$this->$col = \$this->formatarDataTimeParaComandoSQL(\$this->$col); \n";
                $acumulador2.= "\t\$this->$col = \$this->formatarDataTimeParaExibicao(\$this->$col); \n";

                $acumuladorFunCampo .= "\t\treturn \$this->campoDataTime(\$objArguments);\n\n";


            }
            elseif(strpos($col, "_TIME") !== false){

                $acumulador.= "\t\$this->$col = \$this->formatarHoraParaComandoSQL(\$this->$col); \n";
                $acumulador2.= "\t\$this->$col = \$this->formatarHoraParaExibicao(\$this->$col); \n";

                $acumuladorFunCampo .= "\t\treturn \$this->campoHora(\$objArguments);\n\n";

            }
            elseif(strpos($col, "_HTML") !== false){
                
                $acumulador .= "\$this->{$col} = htmlentities(\$this->{$col}, ENT_QUOTES);\n";
                $acumulador2 .= "\t\$this->{$col} = html_entity_decode(\$this->{$col}); \n";
                
            }
            elseif(strpos($col, "_DATE") !== false){

                $acumulador.= "\t\$this->$col = \$this->formatarDataParaComandoSQL(\$this->$col); \n";
                $acumulador2.= "\t\$this->$col = \$this->formatarDataParaExibicao(\$this->$col); \n";

                $acumuladorFunCampo .= "\t\treturn \$this->campoData(\$objArguments);\n\n";

            }
            elseif(strpos($col, "_FLOAT") !== false){


                $acumulador.= "\t\$this->$col = \$this->formatarFloatParaComandoSQL(\$this->$col); \n";
                $acumulador2.= "\t\$this->$col = \$this->formatarFloatParaExibicao(\$this->$col); \n";

                $acumuladorFunCampo .= "\t\treturn \$this->campoMoeda(\$objArguments);\n\n";

            }

            elseif(strpos($col, "_INT") !== false){

                $preOperacao .= "\t\tif(\$this->$col == null){

\t\t\t\$this->$col = \"null\";

\t\t}
\n";

                $acumuladorFunCampo .= "\t\treturn \$this->campoInteiro(\$objArguments);\n\n";


            }
            elseif(strpos($col, "_BOOLEAN") !== false){

                $preOperacao .= "\t\tif(\$this->$col == null){

\t\t\t\$this->$col = \"null\";

\t\t}
\n";

                $acumuladorFunCampo .= "\t\treturn \$this->campoBoolean(\$objArguments);\n\n";


            } else {
			    $aux123 = "\t\tif(\$this->$col == null){

\t\t\t\$this->$col = \"null\";

\t\t}
				\n";
				$acumulador .= "\t\t\$this->{$col} = \$this->formatarDadosParaSQL(\$this->{$col});\n";
				if(strpos($col, "_IMAGEM") !== false){
					
					$acumuladorFunCampo .= "\t\treturn \$this->campoImagem(\$objArguments);\n\n";

				}
				elseif(strpos($col, "_ARQUIVO") !== false){

					$acumuladorFunCampo .= "\t\treturn \$this->campoArquivo(\$objArguments);\n\n";

				}
				elseif(strpos(strtolower($col), "cpf") !== false){

					$acumuladorFunCampo .= "\t\treturn \$this->campoCPF(\$objArguments);\n\n";

				}
				elseif(strpos(strtolower($col), "cnpj") !== false){

					$acumuladorFunCampo .= "\t\treturn \$this->campoCNPJ(\$objArguments);\n\n";

				}
				elseif(strpos(strtolower($col), "cep") !== false){

					
					$acumuladorFunCampo .= "\t\treturn \$this->campoCep(\$objArguments);\n\n";

				}
				elseif(strpos(strtolower($col), "email") !== false){

					$acumuladorFunCampo .= "\t\treturn \$this->campoEmail(\$objArguments);\n\n";

				}
				elseif(strpos(strtolower($col), "telefone") !== false || strpos(strtolower($col), "celular") !== false
					  || substr(strtolower($col), strlen($col)-3, strlen($col)) == "tel"){

					$acumuladorFunCampo .= "\t\treturn \$this->campoTelefone(\$objArguments);\n\n";

				}
				else{

					$acumuladorFunCampo .= "\t\treturn \$this->campoTexto(\$objArguments);\n\n";

				}
				
			}

            $acumuladorFunCampo .= "\t}\n\n";

        }

    }


    $c .= "

    //*************************************************
    //FUNCOES QUE RETORNAM O NOME DO CAMPO EM QUESTAO
    //*************************************************

$acumuladorGetNome


    //************************************************************************
    //FUNCOES QUE RETORNAM A STRING HTML PARA CONSTRUIR OS CAMPOS DE TEXTO
    //************************************************************************

$acumuladorFunCampo


    //**********************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA GRAVAR NO BANCO
    //**********************************************************************************

    public function formatarParaSQL(){

$preOperacao

$acumulador

    }


    //****************************************************************************
    //FUNCAO PARA FORMATAR OS VALORES DE DATA E VALORES DECIMAIS PARA EXIBI��O
    //****************************************************************************

    public function formatarParaExibicao(){

$acumulador2

    }

    ";




    //SETS POR POST, GET E SESSION

    $sql = "SHOW COLUMNS FROM $table;";
    $database->query($sql);
    $result = $database->result;
    while ($row = mysqli_fetch_row($result))
    {

        //if($row[0]!=$key){

            //$camposPost .= "if(\$_POST[\"" . $row[0] . "\"] != \"\" ){\n";
            $camposPost .= "\t\t\$this->" . $row[0] . " = Helper::POST(\"" . $row[0] . "{\$numReg}\"); \n";
            //$camposPost .= "}\n\n";

            //$camposGet .= "if(\$_GET[\"" . $row[0] . "\"] != \"\" ){\n";
            $camposGet .= "\t\t\$this->" . $row[0] . " = Helper::GET(\"" . $row[0] . "{\$numReg}\"); \n";
            //$camposGet .= "}\n\n";

            //$camposSession .= "if(\$_SESSION[\"" . $row[0] . "\"] != \"\" ){\n";
            $camposSession .= "\t\t\$this->" . $row[0] . " = Helper::SESSION(\"" . $row[0] . "{\$numReg}\"); \n";
            //$camposSession .= "}\n\n";

            $camposCriarSession .= "\t\tHelper::setSession(\"{$row[0]}\", \$this->" . $row[0] . "); \n";

            $camposLimparSession .= "\t\tHelper::clearSession(\"{$row[0]}\");\n";

        //}

    }



    $c .="
    // ****************************
    // CRIAR VARIAVEIS DE SESSAO
    // ****************************

    public function createSession(){

$camposCriarSession

    }

    // ***************************
    // LIMPAR SESSAO
    // ***************************

    public function limparSession(){

$camposLimparSession

    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL SESSION
    // ****************************

    public function setBySession(\$numReg){

$camposSession

    }


    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL POST
    // ****************************

    public function setByPost(\$numReg){

$camposPost

    }

    // ****************************
    // SETAR CAMPOS POR SUPERGLOBAL GET
    // ****************************

    public function setByGet(\$numReg){

$camposGet

    }
    ";


    // UPDATE ----------------------------------------

    $zeile1 = "\$this->$key = \"\"";
    $zeile2 = "UPDATE $table SET";
    $zeile5= ")";
    $zeile3 = "";
    $zeile4 = "";
    $zeile6 = "VALUES (";

    $upd = "";

    $sql = "SHOW COLUMNS FROM $table;";
    $database->query($sql);
    $result = $database->result;
    while ($row = mysqli_fetch_row($result))
    {

        $col=$row[0];

        if($col!=$key)
        {
            $zeile3.= "$col" . ",";
            $zeile4.= "\$this->$col" . ",";

            if(strpos($col, "dataEdicao") !== false ){

                $condicao .= "\t\t\$upd.= " . "\"" . "$col = NOW(), \";\n\n";

            }

            $condicao .= "\tif(isset(\$tipo[\"$col{\$numReg}\"]) || \$tipo == null){\n\n";


            if(strpos($col, "_FLOAT") !== false || strpos($col, "_INT") !== false || strpos($col, "_BOOLEAN") !== false || strpos($col, "_TIME") !== false ||strpos($col, "_DATE") !== false || strpos($col, "_DATETIME") !== false){

                $condicao .= "\t\t\$upd.= " . "\"" . "$col = \$this->$col, \";\n\n";

            }
            else{

                $condicao .= "\t\t\$upd.= " . "\"" . "$col = \$this->$col, \";\n\n";

            }

            $condicao .= "\t} \n\n";

        }

    }

    $zeile3 = substr($zeile3, 0, -1);
    $zeile4 = substr($zeile4, 0, -1);
    $condicao .= "\t\t\$upd = substr(\$upd, 0, -2);";
    $sql = "\$sql = \"";
    $zeile7 = "\$msg = \$this->database->queryMensagem(\$sql);\n";
	$zeile7 .= "if(\$msg != null && \$msg->erro()) return \$msg;\n";
    $zeile10 = "\$this->$key = \$row->$key";
    $id = "\$id";
    $where = "WHERE " . "$key = \$id";

    $c.="
    // **********************
    // UPDATE
    // **********************

    public function update($id, \$tipo = null, \$numReg=1)
    {
		
		\$upd = \"\";
$condicao

    \t$sql $zeile2 \$upd $where \";

    \t$zeile7


    ";


    $c.="
    }
    ";

    $c.= "

    } // classe: fim
";
    fwrite($file, $c);
    fclose($file);

	
	$strGerouExt = "<p class=\"mensagem_retorno\">
		&bull;&nbsp;&nbsp;Classe <b>$class</b> gerada com sucesso no arquivo <b>$pathResumido</b>.
		</p>";

    print $strGerouExt;


    }

}

?>
