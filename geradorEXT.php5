<?php

function gerarEXT($table, $class, $key, $label, $ext, $sobrescrever){

    $database = new Database();

    $objBanco = new Database();

    $idProjeto = $_POST["projeto"];

    $objBanco->query("SELECT p.nomeBanco, c.hostnameBanco, c.usuarioBanco, c.senhaBanco, p.diretorioClassesEXTDAO FROM projetos p, conexoes c WHERE c.id = p.conexaoBanco_INT AND p.id=$idProjeto");

    $database = new Database(Database::mysqli_result($objBanco->result, 0, 0));
    
    $database->database = Database::mysqli_result($objBanco->result, 0, 0);
    $database->host = Database::mysqli_result($objBanco->result, 0, 1);
    $database->user = Database::mysqli_result($objBanco->result, 0, 2);
    $database->password = Database::mysqli_result($objBanco->result, 0, 3);
    $diretorio = Database::mysqli_result($objBanco->result, 0, 4);

    $database->OpenLink();
    
    $keyUpper = ucfirst($key);

    $dir = dirname(__FILE__);
    $filedate = date("d.m.Y");

    $filename = $dir . "/../../" . $diretorio ."/" . $ext . ".php";

    $sql = "SHOW TABLES LIKE '$table';";
    $database->query($sql);

    if($database->rows < 1){

        return;

    }

    $permissaoSobreescrita = Helper::conferirPermissaoSobreEscrita($filename);

    // if file exists, then delete it
    if($permissaoSobreescrita && $sobrescrever && file_exists($filename))
    {
        unlink($filename);
    }

    if(!file_exists($filename)){

        $file = fopen($filename, "w+");

        $sql = "SHOW COLUMNS FROM $table;";
        $database->query($sql);
        $result = $database->result;

        $cadastrado[0] = "cadastrado";
        $cadastrado[1] = "cadastrada";
        $cadastrado[2] = "cadastrados";
        $cadastrado[3] = "cadastradas";

        $modificado[0] = "modificado";
        $modificado[1] = "modificada";
        $modificado[2] = "modificados";
        $modificado[3] = "modificadas";

        $excluido[0] = "exclu�do";
        $excluido[1] = "exclu�da";
        $excluido[2] = "exclu�dos";
        $excluido[3] = "exclu�das";

        $foi[0] = "foi";
        $foi[1] = "foi";
        $foi[2] = "foram";
        $foi[3] = "foram";

        $artigo[0] = "O";
        $artigo[1] = "A";
        $artigo[2] = "Os";
        $artigo[3] = "As";

        $genero   = $_POST["genero_entidade"]; //F ou M
        $nomeSing = $_POST["entidade_singular"];
        $nomePlu  = $_POST["entidade_plural"];
        $numeroCadastros = $_POST["numeroCadastros"];

        $index = 0;

        if($genero == "F")
            $index += 1;

        $nomeMsg = $nomeSing;

        $mensagemExclusaoSucesso = $artigo[$index] . " " . $nomeMsg  . " " . $foi[$index] . " " . $excluido[$index] . " com sucesso.";

        if($numeroCadastros > 1){

            $index += 2;
            $nomeMsg = $nomePlu;

        }

        $mensagemCadastroSucesso = $artigo[$index] . " " . $nomeMsg . " " . $foi[$index] . " " . $cadastrado[$index] . " com sucesso.";

        $mensagemEdicaoSucesso = $artigo[$index] . " " . $nomeMsg  . " " . $foi[$index] . " " . $modificado[$index] . " com sucesso.";
        
        while($row = mysqli_fetch_row($result)){

            $col = $row[0];
            $colUpper = ucfirst($col);
            $nomeColunaFormatado = ucwords($_POST["nomexcampo_" . $col]);

            $ocorrencias = array(" De ", " Da ", " Do ", " Das ", " Dos ", " Em ", " No ", " Na ", " Nos ", " Nas ");
            $substituicoes = array(" de ", " da ", " do ", " das ", " dos ", " em ", " no ", " na ", " nos ", " nas ");

            $nomeColunaFormatado = str_replace($ocorrencias, $substituicoes, $nomeColunaFormatado);
         
            $instLabel .= "\t\t\t\$this->label_{$col} = \"{$nomeColunaFormatado}\";\n";
            
            if(substr_count($col, "_ARQUIVO") > 0 || substr_count($col, "_IMAGEM") > 0){

               $instDiretorio .= "\t\t\t\$this->diretorio_{$col} = \"\";        //caminho a partir de da raiz, com '/' no final\n";
  
               if(substr_count($col, "_IMAGEM") > 0){

                    $instDimensoesImagem .= "\t\t\t\$this->dimensoes_{$col} = array(\"\", \"\");    //largura, altura (em pixels)\n";
                
               }
               
            }

        }
        
        if(strlen($instDiretorio)){
            
            $instDiretorio = "
              
        public function setDiretorios(){

            $instDiretorio

        }
            
        ";
            
            $strChamadas .= "\t\t\t\$this->setDiretorios();\n";
            
        }
        
        if(strlen($instDimensoesImagem)){
            
            $instDimensoesImagem = "
              
        public function setDimensoesImagens(){

            $instDimensoesImagem

        }
        ";
            
            $strChamadas .= "\t\t\t\$this->setDimensoesImagens();\n";
            
        }

        $conteudo = "<? //@@NAO_MODIFICAR

    /*
    *
    * -------------------------------------------------------
    * NOME DA CLASSE:     $ext
    * NOME DA CLASSE DAO: $class
    * DATA DE GERA��O:    $filedate
    * ARQUIVO:            $ext.php
    * TABELA MYSQL:       $table
    * BANCO DE DADOS:     $database->database
    * -------------------------------------------------------
    *
    */

    // **********************
    // DECLARA��O DA CLASSE
    // **********************

    class {$ext} extends {$class}
    {

        public function __construct(\$configDAO = null){

            parent::__construct(\$configDAO);

            \t\$this->nomeClasse = \"{$ext}\";

            if(\$id != 0){

                \$this->select(\$id);

            }

            if(\$setLabels){

                \$this->setLabels();

            }

$strChamadas


        }

        public function setLabels(){

$instLabel

        }

$instDiretorio

$instDimensoesImagem

        public static function factory(){

            return new $ext();

        }

	}

    ";

        fwrite($file, $conteudo);
        fclose($file);
		
        $strGerouExt = "<p class=\"mensagem_retorno\">
                        &bull;&nbsp;&nbsp;Classe <b>{$ext}</b> gerada com sucesso no arquivo <b>{$ext}.php</b>.
                        </p>";

    print $strGerouExt;

    }

}

?>